#!/bin/bash

if [ ! -f ./hskel.cabal ]; then
  echo "You must run this script in the directory containing hskel.cabal"
  exit 1
fi

echo "RECOMPILING SKEL"
echo
echo

stack clean
stack build

printf '\e]50;ClearScrollback\a'
echo "_______________________________________________________________________________"
echo "                        STARTING TEST ENVIRONMENT"
echo "_______________________________________________________________________________"
echo
echo

if [ ! -d $PWD/.tmp ]; then
  mkdir .tmp
fi

MD5_HS=md5modif_hs
MD5_SKEL=md5modif_skel
if [ -f $PWD/.tmp/$MD5_HS ]; then
  rm $PWD/.tmp/$MD5_HS
fi

if [ -f $PWD/.tmp/$MD5_SKEL ]; then
  rm $PWD/.tmp/$MD5_SKEL
fi

find $PWD/src -iname '*.hs' -exec md5 {} \; > $PWD/.tmp/$MD5_HS

#find $PWD/examples -iname '*.skel' -exec md5 {} \; > $PWD/.tmp/$MD5_SKEL
DIR=`find .stack-work/ -name 'bin' -type d`

INIT=". \"$HOME/.bashrc\";\
      export ROOT=${PWD};\
      export PATH=${PWD}'/'${DIR}:\${PATH};\
      export PS1='\[\033[02;31m\]skel|\[\033[02;36m\] \W \[\033[02;31m\]>\[\033[00m\] ';\
      cd ${PWD}/examples;\
      reinstall()\
      {\
        TMP=\$PWD;\
        cd \${ROOT};\
        stack build;\
        cd \${TMP}\
        TMP=;\
      };\
      rebuild()\
      {\
        TMP=\$PWD;\
        cd \${ROOT};\
        stack build;\
        cd \${TMP}\
        TMP=;\
      };\
      cls()\
      {\
        printf '\e]50;ClearScrollback\a'
      };\
      repl()\
      {\
        TMP=\$PWD;\
        cd \${ROOT};\
        stack ghci
        cd \${TMP}\
        TMP=;\
      };\
      recomp(){\
        L=\`find \$ROOT/src -iname '*.hs' -exec md5 {} \;\`;\
        P=\`cat \$ROOT/.tmp/$MD5_HS\`;\
        if [ \"\$L\" != \"\$P\" ];\
        then\
          find \$ROOT/src -iname '*.hs' -exec md5 {} \; > \$ROOT/.tmp/$MD5_HS
          reinstall;\
          echo;\
          echo \"----------------------------------------------------------------------\";\
          echo;\
        fi\
      };\
      skel(){\
        recomp;\
        command skel \$1;\
      };\
      run_tests()\
      {\
        TMP=\$PWD;\
        cd \${ROOT}/examples;\
        for i in \`find . -iname '*.skel'\`; do\
          echo \"skel \$i\";\
          skel \$i;\
          echo \"DONE\";\
          echo;\
        done;\
        find \$ROOT/examples -iname '*.skel' -exec md5 {} \; > \$ROOT/.tmp/$MD5_SKEL;\
        cd \${TMP}\
        TMP=;\
      };\
      retest()\
      {\
        P=\`cat \$ROOT/.tmp/$MD5_HS\`;\
        N=\`find \$ROOT/src -iname '*.hs' -exec md5 {} \;\`;\
        if [ \"\$N\" != \"\$P\" ];\
        then\
          run_tests;\
        else\
          if [ ! -f \$ROOT/.tmp/$MD5_SKEL ];then\
            touch \$ROOT/.tmp/$MD5_SKEL;\
          fi;\
          P=\`cat \$ROOT/.tmp/$MD5_SKEL\`;\
          N=\`find \$ROOT/examples -iname '*.skel' -exec md5 {} \;\`;\
          R=\`comm -23 <(echo \"\$P\" | sort) <(echo \"\$N\" | sort)\`;\
          E=\`echo \"\$R\" | awk -F \"[()]\" '{ for (i=2; i<NF; i+=2) array[\$1]=\$i; print array[\$1] }'\`;\
          for i in \$E; do\
            echo \"Testing changed files:\";\
            echo \"skel \"\$i;\
            command skel \$i;\
            echo;\
            echo \"DONE\";\
            echo;\
          done;\
          find \$ROOT/examples -iname '*.skel' -exec md5 {} \; > \$ROOT/.tmp/$MD5_SKEL;\
        fi;\
      };\
      add(){\
        if [ ! -f \$1 ];then\
          echo \"Must provide a valid test file\";\
        else\
          FOCUS=\" \$PWD/\$1 \"\$FOCUS;\
        fi;\
      };\
      remove(){\
        if [ -f \$1 ]; then\
          TMP=;\
          for i in \${FOCUS}; do\
            if [ ! \"\$i\" -ef \"\$1\" ]; then\
              TMP=\$TMP\" \$i \";\
            fi;\
          done;\
          FOCUS=\${TMP};\
          TMP=;\
        fi;\
      };\
      test(){\
        for i in \$FOCUS;do\
          echo \"skel \"\$i;\
          skel \$i;
          echo;\
          echo \"DONE\";\
          echo;\
        done;\
      };\
      showtest(){\
        T=\`echo \"\$FOCUS\" | xargs -n1 | sort -u \`;\
        for i in \$T;do\
          echo \"skel \"\$i;\
        done;\
        T=;\
      };\
      clear_test(){\
        FOCUS=;\
      };\
      todo(){\
        if [ ! -f \${ROOT}/.todo ]; then\
          touch \${ROOT}/.todo;\
          echo \"0 TODO\" >> \${ROOT}/.todo;\
        fi;\
        LASTIDX=\`tail -1 \$ROOT/.todo | awk '{print \$1}'\`;\
        NEXTIDX=\$((LASTIDX+1));\
        TMP=\$IFS;\
        IFS=\" \";\
        CONTENTS=\`echo \"\$*\" | sed \"s/\n/ /g\"\`;\
        IFS=\$TMP;\
        TMP=;\
        if [ \$NEXTIDX  == \"1\" ]; then\
          echo \"\${NEXTIDX} \${CONTENTS}\" > \${ROOT}/.todo;\
        else\
          echo \"\${NEXTIDX} \${CONTENTS}\" >> \${ROOT}/.todo;\
        fi;\
      };\
      tick(){\
        CONTENTS=;\
        NIDX=1;\
        while read L; do\
          IDX=\`echo \$L | awk '{print \$1}'\`;\
          if [ \"\$IDX\" != \"\$1\" ]; then\
            OLDCONTENTS=\`echo \$L | awk '{\$1=\"\"; print \$0}'\`;\
            CONTENTS=\"\$CONTENTS\$NIDX \$OLDCONTENTS\n\";\
            ((NIDX += 1));\
          fi;\
        done < \${ROOT}/.todo;\
        printf \"\$CONTENTS\" > \${ROOT}/.todo
      };\
      show_todo(){\
        cat \${ROOT}/.todo;\
      };\
      next_todo(){\
        echo \$(head -n 1 \${ROOT}/.todo);\
      };\
      help () {\
        echo \"Commands :\";\
        echo \"  * help ----------- This menu\";\
        echo \"  * skel ----------- Skel compiler\";\
        echo \"  * repl ----------- Skel interactive\";\
        echo \"  * run_tests ------ Runs all tests \";\
        echo \"  * retest --------- Runs changed test files \";\
        echo \"  * add ------------ Select file for testing \";\
        echo \"  * remove --------- Do not test file \";\
        echo \"  * clear_test ----- Remove all files from test focus \";\
        echo \"  * test ----------- Run tests on currenty selected files \";\
        echo \"  * showtest ------- Run files selected for testing \";\
        echo \"  * todo ----------- Add to todo list\";\
        echo \"  * tick ----------- Remove from todo list\";\
        echo \"  * show_todo ------ Show todo list\";\
        echo \"  * next_todo ------ Show next element in TODO list\";\
      };\
      help\
      "
bash --init-file <(echo "$INIT")
