{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE ViewPatterns          #-}
module Ty where

import Control.Monad.Except
import Control.Monad.State.Strict
import Control.Monad.Writer
import Text.Parsec hiding ( State )

import Control.Arrow         ( first                 )
import Data.Char             ( isUpper
                             , isLower               )
import Data.Functor.Identity ( Identity              )
import Data.Maybe            ( maybeToList           )
import Data.Map.Strict       ( Map                   )
import Text.Parsec.Expr      ( Operator (..)
                             , Assoc    (..)
                             , OperatorTable
                             , buildExpressionParser )
import Text.Parsec.Pos       ( SourcePos             )
import Text.Parsec.Token     ( GenLanguageDef
                             , GenTokenParser        )
import Text.PrettyPrint      ( (<>)
                             , ($$)
                             , (<+>)                 )

import qualified Control.Monad.State  as LState
import qualified Data.Map.Strict      as Map
import qualified Text.Parsec          as Parsec
import qualified Text.Parsec.Expr     as Expr
import qualified Text.Parsec.Indent   as Indent
import qualified Text.Parsec.Language as Language
import qualified Text.Parsec.Pos      as Pos
import qualified Text.Parsec.Token    as Token
import qualified Text.PrettyPrint     as Pretty

-------------------------------------------------------------------------------
-- TYPES
-------------------------------------------------------------------------------

type Name = String

-- | Primitive Types
-- TODO: C types?
data PrimTy = BOOL | INT | STRING
  deriving Eq

infixl 7 :+:
infixl 8 :*:
infixr 5 :->
infixl 6 :@:

-- | Types
data Ty where
 -- | For typechecking purposes only
  Meta  :: Int              -> Ty

  Unit  ::                     Ty
  PrimT :: PrimTy           -> Ty
  (:+:) :: Ty -> Ty         -> Ty
  (:*:) :: Ty -> Ty         -> Ty
  (:->) :: Ty -> Ty         -> Ty
  (:@:) :: F          -> Ty -> Ty
  Mu    :: F                -> Ty
  deriving Eq

instance Show Ty where
  show (Meta i) = "M" ++ show i
  show Unit     = "1"
  show (PrimT t) = show t
  show (t1 :+: t2) = show t1 ++ "+" ++ show t2
  show (t1 :*: t2) = show t1 ++ "*" ++ show t2
  show (t1 :-> t2) = show t1 ++ "->" ++ show t2
  show (t1 :@: t2) = show (subst t1 t2)
  show (Mu f)      = "Mu " ++ show (subst f (Meta 1))

-- | Bifunctor representation using HOAS
data BiF = MkB Name (Ty -> Ty -> Ty)

-- | Assumption: g1 and g2 are closed (no metavariables)
instance Eq BiF where
  MkB n1 g1 == MkB n2 g2 =
    n1 == n2 && g1 (Meta 0) (Meta 1) == g2 (Meta 0) (Meta 1)

-- | Functors can be defined either directly using HOAS, as the partial
-- appication of some bifunctor, or a polymorphic data type defined as
-- the fixpoint of a bifunctor
data F = MkF Name (Ty -> Ty) -- F A = <type using A>
       | App  BiF Ty         -- ^ F A = G B A
       | Poly BiF            -- ^ F A = Mu (G A)

-- | Assumption: F is closed (no metavariables inside)
instance Eq F where
  MkF n1 f1   == MkF n2 f2   = n1 == n2 && f1 (Meta 0) == f2 (Meta 0)
  App g1 a    == App g2 b    = g1 == g2 && a == b
  Poly g1     == Poly g2     = g1 == g2
  _        == _        = False

instance Show F where
  show _ = "F"

instance Show BiF where
  show _ = "G"


subst :: F -> Ty -> Ty
subst (MkF _ f)          t = f t
subst (App  (MkB _ g) a) t = g a t
subst (Poly (MkB _ g))   t = Mu (MkF "" (g t)) -- ^ TODO: fix name

-------------------------------------------------------------------------------
-- STRUCTURED EXPRESSIONS
-------------------------------------------------------------------------------

type Size = Int
type Cost = Double

-- | Atomic functions
data CFun = MkCFun
  { name     :: Name
  , c_header :: FilePath
  , tyOf     :: Ty
  , size_f   :: Size -> Size
  , cost_f   :: Size -> Cost
  }

-- | Primitive functions
data PrimFun  where
  Id   ::         PrimFun -- ^ Id   : a :-> a
  In   :: F    -> PrimFun -- ^ In   : f :@: (Mu f) :-> Mu f
  Out  :: F    -> PrimFun -- ^ Out  : Mu f :-> f :@: (Mu f)
  AF   :: CFun -> PrimFun -- ^ c : ty  ===>  AF   c : ty

-- | Structured expressions
-- TODO: Lacking peval
data SE where
  -- | Atomic functions
  -- f : a :-> b
  -- ______________
  -- Fun  p : a :-> b
  Fun    :: PrimFun   -> SE

  -- | Natural transformation from App f a functor to App f b
  -- s : a :-> b
  -- ________________________________
  -- PMap f s : (App f a) :@: c :-> (App f b) :@: c
  PMap   :: BiF -> SE -> SE

  -- | Composition
  -- s_1 : b :-> c
  -- s_2 : a :-> b
  -- ________________________________
  --      s_1 :.: s_2 : a :-> c
  Comp  :: SE  -> SE -> SE

  -- | Hylomorphisms
  -- s_1 : f :@: b :-> b
  -- s_2 : a :-> f :@: a
  -- ________________________________
  --   Hylo s_1 s_2 :.: a :-> b
  Hylo   :: SE  -> SE -> SE

-------------------------------------------------------------------------------
-- TYPECHECKING MACHINERY: TODO: move to a different module
-------------------------------------------------------------------------------

mapSndM :: Monad m => (b -> m c) -> (a, b) -> m (a, c)
mapSndM f (a, b) = f b >>= return . (a,)

mapFstM :: Monad m => (a -> m c) -> (a, b) -> m (c, b)
mapFstM f (a, b) = f a >>= return . (,b)

mappend3 :: Monoid m => m -> (a, b, m) -> (a, b, m)
mappend3 m (a, b, m') = (a, b, m `mappend` m')

unlessM :: Monad m => m Bool -> m () -> m ()
unlessM mb m = mb >>= flip unless m

data TcMonad e s l a = TcM { runTcM :: s -> (Either e a, s, l) }

deriving instance Functor (TcMonad e s l)

instance Monoid l => Applicative (TcMonad e s l) where
  pure a = TcM { runTcM = \s -> (Right a, s, mempty) }
  af <*> ax = TcM { runTcM = doApp }
    where
      doApp (runTcM af -> (Left  e, s, l)) = (Left  e, s, l)
      doApp (runTcM af -> (Right f, s, l)) = mappend3 l $ runTcM (fmap f ax) s

instance Monoid l => Monad (TcMonad e s l) where
  return  = pure
  m >>= f = TcM { runTcM = doBind }
    where
      doBind (runTcM m -> (Left  e, s, l)) = (Left e, s, l)
      doBind (runTcM m -> (Right x, s, l)) = doBind2 (f x)
        where
          doBind2 (flip runTcM s -> (Left  e, s', l')) = (Left  e, s', l')
          doBind2 (flip runTcM s -> (Right y, s', l')) = (Right y, s', l `mappend` l')

instance Monoid l => MonadError e (TcMonad e s l) where
  throwError e = TcM { runTcM = doThrowError }
    where doThrowError s = (Left e, s, mempty)

  catchError ma fma = TcM { runTcM = doCatch }
    where
      doCatch (runTcM ma -> (Left  e, s, l)) = mappend3 l $ runTcM (fma e) s
      doCatch (runTcM ma -> (Right x, s, l)) = (Right x, s, l)

instance Monoid l => MonadWriter l (TcMonad e s l) where
  writer (a, l) = TcM { runTcM = doWriter }
    where doWriter s = (Right a, s, l)
  listen ma     = TcM { runTcM = doListen }
    where doListen (runTcM ma -> (Left  e, s, l)) = (Left e, s, l)
          doListen (runTcM ma -> (Right x, s, l)) = (Right (x,l), s, l)
  pass mw       = TcM { runTcM = doPass }
    where doPass (runTcM mw -> (Left  e    , s, l)) = (Left e   , s, l)
          doPass (runTcM mw -> (Right (x,f), s, l)) = (Right x  , s, f l)

instance Monoid l => MonadState s (TcMonad e s l) where
  state f = TcM { runTcM = doState }
    where doState (f -> (x, s)) = (Right x, s, mempty)

-------------------------------------------------------------------------------
-- TYPECHECKING MONAD
-------------------------------------------------------------------------------

data TcError =
    TypeNotClosed Int
  | TypeMismatch Ty Ty
  | FunctorMismatch F F
  | BiFunctorMismatch BiF BiF
  | Unexpected Ty Ty
  | UnifErr Int

showTcError :: TcError -> String
showTcError (TypeNotClosed i)         = "Type not closed"
showTcError (TypeMismatch t1 t2)      = "Type mismatch"
showTcError (FunctorMismatch f1 f2)   = "Functor mismatch"
showTcError (BiFunctorMismatch g1 g2) = "Bifunctor mismatch"
showTcError (Unexpected t1 t2)        = "Unexpected type"
showTcError (UnifErr i)               = "Unification error"

newtype Subst = MkSubst (Map Int Ty)
  deriving Show
data Env   = MkEnv !Int !Subst
  deriving Show

emptyEnv :: Env
emptyEnv = MkEnv 0 $ MkSubst Map.empty

type    Log   = String

type TcM = TcMonad TcError Env Log

free :: TcM Int
free = state $ \(MkEnv i s) -> (i, MkEnv (i+1) s)

lookupTy :: Int -> TcM (Maybe Ty)
lookupTy i = get >>= \(MkEnv _ (MkSubst s)) -> return $ Map.lookup i s

extendSubst :: Int -> Ty -> TcM ()
extendSubst i ty = get >>= \(MkEnv j (MkSubst s)) ->
  put $ MkEnv j $ MkSubst $ Map.insert i ty s

evalTcM :: TcM a -> IO a
evalTcM (flip runTcM emptyEnv -> (x, _, w)) =
  either (error . showTcError) (\y -> putStrLn ("LOG: " ++ w) >> return y) x

-------------------------------------------------------------------------------
-- NORMAL TYPECHECKING
-------------------------------------------------------------------------------

isArrow :: Ty -> TcM (Ty, Ty)
isArrow (a :-> b) = return (a, b)
isArrow ty        = do
  a <- fmap Meta free
  b <- fmap Meta free
  throwError $ Unexpected ty (a :-> b)

isFunctorApp :: Ty -> TcM (F, Ty)
isFunctorApp (f :@: x) = return (f, x)
isFunctorApp ty        = do
  f <- fmap (\i -> MkF "F" $ \t -> Meta i) free -- ^ TODO: check name
  a <- fmap Meta free
  throwError $ Unexpected ty (f :@: a)

closed :: Ty -> TcM ()
closed (Meta i)  = throwError $ TypeNotClosed i
closed Unit      = return ()
closed (PrimT _) = return ()
closed (a :+: b) = closed a >> closed b
closed (a :*: b) = closed a >> closed b
closed (a :-> b) = closed a >> closed b
closed (f :@: a) = closed a >> closed (subst f a)
closed (Mu f)    = closed (subst f Unit)

closedF :: F -> TcM ()
closedF f = closed (subst f Unit)

closedBiF :: BiF -> TcM ()
closedBiF f = closedF (App f Unit)

eq :: Ty -> Ty -> TcM Bool
eq (Unit     ) (Unit     ) =
  return True
eq (PrimT p  ) (PrimT q  ) =
  return (p == q)
eq (a1 :+: b1) (a2 :+: b2) = do
  a <- eq a1 a2
  b <- eq b1 b2
  return $ a && b
eq (a1 :*: b1) (a2 :*: b2) = do
  a <- eq a1 a2
  b <- eq b1 b2
  return $ a && b
eq (a1 :-> b1) (a2 :-> b2) = do
  a <- eq a1 a2
  b <- eq b1 b2
  return $ a && b
eq (Mu f     ) (Mu g     ) = do
  a <- fmap Meta free
  subst f a `eq` subst f a
eq (f1 :@: t1) (f2 :@: t2) = do
  subst f1 t1 `eq` subst f2 t2
eq (Meta i   ) (Meta j)    =
  return $ i == j
eq a           b           =
  return False

unifyMeta :: Int -> Ty -> TcM Ty
unifyMeta i ty = do
  mty <- lookupTy i
  case mty of
    Nothing  -> extendSubst i ty >> return ty
    Just ty' -> do
      ty'' <- unify ty ty'
      extendSubst i ty''
      return ty''

unify :: Ty -> Ty -> TcM Ty
unify  (Unit     ) (Unit     ) =
  return Unit
unify  a@(PrimT p) b@(PrimT q)
  | p == q    = return a
  | otherwise = throwError $ TypeMismatch a b
unify  (a1 :+: b1) (a2 :+: b2) = do
  a <- unify a1 a2
  b <- unify b1 b2
  return $ a :+: b
unify  (a1 :*: b1) (a2 :*: b2) = do
  a <- unify a1 a2
  b <- unify b1 b2
  return $ a :*: b
unify  (a1 :-> b1) (a2 :-> b2) = do
  a <- unify a1 a2
  b <- unify b1 b2
  return $ a :-> b
unify  (Mu f     ) (Mu g     ) = do
  unifyF f g
  return $ Mu f
unify  (f1 :@: t1) (f2 :@: t2) = do
  unifyF f1 f2
  t <- unify t1 t2
  return $ f1 :@: t
unify  (Meta i   ) b           =
  unifyMeta i b
unify  a           (Meta i   ) =
  unifyMeta i a
unify  a           b           = throwError $ TypeMismatch a b

unifyBiF :: BiF -> BiF -> TcM ()
unifyBiF f g = do
  closedBiF f
  closedBiF g
  if f == g then return ()
            else throwError $ BiFunctorMismatch f g

unifyF :: F -> F -> TcM ()
unifyF f@(MkF _ _) g@(MkF _ _) = do
  closedF f
  closedF g
  if f == g then return ()
            else throwError $ FunctorMismatch f g
unifyF (App f a) (App g b) = do
  unify a b
  unifyBiF f g
unifyF (Poly f) (Poly g) =
  unifyBiF f g
unifyF f g =
  throwError $ FunctorMismatch f g

inferPrimTy :: PrimFun -> TcM Ty
inferPrimTy Id       = fmap Meta free >>= \a ->
                                      return $ a            :-> a
inferPrimTy (In  f)  =                return $ f :@: (Mu f) :-> Mu f
inferPrimTy (Out f)  =                return $ Mu f         :-> f :@: (Mu f)
inferPrimTy (AF   c) =                return $ tyOf c

inferTy' :: SE -> TcM Ty
inferTy' (Fun p)        =
  inferPrimTy p
inferTy' (PMap f s)     = do
  c      <- fmap Meta free
  (a, b) <- inferTy' s >>= isArrow
  return $ App f a :@: c :-> App f b :@: c
inferTy' (Comp s1 s2)    = do
  (a, b1) <- inferTy' s1 >>= isArrow
  (b2, c) <- inferTy' s2 >>= isArrow
  unify b1 b2 >>= closed
  return (a :-> c)
inferTy' (Hylo s1 s2) = do
  (a1, (f1, a2)) <- inferTy' s2 >>= isArrow >>= mapSndM isFunctorApp
  ((f2, b1), b2) <- inferTy' s1 >>= isArrow >>= mapFstM isFunctorApp
  unifyF f1 f2
  a <- unify a1 a2
  closed a
  b <- unify b1 b2
  closed b
  return (a :-> b)

applySubstF :: F -> TcM F
applySubstF (App f t) =
  fmap (App f) $ applySubst t
applySubstF f =
  return f

applySubst :: Ty -> TcM Ty
applySubst (Meta i)     =
  lookupTy i >>=
  maybe (throwError $ UnifErr i) return
applySubst Unit         =
  return Unit
applySubst ty@(PrimT p) =
  return ty
applySubst (a :+: b)    = do
  a' <- applySubst a
  b' <- applySubst b
  return $ a' :+: b'
applySubst (a :*: b)    = do
  a' <- applySubst a
  b' <- applySubst b
  return $ a' :*: b'
applySubst (a :-> b)    = do
  a' <- applySubst a
  b' <- applySubst b
  return $ a' :-> b'
applySubst (f :@: a)    = do
  f' <- applySubstF f
  a' <- applySubst a
  return $ f' :@: a'
applySubst (Mu f)       =
  fmap Mu $ applySubstF f

inferTy :: SE -> TcM Ty
inferTy s = do
  t1 <- inferTy' s
  t2 <- applySubst t1
  closed t2
  return t2

checkTy :: SE -> Ty -> TcM ()
checkTy s t = do
  t' <- inferTy s
  unlessM (t `eq` t') $ throwError $ TypeMismatch t t'

-------------------------------------------------------------------------------
-- SE STRUCTURE
-------------------------------------------------------------------------------

data Prof = MkProf { size_p   :: Size -> Size
                   , cost_p   :: Size -> Cost
                   }

-- The equality is only on the structure, we ignore the profiling information
instance Eq Prof where
  _ == _ = True

-- | Primitive function structure
data SPrimFun  where
  ID_P    ::         SPrimFun -- ^
  IN_P    :: F    -> SPrimFun -- ^
  OUT_P   :: F    -> SPrimFun -- ^
  A_P     :: Prof -> SPrimFun -- ^
  deriving Eq


instance Show SPrimFun where
  show (ID_P   ) = "ID"
  show (IN_P  _) = "IN"
  show (OUT_P _) = "OUT"
  show (A_P   _) = "A"

-- | SE Structure
-- TODO: Lacking peval
data StructS where
  -- | For unification purposes
  PRIM  :: SPrimFun                -> StructS
  F_    :: BiF -> StructS          -> StructS
  COMP  :: StructS -> StructS      -> StructS
  HYLO  :: F -> StructS -> StructS -> StructS
  deriving (Eq, Show)

iD    ::         StructS -- ^
iD = PRIM ID_P

iN    :: F ->    StructS -- ^
iN = PRIM . IN_P

oUT   :: F ->    StructS -- ^
oUT = PRIM . OUT_P

aF     :: Prof -> StructS -- ^
aF = PRIM . A_P

class Composable a where
  (<.>) :: a -> a -> a

instance Composable SE where
  (<.>) = Comp

instance Composable StructS where
  (<.>) = COMP


mAP :: BiF -> StructS -> StructS
mAP g s = HYLO (Poly g) ((iN $ Poly g) <.> F_ g s) (oUT $ Poly g)

cATA :: F -> StructS -> StructS
cATA f s = HYLO f s (oUT f)

aNA :: F -> StructS -> StructS
aNA f s = HYLO f (iN f) s

-- infix 1 >:

-- | Properties:
--      recomp . decomp = id
--      decomp . recomp = id
class Struct a b where
  decomp  :: a -> (b, [CFun])
  recomp' :: (b, [CFun]) -> (a, [CFun])

  recomp :: (b, [CFun]) -> a
  recomp (recomp' -> (a, f)) =
    case f of
      [] -> a
      _  -> error "PANIC! Wrong functionality in 'recomp'"


instance Struct CFun Prof where

  decomp c = ( MkProf { size_p = size_f c
                         , cost_p = cost_f c
                         }
             , [c]
             )
  recomp' (_, c:r) = (c, r)

instance Struct PrimFun SPrimFun where

  decomp (Id    ) = (ID_P   , [])
  decomp (In   f) = (IN_P  f, [])
  decomp (Out  f) = (OUT_P f, [])
  decomp (AF   c) = (A_P   a, b )
    where (a, b) = decomp c

  recomp' (ID_P   , a)  = (Id   , a )
  recomp' (IN_P  f, a)  = (In  f, a )
  recomp' (OUT_P f, a)  = (Out f, a )
  recomp' (A_P   a, b)  = (AF  x, bs)
    where (x, bs) = recomp' (a, b)

instance Struct SE StructS where
  decomp (Fun p)      = (PRIM a, b)
    where (a, b) = decomp p
  decomp (PMap g s)   = (F_ g a, b)
    where (a, b) = decomp s
  decomp (Comp s1 s2) = (COMP a1 a2, b1 ++ b2)
    where (a1, b1) = decomp s1
          (a2, b2) = decomp s2
  decomp (Hylo s1 s2) = (HYLO f a1 a2, b1 ++ b2)
    where (a1, b1) = decomp s1
          (a2, b2) = decomp s2
          f        = extractFunctor $
                      flip runTcM emptyEnv $
                        inferTy' s2 >>=
                          isArrow >>=
                            mapSndM isFunctorApp
          extractFunctor (Right (_, (f, _)), _, _)
            = f
          extractFunctor _
            = error $ "PANIC! ill-typed term in 'decomp'"

  recomp' (PRIM a      , b) = first Fun       $ recomp' (a , b )
  recomp' (F_ g a      , b) = first (PMap g)  $ recomp' (a , b )
  recomp' (COMP a1 a2  , b) = first (Comp s1) $ recomp' (a2, b')
    where (s1, b') = recomp' (a1, b)
  recomp' (HYLO f a1 a2, b) = first (Hylo s1) $ recomp' (a2, b')
    where (s1, b') = recomp' (a1, b)

-------------------------------------------------------------------------------
-- REWRITING SYSTEM
-------------------------------------------------------------------------------

-- | Normalisation: apply rules until no redex left
-- TODO: choose an efficient normalisation, instead of applying all rules until
-- none is applicable
normS :: StructS -> StructS -- We pick right associativity for normalisation
normS (rwsS -> Right s) = normS s
normS (rwsS -> Left  s) = s

mapEither :: (a -> b) -> Either a a -> Either b b
mapEither f (Left  x) = Left  $ f x
mapEither f (Right x) = Right $ f x

-- Apply rule of the RWS somewhere
rwsS :: StructS -> Either StructS StructS
rwsS (ruleS -> Right s) = Right s
rwsS (ruleS -> Left  s) = traverse s
  where
    traverse (PRIM p)       = Left (PRIM p)
    traverse (F_ g s)       = mapEither (F_ g) $ rwsS s
    traverse (COMP s1 s2)   =
      case rwsS s1 of
        Right s1'          -> Right (COMP s1' s2)
        Left _             -> mapEither (COMP s1) $ rwsS s2
    traverse (HYLO f s1 s2) =
      case rwsS s1 of
        Right s1'          -> Right (HYLO f s1' s2)
        Left _             -> mapEither (HYLO f s1) $ rwsS s2

ruleS :: StructS -> Either StructS StructS
ruleS (assoc -> s) = rwRuleS s

assoc :: StructS -> StructS
assoc = assocR

assocR :: StructS -> StructS
assocR (COMP (COMP s1 s2) s3) = assocR (COMP s1 (COMP s2 s3))
assocR s@(PRIM _)     = s
assocR (F_ f s1)      = F_ f $ assocR s1
assocR (COMP s1 s2)   = COMP (assocR s1) (assocR s2)
assocR (HYLO f s1 s2) = HYLO f (assocL s1) (assocR s2)

assocL :: StructS -> StructS
assocL (COMP s1 (COMP s2 s3)) = assocL (COMP (COMP s1 s2) s3)
assocL s@(PRIM _)     = s
assocL (F_ f s1)      = F_ f $ assocL s1
assocL (COMP s1 s2)   = COMP (assocL s1) (assocL s2)
assocL (HYLO f s1 s2) = HYLO f (assocL s1) (assocR s2)

-- Rules of the RWS
rwRuleS :: StructS -> Either StructS StructS
rwRuleS (COMP (PRIM ID_P) s)
  = Right s
rwRuleS (COMP s (PRIM ID_P))
  = Right s
rwRuleS (COMP (PRIM (OUT_P _)) (PRIM (IN_P  _)))
  = Right $ PRIM ID_P
rwRuleS (COMP (PRIM (IN_P _))  (PRIM (OUT_P _)))
  = Right $ PRIM ID_P
rwRuleS (F_ g (COMP s1 s2))
  = Right $ COMP (F_ g s1) (F_ g s2)
rwRuleS (HYLO _ (PRIM (IN_P _)) (PRIM (OUT_P _)))
  = Right $ PRIM ID_P
rwRuleS   (HYLO (Poly f)
            (PRIM (IN_P _))
            (COMP (F_ g s)
                  (PRIM (OUT_P _))))    = Right $ mAP f s
rwRuleS s@(HYLO (Poly f)
            (COMP s1 (F_ g s2))
            (PRIM (OUT_P _)))
  | f == g           = Right $ cATA (Poly f) s1 <.> mAP f s2
  | otherwise        = Left s
rwRuleS s@(HYLO (Poly f)
            (PRIM (IN_P _))
            (COMP (F_ g s1) s2))
  | f == g           = Right $ mAP f s1 <.> aNA (Poly f) s2
  | otherwise        = Left s
rwRuleS s@(HYLO f (PRIM (IN_P   _)) s1) = Left s
rwRuleS s@(HYLO f  s1 (PRIM (OUT_P _))) = Left s
rwRuleS   (HYLO f  s1 s2              ) = Right $ cATA f s1 <.> aNA f s2
rwRuleS s                               = Left s


normP :: StructS -> StructS   -- Parallelism erasure
normP s = s -- TODO: after adding parallel structures

norm :: StructS -> StructS
norm = normS . normP

-------------------------------------------------------------------------------
-- PARSER AND PRETTY PRINTER
-------------------------------------------------------------------------------

-- AST of functor definitions
data FunctorDef = FD { f_name :: Name
                     , f_args :: [Name]
                     , f_body :: TyDef
                     }

-- AST of types
data TyDef =
    TdUnit
  | TdVar   Name
  | TdConst PrimTy
  | TdSum  TyDef TyDef
  | TdProd TyDef TyDef
  | TdApp  TyDef TyDef
  | TdArr  TyDef TyDef
  | TdFix  TyDef

-- AST of function definitions
data FunDef = FunDef { fd_name :: Name
                     , fd_type :: TyDef
                     , fd_body :: ExprDef
                     }

-- AST of structured expressions
data ExprDef =
    EUnit
  | ELit   Literal
  | EVar   Name
  | EApp   ExprDef ExprDef
  | EComp  ExprDef ExprDef
  | EMap   Name ExprDef
  | EHylo  TyDef ExprDef ExprDef

data Literal =
    ILit Int
  | BLit Bool
  | SLit String

type LangDef st   = GenLanguageDef Name st (LState.State SourcePos)
type Lexer   st   = GenTokenParser Name st (LState.State SourcePos)
type Parser  st a = Indent.IndentParser Name st a

-- | Language def for the DSL
-- C-like comments
parDef :: LangDef st
parDef = Language.emptyDef
           { Token.commentStart   = "/*"
           , Token.commentEnd     = "*/"
           , Token.commentLine    = "//"
           , Token.nestedComments = True
           , Token.identStart     = letter   <|> char '_'
           , Token.identLetter    = alphaNum <|> oneOf "_'"
           , Token.opStart        = Token.opLetter parDef
           , Token.opLetter       = oneOf ":!#$%&*+./<=>?@\\^|-~"
           , Token.reservedOpNames= ["+", "*", "~", "|->", "->", "=", ":="]
           , Token.reservedNames  = ["fix"]
           , Token.caseSensitive  = True
           }

-- | The lexer
lexer :: Lexer st
lexer = Token.makeTokenParser parDef

identifier     ::                Parser st Name
reserved       :: String ->      Parser st ()
operator       ::                Parser st Name
reservedOp     :: String ->      Parser st ()
charLiteral    ::                Parser st Char
stringLiteral  ::                Parser st Name
natural        ::                Parser st Integer
integer        ::                Parser st Integer
float          ::                Parser st Double
naturalOrFloat ::                Parser st (Either Integer Double)
decimal        ::                Parser st Integer
hexadecimal    ::                Parser st Integer
octal          ::                Parser st Integer
symbol         :: String ->      Parser st Name
lexeme         :: Parser st a -> Parser st a
whiteSpace     ::                Parser st ()
parens         :: Parser st a -> Parser st a
braces         :: Parser st a -> Parser st a
angles         :: Parser st a -> Parser st a
brackets       :: Parser st a -> Parser st a
squares        :: Parser st a -> Parser st a
semi           ::                Parser st Name
comma          ::                Parser st Name
colon          ::                Parser st Name
dot            ::                Parser st Name
semiSep        :: Parser st a -> Parser st [a]
semiSep1       :: Parser st a -> Parser st [a]
commaSep       :: Parser st a -> Parser st [a]
commaSep1      :: Parser st a -> Parser st [a]

identifier     = Token.identifier     lexer
reserved       = Token.reserved       lexer
operator       = Token.operator       lexer
reservedOp     = Token.reservedOp     lexer
charLiteral    = Token.charLiteral    lexer
stringLiteral  = Token.stringLiteral  lexer
natural        = Token.natural        lexer
integer        = Token.integer        lexer
float          = Token.float          lexer
naturalOrFloat = Token.naturalOrFloat lexer
decimal        = Token.decimal        lexer
hexadecimal    = Token.hexadecimal    lexer
octal          = Token.octal          lexer
symbol         = Token.symbol         lexer
lexeme         = Token.lexeme         lexer
whiteSpace     = Token.whiteSpace     lexer
parens         = Token.parens         lexer
braces         = Token.braces         lexer
angles         = Token.angles         lexer
brackets       = Token.brackets       lexer
squares        = Token.squares        lexer
semi           = Token.semi           lexer
comma          = Token.comma          lexer
colon          = Token.colon          lexer
dot            = Token.dot            lexer
semiSep        = Token.semiSep        lexer
semiSep1       = Token.semiSep1       lexer
commaSep       = Token.commaSep       lexer
commaSep1      = Token.commaSep1      lexer

capitalIdent :: Parser st Name
capitalIdent = Indent.sameOrIndented >> do
  i <- identifier
  case i of
    (c:_) | isUpper c -> return i
    _                 -> fail "expected uppercase letter"

lowerIdent :: Parser st Name
lowerIdent = Indent.sameOrIndented >> do
  i <- identifier
  case i of
    (c:_) | isLower c -> return i
    _                 -> fail "expected uppercase letter"

tyTable :: OperatorTable Name st (LState.State SourcePos) TyDef
tyTable =
  [ [ Prefix ((Indent.sameOrIndented
               >> reserved "fix"  >> return TdFix ) <?> "type")            ]
  , [ Infix  ((Indent.sameOrIndented
               >>                    return TdApp ) <?> "type") AssocLeft  ]
  , [ Infix  ((Indent.sameOrIndented
               >> reservedOp "*"  >> return TdProd) <?> "type") AssocLeft  ]
  , [ Infix  ((Indent.sameOrIndented
               >> reservedOp "+"  >> return TdSum ) <?> "type") AssocLeft  ]
  , [ Infix  ((Indent.sameOrIndented
               >> reservedOp "->" >> return TdArr ) <?> "type") AssocRight ]
  ]

primTy :: Parser st PrimTy
primTy = Indent.sameOrIndented >> capitalIdent >>= \i ->
  case i of
    "Bool"   -> return BOOL
    "Int"    -> return INT
    "String" -> return STRING
    _        -> fail $ "Not a primitive type: " ++ show i

tyTerm :: Parser st TyDef
tyTerm = Indent.sameOrIndented >>
  (   (try (symbol "(" >> symbol ")" >> return TdUnit))
  <|> parens parseTyDef
  <|> liftM TdConst (try primTy)
  <|> liftM TdVar capitalIdent
  <?> "type")

parseTyDef :: Parser st TyDef
parseTyDef = (Indent.sameOrIndented >> buildExpressionParser tyTable tyTerm) <?> "type"

parseFunctorDef :: Parser st FunctorDef
parseFunctorDef = do
  Indent.checkIndent
  f    <- capitalIdent
  args <- many capitalIdent
  reservedOp ":="
  ty   <- parseTyDef
  return $ FD { f_name = f
              , f_args = args
              , f_body = ty
              }

parseDefTy :: Parser st (Name, TyDef)
parseDefTy =  (do
  Indent.checkIndent
  f  <- identifier
  reservedOp ":"
  td <- parseTyDef
  return (f, td)
  ) <?> "type declaration"

parseDef :: Parser st FunDef
parseDef = parseDefTy >>= uncurry parseBody

parseBody :: Name -> TyDef -> Parser st FunDef
parseBody n ty = (do
  Indent.checkIndent
  symbol n
  reservedOp "="
  e <- parseExpr
  return $ FunDef { fd_name = n
                  , fd_type = ty
                  , fd_body = e
                  }
  ) <?> "definition"

parseExpr :: Parser st ExprDef
parseExpr = Indent.sameOrIndented
              >> (buildExpressionParser exprTable pExpr
                 <?> "structured expression")

exprTable :: OperatorTable Name st (LState.State SourcePos) ExprDef
exprTable =
  [ [ Infix  ((Indent.sameOrIndented
                >> return EApp                         ) <?> sE) AssocLeft  ]
  , [ Prefix ((Indent.sameOrIndented
                >> liftM EMap (try capitalIdent)       ) <?> sE)            ]
  , [ Infix  ((Indent.sameOrIndented
                >> try (reservedOp ".") >> return EComp) <?> sE) AssocRight ]
  ]
  where sE = "structured expression"

pExpr :: Parser st ExprDef
pExpr = (Indent.sameOrIndented >>
          ((try (reserved "hylo") >>
              tyTerm >>= \f ->
                liftM2 (EHylo f) aExpr aExpr)
           <|> aExpr
           <?> "structured expression"))

aExpr :: Parser st ExprDef
aExpr = Indent.sameOrIndented >>
         ((try (symbol "(" >> symbol ")" >> return EUnit))
     <|> parens parseExpr
     <|> liftM EVar (try lowerIdent)
     <|> liftM (ELit . ILit . fromInteger) (try integer)
     <|> (reserved "True"  >> return (ELit $ BLit True))
     <|> (reserved "False" >> return (ELit $ BLit False))
     <|> liftM (ELit . SLit) stringLiteral
     <?> "structured expression")

-- | The pretty printer
class Ppr a where
  ppr     :: a -> Pretty.Doc
  showPpr :: a -> String
  showPpr = Pretty.renderStyle skelStyle . ppr
    where skelStyle = Pretty.Style
                        { Pretty.mode           = Pretty.PageMode
                        , Pretty.lineLength     = 80
                        , Pretty.ribbonsPerLine = 0
                        }

instance Ppr Name where
  ppr = Pretty.text

instance Ppr [Name] where
  ppr = Pretty.hsep . map ppr

instance Ppr PrimTy where
  ppr BOOL   = Pretty.text "Bool"
  ppr INT    = Pretty.text "Int"
  ppr STRING = Pretty.text "String"

instance Ppr TyDef where
  ppr  TdUnit         = Pretty.parens Pretty.empty
  ppr (TdVar   n)     = Pretty.text n
  ppr (TdConst p)     = ppr p
  ppr (TdSum   t1 t2) = Pretty.hsep [ needParens t1 (ppr t1)
                                    , Pretty.char '+'
                                    , needParens t2 (ppr t2)]
    where needParens (TdArr _ _) p = Pretty.parens p
          needParens _           p = p
  ppr (TdProd  t1 t2) = Pretty.hsep [ needParens t1 (ppr t1)
                                    , Pretty.char '*'
                                    , needParens t2 (ppr t2)]
    where needParens (TdArr _ _) p = Pretty.parens p
          needParens (TdSum _ _) p = Pretty.parens p
          needParens _           p = p
  ppr (TdApp   t1 t2) = Pretty.hsep $
                          map (\t -> needParens t (ppr t))
                              (getApp t1 [t2])
    where getApp (TdApp t3 t4) ts = getApp t3 (t4:ts)
          getApp t             ts = t:ts
          needParens (TdArr  _ _) p = Pretty.parens p
          needParens (TdSum  _ _) p = Pretty.parens p
          needParens (TdProd _ _) p = Pretty.parens p
          needParens (TdApp  _ _) p = Pretty.parens p
          needParens (TdFix  _  ) p = Pretty.parens p
          needParens _            p = p
  ppr (TdArr   t1 t2) = Pretty.hsep [ppr t1, Pretty.text "->", ppr t2]
  ppr (TdFix   t)     = Pretty.hsep [Pretty.text "fix", needParens t $ ppr t]
    where needParens (TdArr  _ _) p = Pretty.parens p
          needParens (TdSum  _ _) p = Pretty.parens p
          needParens (TdProd _ _) p = Pretty.parens p
          needParens (TdApp  _ _) p = Pretty.parens p
          needParens (TdFix  _  ) p = Pretty.parens p
          needParens _            p = p

instance Ppr FunctorDef where
  ppr fd = Pretty.hsep $ [ ppr (f_name fd)
                         , ppr (f_args fd)
                         , Pretty.text ":="
                         , ppr (f_body fd)
                         ]

instance Ppr Literal where
  ppr (ILit i)     = Pretty.int i
  ppr (BLit True)  = Pretty.text "True"
  ppr (BLit False) = Pretty.text "False"
  ppr (SLit s)     = Pretty.doubleQuotes (Pretty.text s)

instance Ppr ExprDef where
  ppr EUnit            = Pretty.parens Pretty.empty
  ppr (ELit l)         = ppr l
  ppr (EVar n)         = Pretty.text n
  ppr (EApp e1 e2)     = Pretty.hsep $ map parens $ chaseApp e1 [e2]
    where chaseApp (EApp e1 e2) es = chaseApp e1 (e2:es)
          chaseApp e            es = e:es
          parens e@(EHylo _ _ _)  = Pretty.parens $ ppr e
          parens e@(EMap  _ _  )  = Pretty.parens $ ppr e
          parens e@(EComp _ _  )  = Pretty.parens $ ppr e
          parens e@(EApp  _ _  )  = Pretty.parens $ ppr e
          parens e                = ppr e
  ppr (EComp e1 e2)    = flatten [e1,e2]
    where
      flatten ((EComp e1 e2):es) = flatten (e1:e2:es)
      flatten [e1]               = ppr e1
      flatten (e1           :es) = ppr e1 <+> Pretty.char '.' <+> flatten es
  ppr (EMap f e)       = Pretty.text f <+> parens e
    where
      parens e@(EUnit )            = ppr e
      parens e@(ELit _)            = ppr e
      parens e@(EVar _)            = ppr e
      parens e@(_     )            = Pretty.parens $ ppr e
  ppr (EHylo td e1 e2) = Pretty.text "hylo"
                           <+> parensTy td
                           <+> parens e1
                           <+> parens e2
    where
      parensTy e@(TdUnit   ) = ppr e
      parensTy e@(TdVar _  ) = ppr e
      parensTy e@(TdConst _) = ppr e
      parensTy e@(_        ) = Pretty.parens $ ppr e
      parens e@(EUnit )            = ppr e
      parens e@(ELit _)            = ppr e
      parens e@(EVar _)            = ppr e
      parens e@(_     )            = Pretty.parens $ ppr e

instance Ppr FunDef where
  ppr fd = (Pretty.text (fd_name fd) <+> Pretty.colon    <+> ppr (fd_type fd))
        $$ (Pretty.text (fd_name fd) <+> Pretty.char '=' <+> ppr (fd_body fd))


instance Show PrimTy where
  show = showPpr
instance Show TyDef where
  show = showPpr
instance Show FunctorDef where
  show = showPpr
instance Show Literal where
  show = showPpr
instance Show ExprDef where
  show = showPpr
instance Show FunDef where
  show = showPpr

-- FIXME: for testing
runP s = either (error . show) id $ Indent.runIndent "" (runParserT (Indent.withPos parseDef)  () "" s)

