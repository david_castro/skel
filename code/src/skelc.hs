module Main where

import FrontEnd
import Core hiding ( parseFile )
import TcCore

import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    []    -> putStrLn "Usage: skelc <prog.skel>"
    (a:_) -> compile a

compile :: String -> IO ()
compile fn = do
  (s, st1) <- parseF fn
  print s
--st2      <- typeCheck st1 s
--print st2
