module FrontEnd.Type
  ( EType (..)
  , pType
  , aType

  , ForAll (..)
  , pForAll
  ) where

import Control.Monad      ( liftM, liftM2 )
import Control.Monad.Fix  ( mfix )
import Data.Map.Strict    ( Map )
import Data.Sequence      ( Seq )
import Text.Parsec        ( Stream, ParsecT, manyTill
                          , try, many, many1, sepBy1
                          , (<|>), (<?>) )
import Text.Parsec.Indent ( withBlock )
import Text.PrettyPrint   ( hsep, text, (<+>), (<>)
                          , char, int, punctuate, vcat
                          , nest, ($$) )

import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Ppr
import qualified Data.Map.Strict      as Map

import Common.Parser
import Common.Ppr
import Common.Literal
import Common.Prim


data EType =
    TyVar  Name
  | TyMeta Int

  | TySum  [EType]
  | TyProd [EType]

  | TyApp  [EType]                -- F A B
  | TyArr  EType EType            -- A -> B TODO: Tag a structure!

instance Ppr EType where
  ppr (TyVar  n    ) = text n
  ppr (TyMeta i    ) = char '?' <> int i
  ppr (TySum  ts   ) = hsep $ punctuate (char '+') $ map parensSum ts
    where
      parensSum p@(TySum _) = Ppr.parens $ ppr p
      parensSum p@ TyArr{}  = Ppr.parens $ ppr p
      parensSum p           = ppr p
  ppr (TyProd ts   ) = hsep $ punctuate (char '*') $ map parensProd ts
    where
      parensProd p@(TySum  _) = Ppr.parens $ ppr p
      parensProd p@(TyProd _) = Ppr.parens $ ppr p
      parensProd p@ TyArr{}   = Ppr.parens $ ppr p
      parensProd p            = ppr p
  ppr (TyApp  ts   ) = hsep $ map parensApp ts
    where
      parensApp p@(TySum  _) = Ppr.parens $ ppr p
      parensApp p@(TyProd _) = Ppr.parens $ ppr p
      parensApp p@ TyArr{}   = Ppr.parens $ ppr p
      parensApp p@(TyApp _)  = Ppr.parens $ ppr p
      parensApp p            = ppr p
  ppr (TyArr  t1 t2) = parensArr t1 <+> arrow <+> ppr t2
    where
      parensArr p@(TyArr _ _) = Ppr.parens $ ppr p
      parensArr p             = ppr p


pType :: Parser Int EType
pType = indent $ fmap (foldr1 TyArr) (sepBy1 sumType $ reservedOp "->")

sumType :: Parser Int EType
sumType = indent $ fmap TySum (sepBy1 prodType $ reservedOp "+")

prodType :: Parser Int EType
prodType = indent $ fmap TyProd (sepBy1 appType $ reservedOp "*")

appType :: Parser Int EType
appType = indent $ fmap TyApp (many1 aType)

aType :: Parser Int EType
aType = indent $
      try (parens pType)
  <|>      fmap TyVar capitalIdent

data ForAll =
  ForAll [Name] EType

instance Ppr ForAll where
  ppr (ForAll [] e) = ppr e
  ppr (ForAll ns e) = hsep $ text "forall" : (map ppr ns ++ [Ppr.comma, ppr e])

pForAll :: Parser Int ForAll
pForAll = indent $ do
  ns <- try (reserved "forall" *> many1 capitalIdent <* comma) <|> return []
  ty <- pType
  return (ForAll ns ty)
