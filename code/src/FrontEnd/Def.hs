module FrontEnd.Def
  ( Def  (..)
  , pFnDef
  , pTyDef


  , Constr
  , pConstrDecl
  ) where

import Control.Monad      ( liftM, liftM2 )
import Control.Monad.Fix  ( mfix )
import Data.Map.Strict    ( Map )
import Data.Sequence      ( Seq )
import Text.Parsec        ( Stream, ParsecT, manyTill
                          , try, many, many1, sepBy1
                          , (<|>), (<?>) )
import Text.Parsec.Indent ( withBlock )
import Text.PrettyPrint   ( hsep, text, (<+>), (<>)
                          , char, int, punctuate, vcat
                          , nest, ($$), ($+$) )

import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Ppr
import qualified Data.Map.Strict      as Map

import Common.Parser
import Common.Ppr
import Common.Literal
import Common.Prim

import FrontEnd.Expr
import FrontEnd.Pattern
import FrontEnd.Type

data Def =
    FnDef Name ForAll [([Pattern], Expr)] -- d : A -> B // d p1 ... = e1 // d p2 ... = e2 ..
  | TyDef Name [Name] (Map Name Constr) -- data Ty A B = C1 ... | C2 ... | ...

instance Ppr Def where
  ppr (FnDef n ty alts) = vcat (sig:ps) $+$ text ""
    where
      sig = text n <+> Ppr.colon <+> ppr ty
      ps  = map ((text n <+>) . pprAlt) alts

      pprAlt (ps, e) = hsep $ map pprAPat ps ++ [char '=', ppr e]

  ppr (TyDef  n ns cs)
    = text "data" <+> text n <+> hsep (map text ns) <+> char '=' $$
      nest 2 (punctuateIndent (char '|') $ map ppr $ Map.elems cs) $+$ text ""

punctuateIndent :: Ppr.Doc -> [Ppr.Doc] -> Ppr.Doc
punctuateIndent _ []  = Ppr.empty
punctuateIndent _ [d] = d
punctuateIndent c (d1:ds) = nest len d1 $$ aux ds
  where
    len         = length (showPpr c) + 1
    aux []      = Ppr.empty
    aux (d1:ds) = c <+> d1 $$ aux ds

instance Show Def where
  show = showPpr

pFnDef :: Parser Int Def
pFnDef = pFnDefType >>= \(n, sch) -> fmap (FnDef n sch) (pBody n)

pFnDefType :: Parser Int (Name, ForAll)
pFnDefType = do
  Indent.checkIndent
  f   <- lowerIdent
  reservedOp ":"
  sch <- pForAll
  return (f, sch)
  <?> "type declaration"

pBody :: Name -> Parser Int [([Pattern], Expr)]
pBody n = many aux
  where aux = do Indent.checkIndent
                 symbol n
                 ps <- many aPattern
                 reservedOp "="
                 e  <- pExpr
                 return (ps, e)
                 <?> "definition body"

data Constr =
    CInd Name Name [EType] -- CInd ty "n" [t1, t2, ...] ===> n : t1 -> t2 -> ... -> ty

instance Show Constr where
  show = showPpr
instance Ppr Constr where
  ppr (CInd ty c args) = hsep $ text c : map parensPpr args
    where
      parensPpr t@(TyVar  n    ) = ppr t
      parensPpr t@(TyMeta i    ) = ppr t
      parensPpr t@(TySum  ts   ) = Ppr.parens (ppr t)
      parensPpr t@(TyProd ts   ) = Ppr.parens (ppr t)
      parensPpr t@(TyApp  ts   ) = Ppr.parens (ppr t)
      parensPpr t@(TyArr  t1 t2) = Ppr.parens (ppr t)

pConstrDecl :: Name -> Parser Int Constr
pConstrDecl n = indent $ liftM2 (CInd n) capitalIdent (many aType)

pTyDef :: Parser Int Def
pTyDef = do
  Indent.checkIndent
  reservedOp "data"
  ty <- capitalIdent
  liftM2 (TyDef ty)
    (many capitalIdent <* reservedOp "=")
    (toMap <$> sepBy1 (pConstrDecl ty) (indent $ reservedOp "|"))
  where toMap = Map.fromList . map liftName
        liftName c@(CInd _ n _) = (n, c)
