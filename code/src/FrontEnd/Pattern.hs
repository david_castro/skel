module FrontEnd.Pattern
  ( Pattern
  , pPattern
  , aPattern

  , pprAPat
  ) where

import Control.Monad      ( liftM2 )
import Text.Parsec        ( try, many, sepBy1, (<|>), (<?>) )
import Text.PrettyPrint   ( hsep, text, (<+>), (<>), punctuate )

import qualified Text.PrettyPrint     as Ppr

import Common.Parser
import Common.Ppr

data Pattern =
    PVar    Name         -- Variable:    x
  | PSum    Integer  Pattern  -- Sum type constructor
  | PProd           [Pattern] -- Product type constructor

  | PConstr Name    [Pattern] -- User defined constructor: c p1 .. pn

instance Show Pattern where
  show = showPpr

instance Ppr Pattern where
  ppr (PVar n)   = text n
  ppr (PSum i p) = text "inj_" <> Ppr.integer i <> pprAPat p
  ppr (PProd ps) = Ppr.parens $ hsep $ punctuate Ppr.comma $ map ppr ps
  ppr (PConstr n ps) = text n <+> hsep (map pprAPat ps)

pprAPat :: Pattern -> Ppr.Doc
pprAPat p@(PVar _)       = ppr p
pprAPat p@(PConstr _ []) = ppr p
pprAPat p@(PProd _)      = ppr p
pprAPat p                = Ppr.parens $ ppr p

pPattern :: Parser Int Pattern
pPattern = indent (
   -- <|> try (inj >>= \i -> fmap (PConstr (sumConstr i) . (:[])) aPattern)
       try (inj >>= \i -> fmap (PSum i) aPattern)
   <|> try (liftM2 PConstr capitalIdent $ many aPattern)
   <|>     aPattern
   <?> "pattern" )


aPattern :: Parser Int Pattern
aPattern = indent (
       try (parens pPattern)
   <|> try (fmap PProd (parens $ sepBy1 pPattern comma))
   <|> try (fmap (flip PConstr []) capitalIdent)
   <|>      fmap PVar  lowerIdent
   <?> "atomic pattern" )
