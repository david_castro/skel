module FrontEnd.Expr
  ( Expr (..)
  , pExpr
  , aExpr
  , pAlt
  , pprAPat
  ) where

import Control.Monad      ( liftM, liftM2 )
import Control.Monad.Fix  ( mfix )
import Data.Map.Strict    ( Map )
import Data.Sequence      ( Seq )
import Text.Parsec        ( Stream, ParsecT, manyTill
                          , try, many, many1, sepBy1
                          , (<|>), (<?>) )
import Text.Parsec.Indent ( withBlock )
import Text.PrettyPrint   ( hsep, text, (<+>), (<>)
                          , char, int, punctuate, vcat
                          , nest, ($$) )

import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Ppr
import qualified Data.Map.Strict      as Map

import Common.Parser
import Common.Ppr
import Common.Literal
import Common.Prim

import FrontEnd.Pattern

data Expr =
    ELit  Literal
  | EVar  Name

  {- Abstraction and pattern matching expressions not in Core -}
  | EAbs  [Pattern] Expr
  | ECase Expr      [(Pattern, Expr)]

  | EPrim Prim
  | EOp1  Op1 Expr
  | EOp   Op (Seq Expr)
  | EApp  Expr      Expr

--  | EComp (Seq Expr) -- flattened function composition
--  | EMap  (Seq Expr) -- should be disallowed in the FrontEnd?
--  | EHylo Expr Expr  -- should be disallowed in the FrontEnd?

instance Show Expr where
  show = showPpr

instance Ppr Expr where
  ppr (ELit l) = ppr l
  ppr (EVar n) = text n
  ppr (EApp e1 e2) = hsep $ map pprPar $ chase e1 [e2]
    where chase (EApp e1 e2) e3 = chase e1 (e2:e3)
          chase e1           e2 = e1:e2
          pprPar p@(ELit _) = ppr p
          pprPar p@(EVar _) = ppr p
          pprPar p          = Ppr.parens $ ppr p
  ppr (EAbs ps e) = char '\\' <> hsep (map pprAPat ps) <> Ppr.comma <+> ppr e
  ppr (ECase e ps) = text "case" <+> ppr e <+> text "of" $$ nest 2 (vcat $ map pprAlts ps)
    where pprAlts (p, e) = ppr p <+> arrow <+> ppr e

pExpr :: Parser Int Expr
pExpr = indent $
      try $ fmap (foldl1 EApp) (many1 aExpr)
  <|> try (reservedOp "\\"   *> liftM2    EAbs  (manyTill1 aPattern comma) pExpr)
  <|>      withBlock ECase (reserved   "case" *> pExpr <* reserved "of") pAlt

aExpr :: Parser Int Expr
aExpr = indent $
      try (parens pExpr)
  <|> try (fmap ELit pLiteral)
  <|>      fmap EVar identifier

pAlt :: Parser Int (Pattern, Expr)
pAlt = liftM2 (,) (pPattern <* reservedOp "=>") pExpr
