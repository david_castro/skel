{-# LANGUAGE DeriveFunctor      #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections      #-}
module Core.Ty
        ( PrimTy     (..)
        , FunctorDef (..)
        , numArgs
        , pFunctorDef

        , ForAll     (..)
        , structCnstr
        , pForAll
        , TyDef      (..)
        , tyCnstr
        , L          (..)
        , toList
        , aType
        , pType
        , pSignature
        ) where

import Common.Parser
import Common.Ppr
import Core.Struct

import Text.Parsec hiding ( State )
import Text.PrettyPrint
        ( (<>) )

import Control.Monad
        ( liftM
        , liftM2 )
import Data.List
        ( foldl' )

import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Pretty

-------------------------------------------------------------------------------
-- PRIMITIVE TYPES
-------------------------------------------------------------------------------
-- | Primitive Types
-- TODO: C types?
data PrimTy = BOOL | INT | STRING
  deriving (Eq, Ord)

-- | Primitive type parser
primTy :: Parser Int PrimTy
primTy = Indent.sameOrIndented >> capitalIdent >>= \i ->
  case i of
    "Bool"   -> return BOOL
    "Int"    -> return INT
    "String" -> return STRING
    _        -> fail $ "Not a primitive type: " ++ show i

instance Ppr PrimTy where
  ppr BOOL   = Pretty.text "Bool"
  ppr INT    = Pretty.text "Int"
  ppr STRING = Pretty.text "String"

instance Show PrimTy where
  show = showPpr
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- TYPES
-------------------------------------------------------------------------------
-- AST of functor definitions
data FunctorDef a =
    FD { fName :: Name, fArgs :: [Name], fBody :: TyDef a }
  | AT { fName :: Name }

numArgs :: FunctorDef a -> Int
numArgs f@FD{} = length (fArgs f)
numArgs _      = 0

pFunctorDef :: Parser Int [FunctorDef ()]
pFunctorDef = Indent.checkIndent >> (try pAT <|> pFD <?> "functor or type definition")

pFD :: Parser Int [FunctorDef ()]
pFD = do
  Indent.checkIndent
  f    <- capitalIdent
  args <- many capitalIdent
  reservedOp ":="
  ty   <- pType
  return [ FD { fName = f
              , fArgs = args
              , fBody = ty
              } ]

pAT :: Parser Int [FunctorDef ()]
pAT = do
  Indent.checkIndent
  reserved "parameter"
  liftM (map AT) $ many1 (Indent.sameOrIndented *> capitalIdent)

instance Ppr (FunctorDef a) where
  ppr f@FD{} = Pretty.hsep [ ppr (fName f)
                           , ppr (fArgs f)
                           , Pretty.text ":="
                           , ppr (fBody f)
                           ]
  ppr f@AT{} = Pretty.hsep [ Pretty.text "parameter", ppr (fName f) ]

instance Show (FunctorDef a) where
  show = showPpr


data ForAll a =
  ForAll [Name] (TyDef a)
  deriving (Eq, Ord)

structCnstr :: ForAll a -> [(Name, a)]
structCnstr (ForAll _ns t) = tyCnstr t

pSignature :: Parser Int (Name, ForAll ())
pSignature =  (do
  f  <- lowerIdent
  reservedOp ":"
  td <- pForAll
  return (f, td)
  ) <?> "type declaration"

pForAll :: Parser Int (ForAll ())
pForAll = Indent.sameOrIndented >>
  (   try (reserved "forall" *>
        liftM2 ForAll (many capitalIdent) (dot *> pType))
  <|> liftM (ForAll []) pType
  ) <?> "type scheme"

instance Ppr (ForAll a) where
  ppr (ForAll [] ty) = ppr ty
  ppr (ForAll vs ty) = Pretty.hsep [ Pretty.text "forall"
                                   , Pretty.hsep $ map ppr vs
                                   , Pretty.char '.'
                                   , ppr ty
                                   ]

instance Show (ForAll a) where
  show = showPpr



data L a =
    LNil
  | LMeta Int
  | LCons a (L a)
  deriving (Eq, Ord)

deriving instance Functor L

-- | AST of types
data TyDef a =
    TdUnit
  | TdMeta   Int
  | TdVar    Name
  | TdConst  PrimTy
  | TdSum    (L (TyDef a))
  | TdProd   (L (TyDef a))
  | TdApp    (TyDef a)  (TyDef a)
  | TdArr    [(Name, a)] (TyDef a) (TyDef a)
  | TdFix    (TyDef a)
  deriving (Eq, Ord)

tyCnstr :: TyDef a -> [(Name, a)]
tyCnstr (TdSum     l) = foldl' (flip $ (++) . tyCnstr) [] $ toList l
tyCnstr (TdProd    l) = foldl' (flip $ (++) . tyCnstr) [] $ toList l
tyCnstr (TdApp   a b) = tyCnstr a ++ tyCnstr b
tyCnstr (TdArr n a b) = n ++ tyCnstr a ++ tyCnstr b
tyCnstr (TdFix     a) = tyCnstr a
tyCnstr _             = []


toList :: L (TyDef a) -> [TyDef a]
toList LNil = []
toList (LCons a b) = a : toList b
toList (LMeta i)   = [TdMeta i]

arrowType :: Parser Int (TyDef ())
arrowType = Indent.sameOrIndented >> liftM tArrow (sepBy1 annType (reservedOp "->"))
  where tArrow [] = error "Panic! Bug in Core.Ty.arrowType!"
        tArrow [x] = x
        tArrow (x:xs) = TdArr [] x $ tArrow xs

pAnnArrow :: Parser Int (TyDef () -> TyDef () -> TyDef ())
pAnnArrow =  reservedOp "|-"
          *> liftM TdArr (sepBy1 (liftM (,()) lowerIdent) comma)
          <* reservedOp "->"

annType :: Parser Int (TyDef ())
annType = Indent.sameOrIndented >>
            chainr1 sumType pAnnArrow

sumType :: Parser Int (TyDef ())
sumType = Indent.sameOrIndented >>   liftM (tSum . toL) (try (sepBy1 prodType (reservedOp "+")))
  where tSum LNil           = error "Panic! Bug in Core.Ty.sumType!"
        tSum (LMeta _)      = error "Panic! Bug in Core.Ty.sumType!"
        tSum (LCons a LNil) = a
        tSum l@(LCons _ (LCons _ _)) = TdSum l
        tSum (LCons _ (LMeta _)) = error "Panic! Bug in Core.Ty.sumType!"

prodType :: Parser Int (TyDef ())
prodType = Indent.sameOrIndented >>  liftM (tProd . toL) (try (sepBy1 appType (reservedOp "*")))
  where tProd LNil           = error "Panic! Bug in Core.Ty.sumType!"
        tProd (LMeta _)      = error "Panic! Bug in Core.Ty.sumType!"
        tProd (LCons a LNil) = a
        tProd l@(LCons _ (LCons _ _)) = TdProd l
        tProd (LCons _ (LMeta _)) = error "Panic! Bug in Core.Ty.sumType!"

appType :: Parser Int (TyDef ())
appType = Indent.sameOrIndented >> liftM (tApp . reverse) (try (many1 aType))
  where tApp []  = error "Panic! Bug in Core.Ty.appType!"
        tApp [x] = x
        tApp (x:xs) = TdApp (tApp xs) x

aType :: Parser Int (TyDef ())
aType = Indent.sameOrIndented >>
      (     liftM TdFix (try (reserved "fix") *> aType)
        <|> liftM TdConst (try primTy)
        <|> liftM TdVar (try capitalIdent)
        <|> try (symbol "(" >> symbol ")" >> return TdUnit)
        <|> parens pType
      )

pType :: Parser Int (TyDef ())
pType = Indent.sameOrIndented >> arrowType

toL :: [TyDef a] -> L (TyDef a)
toL = foldr LCons LNil

pprArr :: [(Name, a)] -> Pretty.Doc
pprArr []       = Pretty.text "->"
pprArr [("",_)] = Pretty.text "->"
pprArr ns       = Pretty.hcat $
                    [ Pretty.text "|- " ]
                ++  Pretty.punctuate Pretty.comma (map (ppr . fst) ns)
                ++  [ Pretty.text "->" ]

instance Ppr (TyDef a) where
  ppr  TdUnit         = Pretty.parens Pretty.empty
  ppr (TdMeta  i)     = Pretty.hcat [Pretty.char '@', Pretty.int i]
  ppr (TdVar   n)     = Pretty.text n
  ppr (TdConst p)     = ppr p
  ppr (TdSum   ts)    = Pretty.hsep $ Pretty.punctuate op
                          $ map needParens $ toList ts
    where needParens t@TdArr{} = Pretty.parens $ ppr t
          needParens t@TdSum{} = Pretty.parens $ ppr t
          needParens t         = ppr t
          op = Pretty.space <> Pretty.char '+'
  ppr (TdProd  ts)    = Pretty.hsep $ Pretty.punctuate op
                          $ map needParens $ toList ts
    where needParens p@TdArr{}  = Pretty.parens $ ppr p
          needParens p@TdSum{}  = Pretty.parens $ ppr p
          needParens p@TdProd{} = Pretty.parens $ ppr p
          needParens p          = ppr p
          op = Pretty.space <> Pretty.char '*'
  ppr (TdApp   t1 t2) = Pretty.hsep $
                          map (\t -> needParens t (ppr t))
                              (getApp t1 [t2])
    where getApp (TdApp t3 t4) ts = getApp t3 (t4:ts)
          getApp t             ts = t:ts
          needParens TdArr{}  p = Pretty.parens p
          needParens TdSum{}  p = Pretty.parens p
          needParens TdProd{} p = Pretty.parens p
          needParens TdApp{}  p = Pretty.parens p
          needParens TdFix{}  p = Pretty.parens p
          needParens _            p = p
  ppr (TdArr    n t1 t2) = Pretty.hsep [parensL t1, pprArr n, ppr t2]
    where parensL t@TdArr{} = Pretty.parens $ ppr t
          parensL t         = ppr t
  ppr (TdFix   t)     = Pretty.hsep [Pretty.text "fix", needParens t $ ppr t]
    where needParens TdArr{}   p = Pretty.parens p
          needParens TdSum{}   p = Pretty.parens p
          needParens TdProd{}  p = Pretty.parens p
          needParens TdApp{}   p = Pretty.parens p
          needParens TdFix{}   p = Pretty.parens p
          needParens _            p = p

instance Show (TyDef a) where
  show = showPpr
-------------------------------------------------------------------------------
