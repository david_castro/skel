{-# LANGUAGE TupleSections #-}
module Core.Expr
         ( FunDef  (..)
         , ExprDef (..)
         , Literal (..)
         , Op1     (..)
         , pprOp1
         , Op      (..)
         , pprOp
         , Prim    (..)

         , noAnn

         , pFunDef

         , pExprDef
         , hExprDef
         , aExprDef

         , pLiteral
         , pPrim
         ) where

import Common.Parser ( Name, Parser )
import Common.Prim
import Common.Literal
import qualified Common.Parser as P
import Common.Ppr
import Core.Struct
import Core.Ty

import Control.Monad.Except
import Text.Parsec hiding ( State )


import Control.Monad
         ( join )
import Data.Map.Strict
         ( Map )
import Text.Parsec.Expr
         ( Operator (..)
         , Assoc    (..)
         , OperatorTable
         , buildExpressionParser )
import Text.PrettyPrint
         ( ($$)
         , (<+>) )

import qualified Data.Map.Strict      as Map
import qualified Control.Monad.State  as LState
import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Pretty

-- AST of function definitions
data FunDef =
    FnD { fdName :: Name
        , fdArgs :: [Name]
        , fdType :: ForAll SeqS
        , fdBody :: ExprDef SeqS
        }
  | AFn { fdName :: Name
        , fdType :: ForAll SeqS
        }

pFunDef :: Parser Int [FunDef]
pFunDef = Indent.checkIndent >>
          (try pAFn <|> liftM (:[]) pFnD <?> "definition")

pAFn :: Parser Int [FunDef]
pAFn = liftM (map (uncurry AFn))
             (Indent.checkIndent >>
                Indent.withBlock' (P.reserved "atomic") pSig)
  where
    pSig :: Parser Int (Name, ForAll SeqS)
    pSig = secondM clean =<< pSignature

    clean :: ForAll () -> Parser Int (ForAll SeqS)
    clean (ForAll ns t)    = liftM (ForAll ns) $ noAnn t

    secondM f (a, b)       = liftM (a,) (f b)

noAnn :: TyDef () -> Parser Int (TyDef SeqS)
noAnn TdUnit           = return   TdUnit
noAnn (TdMeta       i) = return $ TdMeta                         i
noAnn (TdVar        v) = return $ TdVar                          v
noAnn (TdConst      c) = return $ TdConst                        c
noAnn (TdSum        l) = liftM    TdSum                 (noAnnL l)
noAnn (TdProd       l) = liftM    TdProd                (noAnnL l)
noAnn (TdApp    t1 t2) = liftM2   TdApp      (noAnn t1) (noAnn t2)
noAnn (TdArr [] t1 t2) = liftM2   (TdArr []) (noAnn t1) (noAnn t2)
noAnn (TdArr ns t1 t2) = fail "Labels not allowed in type of atomic function"
noAnn (TdFix       t1) = liftM    TdFix                 (noAnn t1)

noAnnL LNil            = return   LNil
noAnnL (LMeta i)       = return $ LMeta i
noAnnL (LCons x xs)    = liftM2   LCons (noAnn x) (noAnnL xs)



pFnD :: Parser Int FunDef
pFnD = Indent.checkIndent >> pSignature >>= uncurry parseBody

parseBody :: Name -> ForAll () -> Parser Int FunDef
parseBody n ty = (do
  Indent.checkIndent
  _   <- P.symbol n
  as  <- many P.lowerIdent
  P.reservedOp "="
  e   <- pExprDef
  p   <- option [] pStructDecl
  ty' <- labelType ty p
  return FnD { fdName = n
             , fdArgs = as
             , fdType = ty'
             , fdBody = e
             }
  ) <?> "definition"

labelType :: ForAll () -> [(Name, SeqS)] -> Parser Int (ForAll SeqS)
labelType (ForAll ns t) lbs = liftM (ForAll ns) $ labelTy t lbs

labelTy :: TyDef () -> [(Name, SeqS)]
        -> Parser Int (TyDef SeqS)
labelTy TdUnit        ns = return  TdUnit
labelTy (TdMeta  i)   ns = return (TdMeta  i)
labelTy (TdVar   v)   ns = return (TdVar   v)
labelTy (TdConst c)   ns = return (TdConst c)
labelTy (TdSum   a)   ns = liftM TdSum  $ labelL a ns
labelTy (TdProd  a)   ns = liftM TdProd $ labelL a ns
labelTy (TdApp a b)   ns = liftM2 TdApp (labelTy a ns) (labelTy b ns)
labelTy (TdArr n a b) ns = liftM3 TdArr (go n) (labelTy a ns) (labelTy b ns)
  where go []          = return []
        go ((x, _):ys)
          | ss <- filterS x, not (null ss) = liftM (ss ++) $ go ys
          | otherwise                      = liftM2 (:) (mkMeta x) (go ys)

        filterS x = filter ((== x) . fst) ns
        mkMeta x = liftM ((x,) . SMeta) getState <* modifyState (+1)

labelTy (TdFix   a) ns = liftM TdFix $ labelTy a ns


labelL :: L (TyDef ()) -> [(Name, SeqS)]
       -> Parser Int (L (TyDef SeqS))
labelL LNil         ns = return   LNil
labelL (LMeta i)    ns = return $ LMeta i
labelL (LCons x xs) ns = liftM2 LCons (labelTy x ns) (labelL xs ns)

pStructDecl :: Parser Int [(Name, SeqS)]
pStructDecl = Indent.withBlock' (P.reserved "struct") pStructAnn

pStructAnn :: Parser Int (Name, SeqS)
pStructAnn = liftM2 (,) P.lowerIdent (P.reservedOp "~" *> pSeqS)

instance Ppr FunDef where
  ppr fd@FnD{} = (Pretty.text (fdName fd) <+> Pretty.colon    <+> ppr (fdType fd))
              $$ (Pretty.text (fdName fd) <+> ppr (fdArgs fd) <+>
                    Pretty.char '=' <+> ppr (fdBody fd))
              $$ rest
    where str = Pretty.nest 2 ( Pretty.vcat
                  [ Pretty.text "struct"
                  , Pretty.nest 2 (
                      Pretty.vcat (map (\(n,p) -> Pretty.text n
                                              <+> Pretty.char '~'
                                              <+> ppr p )
                                       cnstrs))
                  ] )
          rest
            | not (null cnstrs) = str
            | otherwise         = Pretty.empty
          cnstrs = structCnstr $ fdType fd

  ppr fd@AFn{} = Pretty.hsep [ Pretty.text "atomic"
                             , ppr (fdName fd)
                             , Pretty.colon
                             , ppr (fdType fd) ]

instance Show FunDef where
  show = showPpr

-- AST of structured expressions
-- Ord class used just for efficiently searching atoms
data ExprDef a =
    ELit   Literal
  | EVar   Name
  | EPrim  Prim
  | EOp1   Op1         (ExprDef a)
  | EOp    Op          [ExprDef a]
  | EApp   (ExprDef a) (ExprDef a)
  | EComp  (ExprDef a) (ExprDef a)
  | EMap   (TyDef a)   (ExprDef a)
  | EHylo  (TyDef a)   (ExprDef a) (ExprDef a)
  deriving (Eq, Ord)


pExprDef :: Parser Int (ExprDef SeqS)
pExprDef = Indent.sameOrIndented
              >> (buildExpressionParser exprTable hExprDef
                 <?> "structured expression")

exprTable :: OperatorTable Name Int (LState.State SourcePos) (ExprDef SeqS)
exprTable =
  [ [ Postfix ((Indent.sameOrIndented
             >> try (P.reservedOp "?") >> return (EOp1 Pred)) <?> sE) ]
  , [ Infix  ((Indent.sameOrIndented
                >> return EApp                         ) <?> sE) AssocLeft    ]
  , [ Prefix ((Indent.sameOrIndented
                >> liftM (EMap . TdVar) (try P.capitalIdent)     ) <?> sE)              ]
  , [ Infix  ((Indent.sameOrIndented
                >> try (P.reservedOp ".") >> return EComp) <?> sE) AssocRight ]
  ]
  where sE = "structured expression"

hExprDef :: Parser Int (ExprDef SeqS)
hExprDef = Indent.sameOrIndented >>
          (  (try (P.reserved "hylo") >>
                aType >>= noAnn >>= \f ->
                  liftM2 (EHylo f) aExprDef aExprDef)
         <|> aExprDef
         <?> "structured expression"
          )

aExprDef :: Parser Int (ExprDef SeqS)
aExprDef = Indent.sameOrIndented >>
         (  liftM ELit  (try pLiteral)
        <|> liftM EPrim (try pPrim)
        <|> liftM EVar  (try P.lowerIdent)
        <|> liftM (EOp FPlus)
              (try (P.symbol "[+|") *> P.commaSep1 pExprDef <* P.symbol "]")
        <|> liftM (EOp FProd)
              (try (P.symbol "<*|") *> P.commaSep1 pExprDef <* P.symbol ">")
        <|> P.parens pExprDef
        <?> "atomic expression"
         )

instance Ppr (ExprDef a) where
  ppr (ELit  l)         = ppr l
  ppr (EVar  n)         = Pretty.text n
  ppr (EPrim p)         = ppr p
  ppr (EOp1 p s1)       = pprOp1 p $ parens s1
    where parens EVar{} = ppr s1
          parens EPrim{} = ppr s1
          parens _      = Pretty.parens $ ppr s1
  ppr (EOp p s    )     = pprOp p $ map ppr s
  ppr (EApp  s1 s2)     = Pretty.hsep $ map parens $ chaseApp s1 [s2]
    where chaseApp (EApp e1 e2) es = chaseApp e1 (e2:es)
          chaseApp e            es = e:es
          parens e@EHylo{} = Pretty.parens $ ppr e
          parens e@EMap{}  = Pretty.parens $ ppr e
          parens e@EComp{} = Pretty.parens $ ppr e
          parens e@EApp{}  = Pretty.parens $ ppr e
          parens e         = ppr e
  ppr (EComp s1 s2)    = flatten [s1,s2]
    where
      flatten (EComp e1 e2 : es) = flatten (e1:e2:es)
      flatten [e1]               = ppr e1
      flatten (e1          : es) = ppr e1 <+> Pretty.char '.' <+> flatten es
      flatten []                 =
        error "Panic! Impossible, flatten [] in ppr Expr"
  ppr (EMap f e)       = ppr f <+> parens e
    where
      parens ELit{}  = ppr e
      parens EVar{}  = ppr e
      parens EPrim{} = ppr e
      parens _       = Pretty.parens $ ppr e
  ppr (EHylo td e1 e2) = Pretty.text "hylo"
                           <+> parensTy td
                           <+> parens e1
                           <+> parens e2
    where
      parensTy e@TdUnit    = ppr e
      parensTy e@TdVar{}   = ppr e
      parensTy e@TdConst{} = ppr e
      parensTy e           = Pretty.parens $ ppr e
      parens e@ELit{}  = ppr e
      parens e@EVar{}  = ppr e
      parens e@EPrim{} = ppr e
      parens e         = Pretty.parens $ ppr e

instance Show (ExprDef a) where
  show = showPpr
