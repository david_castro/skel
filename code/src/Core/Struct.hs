module Core.Struct
        ( SeqS (..)
        , sComp

        , pSeqS
        , labelRefs
        , metaStruct
        ) where

import Control.Monad    ( liftM
                        , liftM3 )
import Data.Char        ( toUpper )
import Data.List        ( (\\) )
import Text.Parsec      ( SourcePos
                        , (<?>)
                        , (<|>)
                        , try
                        , getState
                        , modifyState )

import Text.Parsec.Expr ( OperatorTable
                        , Operator (..)
                        , Assoc    (..)
                        , buildExpressionParser )

import Text.PrettyPrint ( (<+>)   )

import qualified Control.Monad.State as LState
import qualified Text.PrettyPrint    as Pretty
import qualified Text.Parsec.Indent  as Indent

import Common.Ppr
import Common.Prim

import Common.Parser      ( Name
                          , Parser )

import qualified Common.Parser as P

type MetaName = Either Int Name

data SeqS =
    -- Atomic structure
    -- Annotations can collapse any complex structure into "atomic"
    -- Right now, we just keep an index. However, we can plug here all
    -- sorts of information for static analysis
    Atomic Bool Int
    -- Metavariables (for partially specified structures, only in templates)
  | SMeta    Int
    -- It is sometimes good use the primitive operations in the structure
  | SPrim    Prim

  -- Arrow structures: constraints on structures + arrows from
  -- structure to structure
  | SLabel   Name
  | SCnstr   Name [(Name, SeqS)]
  | SFun     [(Name, SeqS)] SeqS

  | SOp1     Op1    SeqS
  | SOp      Op     [SeqS]
  | SComp    [SeqS]
  | SMap     MetaName  SeqS
  -- We just keep (base F)
  | SHylo    MetaName  SeqS   SeqS
  deriving (Eq, Ord)

sComp :: SeqS -> SeqS -> SeqS
sComp (SComp xs) (SComp ys) = SComp $ xs ++ ys
sComp (SComp xs) y          = SComp $ xs ++ [y]
sComp x          (SComp ys) = SComp $ x  :  ys
sComp x          y          = SComp [ x  ,  y ]

pSeqS :: Parser Int SeqS
pSeqS = Indent.sameOrIndented
              >> (buildExpressionParser exprTable hExprDef
                 <?> "structure")

exprTable :: OperatorTable Name Int (LState.State SourcePos) SeqS
exprTable =
  [ [ Postfix ((Indent.sameOrIndented
             >> try (P.reservedOp "?") >> return (SOp1 Pred)) <?> sE) ]
  , [ Prefix ((Indent.sameOrIndented
                >> liftM SMap (try pMN)) <?> sE)              ]
  , [ Infix  ((Indent.sameOrIndented
                >> try (P.reservedOp ".") >> return sComp) <?> sE) AssocRight ]
  ]
  where sE = "structure expression"

hExprDef :: Parser Int SeqS
hExprDef = Indent.sameOrIndented >>
          (  try (P.reserved "HYLO" >>
                  liftM3 SHylo pMN aExprDef aExprDef)
         <|> aExprDef
         <?> "hylo structure"
          )

pMN :: Parser Int MetaName
pMN = Indent.sameOrIndented >>
      liftM Right (try P.capitalIdent)
  -- <|> liftM Left  (try $ P.reservedOp "_" *> metaN)

 -- where metaN = getState <* modifyState (+1)

aExprDef :: Parser Int SeqS
aExprDef = Indent.sameOrIndented >>
         (  try (P.reserved    "_A" *> atom)
        <|> try (P.reservedOp "_"   *> meta)
        <|> liftM SPrim  (try pPrimC)
        <|> liftM SLabel (try P.lowerIdent)
        <|> liftM (SOp FPlus)
              (try (P.symbol "[+|") *> P.commaSep1 pSeqS <* P.symbol "]")
        <|> liftM (SOp FProd)
              (try (P.symbol "<*|") *> P.commaSep1 pSeqS <* P.symbol ">")
        <|> P.parens pSeqS
        <?> "atomic structure"
         )

meta :: Parser Int SeqS
meta = liftM SMeta getState <* modifyState (+1)

atom :: Parser Int SeqS
atom = liftM (Atomic False) getState <* modifyState (+1)

pPrimC :: Parser st Prim
pPrimC = Indent.sameOrIndented >>
      (  liftM (const In  ) (try (P.reserved "IN" ))
     <|> liftM (const Out ) (try (P.reserved "OUT"))
     <|> liftM (const Id  ) (try (P.reserved "ID" ))
     <|> liftM Pi           (try  P.projS          )
     <|> liftM Inj                P.injS
     <?> "primitive structure"
      )

instance Ppr SeqS where
  ppr (Atomic  b i   ) -- = Pretty.text "_A"
    | not b     = Pretty.text $ "_A" ++ show i
    | otherwise = Pretty.text "_A"
  ppr (SMeta    _    ) = Pretty.text "_" --Pretty.hcat [Pretty.text "@", Pretty.int a]
  ppr (SPrim    a    ) = Pretty.text (map toUpper $ showPpr a)
  ppr (SLabel   a    ) = Pretty.text a
  ppr (SCnstr n a    ) = Pretty.hsep
                        [ Pretty.text "{"
                        , printFun n
                        , Pretty.hsep $
                            Pretty.punctuate
                              Pretty.comma
                              (map pprLbl a)
                        , Pretty.text "}" ]
    where printFun "" = Pretty.empty
          printFun n  = Pretty.hsep [Pretty.text n, Pretty.char '|']

  ppr (SFun     a b  ) = Pretty.hsep
                          [ ppr (SCnstr "" a), Pretty.text "~>", ppr b ]
  ppr (SOp1     a b  ) = pprOp1 a $ ppr b
  ppr (SOp      a b  ) = pprOp a $ map ppr b
  ppr (SComp    a    ) = flatten a
    where
      flatten (SComp e1 : es) = Pretty.parens (ppr $ SComp e1) <+> Pretty.char '.' <+> ppr (SComp es)
      flatten [e1]            = ppr e1
      flatten (e1       : es) = ppr e1 <+> Pretty.char '.' <+> flatten es
      flatten []              =
        error "Panic! Impossible, flatten [] in ppr Expr"
  ppr (SMap f e)       = pprMN f <+> parens e
    where
      parens Atomic{} = ppr e
      parens SMeta{}  = ppr e
      parens SPrim{}  = ppr e
      parens SLabel{} = ppr e
      parens _        = Pretty.parens $ ppr e
  ppr (SHylo td e1 e2) = Pretty.text "HYLO"
                           <+> pprMN td
                           <+> parens e1
                           <+> parens e2
    where
      parens e@Atomic{} = ppr e
      parens e@SMeta{}  = ppr e
      parens e@SPrim{}  = ppr e
      parens e@SLabel{} = ppr e
      parens e          = Pretty.parens $ ppr e

pprLbl :: (Name, SeqS) -> Pretty.Doc
pprLbl (n, s) = Pretty.hsep [ Pretty.text n
                            , Pretty.text "~"
                            , ppr s ]

pprMN :: MetaName -> Pretty.Doc
pprMN (Right n) = Pretty.text n
pprMN (Left  i) = Pretty.hcat [Pretty.char '_', Pretty.int i]

instance Show SeqS where
  show = showPpr

labelRefs :: SeqS -> [Name]
labelRefs (SLabel   n)     = [n]
labelRefs (SCnstr _ a)     = concatMap (labelRefs . snd) a
labelRefs (SFun     a b)   = labelRefs b \\ map fst a
labelRefs (SOp1     a b)   = labelRefs b
labelRefs (SOp      a b)   = concatMap labelRefs b
labelRefs (SComp    a)     = concatMap labelRefs a
labelRefs (SMap     a b)   = labelRefs b
labelRefs (SHylo    a b c) = labelRefs b ++ labelRefs c
labelRefs _                = []

metaStruct :: SeqS -> [Int]
metaStruct (Atomic False i) = [i]
metaStruct (SMeta    n)     = [n]
metaStruct (SCnstr _ a)     = concatMap (metaStruct . snd) a
metaStruct (SFun     a b)   = metaStruct b
metaStruct (SOp1     a b)   = metaStruct b
metaStruct (SOp      a b)   = concatMap metaStruct b
metaStruct (SComp    a)     = concatMap metaStruct a
metaStruct (SMap     a b)   = metaStruct b
metaStruct (SHylo    a b c) = metaStruct b ++ metaStruct c
metaStruct _                = []
