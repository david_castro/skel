module Core.Func
        ( Func
        , emptyFunc
        , addAtom
        ) where

import Data.Map.Strict  ( Map     )

import qualified Data.Map.Strict  as Map


import Core.Expr

data Func a = Func { nAtoms     :: !Int
                   , atomicDefs :: !(Map Int (ExprDef a))
                   , reverseF   :: !(Map (ExprDef a) Int)
                   }
            deriving Show

emptyFunc :: Func a
emptyFunc = Func { nAtoms     = 0
                 , atomicDefs = Map.empty
                 , reverseF   = Map.empty
                 }

addAtom :: Ord a => ExprDef a -> Func a -> (Int, Func a)
addAtom e f
  | Just j <- Map.lookup e (reverseF f) = (j, f )
  | otherwise                           = (i, f')
  where i  = nAtoms f
        f' = f { nAtoms     = i + 1
               , atomicDefs = Map.insert i e (atomicDefs f)
               , reverseF   = Map.insert e i (reverseF f)
               }
