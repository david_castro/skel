module TcCore
         ( typeCheck
         , tcCoreDef
         , tcCore

         , module Exports
         ) where

import Core
import TcCore.TcExpr_2 as Exports
import TcCore.TcM      as Exports
import TcCore.TcTy     as Exports

import Control.Monad ( liftM )

typeCheck :: Int -> Core -> IO Core
typeCheck i = runTcM i . tcCore

tcCore :: Core -> TcM Core
tcCore (Core f defs) = pushContext f *> liftM (Core f) (mapM go defs) <* popContext
  where go d = tcCoreDef d >>= insert

tcCoreDef :: CoreDef -> TcM CoreDef
tcCoreDef (TD fd) = liftM TD $ tcFunctorDef fd
tcCoreDef (FN fd) = liftM FN $ tcFunDef     fd

insert :: CoreDef -> TcM CoreDef
insert d@(TD f) = insertT f *> return d
insert d@(FN f) = insertF f *> return d
