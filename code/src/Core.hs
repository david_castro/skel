{-# LANGUAGE ViewPatterns #-}
module Core
         ( Name
         , Core    (..)
         , CoreDef (..)

         , TyDef
         , ExprDef
         , Forall
         , X.ForAll (..)
         , FunctorDef

         , pCore
         , pCoreDef
         , parseFile
         , parseExpr
         , parseTy
         , parseSign

         , module X
         ) where

import Common.Parser
        ( Name
        , Parser
        , whiteSpace )

import Common.Ppr
import Core.Expr hiding ( ExprDef )
import qualified Core.Expr   as X
import Core.Struct as X
import Core.Func   as X
import Core.Ty     hiding ( TyDef, ForAll, FunctorDef )
import qualified Core.Ty     as X


import Control.Monad.Except
import Text.Parsec
  ( (<|>)
  , (<?>)
  , try )

import qualified Text.Parsec          as Parsec
import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Pretty

type TyDef      = X.TyDef      SeqS
type ExprDef    = X.ExprDef    SeqS
type Forall     = X.ForAll     SeqS
type FunctorDef = X.FunctorDef SeqS

data Core = Core Name [CoreDef]

parseFile :: FilePath -> IO (Core, Int)
parseFile fp = liftM (either (error . show) id .
                        Indent.runIndent fp .
                          Parsec.runParserT pr 0 fp)
                     (readFile fp)
  where pr = liftM2 (,) (pCore fp) Parsec.getState

parseExpr :: String -> Core.ExprDef
parseExpr = either (error . show) id .
                  Indent.runIndent "(none)" .
                      Parsec.runParserT pExprDef 0 "(none)"

parseTy :: String -> X.ForAll ()
parseTy = either (error . show) id .
                  Indent.runIndent "(none)" .
                      Parsec.runParserT pForAll 0 "(none)"

parseSign :: String -> (Name, X.ForAll ())
parseSign = either (error . show) id .
                  Indent.runIndent "(none)" .
                      Parsec.runParserT pSignature 0 "(none)"

pCore :: FilePath -> Parser Int Core
pCore f = whiteSpace *> liftM (Core (clean f) . concat) (Indent.block pCoreDef) <* Parsec.eof
  where clean (break (== '.') -> (prev, _)) = map replace prev
        replace '/' = '.'
        replace c   = c

instance Ppr Core where
  ppr (Core _ ds) = Pretty.vcat $ init $ concatMap (\d -> [ppr d, Pretty.text ""]) ds

instance Show Core where
  show = showPpr

data CoreDef = TD Core.FunctorDef | FN FunDef

pCoreDef :: Parser Int [CoreDef]
pCoreDef = Indent.checkIndent >>
             (  liftM (map TD) (try (mapM noAnnF =<< pFunctorDef))
            <|> liftM (map FN) pFunDef
            <?> "definition"
             )
  where noAnnF f@FD{} = noAnn (fBody f) >>= \b -> return f { fBody = b}
        noAnnF (AT n) = return $ AT n

instance Ppr CoreDef where
  ppr (TD fd) = ppr fd
  ppr (FN fd) = ppr fd

instance Show CoreDef where
  show = showPpr
