{-# LANGUAGE FlexibleInstances #-}
module Common.Ppr
        ( Ppr(..)
        , arrow
        ) where

import qualified Text.PrettyPrint as Pretty

class Ppr a where
  ppr     :: a -> Pretty.Doc
  showPpr :: a -> String
  showPpr = Pretty.renderStyle skelStyle . ppr
    where skelStyle = Pretty.Style
                        { Pretty.mode           = Pretty.PageMode
                        , Pretty.lineLength     = 80
                        , Pretty.ribbonsPerLine = 0
                        }

instance Ppr String where
  ppr = Pretty.text

instance Ppr [String] where
  ppr = Pretty.hsep . map ppr

instance Ppr Pretty.Doc where
  ppr = id

arrow :: Pretty.Doc
arrow = Pretty.text "->"
