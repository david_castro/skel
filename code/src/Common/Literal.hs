module Common.Literal
         ( Literal (..)
         , pLiteral
         ) where

import Common.Parser ( Name, Parser )
import qualified Common.Parser as P
import Common.Ppr

import Text.Parsec hiding ( State )

import Control.Monad
         ( join
         , liftM )
import Data.Map.Strict
         ( Map )

import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Pretty


data Literal =
    ULit
  | ILit Int
  | BLit Bool
  | SLit String
  deriving (Eq, Ord)

pLiteral :: Parser Int Literal
pLiteral = Indent.sameOrIndented >>
         (  try (P.symbol "(" >> P.symbol ")" >> return ULit)
        <|> liftM (ILit . fromInteger) (try P.integer)
        <|> (try (P.reserved "True")  >> return (BLit True))
        <|> (try (P.reserved "False") >> return (BLit False))
        <|> liftM SLit P.stringLiteral
        <?> "literal"
         )

instance Ppr Literal where
  ppr ULit         = Pretty.parens Pretty.empty
  ppr (ILit i)     = Pretty.int i
  ppr (BLit True)  = Pretty.text "True"
  ppr (BLit False) = Pretty.text "False"
  ppr (SLit s)     = Pretty.doubleQuotes (Pretty.text s)

instance Show Literal where
  show = showPpr
