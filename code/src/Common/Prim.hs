module Common.Prim
         ( Op1     (..)
         , pprOp1
         , Op      (..)
         , pprOp
         , Prim    (..)

         , pPrim
         ) where

import Common.Parser ( Parser )
import qualified Common.Parser as P
import Common.Ppr

import Control.Monad.Except
import Text.Parsec hiding ( State )


import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Pretty

data Op1 = Pred
  deriving (Eq, Ord)

pprOp1 :: Op1 -> Pretty.Doc -> Pretty.Doc
pprOp1 Pred p = Pretty.hcat [p, Pretty.char '?']

data Op = FPlus | FProd
  deriving (Eq, Ord)

pprOp :: Op -> [Pretty.Doc] -> Pretty.Doc
pprOp FPlus ps =
  Pretty.hsep [ Pretty.text "[+|"
              , Pretty.hsep (Pretty.punctuate (Pretty.char ',') ps)
              , Pretty.char ']'
              ]
pprOp FProd ps =
  Pretty.hsep [ Pretty.text "<*|"
              , Pretty.hsep (Pretty.punctuate (Pretty.char ',') ps)
              , Pretty.char '>'
              ]

data Prim =
    In
  | Out
  | Id

  | Pi  Integer -- pi_ i
  | Inj Integer -- inj i
  deriving (Eq, Ord)

pPrim :: Parser st Prim
pPrim = Indent.sameOrIndented >>
      (  liftM (const In  ) (try (P.reserved "in" ))
     <|> liftM (const Out ) (try (P.reserved "out"))
     <|> liftM (const Id  ) (try (P.reserved "id" ))
     <|> liftM Pi           (try  P.proj           )
     <|> liftM Inj                P.inj
     <?> "primitive"
      )

instance Ppr Prim where
  ppr In      = Pretty.text "in"
  ppr Out     = Pretty.text "out"
  ppr Id      = Pretty.text "id"
  ppr (Pi i)  = Pretty.hcat [Pretty.text "proj_", Pretty.integer i]
  ppr (Inj i) = Pretty.hcat [Pretty.text  "inj_", Pretty.integer i]

instance Show Prim where
  show = showPpr
