{-# LANGUAGE FlexibleInstances #-}
module Common.Parser
        ( Name
        , Parser

        , identifier
        , reserved
        , operator
        , reservedOp
        , charLiteral
        , stringLiteral
        , natural
        , integer
        , float
        , naturalOrFloat
        , decimal
        , hexadecimal
        , octal
        , symbol
        , lexeme
        , whiteSpace
        , parens
        , braces
        , angles
        , brackets
        , squares
        , semi
        , comma
        , colon
        , dot
        , semiSep
        , semiSep1
        , commaSep
        , commaSep1

        , capitalIdent
        , lowerIdent
        , positive
        , proj
        , inj
        , projS
        , injS

        , Indent.withPos
        , Indent.sameOrIndented
        , indent
        , manyTill1
        ) where

import Text.Parsec hiding ( State )

import Control.Monad         ( liftM2                )
import Data.Char             ( isUpper
                             , isLower               )
import Text.Parsec.Token     ( GenLanguageDef
                             , GenTokenParser        )

import qualified Control.Monad.State  as LState
import qualified Text.Parsec.Indent   as Indent
import qualified Text.Parsec.Language as Language
import qualified Text.Parsec.Token    as Token

type Name = String

type LangDef st   = GenLanguageDef Name st (LState.State SourcePos)
type Lexer   st   = GenTokenParser Name st (LState.State SourcePos)
type Parser  st a = Indent.IndentParser Name st a

-- | Language def for the DSL
-- C-like comments
parDef :: LangDef st
parDef = Language.emptyDef
           { Token.commentStart   = "/*"
           , Token.commentEnd     = "*/"
           , Token.commentLine    = "//"
           , Token.nestedComments = True
           , Token.identStart     = letter   <|> char '_'
           , Token.identLetter    = alphaNum <|> oneOf "_'"
           , Token.opStart        = Token.opLetter parDef
           , Token.opLetter       = oneOf ":!#$%&*+./<=>?@\\^|-~"
           , Token.reservedOpNames= ["+", "*", "~", "|-", "->", "=", ":=", "_"]
           , Token.reservedNames  = [ "fix", "in", "out", "id", "parameter"
                                    , "atomic", "struct", "HYLO", "_A", "IN", "OUT", "ID"
                                    , "forall" ]
           , Token.caseSensitive  = True
           }

-- | The lexer
lexer :: Lexer st
lexer = Token.makeTokenParser parDef

identifier     ::                Parser st Name
reserved       :: String ->      Parser st ()
operator       ::                Parser st Name
reservedOp     :: String ->      Parser st ()
charLiteral    ::                Parser st Char
stringLiteral  ::                Parser st Name
natural        ::                Parser st Integer
integer        ::                Parser st Integer
float          ::                Parser st Double
naturalOrFloat ::                Parser st (Either Integer Double)
decimal        ::                Parser st Integer
hexadecimal    ::                Parser st Integer
octal          ::                Parser st Integer
symbol         :: String ->      Parser st Name
lexeme         :: Parser st a -> Parser st a
whiteSpace     ::                Parser st ()
parens         :: Parser st a -> Parser st a
braces         :: Parser st a -> Parser st a
angles         :: Parser st a -> Parser st a
brackets       :: Parser st a -> Parser st a
squares        :: Parser st a -> Parser st a
semi           ::                Parser st Name
comma          ::                Parser st Name
colon          ::                Parser st Name
dot            ::                Parser st Name
semiSep        :: Parser st a -> Parser st [a]
semiSep1       :: Parser st a -> Parser st [a]
commaSep       :: Parser st a -> Parser st [a]
commaSep1      :: Parser st a -> Parser st [a]

identifier     = notFollowedBy (proj <|> inj) >> Token.identifier     lexer
reserved       = Token.reserved       lexer
operator       = Token.operator       lexer
reservedOp     = Token.reservedOp     lexer
charLiteral    = Token.charLiteral    lexer
stringLiteral  = Token.stringLiteral  lexer
natural        = Token.natural        lexer
integer        = Token.integer        lexer
float          = Token.float          lexer
naturalOrFloat = Token.naturalOrFloat lexer
decimal        = Token.decimal        lexer
hexadecimal    = Token.hexadecimal    lexer
octal          = Token.octal          lexer
symbol         = Token.symbol         lexer
lexeme         = Token.lexeme         lexer
whiteSpace     = Token.whiteSpace     lexer
parens         = Token.parens         lexer
braces         = Token.braces         lexer
angles         = Token.angles         lexer
brackets       = Token.brackets       lexer
squares        = Token.squares        lexer
semi           = Token.semi           lexer
comma          = Token.comma          lexer
colon          = Token.colon          lexer
dot            = Token.dot            lexer
semiSep        = Token.semiSep        lexer
semiSep1       = Token.semiSep1       lexer
commaSep       = Token.commaSep       lexer
commaSep1      = Token.commaSep1      lexer

capitalIdent :: Parser st Name
capitalIdent = do
  i <- identifier
  case i of
    (c:_) | isUpper c -> return i
    _                 -> fail "expected uppercase letter"

lowerIdent :: Parser st Name
lowerIdent = do
  i <- identifier
  case i of
    (c:_) | isLower c -> return i
    _                 -> fail "expected lowercase letter"

positive :: Parser st Integer
positive = do
  i <- natural
  if i <= 0 then fail "expected positive number > 0"
            else return i

proj :: Parser st Integer
proj = lexeme $ try $ string "proj_" *> positive

inj :: Parser st Integer
inj = lexeme $ try $ string "inj_" *> positive

projS :: Parser st Integer
projS = lexeme $ try $ string "PROJ_" *> positive

injS :: Parser st Integer
injS = lexeme $ try $ string "INJ_" *> positive


indent :: Parser st a -> Parser st a
indent = (Indent.sameOrIndented >>)

manyTill1 ::  Stream s m t => ParsecT s u m a -> ParsecT s u m end -> ParsecT s u m [a]
manyTill1 p = liftM2 (:) p . manyTill p

