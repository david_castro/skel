{-# LANGUAGE TupleSections #-}
module Util.Util
        ( mapSndM
        , mapFstM
        , mappend3
        , maybeM
        , splitL
        ) where

import Control.Monad
         ( liftM
         , join
         )

mapSndM :: Monad m => (b -> m c) -> (a, b) -> m (a, c)
mapSndM f (a, b) = liftM (a,) (f b)

mapFstM :: Monad m => (a -> m c) -> (a, b) -> m (c, b)
mapFstM f (a, b) = liftM (,b) (f a)

mappend3 :: Monoid m => m -> (a, b, m) -> (a, b, m)
mappend3 m (a, b, m') = (a, b, m `mappend` m')

maybeM :: Monad m => m b -> (a -> m b) -> m (Maybe a) -> m b
maybeM mb f = join . liftM (maybe mb f)

splitL :: [a] -> ([a], a)
splitL [] = error "Bug when calling Util.Util.splitL: empty list"
splitL [x] = ([], x)
splitL (x:xs) = (x:ys, y)
  where (ys, y) = splitL xs
