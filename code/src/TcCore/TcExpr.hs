{-# LANGUAGE TupleSections #-}
module TcCore.TcExpr
          ( tcFunDef
          , tcExprDef
          ) where

import Core
import TcCore.TcM
import TcCore.TcStruct
import TcCore.TcTy

import Control.Arrow
        ( first )
import Control.Monad.Extra
        ( liftM
        , liftM2
        , void
        , when
        , whenM )
import Data.List
        ( foldl' )
import Data.Map.Strict
        ( Map )
import Data.Maybe
        ( isNothing )
import qualified Data.Map.Strict as Map

type LEnv  = Map Name TyDef

tcFunDef :: FunDef -> TcM ()
tcFunDef fd@FnD{} = do
  pushContext (fdName fd)
  whenM (existsF $ fdName fd) $ duplicateDefn $ fdName fd
  (m, ty')     <- tcForAll ty
  (env, ty'')  <- tcTyArgs (fdArgs fd) ty'
  (s  , subst) <- tcExprDef (env `Map.union` m) Map.empty (fdBody fd) ty''
  warn $ "TODO: Apply substitution: \n" ++ show subst ++ "\n\nto\n\n" ++ show s ++ "\n" ++ show m
  tcStructAnn fd s  ty'' env
  popContext
  where
    ty = fdType fd
    tcTyArgs []     t = return (Map.empty, t)
    tcTyArgs (a:as) (TdArr [] t1 t2) = do
      (e, t') <- tcTyArgs as t2
      return (Map.insert a t1 e, t')
    tcTyArgs _      t             = do
      t' <- liftM2 (TdArr []) freshTy freshTy
      unexpected t t'

tcFunDef fd@AFn{} = do
  pushContext (fdName fd)
  whenM (existsF $ fdName fd) $ duplicateDefn $ fdName fd
  void $ tcForAll_ (fdType fd)
  s <- atom (EVar $ fdName fd)
  structOf (fdName fd) s
  popContext

tcExprDef :: LEnv -> Subst -> ExprDef -> TyDef -> TcM (SeqS, Subst)
tcExprDef _   s e@(ELit  l       ) ty = tcLiteral     s e l            ty
tcExprDef env s e@(EVar  v       ) ty = tcVar     env s e v            ty
tcExprDef env s e@(EOp1  p  e1   ) ty = tcOp1     env s e (p, e1)      ty
tcExprDef env s e@(EOp   p  e1   ) ty = tcOp      env s e (p, e1)      ty
tcExprDef _   s e@(EPrim p       ) ty = tcPrim        s e p            ty
tcExprDef env s e@(EApp  e1 e2   ) ty = tcApp     env s e (e1, e2    ) ty
tcExprDef env s e@(EComp e1 e2   ) ty = tcComp    env s e (e1, e2    ) ty
tcExprDef env s e@(EMap  n  e2   ) ty = tcMap     env s e (n , e2    ) ty
tcExprDef env s e@(EHylo f  e1 e2) ty = tcHylo    env s e (f , e1, e2) ty

{-# INLINE tcLiteral #-}
tcLiteral :: Subst -> ExprDef -> Literal -> TyDef -> TcM (SeqS, Subst)
tcLiteral s e l ty = liftM2 (,) (atom e) (tcAux l)
  where
    tcAux ULit     = liftM snd $ unify ty (TdUnit        , s)
    tcAux (BLit _) = liftM snd $ unify ty (TdConst BOOL  , s)
    tcAux (ILit _) = liftM snd $ unify ty (TdConst INT   , s)
    tcAux (SLit _) = liftM snd $ unify ty (TdConst STRING, s)

{-# INLINE tcVar #-}
tcVar :: LEnv -> Subst -> ExprDef -> Name -> TyDef -> TcM (SeqS, Subst)
tcVar le s e n t = liftM2 (,) template tcAux
  where
    tcAux
      | Just t' <- Map.lookup n le
         = liftM snd $ unify t (t', s)
      | otherwise
         = liftM snd . unify t =<<
              liftM ((,s) . snd) (tcForAll . fdType =<< lookupF n)
    template = liftM mkT $ lookupS n
    mkT a@(Atomic _ _) = a
    mkT a              = Template n a

{-# INLINE tcPrim #-}
tcPrim :: Subst -> ExprDef -> Prim -> TyDef -> TcM (SeqS, Subst)
tcPrim s _ In  ty = liftM mkT . unify ty =<< liftM  ((,s) . inTy)  freshTy
  where inTy  v = TdArr [] (TdApp v (TdFix v)) (TdFix v)
        mkT = (,) (SPrim In) . snd
tcPrim s _ Out ty = liftM mkT . unify ty =<< liftM  ((,s) . outTy) freshTy
  where outTy v = TdArr [] (TdFix v) (TdApp v (TdFix v))
        mkT = (,) (SPrim Out) . snd
tcPrim s _ Id  ty = liftM mkT . unify ty =<< liftM  ((,s) . idTy)  freshTy
  where idTy v  = TdArr [] v v
        mkT = (,) (SPrim Id) . snd

tcPrim s _ (Pi i)   ty = liftM mkT . unify ty =<< liftM tyProj (freshL $ fromInteger i)
  where tyProj vs = (TdArr [] (TdProd vs) (vs `at` (fromInteger i-1)), s)
        mkT = (,) (SPrim $ Pi i) . snd
tcPrim s _ (Inj i)  ty = liftM mkT . unify ty =<< liftM tyInj (freshL $ fromInteger i)
  where tyInj vs = (TdArr [] (vs `at` (fromInteger i-1)) (TdSum  vs), s)
        mkT = (,) (SPrim $ Inj i) . snd

at :: L a -> Int -> a
at (LCons a _) 0         = a
at (LCons _ a) n | n > 0 = at a (n - 1)
at _ _ = error "TcCore.TcExpr.at! (TODO: move function to Core.Ty)"

{-# INLINE tcOp1 #-}
tcOp1 :: LEnv -> Subst -> ExprDef -> (Op1, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcOp1 env s _ (Pred, e1) ty = do
  t@(TdArr _ v _) <- liftM predTy freshTy
  (_, s1)       <- unify ty (t, s)
  liftM (first (SOp1 Pred)) $
    tcExprDef env s1 e1
      (TdArr [] v (TdSum (LCons TdUnit $ LCons TdUnit LNil)))
  where predTy v = TdArr [] v (TdSum (LCons v $ LCons v LNil))

{-# INLINE tcOp #-}
tcOp :: LEnv -> Subst -> ExprDef -> (Op, [ExprDef]) -> TyDef -> TcM (SeqS, Subst)
tcOp env s _ (FProd, e1) ty = do
  t1@(TdArr _ v (TdProd vs)) <- liftM2 auxT freshTy (freshTuple i)
  (_, s1)                    <- unify ty (t1, s)
  liftM (first (SOp FProd)) $ tcExprs s1 v $ zipL e1 vs
  where i = length e1
        freshTuple n
          | n <= 0    = return LNil
          | otherwise = liftM2 LCons freshTy (freshTuple $ n - 1)
        tcExprs s1 _ [] = return ([], s1)
        tcExprs s1 v ((e,v'):es) = do
          (sig , s2) <- tcExprDef env s1 e (TdArr [] v v')
          (sigs, s3) <- tcExprs s2 v es
          return (sig:sigs, s3)
        auxT v vs = TdArr [] v (TdProd vs)
tcOp env s _ (FPlus , e1) ty = do
  t1@(TdArr _ (TdSum  vs) v) <- liftM2 auxT freshTy (freshTuple i)
  (_, s1)                    <- unify ty (t1, s)
  liftM (first (SOp FProd)) $ tcExprs s1 v $ zipL e1 vs
  where i = length e1
        freshTuple n
          | n <= 0    = return LNil
          | otherwise = liftM2 LCons freshTy (freshTuple $ n - 1)
        tcExprs s1 _ [] = return ([], s1)
        tcExprs s1 v ((e,v'):es) = do
          (sig , s2) <- tcExprDef env s1 e (TdArr [] v' v)
          (sigs, s3) <- tcExprs s2 v es
          return (sig:sigs, s3)
        auxT v vs = TdArr [] (TdSum vs) v

zipL :: [ExprDef] -> L TyDef -> [(ExprDef, TyDef)]
zipL [] LNil = []
zipL (x:xs) (LCons y ys) = (x, y) : zipL xs ys
zipL _      _            = error "Panic! Bug in TcCore.TcExpr.zipL"

{-# INLINE tcApp #-}
tcApp :: LEnv -> Subst -> ExprDef -> (ExprDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcApp env s _ (e1, e2) td = do
  t1'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
  (_ , s1)            <- unify td (t12, s)
  (sig1, s2)          <- tcExprDef env s1 e1 t1'
  (_   , s3)          <- tcExprDef env s2 e2 t11
  --checkSubst s3 t1'
  return (sig1, s3)

{-# INLINE tcComp #-}
tcComp :: LEnv -> Subst -> ExprDef -> (ExprDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcComp env s _ (e1, e2) ty = do
  t1'@(TdArr _ t21 t22) <- liftM2 (TdArr []) freshTy freshTy
  t2'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
  (_, s1)               <- unify (TdArr [] t11 t22) (ty, s)
  (t, s2)               <- unify t12 (t21, s1)
  (sig1, s3)            <- tcExprDef env s2 e1 t1'
  (sig2, s4)            <- tcExprDef env s3 e2 t2'
  --checkSubst s4 t11
  --checkSubst s4 t22
  return (sComp sig1 sig2, s4)

{-# INLINE tcMap #-}
tcMap :: LEnv -> Subst -> ExprDef -> (Name, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcMap env s _ (n, e) ty
  | Just meta <- Map.lookup n env = do
      t1'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
      t2                    <- freshTy
      let (TdArr _ x1 y1)     = ty
      (_, s1)               <- unify (mapTyM meta t11 t12 t2) (ty, s)
      liftM (first $ SMap $ mn n) $ tcExprDef env s1 e t1'
  | otherwise = do
      tcBiFunctor env $ TdVar n
      t1'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
      t2                    <- freshTy
      (_, s1)               <- unify (mapTy t11 t12 t2) (ty, s)
      liftM (first $ SMap $ mn n) $ tcExprDef env s1 e t1'
  where mapTy a b c = TdArr [] (TdApp (TdApp (TdVar n) a) c)
                               (TdApp (TdApp (TdVar n) b) c)
        mapTyM meta a b c = TdArr [] (TdApp (TdApp meta a) c)
                                     (TdApp (TdApp meta b) c)
        mn n
          | Just (TdMeta i) <- Map.lookup n env = Left  i
          | otherwise                           = Right n

{-# INLINE tcHylo #-}
tcHylo :: LEnv -> Subst -> ExprDef -> (TyDef, ExprDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcHylo env s _ (f , e1, e2) ty = do
  tcFunctor env f
  t1'@(TdArr _ _ t2) <- liftM cataTy freshTy
  t2'@(TdArr _ t1 _) <- liftM anaTy  freshTy
  (_, s1)            <- unify (TdArr [] t1 t2) (ty, s)
  n                  <- base env f
  (sig1, s2)         <- tcExprDef env s1 e1 t1'
  (sig2, s3)         <- tcExprDef env s2 e2 t2'
  return (SHylo (mn n) sig1 sig2, s3)
  where
    cataTy t = TdArr [] (TdApp f t) t
    anaTy  t = TdArr [] t (TdApp f t)
    mn n
      | Just (TdMeta i) <- Map.lookup n env = Left  i
      | otherwise                           = Right n

