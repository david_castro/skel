{-# LANGUAGE ViewPatterns #-}
module TcCore.TcM
         ( TcM
         , runTcM
         , evalTcM

         , getContext
         , pushContext
         , popContext

         , warn
         , warnAmbiguous
         , warnUnusedLbl

         , duplicateDefn
         , notAFunctor
         , undefinedNm
         , functorMismatch
         , biFunctorMismatch
         , typeMismatch
         , ambiguousType
         , unifyError
         , unifyStructErr
         , wrongArgs
         , unexpected
         , undefinedLabel
         , duplicateLbl
         , unexpectedLbl
         , cycleLbl

         , existsT
         , existsF
         , lookupT
         , lookupF
         , insertT
         , insertF
         , freshTy
         , freshL
         , freshMeta
         , freshAtom

         , atom
         , existsS
         , lookupS
         , structOf

         , Env
         , showStructs
         ) where

import Control.Monad
         ( liftM
         , liftM2 )
import Control.Monad.Except
         ( throwError )
import Control.Monad.State.Strict
         ( get
         , state
         , modify )
import Control.Monad.Writer
         ( tell   )
import Data.List
         ( foldl' )
import Data.Maybe
         ( isJust )
import Data.Map.Strict
         ( Map )
import System.IO
         ( hPutStrLn
         , stderr )
import Text.PrettyPrint
         ( (<+>)
         , (<>) )

import qualified Data.Map.Strict  as Map
import qualified Text.PrettyPrint as Pretty

import Util.RWS
import Util.Util


import Common.Ppr
import Core


type Log = String

type TcM = RWS TcError Env Log

warn :: String -> TcM ()
warn s = tell $ "\nWarning: " ++ s ++ "\n"

existsT :: Name -> TcM Bool
existsT n = liftM (isJust . lookupEnvT n) get

existsF :: Name -> TcM Bool
existsF n = liftM (isJust . lookupEnvF n) get

lookupT :: Name -> TcM FunctorDef
lookupT n = maybeM (undefinedNm n) return $ liftM (lookupEnvT n) get

insertT :: FunctorDef -> TcM ()
insertT fd = modify doInsert
  where doInsert e = e { tyEnv = Map.insert (fName fd) fd (tyEnv e) }

lookupF :: Name -> TcM FunDef
lookupF n = maybeM (undefinedNm n) return $ liftM (lookupEnvF n) get

insertF :: FunDef -> TcM ()
insertF fd = modify doInsert
  where doInsert e = e { fnEnv = Map.insert (fdName fd) fd (fnEnv e) }

freshTy :: TcM TyDef
freshTy = state freshEnvTy

freshL :: Int -> TcM (L TyDef)
freshL n
  | n <= 0    = liftM LMeta (state freshEnvInt)
  | otherwise = liftM2 LCons freshTy (freshL $ n-1)

freshMeta :: TcM SeqS
freshMeta = liftM SMeta (state freshEnvInt)

freshAtom :: TcM SeqS
freshAtom = liftM (Atomic False) (state freshEnvInt)

atom :: ExprDef -> TcM SeqS
atom e = state (atomEnv e)

existsS :: Name -> TcM Bool
existsS n = liftM (existsEnvS n) get

lookupS :: Name -> TcM SeqS
lookupS n = maybeM (atom (EVar n)) return $ liftM (lookupEnvS n) get

structOf :: Name -> SeqS -> TcM ()
structOf n s = modify (addStruct n s)

getContext :: TcM Context
getContext = liftM ctx get

pushContext :: Name -> TcM ()
pushContext n = modify (pushCtxEnv n)

popContext :: TcM ()
popContext = modify popCtxEnv

runTcM :: Int -> TcM a -> IO a
runTcM i (flip runRWS (emptyEnv i) -> (x, _, w)) =
  either (error . show) (\y -> hPutStrLn stderr w >> return y) x

evalTcM :: Int -> TcM () -> IO Env
evalTcM i (flip runRWS (emptyEnv i) -> (x, s, w)) =
  either (error . show) (\_ -> hPutStrLn stderr w >> return s) x


-------------------------------------------------------------------------------
-- WARNINGS
-------------------------------------------------------------------------------

warnAmbiguous :: SeqS -> SeqS -> TcM ()
warnAmbiguous s1 s2 = do
  ct <- getContext
  warn $   "Ambiguous structure " ++ show ct ++ " :\n\n"
                        ++ al1
                        ++ sim
                        ++ al2
  where sl1       = "`" ++ show s1 ++ "'"
        sl2       = "`" ++ show s2 ++ "'"
        l1        = length sl1
        l2        = length sl2
        width     = max l1 l2
        mid       = 4 + width `div` 2
        padding   = 4 + (width - min l1 l2) `div` 2
        align i t = replicate i ' ' ++ t
        al1       = if l1 < l2 then align padding sl1 ++ "\n"
                               else "    " ++ sl1 ++ "\n"
        al2       = if l2 < l1 then align padding sl2 ++ "\n"
                               else "    " ++ sl2 ++  "."
        sim       = align mid "~" ++ "\n"

warnUnusedLbl :: Name -> TcM ()
warnUnusedLbl lbl = do
  ct <- getContext
  warn $  "Defined but not used in " ++ show ct ++ ":\n\n    "
          ++ " Label `" ++ show lbl ++ "'."



-------------------------------------------------------------------------------
-- EXCEPTIONS
-------------------------------------------------------------------------------

data TcError = TcError TcErrorCode Context

instance Show TcError where
  show (TcError cd ct) = "Type error in " ++ show ct ++ ":\n    " ++ show cd

data TcErrorCode =
    DuplicateDefinition Name
  | UndefinedNm         Name
  | NotAFunctor         TyDef
  | FunctorMismatch     TyDef Int Int
  | BiFunctorMismatch   TyDef
  | TypeMismatch        ExprDef TyDef
  | AmbiguousType       TyDef
  | UnifyError          TyDef TyDef
  | UnifyStruct         SeqS SeqS
  | WrongArgs           Name [TyDef]
  | Unexpected          TyDef TyDef
  | UndefinedLabel      Name
  | DuplicateLabel      Name
  | LblCycle            Name
  | UnexpectedLbl       Name Forall

instance Show TcErrorCode where
  show (DuplicateDefinition     n ) = "Duplicate definition: " ++ show n
  show (AmbiguousType           t ) = "Ambiguous type: " ++ show t
  show (UndefinedNm             n ) = "Undefined symbol: " ++ show n
  show (NotAFunctor             td) = "Expecting functor, found: " ++ show td
  show (FunctorMismatch    td 0 n ) =
    "Expecting type, found " ++ show n ++ "-functor: " ++ show td
  show (FunctorMismatch    td n n') =
    "Expecting " ++ show n ++ "-functor, found " ++ show n' ++ "-functor: " ++ show td
  show (BiFunctorMismatch    td) =
    "Expecting bifunctor variable, found `" ++ show td ++ "'"
  show (TypeMismatch       e  t   ) =
    "Wrong type " ++ show t ++ " for expression " ++ show e
  show (UnifyError         t1 t2  ) =
    "Cannot unify " ++ show t1 ++ " with " ++ show t2
  show (UnifyStruct        t1 t2  ) =
    "Cannot unify " ++ show t1 ++ " with " ++ show t2
  show (WrongArgs            n a  ) =
    "Wrong number of arguments for " ++ show n ++ ": " ++ show a
  show (Unexpected t1 t2) =
    "Unexpected type `" ++ show t1 ++ "', expecting `" ++ show t2 ++ "'"
  show (UndefinedLabel n) =
    "Undefined label `" ++ show n ++ "'"
  show (DuplicateLabel n) =
    "Duplicate label `" ++ show n ++ "'"
  show (UnexpectedLbl n f) =
    "Unexpected label `" ++ show n ++ "' in type `" ++ show f ++ "'"
  show (LblCycle n) =
    "Cycle in label `" ++ show n ++ "'"


duplicateDefn :: Name -> TcM a
duplicateDefn n =
  throwError =<< liftM (TcError $ DuplicateDefinition n) getContext

undefinedNm :: Name -> TcM a
undefinedNm n = throwError =<< liftM (TcError $ UndefinedNm n) getContext

notAFunctor :: TyDef -> TcM a
notAFunctor td  = throwError =<< liftM (TcError $ NotAFunctor td) getContext

functorMismatch :: TyDef -> Int -> Int -> TcM a
functorMismatch td n1 n2 =
  throwError =<< liftM (TcError $ FunctorMismatch td n1 n2) getContext

biFunctorMismatch :: TyDef -> TcM a
biFunctorMismatch td =
  throwError =<< liftM (TcError $ BiFunctorMismatch td ) getContext

typeMismatch :: ExprDef -> TyDef -> TcM a
typeMismatch e t = throwError =<< liftM (TcError $ TypeMismatch e t) getContext

ambiguousType :: TyDef -> TcM a
ambiguousType t = throwError =<< liftM (TcError $ AmbiguousType t) getContext

unifyError :: TyDef -> TyDef -> TcM a
unifyError t1 t2 = throwError =<< liftM (TcError $ UnifyError t1 t2) getContext

unifyStructErr :: SeqS -> SeqS -> TcM a
unifyStructErr t1 t2 =
  throwError =<< liftM (TcError $ UnifyStruct t1 t2) getContext

wrongArgs :: Name -> [TyDef] -> TcM a
wrongArgs a t = throwError =<< liftM (TcError $ WrongArgs a t) getContext

unexpected :: TyDef -> TyDef -> TcM a
unexpected t1 t2 = throwError =<< liftM (TcError $ Unexpected t1 t2) getContext

undefinedLabel :: Name -> TcM a
undefinedLabel n = throwError =<< liftM (TcError $ UndefinedLabel n) getContext

duplicateLbl :: Name -> TcM a
duplicateLbl n = throwError =<< liftM (TcError $ DuplicateLabel n) getContext

unexpectedLbl :: Name -> Forall -> TcM a
unexpectedLbl n f = throwError =<< liftM (TcError $ UnexpectedLbl n f) getContext

cycleLbl :: Name -> TcM a
cycleLbl n = throwError =<< liftM (TcError $ LblCycle n) getContext

-------------------------------------------------------------------------------
-- TYPE CHECKING ENVIRONMENT
-------------------------------------------------------------------------------

newtype Context = Ctx [Name]

instance Ppr Context where
  ppr (Ctx ns) = go (reverse ns)
    where go []     = Pretty.empty
          go (n:ls) = foldl' (\a b -> a <> Pretty.text "." <>  Pretty.text b) (Pretty.text n) ls

instance Show Context where
  show = showPpr

pushCtxEnv :: Name -> Env -> Env
pushCtxEnv n e = e { ctx = ctx' }
  where (Ctx ns) = ctx e
        ctx'     = Ctx (n:ns)

popCtxEnv :: Env -> Env
popCtxEnv e = e { ctx = ctx' }
  where Ctx ns = ctx e
        ctx'   = case ns of
                  []     -> Ctx []
                  (_:ls) -> Ctx ls

data Env   = Env
               { uniqGen :: !Int
               , tyEnv   :: !(Map Name FunctorDef)
               , fnEnv   :: !(Map Name FunDef)
               , ctx     :: !Context

               , atoms   :: !(Func SeqS)
               , structs :: !(Map Name SeqS)
               }
  deriving Show

emptyEnv :: Int -> Env
emptyEnv i = Env { uniqGen = i
                 , tyEnv   = Map.empty
                 , fnEnv   = Map.empty

                 , ctx     = Ctx []
                 , atoms   = emptyFunc
                 , structs = Map.empty
                 }

-- Regular type checking
lookupEnvT :: Name -> Env -> Maybe FunctorDef
lookupEnvT n = Map.lookup n . tyEnv

lookupEnvF :: Name -> Env -> Maybe FunDef
lookupEnvF n = Map.lookup n . fnEnv

freshEnvTy :: Env -> (TyDef, Env)
freshEnvTy e = (TdMeta i, e { uniqGen = i + 1 })
  where i = uniqGen e

freshEnvInt :: Env -> (Int, Env)
freshEnvInt e = (i, e { uniqGen = i + 1 })
  where i = uniqGen e

-- Structure
atomEnv :: ExprDef -> Env -> (SeqS, Env)
atomEnv e env = mFunc $ addAtom e $ atoms env
  where mFunc (i, f) = (Atomic True i, env { atoms = f })

existsEnvS :: Name -> Env -> Bool
existsEnvS n = isJust . lookupEnvS n

lookupEnvS :: Name -> Env -> Maybe SeqS
lookupEnvS n (structs -> s) = Map.lookup n s

addStruct :: Name -> SeqS -> Env -> Env
addStruct n s env = env { structs = Map.insert n s $ structs env }

showStructs :: Env -> String
showStructs (structs -> s)
  | s == Map.empty = ""
  | otherwise = Pretty.renderStyle skelStyle $
                  Pretty.vcat $
                    concatMap
                      (\(n, sig) -> [ align n <+> Pretty.char '~' <+> ppr sig ] )
                      (Map.toList s)
  where skelStyle = Pretty.Style
                      { Pretty.mode           = Pretty.PageMode
                      , Pretty.lineLength     = 80
                      , Pretty.ribbonsPerLine = 0
                      }
        alignl    = maximum $ map (length . fst) ss
        align n   = Pretty.text (n ++ replicate (alignl - length n + 1) ' ')
        ss        = Map.toList s
