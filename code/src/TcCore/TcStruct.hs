{-# LANGUAGE RankNTypes    #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns  #-}
module TcCore.TcStruct
          ( tcStructs
          , unifyStructs
          , unifyLbls
          , unify
          , normalise
          , applySubst
          , applyM
          , generaliseS
          , applySubstS
          , ngroups
          ) where

import Control.Arrow   ( second )
import Control.Monad   ( liftM
                       , join
                       , when
                       , liftM3
                       , liftM2 )
import Control.Monad.Extra ( concatMapM )
import Data.Maybe      ( catMaybes, fromMaybe )
import Data.Map.Strict ( Map
                       , traverseWithKey )

import qualified Data.Map.Strict as Map

import Core
import TcCore.TcM
import Util.Util

unifyLbls :: [(Name, SeqS)] -> [(Name, SeqS)] -> TcM [(Name, SeqS)]
unifyLbls l1 l2 = maybeM (unifyStructErr (SCnstr "" l1) (SCnstr "" l2))
                         return
                        (unifyAux l1 l2)
  where unifyAux []         _  = return Nothing
        unifyAux ((l,sl):r) ns = do
          m <- unifyStructs sl ns
          case m of
            --Just [(_, s)] -> liftM (Just . ((l, s) :) . fromMaybe []) $
            --                  unifyAux r ns
            Just ns  -> liftM (Just . ((l, SCnstr "" ns) :) . fromMaybe [])
                              $ unifyAux r ns
            Nothing  -> unifyAux r ns

unifyStructs :: SeqS -> [(Name, SeqS)] -> TcM (Maybe [(Name, SeqS)])
unifyStructs _sn []  = return Nothing
unifyStructs sn ((lbl, sl):lbls)
  | Just _ <- lookup lbl lbls = duplicateLbl lbl
  | otherwise = do
      pushContext lbl
      ms  <- unifyS sn sl
      if null ms
        then popContext *> unifyStructs sn lbls
        else
          do s <- collapse =<< applyM (reverse ms) sl (unifyStructErr sn sl)
             popContext
             r <- unifyStructs sn lbls
             return (Just $ (lbl,s) : fromMaybe [] r)

tcStructs :: SeqS -> [(Name, SeqS)] -> TcM [(Name, SeqS)]
tcStructs (SCnstr n ns') ns = tcStructs_ (SCnstr n $ filterLbls ns') ns
  where filterLbls :: [(Name, SeqS)] -> [(Name, SeqS)]
        filterLbls [] = []
        filterLbls (t@(_,s):ss)
            | null $ labelRefs s = t : filterLbls ss
            | otherwise          = filterLbls ss
tcStructs s            ns = tcStructs_ s ns

tcStructs_ :: SeqS -> [(Name, SeqS)] -> TcM [(Name, SeqS)]
tcStructs_ _sn []  = return []
tcStructs_ sn ((lbl, sl):lbls)
  | Just _ <- lookup lbl lbls = duplicateLbl lbl
  | otherwise = do
      pushContext lbl
      ms  <- unifyS sn sl
      when (null ms)       $ unifyStructErr sn sl
      when (length ms > 1) $ warnAmbiguous sn sl
      s  <- collapse =<< applyM (reverse ms) sl (unifyStructErr sn sl)
      popContext
      r <- tcStructs_ sn lbls

      return ((lbl,s):r)

collapse :: SeqS -> TcM SeqS
collapse (SCnstr n ((_,s):r)) = do
  ms <- unifyAll Map.empty s r
  when (null ms)       $ unifyStructErr s (SCnstr n r)
  when (length ms > 1) $ warnAmbiguous  s (SCnstr n r)
  applyM (reverse ms) s (unifyStructErr s (SCnstr n r))
collapse s = return s


unifyS :: SeqS -> SeqS -> TcM [UEnv]
unifyS sn sl =
  join $ liftM2 (unify Map.empty) (normalise sn) (normalise sl)

applyM :: [UEnv] -> SeqS -> TcM SeqS -> TcM SeqS
applyM [] _  _ = error "Panic! TcCore.TcStruct.select!"
applyM (m:ms) s err = do
  m' <- simplM m mvs
  r  <- applySubst m' s err >>= countSimpl
  go r ms
  where
    go    (_, s') []        = return s'
    go t1@(i, _ ) (m'':ms') = do
       m2 <- simplM m'' mvs
       t2@(i', _) <- applySubst m2 s err >>= countSimpl
       if i' < i then go t2 ms'
                 else go t1 ms'
    mvs = metaStruct s

    simplM :: UEnv -> [Int] -> TcM UEnv
    simplM _m []     = return Map.empty
    simplM  m (i:is) = do
      s'        <- applySubst m (SMeta i) err
      (n1,s'' ) <- simplify s'
      newM      <- simplM m is
      return $ Map.insert i s'' newM

    countSimpl s = do
      (i, _) <- simplify s
      return (i, s)

simplifyM :: UEnv -> TcM (Int, UEnv)
simplifyM m = do
  m' <- mapM simplify m
  return (sumM m', Map.map snd m')
  where sumM = Map.foldl' (\a b -> a + fst b) 0


type UEnv = Map Int SeqS

applySubst :: UEnv -> SeqS -> TcM SeqS -> TcM SeqS
applySubst  e a@(Atomic False i) err
  | Just s <- Map.lookup i e = chase s
  | otherwise                = return a
  where chase (Atomic False j)
          | i == j    = err
          | otherwise = maybe err chase $ Map.lookup j e
        chase a                 = return a
applySubst  _e a@(Atomic True _) _err = return a
applySubst  e m@(SMeta    i) err
  | Just s <- Map.lookup i e     = applySubst e s err
  | otherwise                    = return m
applySubst  e   (SCnstr n s) err =
  liftM (SCnstr n) $
    mapM (\(a,b) -> liftM (a,) $ applySubst e b err) s
applySubst  e (SOp1 o s )  err = liftM (SOp1 o) $ applySubst e s err
applySubst  e (SOp  o ss)  err = liftM (SOp  o) $
                                   mapM (\s -> applySubst e s err) ss
applySubst  e (SComp  ss)  err = liftM mkComp $
                                   mapM (\s -> applySubst e s err) ss
applySubst  e (SMap f s)   err = liftM (SMap f) $ applySubst e s err
applySubst  e (SHylo f s1 s2) err = liftM2 (SHylo f) (applySubst e s1 err)
                                                     (applySubst e s2 err)
applySubst _e a _err = return a

-- TODO: proper unification including sums and products
unify :: UEnv -> SeqS -> SeqS -> TcM [UEnv]
unify env s           (SMeta i  )
  | Just s' <- Map.lookup i env   = unify env s s'
  | otherwise                     = return [ Map.insert i s env ]
unify env (SMeta i  ) s
  | Just s' <- Map.lookup i env   = unify env s' s
  | otherwise                     = return [ Map.insert i s env ]

unify env (SCnstr _ [("", s1)]) s2 = unify env s1 s2
unify env s1 (SCnstr _ [("", s2)]) = unify env s1 s2

unify env (SCnstr _ ns) (SLabel n')
  | Just s' <- lookup n' ns  = return [env]
  | otherwise                = return []
unify env (SLabel n') (SCnstr _ ns)
  | Just s' <- lookup n' ns  = return [env]
  | otherwise                = return []

unify env (SCnstr _ ns) s = unifyAll env s ns
unify env s (SCnstr _ ns) = unifyAll env s ns

unify env c@(SComp _) h@(SHylo f (SMeta i) (SMeta j))
  = addMap env i f >>= \(nenv, m) ->
      normalise (SHylo f m $ SMeta j) >>= \h' ->
        liftM2 (++) (unify nenv c $ doSplit h)
                    (unify nenv c h')


unify env c@(SComp _) h@(SHylo _ (SComp (splitL -> ([SMeta _], SMap _ _))) _)
  = liftM3 app (unify env c $ doSplitMapL h)
               (unify env c $ doSplit     h)
               (doSplitMap env c h)
  where app x y z = x ++ y ++ z

unify env c@(SComp _) h@(SHylo _ (SComp (splitL -> (ssi, SMap _ _))) (SMeta _))
  | ssi /= [SPrim In]
  = liftM2 (++) (unify env c $ doSplitMapL h)
                (unify env c $ doSplit     h)
  | ssi == [SPrim In]
  = liftM2 (++) (unify env c $ doSplit     h)
                (doSplitMap env c h)
  | otherwise
  = unify env c $ doSplit     h
--  where app x y z = x ++ y ++ z


unify env c@(SComp _) h@(SHylo _ (SComp (splitL -> ([SPrim In], SMap _ (SMeta i)))) (SPrim Out))
  = doSplitMap env c h

unify env c@(SComp _) h@(SHylo f (SMeta i) o@(SPrim Out))
  = addMap env i f >>= \(nenv, m) ->
      normalise (SHylo f m o) >>=
        unify nenv c
unify env c@(SComp _) h@(SHylo f i@(SPrim In) (SMeta j))
  = addMap >>= \(nenv, m) ->
        normalise (SHylo f i m) >>=
          unify nenv c
  where
    addMap = do
      k <- freshMeta
      l <- freshMeta
      let nenv = Map.insert j nc env
          nc   = SComp [SMap f k, l]
      return (nenv, nc)

unify env c@(SComp _) h@(SHylo n (SMeta i) a)
  | Just s <- Map.lookup i env = unify env c (SHylo n s a)
  | otherwise                  = liftM2 (++)
      (continue env i c newC newH (,))
      (unify env c $ doSplit h       )
  where newC j k = mkComp [j, SMap n k]
        newH ns  = SHylo n ns a
unify env c@(SComp _) h@(SHylo n a (SMeta i))
  | Just s <- Map.lookup i env = unify env c (SHylo n a s)
  | otherwise                  = liftM2 (++)
      (continue env i c newC newH (,))
      (unify env c $ doSplit h)
  where newC j k = mkComp [SMap n j, k]
        newH     = SHylo n a
unify env h@(SHylo n (SMeta i) a) c@(SComp _)
  | Just s <- Map.lookup i env = unify env (SHylo n s a) c
  | otherwise                  = liftM2 (++)
      (continue env i c newC newH (\x y -> (y,x)))
      (unify env (doSplit h) c)
  where newC j k = mkComp [j, SMap n k]
        newH ns  = SHylo n ns a
unify env h@(SHylo n a (SMeta i)) c@(SComp _)
  | Just s <- Map.lookup i env = unify env (SHylo n a s) c
  | otherwise                  = liftM2 (++)
      (continue env i c newC newH (\x y -> (y,x)))
      (unify env (doSplit h) c)
  where newC j k = mkComp [SMap n j, k]
        newH     = SHylo n a

unify env c@(SComp _) (SMap n (SMeta i))
  | Just s <- Map.lookup i env = unify env c (SMap n s)
  | otherwise                  = continue env i c sComp newM (,)
  where newM = SMap n
unify env (SMap n (SMeta i)) c@(SComp _)
  | Just s <- Map.lookup i env = unify env (SMap n s) c
  | otherwise                  = continue env i c sComp newM (\a b -> (b,a))
  where newM = SMap n

--------------------------------------------
unify env (SComp ss1) (SComp ss2)     = unifyL env ss1 ss2
unify env (SOp1  _ a  ) (SOp1  _ b  ) = unify env a b
unify env (SOp   x a  ) (SOp   y b  )
  | x == y = go env $ zip a b
  where go ev []            = return [ev]
        go ev ((x1, x2):xs) = unify ev x1 x2 >>= liftM concat . mapM (`go` xs)
unify env (SMap  n1 a ) (SMap n2 b  )
  | n1 == n2 = unify env a b
unify env (SHylo n1 a1 b1) (SHylo n2 a2 b2) = do
  let ms1 = unifyN n1 n2
  ms2 <- concatMapM (\ev -> unify ev a1 a2) ms1
  concatMapM (\ev -> unify ev b1 b2) ms2
  where unifyN (Left _) _ = [env]
        unifyN _ (Left _) = [env]
        unifyN (Right x) (Right y)
          | x == y = [env]
          | otherwise = []
--------------------------------------------

unify env   (Atomic True i)   (Atomic True j)
  | i == j    = return [env]
  | otherwise = return []
unify env a@(Atomic True  _)   (Atomic False j)
  = return [Map.insert j a env]
unify env   (Atomic False i) b@(Atomic True  _)
  = return [Map.insert i b env]
unify env a@(Atomic False i) b@(Atomic False j)
  = return [ Map.insert i b $ Map.insert j a env ]


unify env x y
  | x == y    = return [env]
  | otherwise = return []


addMap env i f = do
  k <- freshMeta
  l <- freshMeta
  let nenv = Map.insert i nc env
      nc   = SComp [k, SMap f l]
  return (nenv, nc)

doSplit :: SeqS -> SeqS
doSplit (SHylo n a b) = SComp [SHylo n a (SPrim Out), SHylo n (SPrim In) b]
doSPlit s             = s

doSplitMapL :: SeqS -> SeqS
doSplitMapL (SHylo n (SComp (splitL -> (ssi, SMap _ sf))) sr)
  = SComp [ SHylo n (mkComp ssi)                   (SPrim Out)
          , SHylo n (mkComp [SPrim In, SMap n sf]) sr
          ]

doSplitMap :: UEnv -> SeqS -> SeqS -> TcM [UEnv]
doSplitMap env s
 h@(SHylo n (SComp (splitL -> (ssi, SMap _ (SMeta i)))) a)
  = do j <- freshMeta
       k <- freshMeta
       let env' = Map.insert i newC env
           newC = sComp j k
       h' <- normalise (SHylo n (mkComp $ ssi ++ [SMap n newC]) a)
       unify env' s h'
doSplitMap _env _s _ = return []


unifyAll :: UEnv -> SeqS -> [(Name, SeqS)] -> TcM [UEnv]
unifyAll env s []          = return [env]
unifyAll env s ((_, s'):r) = concatMapM (\env' -> unifyAll env' s r) =<< unify env s s'

{-# INLINE continue #-}
continue :: UEnv -> Int -> SeqS
          -> (SeqS -> SeqS -> SeqS)
          -> (SeqS -> SeqS)
          -> (forall a.a -> a -> (a,a)) -> TcM [UEnv]
continue env i c
         fns
         fn order = do
  j <- freshMeta
  k <- freshMeta
  let nenv = Map.insert i ns env
      ns   = fns j k
  s <- normalise $ fn ns
  uncurry (unify nenv) $ order c s

-- try all alternatives (SComp [x1, x2] ~ y1 ^ x3 ~ y2 || x1 ~ y1 ^ SComp [x2, x3] ~ y2)
-- groups from (difference + 1 to ceiling (average))
unifyL :: UEnv -> [SeqS] -> [SeqS] -> TcM [UEnv]
unifyL env ss1 ss2
  | lss1 == lss2 = go env ss1 ss2
  | otherwise    = liftM concat $ mapM (uncurry (unifyL env)) gss
  where gss    = [ (s1, s2) | s1 <- ngroups groups ss1
                            , s2 <- ngroups groups ss2
                 ]
        groups = min lss1 lss2
        lss1   = length ss1
        lss2   = length ss2
        go ev [] [] = return [ ev ]
        go _  [] _  = error "Panic! Bug in TcCore.TcStruct.unifyL"
        go _  _ []  = error "Panic! Bug in TcCore.TcStruct.unifyL"
        go ev  (x:xs) (y:ys)
                 = do menv' <- unify ev x y
                      liftM concat $  mapM (\ev' -> go ev' xs ys) menv'

ngroups :: Int -> [SeqS] -> [[SeqS]]
ngroups i ss
  | i <= 0    = error "Panic! TcCore.TcStruct.ngroups 0. Report bug."
  | otherwise = map (group ss) idxs
  where l     = length ss
        maxn  =  l - i + 1
        idxs  = perms $ maxn : replicate (i - 1) 1

group :: [SeqS] -> [Int] -> [SeqS]
group [] []     = []
group ss []     = [mkComp ss]
group ss (i:is) = mkComp (take i ss) : group (drop i ss) is

perms :: [Int] -> [[Int]]
perms (i1:i@(i2:is)) = map (i1:) (perms i) ++ rest
  where rest = if i1 > 1 then perms ((i1 - 1):(i2 + 1):is)
                         else []
perms l              = [l]


normalise :: SeqS -> TcM SeqS
normalise s = do
  s' <- red s
  if s == s' then return s
             else normalise s'

red :: SeqS -> TcM SeqS
-- F (f1 . f2) -> F f1 . F f2
red (SMap n (SComp ns))              = return $ SComp $ map (SMap n) ns
-- F id        -> id
red (SMap _ (SPrim Id))              = return $ SPrim Id
-- hylo F in out -> id
red (SHylo _ (SPrim In) (SPrim Out)) = return $ SPrim Id
-- cata (s1 . F s2) | s1 != in -> cata s1 . map s2
red (SHylo n (SComp (splitL -> (ssi, SMap n' s))) o@(SPrim Out))
  | ssi /= [SPrim In] && n == n'
      = return $ SComp [ SHylo n (mkComp ssi                ) o
                       , SHylo n (SComp [SPrim In, SMap n s]) o ]
-- ana (F s1 . s2) | s2 != out -> map s1 . ana s2
red (SHylo n s1 (SComp (sm@(SMap n' _) : ss)))
  | n == n' = return $ SHylo n (sComp s1 sm) (mkComp ss)
red (SHylo n m@(SMeta _) s        ) = liftM (SHylo n m) (red s)
red (SHylo n s      m@(SMeta _) )   = do
  s' <- red s
  return $ SHylo n s' m
red (SHylo n s1 s2)
  | s1 /= SPrim In && s2 /= SPrim Out =
    return $ SComp [ SHylo n s1 (SPrim Out)
                   , SHylo n (SPrim In) s2
                   ]

-- traversals
red (SOp1 o s)  = liftM (SOp1 o) $ red s
red (SOp  o ss) = liftM (SOp o)  $ go ss
  where go []     = return []
        go (x:xs) = do x' <- red x
                       if x == x' then liftM (x:) $ go xs
                                  else return (x' : xs)
red (SComp [])  = return $ SPrim Id
red (SComp [x]) = return x
red (SComp ss)  = liftM mkComp $ redL ss
red (SMap n s) = liftM (SMap n) $ red s
red (SHylo n s1 s2) = do
  s1' <- red s1
  if s1 == s1' then liftM (SHylo n s1) $ red s2
               else return $ SHylo n s1' s2
red x
  = return x


--normaliseL :: [SeqS] -> TcM [SeqS]
--normaliseL xs = do
--  xs' <- redL xs
--  if xs == xs' then return xs
--               else normaliseL xs'

redL :: [SeqS] -> TcM [SeqS]
-- id . f -> f
redL (SPrim Id : s : ss)         = return (s : ss)
-- f . id -> f
redL (s : SPrim Id : ss)         = return (s : ss)
-- in . out -> id
redL (SPrim In : SPrim Out : ss) = return (SPrim Id : ss)
-- out . in -> id
redL (SPrim Out : SPrim In : ss) = return (SPrim Id : ss)
-- flatten composition
redL (SComp ss1 : ss2)           = return (ss1 ++ ss2)

redL (s : ss) = do
  s'  <- red s
  if s == s' then liftM (s :) $ redL ss
             else return (s' : ss)
redL [] = return []


simplify :: SeqS -> TcM (Int, SeqS)
simplify = go 0
  where go i s = do s' <- simpl s
                    if s == s' then return (i, s)
                               else go (i+1) s'

simpl :: SeqS -> TcM SeqS
simpl (SOp1 o s)  = liftM (SOp1 o) $ simpl s
simpl (SOp  o ss) = liftM (SOp o)  $ go ss
  where go []     = return []
        go (x:xs) = do x' <- simpl x
                       if x == x' then liftM (x:) $ go xs
                                  else return (x' : xs)

simpl (SComp [])  = return $ SPrim Id
simpl (SComp [x]) = return x
simpl (SComp ss)  = liftM mkComp $ simplL ss
simpl (SMap n s) = liftM (SMap n) $ simpl s
simpl (SHylo n s1 s2) = liftM2 (SHylo n) (simpl s1) (simpl s2)
simpl x           = return x

simplL :: [SeqS] -> TcM [SeqS]

simplL (SMap _ s1 : SMap n s2 : ss) = return (SMap n (sComp s1 s2) : ss)

simplL (SHylo _ s1 (SPrim Out) : SHylo n (SPrim In) s2 : ss)
  = return (SHylo n s1 s2 : ss)

simplL (SHylo _ s1 (SPrim Out) : SHylo n (SComp [SPrim In, SMap _ s2]) s : ss)
  = return (SHylo n (sComp s1 (SMap n s2)) s : ss)

simplL (SHylo _  s (SComp (SMap n s1 : sc)) : ss)
  = return (SHylo n (sComp s (SMap n s1)) (mkComp sc) : ss)

simplL (SComp xs : ss) = simplL $ xs ++ ss
simplL (s : xs) = do
  s' <- simpl s
  if s == s' then liftM (s :) $ simplL xs
             else return (s' : xs)
simplL [] = return []

mkComp :: [SeqS] -> SeqS
mkComp []        = SPrim Id
mkComp [SComp x] = mkComp $ flatten x
mkComp [x]       = x
mkComp xs        = SComp $ flatten xs

flatten :: [SeqS] -> [SeqS]
flatten [] = []
flatten (SComp x : xs) = x ++ flatten xs
flatten (x : xs)       = x : flatten xs


applySubstS :: (Int -> TcM (Either Int Name)) -> SeqS -> TcM SeqS
applySubstS  subst (SCnstr n a)     = liftM (SCnstr n)
                                      (mapM (mapSndM (applySubstS subst)) a)
applySubstS  subst (SFun   a b)   = liftM2 SFun
                                      (mapM (mapSndM (applySubstS subst)) a)
                                      (applySubstS subst b)
applySubstS  subst (SOp1   o s)   = liftM (SOp1 o)
                                      (applySubstS subst s)
applySubstS  subst (SOp    o s)   = liftM (SOp  o)
                                      (mapM (applySubstS subst) s)
applySubstS  subst (SComp  s)     = liftM mkComp
                                      (mapM (applySubstS subst) s)
applySubstS  subst (SMap   n s)   = liftM2 SMap
                                      (subsN subst n)
                                      (applySubstS subst s)
applySubstS  subst (SHylo  n a b) = liftM3 SHylo
                                      (subsN subst n)
                                      (applySubstS subst a)
                                      (applySubstS subst b)
applySubstS _subst a              = return a

subsN :: (Int -> TcM (Either Int Name)) -> Either Int Name -> TcM (Either Int Name)
subsN  env (Left x) = env x >>= either continue (return . Right)
  where continue x'
          | x == x'   = return $ Left x
          | otherwise = subsN env (Left x')

subsN _env l        = return l

generaliseS :: Map Name TyDef -> SeqS -> SeqS
generaliseS  subst (SCnstr n a)     = SCnstr n $ map (second (generaliseS subst)) a
generaliseS  subst (SFun   a b)   = SFun (map (second (generaliseS subst)) a)
                                         (generaliseS subst b)
generaliseS  subst (SOp1   o s)   = SOp1 o $ generaliseS subst s
generaliseS  subst (SOp    o s)   = SOp  o $ map (generaliseS subst) s
generaliseS  subst (SComp  s)     = mkComp  $ map (generaliseS subst) s
generaliseS  subst (SMap   n s)   = SMap  (genN subst n) $ generaliseS subst s
generaliseS  subst (SHylo  n a b) = SHylo (genN subst n)
                                          (generaliseS subst a)
                                          (generaliseS subst b)
generaliseS _subst a              = a

genN :: Map Name TyDef -> Either Int Name -> Either Int Name
genN  env n@(Right x)
  | Just (TdMeta i) <- Map.lookup x env = Left i
  | Just (TdVar v ) <- Map.lookup x env = genN env (Right v)
  | otherwise                           = n
genN _env l@(Left  _) = l

