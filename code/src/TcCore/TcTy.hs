module TcCore.TcTy
          ( Subst
          , unify
          , applySubst
          , base
          , tcFunctorDef
          , tcForAll
          , tcForAll_
          , generalise
          , tcTy
          , tcFunctor
          , tcBiFunctor
          ) where

import Core
import TcCore.TcM
import TcCore.TcStruct hiding ( applySubst, unify )
import Util.Util ( mapSndM )

import Control.Arrow       ( first, second )
import Control.Monad.Extra ( whenM
                           , liftM
                           , liftM2
                           , liftM3
                           , unless )
import Data.List           ( foldl', (\\), nub )
import Data.Map            ( Map )

import qualified Data.Map as Map

tcFunctorDef :: FunctorDef -> TcM FunctorDef
tcFunctorDef fd@FD{} = do
  whenM (existsT $ fName fd) $ duplicateDefn $ fName fd
  tcTyDef 0 (Map.fromList [ (n, 0) | n <- fArgs fd ]) Map.empty $ fBody fd
  return fd
tcFunctorDef fd@AT{} =
  whenM (existsT $ fName fd) (duplicateDefn $ fName fd)
  *> return fd

tcForAll_ :: Forall -> TcM ()
tcForAll_ (ForAll [] t) = tcTy t
tcForAll_ (ForAll _  t) = ambiguousType t

tcForAll :: Forall -> TcM (Map Name TyDef, TyDef)
tcForAll (ForAll ns t) = liftM ret (genNames ns)
  where
    ret e = (e, generalise e t)
    genNames []     = return Map.empty
    genNames (n:ns) = liftM2 (Map.insert n) freshTy (genNames ns)

--unGeneralise :: TyDef -> TcM (Subst, Forall)
--unGeneralise td = (substM, ForAll (map snd subst)
--                                  (applySubst e substM td))
--  where
--    substM  = Map.fromList $ map (second TdVar) subst
--    subst   = zip (metaVars td) varGen
--    varGen :: [Name]
--    varGen  = freshvs \\ freevs
--    freevs  = freeVars td
--    freshvs = [ [x] | x <- ['A'..'Z'] ]
--           ++ [ x:show i | x <- ['A'..'Z'], i <-[0..]]

freeVars :: TyDef -> [Name]
freeVars TdUnit           = []
freeVars (TdMeta   a    ) = []
freeVars (TdVar    a    ) = [a]
freeVars (TdConst  a    ) = []
freeVars (TdSum    a    ) = concatMap freeVars $ toList a
freeVars (TdProd   a    ) = concatMap freeVars $ toList a
freeVars (TdApp    a b  ) = freeVars b ++ freeVars a
freeVars (TdArr    a b c) = {-freeVars a ++ -} freeVars b ++ freeVars c
freeVars (TdFix    a    ) = freeVars a

metaVars :: TyDef -> [Int]
metaVars = nub . metaVars_
  where
    metaVars_ TdUnit           = []
    metaVars_ (TdMeta   a    ) = [a]
    metaVars_ (TdVar    a    ) = []
    metaVars_ (TdConst  a    ) = []
    metaVars_ (TdSum    a    ) = concatMap metaVars_ $ toList a
    metaVars_ (TdProd   a    ) = concatMap metaVars_ $ toList a
    metaVars_ (TdApp    a b  ) = metaVars a ++ metaVars b
    metaVars_ (TdArr    a b c) = {-freeVars a ++ -} metaVars_ b ++ metaVars_ c
    metaVars_ (TdFix    a    ) = metaVars_ a

tcTy :: TyDef -> TcM ()
tcTy = tcTyDef 0 Map.empty Map.empty

tcFunctor :: Map Name TyDef -> TyDef -> TcM ()
tcFunctor = tcTyDef 1 Map.empty

tcBiFunctor :: Map Name TyDef -> TyDef -> TcM Name
tcBiFunctor env t@(TdVar n) = tcTyDef 2 Map.empty env t *> return n
tcBiFunctor _   t           = biFunctorMismatch t

tcTyDef :: Int -> Map Name Int -> Map Name TyDef -> TyDef -> TcM ()
tcTyDef n _ _ t@TdUnit
  = unless (n == 0) $ notAFunctor t
tcTyDef _ _ _ t@TdMeta{}
  = return () -- FIXME: ambiguousType t
tcTyDef n a m t@(TdVar v)
  | Just _ <- v `Map.lookup` m = return ()
  | Just n' <- v `Map.lookup` a
    = unless (n == n') $ functorMismatch t n n'
  | otherwise
    = do vt <- lookupT v
         let n' = numArgs vt
         unless (n == n') $ functorMismatch t n n'
tcTyDef n _ _ t@TdConst{}
  = unless (n == 0) $ notAFunctor t
tcTyDef n a m t@(TdSum  ts)
  = tcTys n a t m $ toList ts
tcTyDef n a m t@(TdProd ts)
  = tcTys n a t m $ toList ts
tcTyDef n a m (TdApp  t1 t2)
  = tcTyDef (n + 1) a m t1 >>
    tcTyDef 0       a m t2
tcTyDef n a m t@(TdArr _  t1 t2)
  = tcTys n a t m [t1, t2]
tcTyDef n a m t@(TdFix t')
  = unless (n == 0) $ notAFunctor t >>
    tcTyDef 1 a m t'

{-# INLINE tcTys #-}
tcTys :: Int -> Map Name Int -> TyDef -> Map Name TyDef -> [TyDef] -> TcM ()
tcTys n a t m ts = unless (n == 0) (notAFunctor t) >>
                   mapM_ (tcTyDef n a m) ts


type Subst = Map Int  TyDef
type LEnv  = Map Name TyDef

base :: Map Name TyDef -> TyDef -> TcM (Either Int Name)
base m (TdApp a _) = base m a
base m (TdVar v)
  | Just _ <- Map.lookup v m = return $ Right v
  | otherwise = do
      t' <- lookupT v
      case fBody t' of
        TdApp a _     -> base Map.empty a
        TdFix f       -> base Map.empty f
        TdVar v'
          | v == v'   -> return $ Right v
          | otherwise -> base Map.empty (TdVar v')
        _             -> return $ Right v
base m (TdFix f) = base m f
base _ (TdMeta m) = return $ Left m
base _ t         = notAFunctor t


tyIn :: Int -> TyDef -> Bool
tyIn _  TdUnit        = False
tyIn v1 (TdMeta  v2 ) = v1 == v2
tyIn _  (TdVar   _  ) = False
tyIn _  (TdConst _  ) = False
tyIn v1 (TdSum   as ) = tyInL v1 as
tyIn v1 (TdProd  as ) = tyInL v1 as
tyIn v1 (TdApp   a b) = v1 `tyIn` a || v1 `tyIn` b
tyIn v1 (TdArr _ a b) = v1 `tyIn` a || v1 `tyIn` b
tyIn v1 (TdFix   a  ) = v1 `tyIn` a

tyInL :: Int -> L TyDef -> Bool
tyInL v1 (LCons a b) = v1 `tyIn` a || v1 `tyInL` b
tyInL _  _ = False

unify :: LEnv -> TyDef -> (TyDef, Subst) -> TcM (TyDef, Subst)
unify e t (t1@(TdMeta a), s)
  | Just t' <- Map.lookup a s = unify e t (t', s)
  | otherwise =
    do t' <- applySubst e s t
       if      t' ==  t1  then return (t, s)
       else if a `tyIn` t then unifyError t t1
       else return (t, Map.insert a t s)
unify e (t1@(TdMeta a)) (t, s)  = unify e t (t1, s)

unify e TdUnit r@(TdUnit, _) = return r
unify e (TdConst a1) (TdConst a2, s)
  | a1 == a2                  = return (TdConst a1, s)
  | otherwise                 = unifyError (TdConst a1) (TdConst a2)

unify e t1@(TdApp a1 b1) (t2@(TdApp a2 b2), s)
  | (TdMeta _ : _) <- chase t1 = doUnify
  | (TdMeta _ : _) <- chase t2 = doUnify
  | otherwise
    = reduceAndUnify e s t1 t2 doUnify
  where doUnify = do (a, s1) <- unify e a1 (a2, s )
                     (b, s2) <- unify e b1 (b2, s1)
                     return (TdApp a b, s2)
unify e t1@TdApp{} (t2, s) =
  reduceAndUnify e s t1 t2 $ unifyError t1 t2
unify e t2 (t1@TdApp{}, s) =
  reduceAndUnify e s t2 t1 $ unifyError t2 t1

unify e t1@(TdVar v1) (t2@(TdVar v2), s)
  | v1 == v2   = return (TdVar v1, s)
  | otherwise  = reduceAndUnify e s t1 t2 $ unifyError t1 t2
unify e t2 (t1@TdVar{}, s) =
  reduceAndUnify e s t2 t1 $ unifyError t2 t1
unify e t1@TdVar{} (t2, s) =
  reduceAndUnify e s t1 t2 $ unifyError t1 t2


unify e t1@(TdSum  as)  (t2@(TdSum  bs), s) = do
  (ts, s1) <- unifyList matchSum TdSum t1 t2 as bs e s
  return (TdSum ts, s1)
  where matchSum (TdSum l) = return l
        matchSum _         = unifyError t1 t2
unify e t1@(TdProd as)  (t2@(TdProd bs), s) = do
  (ts, s1) <- unifyList matchProd TdProd t1 t2 as bs e s
  return (TdProd ts, s1)
  where matchProd (TdProd l) = return l
        matchProd _          = unifyError t1 t2
unify e (TdArr n1 a1 b1)  (TdArr n2 a2 b2, s) = do
  (a, s1) <- unify e a1 (a2, s )
  (b, s2) <- unify e b1 (b2, s1)
  return (TdArr (n1 ++ n2) a b, s2)
unify e (TdFix  a1   )  (TdFix  a2   , s) =
  liftM (first TdFix) $ unify e a1 (a2, s )

unify e t1              (t2          , _) =
  unifyError t1 t2

{-# INLINE unifyList #-}
unifyList :: (TyDef -> TcM (L TyDef))
          -> (L TyDef -> TyDef)
          -> TyDef
          -> TyDef
          -> L TyDef
          -> L TyDef
          -> LEnv
          -> Subst
          -> TcM (L TyDef, Subst)
unifyList f g t1 t2 (LMeta i) l e s1
  | Just t <- Map.lookup i s1 = do
      l' <- f t
      unifyList f g t1 t2 l' l e s1
  | otherwise                = return (l, Map.insert i (g l) s1)
unifyList f g t1 t2 l (LMeta i) e s1
  | Just t <- Map.lookup i s1 = do
      l' <- f t
      unifyList f g t1 t2 l' l e s1
  | otherwise                = return (l, Map.insert i (g l) s1)
unifyList f g t1 t2 (LCons a1 b1) (LCons a2 b2) e s1 = do
  (c1, s2) <- unify e a1 (a2, s1)
  (l1, s3) <- unifyList f g t1 t2 b1 b2 e s2
  return (LCons c1 l1, s3)
unifyList _ _ _ _ LNil LNil e s1 = return (LNil, s1)
unifyList _ _ t1 t2 _ _ _ _      = unifyError t1 t2

{-# INLINE reduceAndUnify #-}
reduceAndUnify :: LEnv -> Subst -> TyDef -> TyDef -> TcM (TyDef, Subst) -> TcM (TyDef, Subst)
reduceAndUnify e s t1 t2 m = do
  t1' <- reduce s t1
  t2' <- reduce s t2
  if t1 == t1' && t2 == t2'
    then m
    else unify e t2' (t1', s)


chase :: TyDef -> [TyDef]
chase = chaseAux []
  where chaseAux l (TdApp a b) = chaseAux (b : l) a
        chaseAux l t           = t : l

reduce :: Subst -> TyDef -> TcM TyDef
reduce s (TdMeta  i)
  | Just t <- Map.lookup i s = reduce s t
reduce _ i@(TdVar v) = do
  t <- lookupT v
  if numArgs t == 0
    then body t
    else eta i (reverse $ fArgs t) (fBody t)
  where
    eta _ []     es = return es
    eta t (a:as) (TdApp e (TdVar a'))
      | a == a' && not (a `tIn` e) = eta t as e
      | otherwise                  = return t
    eta t _      _                 = return t

    tIn _  TdUnit        = False
    tIn _  (TdMeta  _  ) = False
    tIn v1 (TdVar   v2 ) = v1 == v2
    tIn _  (TdConst _  ) = False
    tIn v1 (TdSum   as ) = tInL v1 as
    tIn v1 (TdProd  as ) = tInL v1 as
    tIn v1 (TdApp   a b) = v1 `tIn` a || v1 `tIn` b
    tIn v1 (TdArr _ a b) = v1 `tIn` a || v1 `tIn` b
    tIn v1 (TdFix   a  ) = v1 `tIn` a

    tInL v1 (LCons a b) = v1 `tIn` a || v1 `tInL` b
    tInL _  _ = False

    body f@FD{} = return $ fBody f
    body   AT{} = return i

reduce s t@TdApp{}     =
  applyFunctor s $ chase t
reduce s (TdSum   as ) =
  liftM TdSum $ reduceL as
  where
    reduceL LNil = return LNil
    reduceL t@(LMeta i)
      | Just (TdSum as') <- Map.lookup i s = reduceL as'
      | Just t'          <- Map.lookup i s
        = liftM (flip LCons LNil) (reduce s t')
      | otherwise                          = return t
    reduceL (LCons a b) = liftM2 LCons (reduce s a) (reduceL b)

reduce s (TdProd  as ) = liftM TdProd $ reduceL as
  where
    reduceL LNil = return LNil
    reduceL t@(LMeta i)
      | Just (TdProd as') <- Map.lookup i s = reduceL as'
      | Just t'           <- Map.lookup i s
        = liftM (flip LCons LNil) (reduce s t')
      | otherwise                           = return t
    reduceL (LCons a b) = liftM2 LCons (reduce s a) (reduceL b)
reduce s (TdArr n a b) =
  liftM2 (TdArr n) (reduce s a) (reduce s b)
reduce s (TdFix   a) =
  liftM TdFix (reduce s a)
reduce _  t = return t

applyFunctor :: Subst -> [TyDef] -> TcM TyDef
applyFunctor s    (TdMeta i : args)
  | Just t <- Map.lookup i s = applyFunctor s $ t : args
applyFunctor _ ts@(TdVar v  : args) = do
  b <- existsT v
  if b then replace args =<< lookupT v
       else return $ mkApp ts
applyFunctor _ args
  = return $ mkApp args

mkApp :: [TyDef] -> TyDef
mkApp = go . reverse
  where go []     = error "Panic! Impossible case, TcCore.TcExpr.mkApp"
        go (t:ts) = foldl' (flip TdApp) t ts

replace :: [TyDef] -> FunctorDef -> TcM TyDef
replace args fd
  | length args <  numArgs fd
    = return $ mkApp $ TdVar (fName fd) : args
  | length args == numArgs fd
    = return $ go (Map.fromList $ zip (fArgs fd) args) $ fBody fd
  | length args >  numArgs fd
    = wrongArgs (fName fd) args
  | otherwise
    = error "Panic! Impossible case, TcCore.TcExpr.replace"
  where go m v@(TdVar   a  )
          | Just t <- Map.lookup a m = t
          | otherwise                = v
        go m (TdSum   a  ) = TdSum   (fmap (go m) a)
        go m (TdProd  a  ) = TdProd  (fmap (go m) a)
        go m (TdApp   a b) = TdApp   (go m a) (go m b)
        go m (TdArr n a b) = TdArr n (go m a) (go m b)
        go m (TdFix   a  ) = TdFix $ go m a
        go _ t             = t

applySubst :: LEnv -> Subst -> TyDef -> TcM TyDef
applySubst e _ t@TdUnit        = return t
applySubst e s t@(TdMeta  i  ) = maybe (return t) (applySubst e s) $ Map.lookup i s
applySubst e _ t@(TdVar   _  ) = return t
applySubst e _ t@(TdConst _  ) = return t
applySubst e s   (TdSum   a  ) = liftM  TdSum  $ applySubstL e s a
applySubst e s   (TdProd  a  ) = liftM  TdProd $ applySubstL e s a
applySubst e s   (TdApp   a b) = liftM2 TdApp   (applySubst e s a) (applySubst e s b)
applySubst e s   (TdArr n a b) = liftM3 TdArr
                                  (mapM (mapSndM $ applySubstS getBase) n)
                                  (applySubst e s a)
                                  (applySubst e s b)
  where
    getBase :: Int -> TcM (Either Int Name)
    getBase i
      | Just (TdMeta i') <- Map.lookup i s = getBase i'
      | Just ty          <- Map.lookup i s = base e ty
      | otherwise                 = base e (TdMeta i)
applySubst e s   (TdFix   a  ) = liftM TdFix   (applySubst e s a)

applySubstL :: LEnv -> Subst -> L TyDef -> TcM (L TyDef)
applySubstL e s LNil        = return LNil
applySubstL e s (LCons x b) = liftM2 LCons (applySubst e s x) (applySubstL e s b)
applySubstL e s il@(LMeta i)
  | Just (TdSum  l) <- Map.lookup i s = return l
  | Just t          <- Map.lookup i s = return $ LCons t LNil
  | otherwise                         = return il


generalise :: Map Name TyDef -> TyDef -> TyDef
generalise _ t@TdUnit        = t
generalise _ t@(TdMeta  _  ) = t
generalise _ t@(TdConst _  ) = t
generalise v t@(TdVar   v' )
  | Just t'  <- Map.lookup v' v = t'
  | otherwise   = t
generalise s   (TdSum   a  ) = TdSum $ generaliseL a
  where generaliseL (LCons x b) = LCons (generalise s x) (generaliseL b)
        generaliseL tl          = tl
generalise s   (TdProd  a  ) = TdProd $ generaliseL a
  where generaliseL (LCons x b) = LCons (generalise s x) (generaliseL b)
        generaliseL tl          = tl
generalise s   (TdApp   a b) = TdApp (generalise s a) (generalise s b)
generalise s   (TdArr n a b) = TdArr (map (second $ generaliseS s) n)
                                     (generalise s a)
                                     (generalise s b)
generalise s   (TdFix   a  ) = TdFix (generalise s a)

