{-# LANGUAGE TupleSections #-}
module TcCore.TcExpr_2
          ( tcFunDef
          , tcExprDef
          ) where

import qualified TcCore.TcStruct as S

import Core
import TcCore.TcM
import TcCore.TcStruct hiding (unify, applySubst)
import TcCore.TcTy
import Util.Util ( mapSndM )

import Control.Arrow
        ( first
        , second )
import Control.Monad.Extra
        ( liftM
        , liftM2
        , liftM3
        , (<=<)
        , void
        , join
        , when
        , whenM )
import Data.List
        ( foldl'
        , (\\) )
import Data.Map.Strict
        ( Map )
import Data.Maybe
        ( isNothing )
import qualified Data.Map.Strict as Map

type LEnv  = Map Name TyDef

tcFunDef :: FunDef -> TcM FunDef
tcFunDef fd@FnD{} = do
  pushContext (fdName fd)
  whenM (existsF $ fdName fd) $ duplicateDefn $ fdName fd
  (m   , ty')        <- tcForAll ty
  (lbls, env, ty'')  <- tcTyArgs (fdArgs fd) ty' -- TODO: do something with labels
  let e = generaliseE m $ fdBody fd
      nenv = env `Map.union` m
  (s  , subst) <- tcExprDef nenv Map.empty e ty''
  let nsubst_ = map (\(a,TdMeta b) -> (b,TdVar a)) $ Map.toList m
  nsubst  <-  mergeS1 nsubst_ subst
  --warn $ "TODO: Apply substitution: \n" ++ show subst ++ "\n\nto\n\n" ++ show s ++ "\n" ++ show m
  annTy   <- rebuildType nenv nsubst (fdArgs fd) env s
  e'      <- applySubstE nenv nsubst e
  popContext
  return fd { fdType = annTy
            , fdBody = e'
            }
  where
    ty@(ForAll qvs _) = fdType fd
    tcTyArgs []     t = return (Map.empty, Map.empty, t)-- $ labels t
    tcTyArgs (a:as) (TdArr [] t1 t2) = do
      (l, e, t') <- tcTyArgs as t2
      ls'        <- labels t1 -- TODO: detect cycles
      ls         <- unionL ls' l
      checkCycles ls $ concatMap (labelRefs . snd) $ Map.toList ls
      return (ls, Map.insert a t1 e, t')
    tcTyArgs _      (TdArr ((n,_):_) _ _) = unexpectedLbl n ty
    tcTyArgs _      t             = do
      t' <- liftM2 (TdArr []) freshTy freshTy
      unexpected t t'

    rebuildType nenv subst args env s
      = liftM (ForAll qvs) $ applySubst nenv subst $ rebuildTy args
      where rebuildTy [] = s
            rebuildTy (x:xs)
              | Just t <- Map.lookup x env = TdArr [] t $ rebuildTy xs
              | otherwise                  = error "Panic! TcCore.TcExpr.rebuildType!"

tcFunDef fd@AFn{} = do
  pushContext (fdName fd)
  whenM (existsF $ fdName fd) $ duplicateDefn $ fdName fd
  void $ tcForAll_ (fdType fd)
  s <- atom (EVar $ fdName fd)
  popContext
  return fd { fdType = ann s $ fdType fd }
    where
      ann s (ForAll v (TdArr _ t1 t2)) = ForAll v $ TdArr [("", s)] t1 t2
      ann _ t                          = t


checkCycles :: Map Name SeqS -> [Name] -> TcM ()
checkCycles _ [] = return ()
checkCycles m (l:ls)
  | Map.member l m = cycleLbl l
  | otherwise      = checkCycles m ls

labels :: TyDef -> TcM (Map Name SeqS)
labels (TdSum    l)     = unionsL =<< mapM labels (toList l)
labels (TdProd   l)     = unionsL =<< mapM labels (toList l)
labels (TdApp    a b)   = join $ liftM2 unionL (labels a) (labels b)
labels (TdArr    l a b) = join $ liftM3 union3 (mkL l) (labels a) (labels b)
  where union3 x y z = unionsL [x,y,z]
labels (TdFix    a)     = labels a
labels _                = return Map.empty

unionsL :: [Map Name SeqS] -> TcM (Map Name SeqS)
unionsL []     = return Map.empty
unionsL (m:ms) = do
  m' <- unionsL ms
  unionL m m'

unionL :: Map Name SeqS -> Map Name SeqS -> TcM (Map Name SeqS)
unionL m1 m2 =
  case Map.intersection m1 m2 of
    m3 | Map.null m3 -> return $ Map.union m1 m2
       | otherwise   -> duplicateLbl $ head $ Map.keys m3

mkL :: [(Name, SeqS)] -> TcM (Map Name SeqS)
mkL [] = return Map.empty
mkL ((l,s):ls) = do
  m <- mkL ls
  if l `Map.member` m
    then duplicateLbl l
    else return $ Map.insert l s m

mergeS1 :: [(Int, TyDef)] -> Subst -> TcM Subst
mergeS1 [] s = return s
mergeS1 ((i, t):ts) s
  | Just j <- followChain i = do
      s' <- mergeS1 ts s
      return $ Map.insert j t s'
  | otherwise = mergeS1 ts s
  where followChain idx
          | Just (TdMeta j) <- Map.lookup idx s = followChain j
          | Nothing         <- Map.lookup idx s = Just idx
          | otherwise                           = Nothing

applySubstE :: LEnv -> Subst -> ExprDef -> TcM ExprDef
applySubstE  env subst (EOp1  o  e ) =
  liftM (EOp1 o) (applySubstE env subst e)
applySubstE  env subst (EOp   o  es) =
  liftM (EOp  o) (mapM (applySubstE env subst) es)
applySubstE  env subst (EApp  e1 e2) =
  liftM2 EApp  (applySubstE env subst e1)
               (applySubstE env subst e2)
applySubstE  env subst (EComp e1 e2) =
  liftM2 EComp (applySubstE env subst e1)
               (applySubstE env subst e2)
applySubstE  env subst (EMap  t  e ) =
  liftM2 EMap  (applySubst env subst t)
               (applySubstE env subst e)
applySubstE  env subst (EHylo t  e1 e2) =
  liftM3 EHylo (applySubst env subst t)
               (applySubstE env subst e1)
               (applySubstE env subst e2)
applySubstE _env _subs e = return e

generaliseE :: Map Name TyDef -> ExprDef -> ExprDef
generaliseE subst (EOp1  o  e ) = EOp1 o $ generaliseE subst e
generaliseE subst (EOp   o  es) = EOp  o $ map (generaliseE subst) es
generaliseE subst (EApp  e1 e2) = EApp  (generaliseE subst e1)
                                        (generaliseE subst e2)
generaliseE subst (EComp e1 e2) = EComp (generaliseE subst e1)
                                        (generaliseE subst e2)
generaliseE subst (EMap  t  e ) = EMap  (generalise subst t)
                                        (generaliseE subst e)
generaliseE subst (EHylo t  e1 e2) = EHylo (generalise subst t)
                                          (generaliseE subst e1)
                                          (generaliseE subst e2)
generaliseE _subs e = e

tcExprDef :: LEnv -> Subst -> ExprDef -> TyDef -> TcM (TyDef, Subst)
tcExprDef env subst e t@(TdArr ns t1 t2)
  | not (null ns) = do
    (s, nsubst) <- tcExprDef_ env subst e t
    s'  <- applySubstS (getBase nsubst) s
    ns' <- tcStructs s' ns
    liftM2 (\t1' t2' -> (TdArr ns' t1' t2', nsubst))
      (applySubst env nsubst t1)
      (applySubst env nsubst t2)
  where getBase ns i
          | Just (TdMeta i) <- Map.lookup i ns = getBase ns i
          | Just ty         <- Map.lookup i ns = base env ty
          | otherwise                          = base env (TdMeta i)
tcExprDef env subst e t = do
    (_, nsubst) <- tcExprDef_ env subst e t
    liftM (, nsubst) (applySubst env nsubst t)

tcExprDef_ :: LEnv -> Subst -> ExprDef -> TyDef -> TcM (SeqS, Subst)
tcExprDef_ env s e@(ELit  l       ) ty = tcLiteral env s e l            ty
tcExprDef_ env s e@(EVar  v       ) ty = tcVar     env s e v            ty
tcExprDef_ env s e@(EOp1  p  e1   ) ty = tcOp1     env s e (p, e1)      ty
tcExprDef_ env s e@(EOp   p  e1   ) ty = tcOp      env s e (p, e1)      ty
tcExprDef_ env s e@(EPrim p       ) ty = tcPrim    env s e p            ty
tcExprDef_ env s e@(EApp  e1 e2   ) ty = tcApp     env s e (e1, e2    ) ty
tcExprDef_ env s e@(EComp e1 e2   ) ty = tcComp    env s e (e1, e2    ) ty
tcExprDef_ env s e@(EMap  f  e2   ) ty = tcMap     env s e (f , e2    ) ty
tcExprDef_ env s e@(EHylo f  e1 e2) ty = tcHylo    env s e (f , e1, e2) ty

{-# INLINE tcLiteral #-}
tcLiteral :: LEnv -> Subst -> ExprDef -> Literal -> TyDef -> TcM (SeqS, Subst)
tcLiteral env s e l ty = liftM2 (,) (atom e) (tcAux l)
  where
    tcAux ULit     = liftM snd $ unify env ty (TdUnit        , s)
    tcAux (BLit _) = liftM snd $ unify env ty (TdConst BOOL  , s)
    tcAux (ILit _) = liftM snd $ unify env ty (TdConst INT   , s)
    tcAux (SLit _) = liftM snd $ unify env ty (TdConst STRING, s)

{-# INLINE tcVar #-}
tcVar :: LEnv -> Subst -> ExprDef -> Name -> TyDef -> TcM (SeqS, Subst)
tcVar le s e n t =
  do t' <- maybe (lookupF n >>= liftM snd . tcForAll . fdType)
                 return
                 (Map.lookup n le)
     (_, s1) <- unify le t (t', s)
     ty  <- applySubst le s1 t'
     str <- getStruct n ty
     return (str, s1)

getStruct :: Name -> TyDef -> TcM SeqS
getStruct n = getStruct_
  where
    getStruct_ :: TyDef -> TcM SeqS
    getStruct_ (TdArr [] a b)                 = liftM2 sFun (getStruct_ a) (getStruct_ b)
      where sFun (SCnstr _ ns) b = SFun ns b
            sFun _             b = b
    getStruct_ (TdArr [("", a@Atomic{})] _ _) = return a
    getStruct_ (TdArr ns a b)                 = return $ SCnstr n ns
    getStruct_ (TdSum     lt)                 = liftM (SOp FPlus) $ mapM getStruct_ $ toList lt
    getStruct_ (TdProd    lt)                 = liftM (SOp FProd) $ mapM getStruct_ $ toList lt
    getStruct_ _                              = freshAtom

{-# INLINE tcPrim #-}
tcPrim :: LEnv -> Subst -> ExprDef -> Prim -> TyDef -> TcM (SeqS, Subst)
tcPrim e s _ In  ty = liftM mkT . unify e ty =<< liftM  ((,s) . inTy)  freshTy
  where inTy  v = TdArr [] (TdApp v (TdFix v)) (TdFix v)
        mkT = (,) (SPrim In) . snd
tcPrim e s _ Out ty = liftM mkT . unify e ty =<< liftM  ((,s) . outTy) freshTy
  where outTy v = TdArr [] (TdFix v) (TdApp v (TdFix v))
        mkT = (,) (SPrim Out) . snd
tcPrim e s _ Id  ty = liftM mkT . unify e ty =<< liftM  ((,s) . idTy)  freshTy
  where idTy v  = TdArr [] v v
        mkT = (,) (SPrim Id) . snd

tcPrim e s _ (Pi i)   ty = liftM mkT . unify e ty =<< liftM tyProj (freshL $ fromInteger i)
  where tyProj vs = (TdArr [] (TdProd vs) (vs `at` (fromInteger i-1)), s)
        mkT = (,) (SPrim $ Pi i) . snd
tcPrim e s _ (Inj i)  ty = liftM mkT . unify e ty =<< liftM tyInj (freshL $ fromInteger i)
  where tyInj vs = (TdArr [] (vs `at` (fromInteger i-1)) (TdSum  vs), s)
        mkT = (,) (SPrim $ Inj i) . snd

at :: L a -> Int -> a
at (LCons a _) 0         = a
at (LCons _ a) n | n > 0 = at a (n - 1)
at _ _ = error "TcCore.TcExpr.at! (TODO: move function to Core.Ty)"

{-# INLINE tcOp1 #-}
tcOp1 :: LEnv -> Subst -> ExprDef -> (Op1, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcOp1 env s _ (Pred, e1) ty = do
  t@(TdArr _ v _) <- liftM predTy freshTy
  (_, s1)         <- unify env ty (t, s)
  liftM (first (SOp1 Pred)) $
    tcExprDef_ env s1 e1
      (TdArr [] v (TdSum (LCons TdUnit $ LCons TdUnit LNil)))
  where predTy v = TdArr [] v (TdSum (LCons v $ LCons v LNil))

{-# INLINE tcOp #-}
tcOp :: LEnv -> Subst -> ExprDef -> (Op, [ExprDef]) -> TyDef -> TcM (SeqS, Subst)
tcOp env s _ (FProd, e1) ty = do
  t1@(TdArr _ v (TdProd vs)) <- liftM2 auxT freshTy (freshTuple i)
  (_, s1)                    <- unify env ty (t1, s)
  liftM (first (SOp FProd)) $ tcExprs s1 v $ zipL e1 vs
  where i = length e1
        freshTuple n
          | n <= 0    = return LNil
          | otherwise = liftM2 LCons freshTy (freshTuple $ n - 1)
        tcExprs s1 _ [] = return ([], s1)
        tcExprs s1 v ((e,v'):es) = do
          (sig , s2) <- tcExprDef_ env s1 e (TdArr [] v v')
          (sigs, s3) <- tcExprs s2 v es
          return (sig:sigs, s3)
        auxT v vs = TdArr [] v (TdProd vs)
tcOp env s _ (FPlus , e1) ty = do
  t1@(TdArr _ (TdSum  vs) v) <- liftM2 auxT freshTy (freshTuple i)
  (_, s1)                    <- unify env ty (t1, s)
  liftM (first (SOp FProd)) $ tcExprs s1 v $ zipL e1 vs
  where i = length e1
        freshTuple n
          | n <= 0    = return LNil
          | otherwise = liftM2 LCons freshTy (freshTuple $ n - 1)
        tcExprs s1 _ [] = return ([], s1)
        tcExprs s1 v ((e,v'):es) = do
          (sig , s2) <- tcExprDef_ env s1 e (TdArr [] v' v)
          (sigs, s3) <- tcExprs s2 v es
          return (sig:sigs, s3)
        auxT v vs = TdArr [] (TdSum vs) v

zipL :: [ExprDef] -> L TyDef -> [(ExprDef, TyDef)]
zipL [] LNil = []
zipL (x:xs) (LCons y ys) = (x, y) : zipL xs ys
zipL _      _            = error "Panic! Bug in TcCore.TcExpr.zipL"

{-# INLINE tcApp #-}
tcApp :: LEnv -> Subst -> ExprDef -> (ExprDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcApp env s _ (e1, e2) td = do
  t1'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
  (_ , s1)            <- unify env td (t12, s)
  (sig1, s2)          <- tcExprDef_ env s1 e1 t1'
  (sig2, s3)          <- tcExprDef_ env s2 e2 t11
  str                 <- applyStruct sig1 sig2

  --checkSubst s3 t1'
  return (str, s3)

applyStruct :: SeqS -> SeqS -> TcM SeqS
applyStruct (SFun ns s1) s2 =
  unifyStructs s2 ns >>=
    maybe (unifyStructErr (SCnstr "" ns) s2)
          (`substLbls` s1)
applyStruct s1           _ = return s1

substLbls :: [(Name, SeqS)] -> SeqS -> TcM SeqS
substLbls []       = return
substLbls (l:lbls) = substLbls lbls <=< substS l

substS :: (Name, SeqS) -> SeqS -> TcM SeqS
substS (l, s1) (SLabel l')  | l == l' = return s1
substS t@(l, s1) (SCnstr nm ns)
  | Just s' <- lookup l ns = do
      ms <- join $ liftM2 (S.unify Map.empty) (normalise s1) (normalise s')
      when (null ms) $ unifyStructErr s1 s'
      when (length ms > 1) $ warnAmbiguous s1 s'
      applyM ms s1 (unifyStructErr s1 s')
  | otherwise = liftM (SCnstr nm) $ mapM (mapSndM $ substS t) ns
substS l       (SFun ns s1)           = liftM2 SFun   (mapM (mapSndM $ substS l) ns)
                                                      (substS l s1)
substS l       (SOp1  o s1)           = liftM (SOp1 o) $ substS l s1
substS l       (SOp   o ss)           = liftM (SOp  o) $ mapM (substS l) ss
substS l       (SComp   ss)           = liftM SComp    $ mapM (substS l) ss
substS l       (SMap  f s1)           = liftM (SMap f) $ substS l s1
substS l       (SHylo f s1 s2)        = liftM2 (SHylo f) (substS l s1) (substS l s2)
substS _       s2                     = return s2

{-# INLINE tcComp #-}
tcComp :: LEnv -> Subst -> ExprDef -> (ExprDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcComp env s _ (e1, e2) ty = do
  t1'@(TdArr _ t21 t22) <- liftM2 (TdArr []) freshTy freshTy
  t2'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
  (_, s1)               <- unify env (TdArr [] t11 t22) (ty, s)
  (t, s2)               <- unify env t12 (t21, s1)
  (sig1, s3)            <- tcExprDef_ env s2 e1 t1'
  (sig2, s4)            <- tcExprDef_ env s3 e2 t2'
  --checkSubst s4 t11
  --checkSubst s4 t22
  return (sComp sig1 sig2, s4)

{-# INLINE tcMap #-}
tcMap :: LEnv -> Subst -> ExprDef -> (TyDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcMap env s _ (f@(TdMeta meta), e) ty = do
      t1'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
      t2                    <- freshTy
      let (TdArr _ x1 y1)     = ty
      (_, s1)               <- unify env (mapTyM f t11 t12 t2) (ty, s)
      liftM (first $ SMap $ Left meta) $ tcExprDef_ env s1 e t1'
tcMap env s _ (f, e) ty = do
      n <- tcBiFunctor env f
      t1'@(TdArr _ t11 t12) <- liftM2 (TdArr []) freshTy freshTy
      t2                    <- freshTy
      (_, s1)               <- unify env (mapTyM f t11 t12 t2) (ty, s)
      liftM (first $ SMap $ Right n) $ tcExprDef_ env s1 e t1'

mapTyM :: TyDef -> TyDef -> TyDef -> TyDef -> TyDef
mapTyM meta a b c = TdArr [] (TdApp (TdApp meta a) c)
                             (TdApp (TdApp meta b) c)

{-# INLINE tcHylo #-}
tcHylo :: LEnv -> Subst -> ExprDef -> (TyDef, ExprDef, ExprDef) -> TyDef -> TcM (SeqS, Subst)
tcHylo env s _ (f , e1, e2) ty = do
  tcFunctor env f
  t1'@(TdArr _ _ t2) <- liftM cataTy freshTy
  t2'@(TdArr _ t1 _) <- liftM anaTy  freshTy
  (_, s1)            <- unify env (TdArr [] t1 t2) (ty, s)
  n                  <- base env f
  (sig1, s2)         <- tcExprDef_ env s1 e1 t1'
  (sig2, s3)         <- tcExprDef_ env s2 e2 t2'
  return (SHylo n sig1 sig2, s3)
  where
    cataTy t = TdArr [] (TdApp f t) t
    anaTy  t = TdArr [] t (TdApp f t)
