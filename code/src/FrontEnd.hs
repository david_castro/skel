module FrontEnd
  ( Prog (..)
  , parseF
  , pProg
  ) where

import Control.Monad      ( liftM, liftM2 )
import Text.Parsec        ( try, (<|>), runParserT, getState, eof )
import Text.Parsec.Indent ( block, runIndent     )
import Text.PrettyPrint   ( vcat )

import Common.Parser
import Common.Ppr
import FrontEnd.Def

parseF :: FilePath -> IO (Prog, Int)
parseF fp = fmap  (either (error . show) id .
                        runIndent fp . runParserT pr 0 fp)
                     (readFile fp)
  where pr = liftM2 (,) (pProg fp) getState




data Prog = Prog { fn    :: FilePath
                 , defns :: [Def]
                 }

instance Ppr Prog where
  ppr = vcat . map ppr . defns

instance Show Prog where
  show = showPpr

pProg :: FilePath -> Parser Int Prog
pProg fp = whiteSpace *> fmap (Prog fp) (block (try pTyDef <|> try pFnDef)) <* eof
