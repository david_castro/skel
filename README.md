# README #

This is a prototype implementation of the ICFP2016 paper "Farms, Pipes, Streams and Reforestation: Reasoning about Structured Parallel Processes using Types and Hylomorphisms". See ICFP2016/icfp2016.pdf