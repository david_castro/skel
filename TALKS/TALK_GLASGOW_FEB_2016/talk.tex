\documentclass[pdf]{beamer}

\usepackage{adjustbox}
\usepackage{mathtools}
\usepackage{semantic}
\usepackage{stmaryrd}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{xcolor}

\mode<presentation>{}

\captionsetup{compatibility=false}
\usetheme{boxes}
\usefonttheme[onlymath]{serif}

\input{../../ICFP2016/macros.ltx}

\setbeamercovered{%
  still covered={\opaqueness<1->{0}},
  again covered={\opaqueness<1->{40}}
}

\newcommand{\nn}[1]{\ensuremath{\mathtt{#1}}}
\newcommand{\bt}[1]{\ensuremath{\mathsf{#1}}}
\newcommand{\any}{\ensuremath{\text{\_}}}

\newcommand{\light}[1]{\textcolor{gray}{#1}}

%\title{Farms, Pipes, Streams and Reforestation}
\title{Reasoning about Structured Parallel Processes using Types and Hylomorphisms}

\author{{\small \textcolor{red}{David Castro} \and Kevin Hammond \and Susmit Sarkar \and Edwin Brady \\ \textcolor{red}{dc84@st-andrews.ac.uk}}}

\begin{document}

\begin{frame}
  \titlepage%
\end{frame}

%MOTIVATION
\begin{frame}[fragile]{Motivation}
  \begin{itemize}
    \item Reasoning about parallelism is a very important issue.
      \begin{itemize}
        \item Hardware is (and will keep) becoming increasingly parallel.
        \item Too many low-level error prone approaches are still being used.
      \end{itemize}
    \item Structured parallelism provides the necessary \textbf{abstractions}.
        \begin{itemize}
          \item \verb$GOTO$ $\leadsto$ sequential structured programming.
          \item \verb$send/receive$, \verb$fork/join$, shared memory, etc. \\ $\leadsto$ structured parallelism using common \textbf{parallel patterns}.
          \item Reason about \textbf{structure}, not about the low level details.
          \item Predictability.
        \end{itemize}
      \end{itemize}
\end{frame}

\begin{frame}[fragile]{Motivation}
      \begin{itemize}
        \item \textbf{How do we \emph{statically} choose the right parallel structure?}
        \begin{itemize}
          \item How do we get the \emph{best} speedup?
          \item The choice is architecture dependent.
          \item Implies changing the program structure several times.
          \item Profiling or using cost models on alternative structures.
        \end{itemize}
  \end{itemize}
\end{frame}

%ALGORITHMIC SKELETONS
\begin{frame}{Structured Parallelism}
  \begin{itemize}
    \item Think about a parallel program in terms of a \emph{high-level} parallel structure.
    \item Algorithmic skeletons: parametric implementations of common patterns of parallelism.
      \begin{itemize}
    %    \item Murray Cole, 1989.
        \item Remove sources of errors: deadlocks, race conditions, synchronisation, etc.
    %    \item Structured parallelism by using nested algorithmic skeletons.
    %    \item High-level structure.
        \item Predictable.
      \end{itemize}
    \item Examples: task farms, parallel pipelines, feedback loops, divide and conquer, \ldots
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Algorithmic Skeletons}

  \begin{figure}
    \centering
    \begin{subfigure}[b]{.49\textwidth}
      \centering
    \resizebox{\textwidth}{!}{%
    \begin{tikzpicture}
     \draw (-0.2,1.25) [thick] rectangle (3,1.75) node[midway]{$\ldots,x_{12}, x_{11},x_{10}$};
     \draw (5,0)    [thick] circle [radius=0.5] node{$f$};
     \draw (5,1.5)  [thick] circle [radius=0.5] node{$f$};
     \draw (5,3)    [thick] circle [radius=0.5] node{$f$};
     \draw (7,1.25) [thick] rectangle (10.2,1.75) node[midway]{$f~x_2,f~x_1,f~x_3,\ldots$};

     \path [->] (3,1.5) edge node [above=3.5pt, near start] {$x_7$} (4.5,3);
     \path [->] (3,1.5) edge node [above, midway] {$x_9$} (4.5,1.5);
     \path [->] (3,1.5) edge node [below=3pt, near start] {$x_8$} (4.5,0);

     \path [->] (5.5,3)   edge node [above=3.5pt, near end] {$f~x_5$} (7,1.5);
     \path [->] (5.5,1.5) edge node [above, midway] {$f~x_6$} (7,1.5);
     \path [->] (5.5,0)   edge node [below=3pt, near end] {$f~x_4$} (7,1.5);
    \end{tikzpicture}
}
\caption{Task Farms}
    \end{subfigure}
    \begin{subfigure}[b]{.49\textwidth}
      \centering
    \resizebox{\textwidth}{!}{%
  \begin{tikzpicture}
  \draw (-1,0.25) [thick] rectangle (3.5,0.75) node[midway]{$\ldots,~x_9,~f~(f~x_7),~f~x_{2}$};
  \draw (5.5,0.5)  [thick] circle [radius=0.5] node{$f$};
  \draw (7.5,0.25) [thick] rectangle (11,0.75) node[midway]{$f~(f~x_1),~f~x_3,~\ldots$};

  \draw [->] (3.5,0.5) to node[above]{$x_6$} (5,0.5);
  \draw [->] (6,0.5) to node[above]{$f~x_5$} (7.5,0.5);
  \draw [->] (6.75, 0.5) to (6.75, -0.5) to node[below]{$f~x_4$} (1.75, -0.5) to (1.75,0.25);
  \node at(0,-1.65) {};
  \end{tikzpicture}
}
\caption{Feedback Loop}
  \end{subfigure}

  \begin{subfigure}[b]{.49\textwidth}
      \centering
    \resizebox{\textwidth}{!}{%
  \begin{tikzpicture}
    \draw (0,2.25) [thick] rectangle (3.5,2.75) node[midway]{$\ldots, x_{12}, x_{11}, x_{10}$};
    \draw (5.5,2.5)  [thick] circle [radius=0.5] node{$f$};

    \draw (1   ,0.25) [thick] rectangle (4.5,0.75) node[midway]{$f~x_7,f~x_6, f~x_5$};
    \draw (6.5 ,0.5)  [thick] circle [radius=0.5] node{$g$};
    \draw (8.5 ,0.25) [thick] rectangle (12.2,0.75) node[midway]{$g~(f~x_2), g~(f~x_1), \ldots$};

    \path [->] (3.5  ,2.5) edge node [above] {$x_9$} (5,2.5);

    \draw [->] (6  ,2.5)  to
               (6.5, 2.5) to [out=0, in=0]
               (6.5, 1.5) to node [above] {$f~x_8$}
               (0.5, 1.5) to [out=180, in=180]
               (0.5, 0.5) to (1, 0.5);

    \path [->] (4.5,0.5) edge node [above] {$f~x_4$} (6,0.5);
    \path [->] (7,0.5) edge node [above] {$g~(f~x_3)$} (8.5,0.5);

    \node at(0,-3) {};
  \end{tikzpicture}
}
\caption{Parallel Pipeline}
  \end{subfigure}
  \begin{subfigure}[b]{.49\textwidth}
      \centering
    \resizebox{!}{.4\textheight}{%
\begin{tikzpicture}
  %DIV
  \draw (0,10) [thick] node[draw,circle,minimum size=1cm] (d)  {$\mathit{div}$};

  \draw[->] (-1,11) to [out=0, in=90] node [above] {$x$} (d) ;

  %DIV
  \draw (-6,8) [thick] node[draw,circle,minimum size=1cm] (d1) {$\mathit{div}$};
  \draw (0 ,8) [thick] node[draw,circle,minimum size=1cm] (d2) {$\mathit{div}$};
  \draw (6 ,8) [thick] node[draw,circle,minimum size=1cm] (d3) {$\mathit{div}$};

  %arrows
  \draw[->] (d) -- node[left] {$x_1$} (d1);
  \draw[->] (d) -- node[left] {$x_2$} (d2);
  \draw[->] (d) -- node[right] {$x_3$} (d3);

  %DIV
  \draw (-8,6) [thick] node[draw,circle,minimum size=1cm] (d11) {$\mathit{div}$};
  \draw (-6,6) [thick] node[draw,circle,minimum size=1cm] (d12) {$\mathit{div}$};
  \draw (-4,6) [thick] node[draw,circle,minimum size=1cm] (d13) {$\mathit{div}$};
  \draw (-2,6) [thick] node[draw,circle,minimum size=1cm] (d21) {$\mathit{div}$};
  \draw (0 ,6) [thick] node[draw,circle,minimum size=1cm] (d22) {$\mathit{div}$};
  \draw ( 2,6) [thick] node[draw,circle,minimum size=1cm] (d23) {$\mathit{div}$};
  \draw ( 4,6) [thick] node[draw,circle,minimum size=1cm] (d31) {$\mathit{div}$};
  \draw ( 6,6) [thick] node[draw,circle,minimum size=1cm] (d32) {$\mathit{div}$};
  \draw ( 8,6) [thick] node[draw,circle,minimum size=1cm] (d33) {$\mathit{div}$};

  %arrows
  \draw[->] (d1) -- node[left] {$x_{11}$} (d11);
  \draw[->] (d1) -- node[left] {$x_{12}$} (d12);
  \draw[->] (d1) -- node[right] {$x_{13}$} (d13);
  \draw[->] (d2) -- node[left] {$x_{21}$} (d21);
  \draw[->] (d2) -- node[left] {$x_{22}$} (d22);
  \draw[->] (d2) -- node[right] {$x_{23}$} (d23);
  \draw[->] (d3) -- node[left] {$x_{31}$} (d31);
  \draw[->] (d3) -- node[left] {$x_{32}$} (d32);
  \draw[->] (d3) -- node[right] {$x_{33}$} (d33);

  %CONQ
  \draw (-8,4) [thick] node[draw,circle,minimum size=1cm] (c11) {$\mathit{conq}$};
  \draw (-6,4) [thick] node[draw,circle,minimum size=1cm] (c12) {$\mathit{conq}$};
  \draw (-4,4) [thick] node[draw,circle,minimum size=1cm] (c13) {$\mathit{conq}$};
  \draw (-2,4) [thick] node[draw,circle,minimum size=1cm] (c21) {$\mathit{conq}$};
  \draw (0 ,4) [thick] node[draw,circle,minimum size=1cm] (c22) {$\mathit{conq}$};
  \draw ( 2,4) [thick] node[draw,circle,minimum size=1cm] (c23) {$\mathit{conq}$};
  \draw ( 4,4) [thick] node[draw,circle,minimum size=1cm] (c31) {$\mathit{conq}$};
  \draw ( 6,4) [thick] node[draw,circle,minimum size=1cm] (c32) {$\mathit{conq}$};
  \draw ( 8,4) [thick] node[draw,circle,minimum size=1cm] (c33) {$\mathit{conq}$};

  %arrows
  \draw[->] (d11) -- node[left] {$y_{11}$}(c11);
  \draw[->] (d12) -- node[left] {$y_{12}$}(c12);
  \draw[->] (d13) -- node[right] {$y_{13}$}(c13);
  \draw[->] (d21) -- node[left] {$y_{21}$}(c21);
  \draw[->] (d22) -- node[left] {$y_{22}$}(c22);
  \draw[->] (d23) -- node[right] {$y_{23}$}(c23);
  \draw[->] (d31) -- node[left] {$y_{31}$}(c31);
  \draw[->] (d32) -- node[left] {$y_{32}$}(c32);
  \draw[->] (d33) -- node[right] {$y_{33}$}(c33);

  %CONQ
  \draw (-6,2) [thick] node[draw,circle,minimum size=1cm] (c1) {$\mathit{conq}$};
  \draw (0 ,2) [thick] node[draw,circle,minimum size=1cm] (c2) {$\mathit{conq}$};
  \draw (6 ,2) [thick] node[draw,circle,minimum size=1cm] (c3) {$\mathit{conq}$};

  %arrows
  \draw[->] (c11) -- node[left] {$y_{11}$}(c1);
  \draw[->] (c12) -- node[left] {$y_{12}$}(c1);
  \draw[->] (c13) -- node[right] {$y_{13}$}(c1);
  \draw[->] (c21) -- node[left] {$y_{21}$}(c2);
  \draw[->] (c22) -- node[left] {$y_{22}$}(c2);
  \draw[->] (c23) -- node[right] {$y_{23}$}(c2);
  \draw[->] (c31) -- node[left] {$y_{31}$}(c3);
  \draw[->] (c32) -- node[left] {$y_{32}$}(c3);
  \draw[->] (c33) -- node[right] {$y_{33}$}(c3);

  %CONQ
  \draw (0 ,0) [thick] node[draw,circle,minimum size=1cm] (c) {$\mathit{conq}$};

  %arrows
  \draw[->] (c1) -- node[left] {$y_{1}$}(c);
  \draw[->] (c2) -- node[left] {$y_{2}$}(c);
  \draw[->] (c3) -- node[right] {$y_{3}$}(c);

  \draw[->] (c) to [out=270, in= 180] node[below] {$y$} (1,-1);

\end{tikzpicture}
}
\caption{Divide and Conquer}
\end{subfigure}
\end{figure}

\end{frame}

%EXAMPLE
\begin{frame}{Quicksort.}
  \only<1>{\phantom{Sources of parallelism: a {\color{red}streaming computation} of a {\color{blue}D\&C.}}}
  \only<2>{Sources of parallelism: a {\color{red}streaming computation} of a {\color{blue}D\&C.}}
  \only<3,4>{The \emph{structure-annotated} arrow, {$\xmapsto{{\only<3>{\color{blue}}\only<4>{\color{red}}\sigma}}$}, introduces parallelism. }
  \only<5>{\alert<5->{$\min$}, fills the holes, \alert<5->{$~\any~$}, so that \alert<5->{$\nn{cost}$} is minimised. }
  \[
    \begin{array}{lll}
      % \multicolumn{3}{l}{\onslide<3->{\only<3>{\vv{F}~\vv{A}~\vv{B}=~1+\vv{A}\times\vv{B}\times\vv{B}}}} \\
      \multicolumn{3}{l}{\onslide<3->{\only<3>{\color{blue}{\sigma~=~\nn{DC}~\any~\any}} \only<4->{\alert<4-5>{\sigma~=~\only<5>{\min~\nn{cost}~(}{\color{blue}\nn{FARM}~\nn{n}~\any}~\parallel~\any\only<5>{)}}}  }} \\ \\
      \nn{qss} &\multicolumn{2}{l}{:~\nn{Stream}~(\nn{List}~\var{A})\only<1-2>{\to}\only<3->{\xmapsto{{\only<3>{\color{blue}}\only<4->{\color{red}}\sigma}}}\nn{Stream}(\nn{List}~\var{A})} \\
      \multicolumn{2}{l}{\alert<2,4>{\nn{qss}}~\nn{nil}} &=~\nn{nil} \\
      \multicolumn{2}{l}{\alert<2,4>{\nn{qss}}~(\nn{cons}~\vx~\vxs)} & =~\nn{cons}~({\only<2-4>{\color{blue}}\nn{qs}}~\vx)~(\alert<2,4>{\nn{qss}}~\vxs) \\\\

      \nn{qs} &\multicolumn{2}{l}{:~\nn{List}~\var{A}\to\nn{List}~\var{A}} \\
      \multicolumn{2}{l}{{\only<2-3>{\color{blue}}\nn{qs}}~[]} & =~[] \\
      \multicolumn{2}{l}{{\only<2-3>{\color{blue}}\nn{qs}}~\var{xs}} & =~{\only<2-3>{\color{blue}}\nn{qs}}~\nn{lt}\doubleplus[x]\doubleplus{\only<2-3>{\color{blue}}\nn{qs}}~\nn{ge} \\
                               & \multicolumn{2}{l}{\quad \bt{where}~(x, \nn{lt},\nn{ge})~=~\nn{split}~\var{cmp}~\var{xs}}
    \end{array}
  \]
\end{frame}

\begin{frame}{Our Approach.}
  \begin{itemize}
      \item The previous example shows many problems:
        \begin{itemize}
          \item It is too general (general recursive functions).
          \item We need to:
              \begin{enumerate}
                  \item relate \emph{sequential} structures with \emph{parallel} structures;
                  \item allow rewritings between equivalent structures, and;
                  \item provide a mechanism for reasoning about cost.
              \end{enumerate}
       \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Our Approach.}
        \begin{itemize}
          \item Solution: focus on an intermediate step.
          \item Define a language that uses implicit recursion patterns (\emph{hylomorphisms}) instead of explicit recursion.
            \begin{itemize}
              \item They represent our \emph{sequential} structures.
              \item Use well-known laws of hylomorphisms for doing rewritings.
              \item There are algorithms to convert a functional language to a composition of hylomorphisms (pointfree style).
            \end{itemize}
        \end{itemize}
\end{frame}

\begin{frame}{Our Approach.}
        \begin{itemize}
          \item Expose the \emph{structure}, $\sigma\in\Sigma$, an abstraction of the AST of the program, at the type level.
            \begin{itemize}
                \item Provides a mechanism for statically reasoning about rewritings.
                \item We can define \emph{cost models} on $\Sigma$, and use them in the types.
                \item The cost models are not built in. We can define different cost models depending
                  on the architecture. E.g:
                  \[
                    \begin{array}{lcl}
                      \multicolumn{3}{l}{\nn{cost}~:~\Sigma\to\nn{Cost}} \\
                      \ldots \\
                      \nn{cost}~(\nn{FARM}~\vn~\sigma) & = & \frac{\nn{cost}~\sigma~+~\kappa_1(\vn)}{\vn}~+~\kappa_2 \\
                      \nn{cost}~(\sigma_1~\parallel~\sigma_2)  & = & \max\{\nn{cost}~\sigma_1,\nn{cost}~\sigma_2\}~+~\kappa \\
                      \ldots \\
                    \end{array}
                  \]
            \end{itemize}
        \end{itemize}
\end{frame}


%DENOTATIONAL SEMANTICS
\begin{frame}{Denotational Semantics of Algorithmic Skeletons}
\[
  \vp \in \Parp  \quad\Coloneqq
  \quad  \pseq{\vT}~\vf~|~\dc{\vn,\vT,\vF}{\vf~\vg}~|~\pipe{\vp_1}{\vp_2}~|~\farm~\vn~\vp~|~\fb{\vp}
\]
\begin{center}
  \[
    \begin{array}{lcl}
      \ssem{\vp~:~\vT~\vA\to\vT~\vB} &~:~& \vA\to\vB \\
      \ssem{\pseq{\vT}~\vf} & = & \hat{\rho}(\vf) \\

      \ssem{\pipe{\vp_1}{\vp_2}} & = & \ssem{\vp_2}\circ\ssem{\vp_1} \\

      \ssem{\farm~\vn~\vp} & = & \ssem{\vp} \\

      \ssem{\fb{\vp}} & = & \iter~\ssem{\vp} \\

      \ssem{\dc{\vn,\vT,\vF}{\vf~\vg}} & = & \var{fold}_\vF~\hat{\rho}(\vf)\circ\var{unfold}_\vF~\hat{\rho}(\vg) \\ \\
      \psem{\vp~:~\vT~\vA\to\vT~\vB} &~:~& \vT~\vA\to\vT~\vB \\
      \psem{\vp} & = & \map{\vT}~\ssem{\vp}

    \end{array}
  \]
\end{center}

\end{frame}

%HYLOMORPHISMS
\begin{frame}{Hylomorphisms}
  \begin{itemize}
    \item Hylomorphisms: equivalent to first \emph{unfolding} a data structure, and then \emph{folding} it.
      \[
         \begin{array}{lcl}
           \hylo{\vF} &:& (\vF~\vB\to\vB)\to(\vA\to\vF~\vA)\to\vA\to\vB \\
           \hylo{\vF}~\vf~\vg &=& \vf\circ\vF~(\hylo{\vF}~\vf~\vg)\circ\vg \\\\
         \end{array}
      \]
    \item $\map{\vT}$, $\iter$, $\var{fold}_\vF$ and $\var{unfold}_\vF$ are
      specific instances of \emph{hylomorphisms}.

      \[
         \begin{array}{lcl}
         \map{\vT}~\vf &~=~& \hylo{\vF\vA}~(\hin{\vF}\circ\vF~\vf~\Idf)~\hout{\vF} \\
                       & & \quad\text{where $\vT~\vA~=~\mu(\vF~\vA)$,}  \\ & & \quad\text{and $\vA~=~\nn{dom}(\vf)$.} \\
           \var{fold}_\vF~\vf&~=~&\hylo{\vF}~\vf~\hout{\vF} \\
           \var{unfold}_\vF~\vf&~=~&\hylo{\vF}~\hin{\vF}~\vf \\
           \var{iter}~\vf&~=~&\hylo{\vF}~(\Idf\triangledown\Idf)~\vf \\
         \end{array}
      \]
  \end{itemize}
\end{frame}

\begin{frame}{Example}
\begin{center}
\[
  \begin{array}{lcll}
    \multicolumn{4}{l}{\vT~\vA~\vB~:=~1+\vA\times\vB\times\vB} \\
    \keyword{split} &~:~& \List~\vA\to\vT~\vA~(\List~\vA) & \\
    \keyword{split}~\nn{nil} & = & \inl~() & \\
    \keyword{split}~(\nn{cons}~\vx~\vxs) & = & \inr~(\vx,~\keyword{leq}~\vx~\vxs,~\keyword{gt}~\vx~\vxs) &  \\ \\

    \keyword{combine} &~:~& \vT~\vA~(\List~\vA)\to\List~\vA & \\
    \keyword{combine}~(\inl~()) & = & [] &  \\
    \keyword{combine}~(\inr~(\vx,\vl,\vr)) & = & \vl\doubleplus[x]\doubleplus\vr & \\ \\

    \keyword{qsort} &~:~& \List~\vA\to\List~\vA & \\
    \keyword{qsort} & = & \hylo{\vT\,\vA}~\keyword{combine}~\keyword{split} & \\
  \end{array}
\]
\end{center}

\end{frame}

%THE PAR LANGUAGE
\begin{frame}{Structured Expressions}
  \begin{itemize}
    \item We introduce a small language with a type system that relates simple \textcolor{blue}{\emph{expressions}} and \emph{\textbf{parallel}} structures.
    \item A \textcolor{red}{\emph{program}} can be either a \textcolor{blue}{\emph{structured expression}} or a \emph{\textbf{parallel}} process.
    \item The language of \textcolor{blue}{\emph{structured expressions}} allows a simple specification of the functional behaviour of a program.
    \item Our type system describes how to rewrite a $\vS$ to a $\vP$.
  \end{itemize}

\begin{center}
$
  \begin{array}{lcl}
    \textcolor{red}{\ve \in \vE} & \Coloneqq & \textcolor{blue}{\vs} \quad | \quad  \textcolor{red}{\nn{par}}~\vp\\
    \textcolor{blue}{\vs \in \Seqp}  & \Coloneqq & \textcolor{blue}{\vf} \quad|\quad
    \textcolor{red}{\ve_1}~\textcolor{blue}{\bullet}~\textcolor{red}{\ve_2} \quad|\quad \textcolor{blue}{\shylo{\vF}}~\textcolor{red}{\ve_1}~\textcolor{red}{\ve_2} \\

  \vp \in \Parp & \Coloneqq &  \pseq{\vT}~\textcolor{blue}{\vs}~|~\dc{\vn,\vT,\vF}{\textcolor{blue}{\vs_1~\vs_2}}~|~\pipe{\vp_1}{\vp_2}~|~\farm~\vn~\vp~|~\fb{\vp}
\end{array}
$
\end{center}
\end{frame}

%THE TYPE SYSTEM
\begin{frame}{The Type System: an Example Rule}
  \resizebox{.8\textwidth}{!}{%
    \begin{minipage}{\textwidth}
\begin{center}
\[
\begin{array}{c}
%%  \inference{}{\tyd\sid~:~\vA\xmapsto{\ID}\vA}\hspace{\columnsep}
%%  \inference{}{\tyd\shin{\vF}~:~\vF(\mu\vF)\xmapsto{\IN}\mu\vF}\hspace{\columnsep}
%%  \inference{}{\tyd\shout{\vF}~:~\mu\vF\xmapsto{\OUT}\vF(\mu\vF)}\\\\%\hspace{\columnsep}
%
%  \inference{\rho(\vf)=\vA\to\vB}{\tyd\vf~:~\vA\xmapsto{\AF}\vB}\hspace{\columnsep}
%%  \inference{\tyd\vs~:~\vA\xmapsto{\sigma}\vB}{\tyd\vF~\vs~:~\vF~\vA~\vC\xmapsto{\vF~\sigma}\vF~\vB~\vC}\hspace{\columnsep}
%  \inference{\tyd\vs_1~:~\vB\xmapsto{\sigma_1}\vC & \tyd\vs_2~:~\vA\xmapsto{\sigma_2}\vB}
%            {\tyd\scomp{\vs_1}{\vs_2}~:~\vA\xmapsto{\COMP{\sigma_1}{~\sigma_2}}\vC}\\\\%\hspace{\columnsep}
%
%            \inference{\tyd\vs_1~:~\vF~\vB\xmapsto{\sigma_1}\vB & \tyd\vs_2~:~\vA\xmapsto{\sigma_2}\vF~\vA & \vG~=~\keyword{base}~\vF}
%            {\tyd\shylo{\vF}~\vs_1~\vs_2~:~\vA\xmapsto{\HYLO{\vG}~\sigma_1~\sigma_2}\vB}
%%\hspace{\columnsep}
%%  \inference{\tyd\vs~:~\vA\xmapsto{\sigma_1}\vB & \sigma_1\equiv\sigma_2}
%%            {\tyd\vs~:~\vA\xmapsto{\sigma_2}\vB}
%\\\\
%  \inference{\tyd~{\vs}~:~\vA\xmapsto{\sigma}\vB & \vF~=~\keyword{base}~\vT}
%          {\tyd~\pseq{\vT}{\vs}~:~\vT~\vA\xmapsto{\FUNC{\vF}~\sigma}\vT~\vB}\hspace{\columnsep}
%  \inference{\vn~:~\mathbb{N} & \tyd\vp~:~\vT~\vA\xmapsto{\sigma}\vT~\vB}{\tyd\farm~\vn~\vp~:~\vT~\vA\xmapsto{\FARM{\vn}~\sigma}\vT~\vB}
%          \hspace{.5\columnsep}\\\\
%  \inference{\tyd\vp_1~:~\vT~\vA\xmapsto{\sigma_1}\vT~\vB & \tyd\vp_2~:~\vT~\vB\xmapsto{\sigma_2}\vT~\vC}
%          {\tyd\vp_1\parallel\vp_2~:~\vT~\vA\xmapsto{\sigma_1~\parallel~\sigma_2}\vT~\vC}\hspace{.5\columnsep}
%  \inference{\tyd\vp~:~\vT~\vA\xmapsto{\sigma}\vT~(\vA+\vB)}{\tyd\fb~\vp~:~\vT~\vA\xmapsto{\FB~\sigma}\vT~\vB}\\\\ %\hspace{.5\columnsep}
  \inference{\tyd\vs_1~:~\vF~\vB\xmapsto{\textcolor{red}{\sigma_1}}\vB &
  \tyd\vs_2~:~\vA\xmapsto{\textcolor{red}{\sigma_2}}\vF~\vA & \vG_1~=~\keyword{base}~\vT &
                    \vG_2~=~\keyword{base}~\vF}
        {\tyd\dc{\vn,\vT,\vF}~\vs_1~\vs_2~:~\vT~\vA\xmapsto{\textcolor{red}{\DC{\vn,\vG_1\vG_2}~\sigma_1~\sigma_2}}\vT~\vB}
\end{array}
\]
\end{center}
\end{minipage} }
\vspace{.5cm}
\begin{itemize}
  \item The typing rules \emph{lift} the structure, \textcolor{red}{$\sigma$}, of a program to the type level.
  \item The \emph{structure} can be thought as the AST, \emph{pruned}, of any functionality: we just keep the structure.
  \item We use a \emph{convertibility} relation for different structures.
\end{itemize}
\end{frame}

%CONVERTIBILITY
\begin{frame}{Convertibility}
  \begin{itemize}
      \item We add an extra rule to allow reasoning about rewritings.
\[
\begin{array}{c}
  \inference{\tyd\vs~:~\vA\xmapsto{\sigma_1}\vB & \sigma_1\equiv\sigma_2 }
            {\tyd\vs~:~\vA\xmapsto{\sigma_2}\vB}\hspace{.5\columnsep}
\end{array}
\] \\

\[
\begin{array}{c c c}
  \inference{\sigma_1\textcolor{red}{\equiv_s}\sigma_2}{\sigma_1\equiv\sigma_2} &
  \inference{\sigma_1\textcolor{blue}{\equiv_p}\sigma_2}{\sigma_1\equiv\sigma_2} & \inference{}{\PEVAL~(\FUNC{\vF}~\sigma)\equiv\MAP{\vF}~\sigma} \\
\end{array}
\]
  \item The equivalence $\equiv\in\Sigma\times\Sigma$  is described in two levels, one in \textcolor{red}{$\vS$} and other in \textcolor{blue}{$\vP$}, plus a rule that relates both.
  \end{itemize}
\end{frame}

\begin{frame}{Convertibility}
  \begin{itemize}

  \item The equivalence $\equiv$ can be easily lifted to $\mathcal{R}\in\vE\times\vE$.
  \item We need a confluent term rewriting system to build a decision procedure for $\equiv$.
  \item That decision procedure, and the correspondence between $\equiv$ and $\mathcal{R}$ is what allows us to rewrite a well-typed program to the desired parallel form (soundness).
  \item[]
  \item $\equiv$ is derived from well-known laws of hylomorphisms, and the denotational semantics of
    parallel processes. E.g.\
    \vspace{.5cm}

\begin{center}
\begin{small}
$\begin{array}{l l}
  \FUNC{\vT}~\sigma_1\parallel\FUNC{\vT}~\sigma_2 &\equiv_p\FUNC{\vT}(\COMP{\sigma_2}{\sigma_1})\\
%  \DC{\vn,\vT,\vF}~\sigma_1~\sigma_2 &\equiv_p\FUNC{\vT}(\HYLO{\vF}~\sigma_1~\sigma_2) \\
%  \FARM{\vn}~\sigma&\equiv_p~\sigma\\
%  \FB(\FUNC{\vT}~\sigma)&\equiv_p\FUNC{\vT}(\HYLO{(+\vB)}~\AF~\sigma)\\ \\ % \hspace{.
  \\ \HYLO{\vF}~\sigma_1~\sigma_2&\equiv_s\HYLO{\vF}~\sigma_1~\OUT\bullet\HYLO{\vF}~\IN~\sigma_2 \\
\end{array}$
\end{small}
\end{center}

  \end{itemize}
\end{frame}

\begin{frame}{Soundness and (Partial) Completeness.}
$
\forall\ve\in\vE,\sigma%
\in\Sigma,
\ \ \tyd\ve:\vA\xmapsto{\sigma}\vB\implies\tyd\ve:\vA\to\vB%
$.
\vspace{1cm}

{\small We define $|\vS|$ as $\vS$ with no occurences of $\peval$ (i.e.\ with no nested parallelism),}
{\small and $\vdash\ve:\vA\overset{\sigma}{\rightarrowtail}\vB$ as $\vdash\ve:\vA\to\vB$ s.t. $\ve$ has structure $\sigma$.}

\vspace{.5cm}

\begin{theorem}{Soundness of Conversion.}
$
\forall\vs\in|\vS|,\sigma%
\in\Sigma,
\ \ \tyd\vs:\vA\xmapsto{\sigma}\vB%
\ \Rightarrow\  \exists\ve\in\vE~\text{s.t.}~\sem{\ve}=_{\text{ext}}\sem{\vs}~\wedge~\tyd\ve:\vA\overset{\sigma}{\rightarrowtail}\vB%
$
\end{theorem}

\begin{theorem}{Partial Completeness of Conversion.}
$
\forall\vs\in|\vS|, \ve\in\vE, \sigma%
\in\Sigma,
\ \ (\tyd\ve:\vA\overset{\sigma}{\rightarrowtail}\vB~\wedge~\ve~\mathcal{R}~\vs)
\ \Rightarrow\ \ \tyd\vs:\vA\xmapsto{\sigma}\vB%
$
\end{theorem}
\end{frame}

%EXAMPLES
\begin{frame}{Example: Quicksort Revisited}
$
\begin{array}{l l}
  \sigma &=~\DC{\vn,\vL,\vF}\,\AF\,\AF \\ \\
  \keyword{qsorts}&~:~\List(\List~\vA)\xmapsto{\sigma}\List(\List~\vA) \\
  \keyword{qsorts}&~=~\smap{\List}~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div})
\end{array}
$

\vspace{.5cm}
\begin{itemize}
\item The type system has to decide whether the following equivalence holds:

  \vspace{.3cm}
$
\MAP{\vL} (\HYLO{\vF}\,\AF\,\AF)\equiv\DC{\vn,\vL,\vF}\,\AF\,\AF%
$
  \vspace{.3cm}

\item
This can be trivially done by applying the following rewriting step:
  \vspace{.3cm}

$
\DC{\vn,\vL,\vF}~\AF~\AF%
\rightsquigarrow~\MAP{\vL}~(\HYLO{\vF}~\AF~\AF)
$
\end{itemize}

\end{frame}

\begin{frame}{Example: Quicksort Revisited}
\begin{itemize}
\item The convertibility proof can be lifted from $\Sigma$ to $\vE$, and it provides
  the necessary rewriting steps to convert the original structured expression to the
  desired parallel form:

  \vspace{.5cm}
$
\smap{\List}~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div})\rightsquigarrow\dc{\vn,\List,\vF}~\keyword{merge}~\keyword{div}
$
\end{itemize}

\end{frame}

\begin{frame}{Example: Quicksort Revisited}
$
\uncover<1,3->{\MAP{\vL}~(\HYLO{\vF}~\AF~\AF)}~\equiv~\uncover<1,2,4->{\PEVAL~(\PIPE{\FARM{\vn}~\sigma_2}{\sigma_1})}
$

\onslide<2->{%
\vspace{.5cm}

\begin{minipage}{\textwidth}
$
\begin{array}{l}
  \uncover<2>{  \PEVAL~(\PIPE{\FARM~\vn~\sigma_2}{\sigma_1}) } \\
  \uncover<2>{  \rightsquigarrow~\PEVAL~(\PIPE{\sigma_2}{\sigma_1})} \\
   \uncover<2>{  \rightsquigarrow~\PEVAL~(\FUNC{\vf}~(\COMP{\sigma_1'}{\sigma_2'}))
      \quad \sigma_1~\sim~\FUNC{\vf}~\sigma_1'\quad%
          \wedge\quad\sigma_2~\sim~\FUNC{\vf}~\sigma_2'} \\
 \uncover<2>{ \rightsquigarrow~\MAP{\vf}~(\COMP{\sigma_1'}{\sigma_2'})
            \quad  \hspace{1cm}\sigma_1~\sim~\FUNC{\vf}~\sigma_1'\quad%
          \wedge\quad\sigma_2~\sim~\FUNC{\vf}~\sigma_2' }\\
\uncover<2,4>{ \rightsquigarrow~\MAP{\vf}~\sigma_1'\bullet\MAP{\vf}~\sigma_2'
            \quad  \hspace{.25cm}\sigma_1~\sim~\FUNC{\vf}~\sigma_1'\quad%
          \wedge\quad\sigma_2~\sim~\FUNC{\vf}~\sigma_2'}
\end{array}
$
\end{minipage}

\vspace{.5cm}

\begin{minipage}{\textwidth}
$
\begin{array}{l}
  \uncover<3>{  \MAP{\vL}~(\HYLO{\vF}~\AF~\AF) } \\
 \uncover<3,4>{ \rightsquigarrow~\MAP{\vL}~(\CATA{\vF}~\AF)~\bullet~\MAP{\vL}~(\ANA{\vF}~\AF)} \\
\end{array}
$
\end{minipage}

\vspace{.5cm}

\uncover<4->{%
\begin{minipage}{\textwidth}
$\begin{array}{l}
  \sigma_1~\sim~\FUNC{\vf}~~~\sigma_1'\quad\wedge\quad\sigma_2~\sim~\FUNC{\vf}~\sigma_2'\quad\wedge \\\sigma_1'~\sim~\CATA{\vF}~\AF~\quad\wedge\quad\sigma_2'~\sim~\ANA{\vF}~\AF
\end{array}$
\end{minipage}
}

\vspace{.5cm}

\uncover<4->{%
\begin{minipage}{\textwidth}
$\smap{\List} (\shylo{\vF\,\vA}\keyword{merge}~\keyword{div})\rightsquigarrow^{\text{*}}$

$\quad\peval~(\farm~\vn~(\pseq{\List}~(\sana{\vF\,\vA}~\keyword{div}))\parallel\pseq{\List} (\scata{\vF\,\vA}\keyword{merge}))$.
\end{minipage}
}

}

\end{frame}

%FUTURE WORK
\begin{frame}{Ongoing Work: A Prototype DSL}
  \begin{itemize}
    \item We are working on a prototype DSL in Haskell that can be easily
      extended with new parallel structures.
    \item Thanks to the type-based approach,
      even the specification of the structure and functional behaviour of the basic parallel structures that we showed here is not built in. E.g.
      \[
        \begin{array}{lcl}
        \multicolumn{3}{l}{\nn{newlevel}~\PEVAL} \\
        \multicolumn{3}{l}{\ldots} \\
        \multicolumn{3}{l}{\quad\nn{define}~(\sigma_1~\parallel~\sigma_2)} \\
        \multicolumn{3}{l}{\qquad\nn{pipe}~:~(\vT~\vA\xmapsto{\sigma_1}\vT~\vB)\to(\vT~\vB\xmapsto{\sigma_2}\vT~\vC)} \\ \multicolumn{3}{l}{\hspace{1.7cm}\to~\vT~\vA\xmapsto{\PIPE{\sigma_1}{\sigma_2}}\vT~\vC} \\
          \qquad\nn{pipe}~\vs_1~\vs_2 & = & \vs_2~\bullet~\vs_1 \\
        \multicolumn{3}{l}{\ldots} \\
        \end{array}
      \]

  \end{itemize}
\end{frame}

\begin{frame}{Future Work}
  \begin{itemize}
    \item Implement an efficient backend for the sequential structures: generate efficient C code from hylomorphisms.
    \item Define an operational semantics for our parallel structures:
      \begin{itemize}
          \item Implement a backend.
          \item Derive \emph{cost models} from this semantics.
          \item Validate the \emph{cost models}.
          \item Verify the parallel structures against the specification (soundness of the operational semantics).
      \end{itemize}
    \item Describe more complex rewritings in $\vS$, to expose more parallelisation alternatives.
      \begin{itemize}
          \item  E.g.\ the third list homomorphism theorem and generalisations.
          \item This theorem states that if a function can be implemented both
            as a \emph{foldl} and as a \emph{foldr}, then it has a divide and
            conquer parallel implementation as well.
      \end{itemize}
  \end{itemize}
\end{frame}

%CONCLUSIONS
\begin{frame}{Summary}
  \begin{itemize}
    \item We have defined a type system that captures the \emph{structure} of a program.
    \item The type system can use this structure to calculate how to introduce parallelism to a program in a sound way.
    \item Using the properties of hylomorphisms proved to be useful for
      \begin{enumerate}
          \item describing the structure of a program, and;
          \item rewriting this structure to introduce parallelism.
      \end{enumerate}
    \item By decoupling the structure from the functionality, we can define a number of analysis on the structure alone (e.g.\ cost models), and use them in our type system.
  \end{itemize}
\end{frame}


\end{document}
