\documentclass[pdf]{beamer}

\usepackage{adjustbox}
\usepackage{mathtools}
\usepackage{semantic}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{xcolor}

\mode<presentation>{}

\usetheme{boxes}
\usefonttheme[onlymath]{serif}

\input{../ICFP2016/macros.ltx}

\newcommand{\nn}[1]{\ensuremath{\mathtt{#1}}}
\newcommand{\bt}[1]{\ensuremath{\mathsf{#1}}}
\newcommand{\any}{\ensuremath{\text{\_}}}

\title{Farms, Pipes, Streams and Reforestation}
\subtitle{Reasoning about Structured Parallel Processes using Types and Hylomorphisms}

\author{{\small \textcolor{red}{David Castro} \and Kevin Hammond \and Susmit Sarkar \and Edwin Brady \\ \textcolor{red}{dc84@st-andrews.ac.uk}}}

\begin{document}

\begin{frame}
  \titlepage%
\end{frame}

%MOTIVATION
\begin{frame}[fragile]{Motivation}
  \begin{itemize}
    \item Reasoning about parallelism is a very important issue.
      \begin{itemize}
        \item Hardware is (and will keep) becoming increasingly parallel.
        \item Too many low-level error prone approaches are still being used.
      \end{itemize}
    \item Structured parallelism provides the necessary \textbf{abstractions}.
        \begin{itemize}
          \item \verb$GOTO$ $\leadsto$ sequential structured programming.
          \item \verb$send/receive$, \verb$fork/join$, shared memory, etc. \\ $\leadsto$ structured parallelism using common \textbf{parallel patterns}.
          \item Reason about \textbf{structure}, not about the low level details.
          \item Predictability.
        \end{itemize}
      \end{itemize}
\end{frame}

\begin{frame}[fragile]{Motivation}
      \begin{itemize}
      \item \textbf{How do we choose the right parallel structure?}
        \begin{itemize}
            \item How do we get the best speedup?
            \item Architecture dependent.
            \item Implies changing the program structure several times.
            \item Profiling.
            \item Cost models.
        \end{itemize}
  \end{itemize}
\end{frame}

%ALGORITHMIC SKELETONS
\begin{frame}{Structured Parallelism}
  \begin{itemize}
    \item Think about a parallel program in terms of a \emph{high-level} parallel structure.
    \item Algorithmic skeletons: parametric implementations of common patterns of parallelism.
      \begin{itemize}
        \item Murray Cole, 1989.
        \item Remove sources of errors: deadlocks, race conditions, synchronisation, etc.
        \item Structured parallelism by using nested algorithmic skeletons.
        \item High-level structure.
        \item Predictable.
      \end{itemize}
    \item Examples:
      \begin{itemize}
          \item Task farms.
          \item Parallel pipelines.
          \item Feedback.
          \item Divide and conquer.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Task Farm}
    \begin{center}
    \begin{tikzpicture}
     \draw (-0.2,1.25) [thick] rectangle (3,1.75) node[midway]{$\ldots,x_{12}, x_{11},x_{10}$};
     \draw (5,0)    [thick] circle [radius=0.5] node{$f$};
     \draw (5,1.5)  [thick] circle [radius=0.5] node{$f$};
     \draw (5,3)    [thick] circle [radius=0.5] node{$f$};
     \draw (7,1.25) [thick] rectangle (10.2,1.75) node[midway]{$f~x_2,f~x_1,f~x_3,\ldots$};

     \path [->] (3,1.5) edge node [above=3.5pt, near start] {$x_7$} (4.5,3);
     \path [->] (3,1.5) edge node [above, midway] {$x_9$} (4.5,1.5);
     \path [->] (3,1.5) edge node [below=3pt, near start] {$x_8$} (4.5,0);

     \path [->] (5.5,3)   edge node [above=3.5pt, near end] {$f~x_5$} (7,1.5);
     \path [->] (5.5,1.5) edge node [above, midway] {$f~x_6$} (7,1.5);
     \path [->] (5.5,0)   edge node [below=3pt, near end] {$f~x_4$} (7,1.5);
    \end{tikzpicture}
    \end{center}

\end{frame}

\begin{frame}[fragile]{Parallel Pipeline}
  \begin{center}
  \resizebox{\textwidth}{!}{%
  \begin{tikzpicture}
    \draw (0,2.25) [thick] rectangle (3.5,2.75) node[midway]{$\ldots, x_{12}, x_{11}, x_{10}$};
    \draw (5.5,2.5)  [thick] circle [radius=0.5] node{$f$};

    \draw (1   ,0.25) [thick] rectangle (4.5,0.75) node[midway]{$f~x_7,f~x_6, f~x_5$};
    \draw (6.5 ,0.5)  [thick] circle [radius=0.5] node{$g$};
    \draw (8.5 ,0.25) [thick] rectangle (12.2,0.75) node[midway]{$g~(f~x_2), g~(f~x_1), \ldots$};

    \path [->] (3.5  ,2.5) edge node [above] {$x_9$} (5,2.5);

    \draw [->] (6  ,2.5)  to
               (6.5, 2.5) to [out=0, in=0]
               (6.5, 1.5) to node [above] {$f~x_8$}
               (0.5, 1.5) to [out=180, in=180]
               (0.5, 0.5) to (1, 0.5);

    \path [->] (4.5,0.5) edge node [above] {$f~x_4$} (6,0.5);
    \path [->] (7,0.5) edge node [above] {$g~(f~x_3)$} (8.5,0.5);
  \end{tikzpicture}
  }
  \end{center}
\end{frame}

\begin{frame}{Feedback.}
  \begin{center}
  \resizebox{\textwidth}{!}{%
  \begin{tikzpicture}
  \draw (-1,0.25) [thick] rectangle (3.5,0.75) node[midway]{$\ldots,~x_9,~f~(f~x_7),~f~x_{2}$};
  \draw (5.5,0.5)  [thick] circle [radius=0.5] node{$f$};
  \draw (7.5,0.25) [thick] rectangle (11,0.75) node[midway]{$f~(f~x_1),~f~x_3,~\ldots$};

  \draw [->] (3.5,0.5) to node[above]{$x_6$} (5,0.5);
  \draw [->] (6,0.5) to node[above]{$f~x_5$} (7.5,0.5);
  \draw [->] (6.75, 0.5) to (6.75, -0.5) to node[below]{$f~x_4$} (1.75, -0.5) to (1.75,0.25);
  \end{tikzpicture}
  }
  \end{center}
\end{frame}


\begin{frame}{D\&C}
\begin{center}
  \resizebox{\textwidth}{!}{
\begin{tikzpicture}
  %DIV
  \draw (0,10) [thick] node[draw,circle,minimum size=1cm] (d)  {$\mathit{div}$};

  \draw[->] (-1,11) to [out=0, in=90] node [above] {$x$} (d) ;

  %DIV
  \draw (-6,8) [thick] node[draw,circle,minimum size=1cm] (d1) {$\mathit{div}$};
  \draw (0 ,8) [thick] node[draw,circle,minimum size=1cm] (d2) {$\mathit{div}$};
  \draw (6 ,8) [thick] node[draw,circle,minimum size=1cm] (d3) {$\mathit{div}$};

  %arrows
  \draw[->] (d) -- node[left] {$x_1$} (d1);
  \draw[->] (d) -- node[left] {$x_2$} (d2);
  \draw[->] (d) -- node[right] {$x_3$} (d3);

  %DIV
  \draw (-8,6) [thick] node[draw,circle,minimum size=1cm] (d11) {$\mathit{div}$};
  \draw (-6,6) [thick] node[draw,circle,minimum size=1cm] (d12) {$\mathit{div}$};
  \draw (-4,6) [thick] node[draw,circle,minimum size=1cm] (d13) {$\mathit{div}$};
  \draw (-2,6) [thick] node[draw,circle,minimum size=1cm] (d21) {$\mathit{div}$};
  \draw (0 ,6) [thick] node[draw,circle,minimum size=1cm] (d22) {$\mathit{div}$};
  \draw ( 2,6) [thick] node[draw,circle,minimum size=1cm] (d23) {$\mathit{div}$};
  \draw ( 4,6) [thick] node[draw,circle,minimum size=1cm] (d31) {$\mathit{div}$};
  \draw ( 6,6) [thick] node[draw,circle,minimum size=1cm] (d32) {$\mathit{div}$};
  \draw ( 8,6) [thick] node[draw,circle,minimum size=1cm] (d33) {$\mathit{div}$};

  %arrows
  \draw[->] (d1) -- node[left] {$x_{11}$} (d11);
  \draw[->] (d1) -- node[left] {$x_{12}$} (d12);
  \draw[->] (d1) -- node[right] {$x_{13}$} (d13);
  \draw[->] (d2) -- node[left] {$x_{21}$} (d21);
  \draw[->] (d2) -- node[left] {$x_{22}$} (d22);
  \draw[->] (d2) -- node[right] {$x_{23}$} (d23);
  \draw[->] (d3) -- node[left] {$x_{31}$} (d31);
  \draw[->] (d3) -- node[left] {$x_{32}$} (d32);
  \draw[->] (d3) -- node[right] {$x_{33}$} (d33);

  %CONQ
  \draw (-8,4) [thick] node[draw,circle,minimum size=1cm] (c11) {$\mathit{conq}$};
  \draw (-6,4) [thick] node[draw,circle,minimum size=1cm] (c12) {$\mathit{conq}$};
  \draw (-4,4) [thick] node[draw,circle,minimum size=1cm] (c13) {$\mathit{conq}$};
  \draw (-2,4) [thick] node[draw,circle,minimum size=1cm] (c21) {$\mathit{conq}$};
  \draw (0 ,4) [thick] node[draw,circle,minimum size=1cm] (c22) {$\mathit{conq}$};
  \draw ( 2,4) [thick] node[draw,circle,minimum size=1cm] (c23) {$\mathit{conq}$};
  \draw ( 4,4) [thick] node[draw,circle,minimum size=1cm] (c31) {$\mathit{conq}$};
  \draw ( 6,4) [thick] node[draw,circle,minimum size=1cm] (c32) {$\mathit{conq}$};
  \draw ( 8,4) [thick] node[draw,circle,minimum size=1cm] (c33) {$\mathit{conq}$};

  %arrows
  \draw[->] (d11) -- node[left] {$y_{11}$}(c11);
  \draw[->] (d12) -- node[left] {$y_{12}$}(c12);
  \draw[->] (d13) -- node[right] {$y_{13}$}(c13);
  \draw[->] (d21) -- node[left] {$y_{21}$}(c21);
  \draw[->] (d22) -- node[left] {$y_{22}$}(c22);
  \draw[->] (d23) -- node[right] {$y_{23}$}(c23);
  \draw[->] (d31) -- node[left] {$y_{31}$}(c31);
  \draw[->] (d32) -- node[left] {$y_{32}$}(c32);
  \draw[->] (d33) -- node[right] {$y_{33}$}(c33);

  %CONQ
  \draw (-6,2) [thick] node[draw,circle,minimum size=1cm] (c1) {$\mathit{conq}$};
  \draw (0 ,2) [thick] node[draw,circle,minimum size=1cm] (c2) {$\mathit{conq}$};
  \draw (6 ,2) [thick] node[draw,circle,minimum size=1cm] (c3) {$\mathit{conq}$};

  %arrows
  \draw[->] (c11) -- node[left] {$y_{11}$}(c1);
  \draw[->] (c12) -- node[left] {$y_{12}$}(c1);
  \draw[->] (c13) -- node[right] {$y_{13}$}(c1);
  \draw[->] (c21) -- node[left] {$y_{21}$}(c2);
  \draw[->] (c22) -- node[left] {$y_{22}$}(c2);
  \draw[->] (c23) -- node[right] {$y_{23}$}(c2);
  \draw[->] (c31) -- node[left] {$y_{31}$}(c3);
  \draw[->] (c32) -- node[left] {$y_{32}$}(c3);
  \draw[->] (c33) -- node[right] {$y_{33}$}(c3);

  %CONQ
  \draw (0 ,0) [thick] node[draw,circle,minimum size=1cm] (c) {$\mathit{conq}$};

  %arrows
  \draw[->] (c1) -- node[left] {$y_{1}$}(c);
  \draw[->] (c2) -- node[left] {$y_{2}$}(c);
  \draw[->] (c3) -- node[right] {$y_{3}$}(c);

  \draw[->] (c) to [out=270, in= 180] node[below] {$y$} (1,-1);

\end{tikzpicture}
}
\end{center}
\end{frame}


%EXAMPLE
\begin{frame}{Quicksort.}
  \only<2>{Sources of parallelism: a streaming computation of a D\&C.}
  \only<3-4>{The \emph{structure-annotated} arrow, \alert<3->{$\xmapsto{\sigma}$}, introduces parallelism. }
  \only<5>{\alert<5->{$\min$}, fills the holes, \alert<5->{$\any$}, so that \alert<5->{$\nn{cost}$} is minimised. }
  \[
    \begin{array}{lll}
      % \multicolumn{3}{l}{\onslide<3->{\only<3>{\vv{F}~\vv{A}~\vv{B}=~1+\vv{A}\times\vv{B}\times\vv{B}}}} \\
      \multicolumn{3}{l}{\onslide<3->{\only<3>{\alert<3>{\sigma~=~\nn{DC}~\any~\any}} \only<4->{\alert<4-5>{\sigma~=~\only<5>{\min~\nn{cost}~(}\nn{FARM}~\nn{n}~\any~\parallel~\any\only<5>{)}}}  }} \\ \\
      \nn{qss} &\multicolumn{2}{l}{:~\nn{Stream}~(\nn{List}~\var{A})\only<1-2>{\to}\only<3->{\alert<3->{\xmapsto{\sigma}}}\nn{Stream}(\nn{List}~\var{A})} \\
      \alert<2,4>{\nn{qss}}~\var{ls}~& |~\nn{empty}~\var{ls} &=~\var{ls} \\
                                   & |~\bt{otherwise} &=~\nn{qs}~(\nn{head}~\var{ls})\lhd\alert<2,4>{\nn{qss}}~(\nn{tail}~\var{ls}) \\\\

      \nn{qs} &\multicolumn{2}{l}{:~\nn{List}~\var{A}\to\nn{List}~\var{A}} \\
      \alert<2-3>{\nn{qs}}~\var{xs} & |~\nn{empty}~\var{xs} &=~\var{xs} \\
                                  & |~\bt{otherwise}     &=~\alert<2-3>{\nn{qs}}~\nn{lt}\doubleplus[x]\doubleplus\alert<2-3>{\nn{qs}}~\nn{ge} \\
                               & \multicolumn{2}{l}{\quad \bt{where}~(x, \nn{lt},\nn{ge})~=~\nn{split}~\var{cmp}~\var{xs}}
    \end{array}
  \]
\end{frame}

\begin{frame}{Our Approach.}
  \begin{itemize}
      \item The previous example shows many problems that need to be solved.
        \begin{itemize}
          \item It is too general (general recursive functions).
          \item We need to relate \emph{sequential} structures with \emph{parallel} structures.
          \item We need a mechanism for reasoning about cost.
       \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Our Approach.}
  \begin{itemize}
      \item In this talk we focus on an ``intermediate step'':
        \begin{itemize}
          \item We use implicit recursion patterns (\emph{hylomorphisms}) instead of general recursion.
          \item Relate the semantics of different \emph{parallel structures} to these recursion patterns.
          \item Expose the \emph{structure}, $\sigma\in\Sigma$, an abstraction of the AST of the program, at the type level.
          \item Encode rewritings as part of the type system.
          \item Parameterise over the cost models.
        \end{itemize}
  \end{itemize}
\end{frame}


%DENOTATIONAL SEMANTICS
\begin{frame}{Denotational Semantics}
\[
  \vp \in \Parp  \quad\Coloneqq
  \quad  \pseq{\vT}~\vf~|~\pipe{\vp_1}{\vp_2}~|~\dc{\vn,\vT,\vF}{\vf~\vg}~|~\farm~\vn~\vp~|~\fb{\vp}
\]
\begin{center}
  \[
    \begin{array}{lcl}
      \ssem{\vp~:~\vT~\vA\to\vT~\vB} &~:~& \vA\to\vB \\
      \ssem{\pseq{\vT}} & = & \hat{\rho}(\vf) \\

      \ssem{\pipe{\vp_1}{\vp_2}} & = & \ssem{\vp_2}\circ\ssem{\vp_1} \\

      \ssem{\farm~\vn~\vp} & = & \ssem{\vp} \\

      \ssem{\fb{\vp}} & = & \iter~\ssem{\vp} \\

      \ssem{\dc{\vn,\vT,\vF}{\vf~\vg}} & = & \var{fold}_\vF~\hat{\rho}(\vf)\circ\var{unfold}_\vF~\hat{\rho}(\vg) \\ \\
      \psem{\vp~:~\vT~\vA\to\vT~\vB} & ~:~ & \vT~\vA\to\vT~\vB \\
      \psem{\vp} & = & \map{\vT}~\ssem{\vp}

    \end{array}
  \]
\end{center}

\end{frame}

%HYLOMORPHISMS
\begin{frame}{Hylomorphisms}
  \begin{itemize}
    \item Hylomorphisms can be thought as a generalisation of a divide and
      conquer. They are equivalent to first \emph{unfolding} a data structure
      using a split function, $\vg$, and then \emph{folding} it using a combine
      function, $\vf$.
      \[
         \begin{array}{lcl}
           \hylo{\vF} &:& (\vF~\vB\to\vB)\to(\vA\to\vF~\vA)\to\vA\to\vB \\
           \hylo{\vF}~\vf~\vg &=& \vf\circ\vF~(\hylo{\vF}~\vf~\vg)\circ\vg \\\\
         \end{array}
      \]
  \end{itemize}
\end{frame}

\begin{frame}{Hylomorphisms}
  \begin{itemize}
    \item $\map{\vT}$, $\iter$, $\var{fold}_\vF$ and $\var{unfold}_\vF$ are
      specific instances of a more general recursion pattern:
      \emph{hylomorphisms}.

      \[
         \begin{array}{lcl}
         \map{\vT}~\vf &~=~& \hylo{\vF\vA}~(\hin{\vF}\circ\vF~\vf~\Idf)~\hout{\vF} \\
                       & & \quad\text{where $\vT~\vA~=~\mu(\vF~\vA)$,}  \\ & & \quad\text{and $\vA~=~\nn{dom}(\vf)$.} \\
           \var{fold}_\vF~\vf&~=~&\hylo{\vF}~\vf~\hout{\vF} \\
           \var{unfold}_\vF~\vf&~=~&\hylo{\vF}~(\hin{\vF})~\vg \\
           \var{iter}~\vf&~=~&\hylo{\vF}~(\Idf\triangledown\Idf)~\vf \\
         \end{array}
      \]
  \end{itemize}
\end{frame}

\begin{frame}{Example}
\begin{center}
\[
  \begin{array}{lcll}
    \multicolumn{4}{l}{\vT~\vA~\vB~=~1+\vA\times\vB\times\vB} \\
    \keyword{split} &~:~& \List~\vA\to\vT~\vA~(\List~\vA) & \\
    \keyword{split}~[] & = & \inl~() & \\
    \keyword{split}~(x\lhd\vl') & = & \inr~(x,~\keyword{leq}~\vx~\vl',~\keyword{gt}~\vx~\vl') &  \\ \\

    \keyword{combine} &~:~& \vT~\vA~(\List~\vA)\to\List~\vA & \\
    \keyword{combine}~(\inl~()) & = & [] &  \\
    \keyword{combine}~(\inr~(x,\vl,\vr)) & = & \vl\doubleplus[x]\doubleplus\vr & \\ \\

    \keyword{qsort} &~:~& \List~\vA\to\List~\vA & \\
    \keyword{qsort} & = & \hylo{\vT\,\vA}~\keyword{combine}~\keyword{split} & \\
  \end{array}
\]
\end{center}

\end{frame}

%THE PAR LANGUAGE
\begin{frame}{Structured Expressions}
  \begin{itemize}
    \item We introduce a small language with a type system that relates simple \textcolor{brown}{\emph{expressions}} and \textcolor{red}{\emph{parallel}} structures.
    \item A \textcolor{blue}{program} can be either a structured expression or a parallel process.
    \item The language of \textcolor{brown}{\emph{structured expressions}} allows a simple specification of the functional behaviour of a program.
    \item We will define a type system that describes how to lift a $\vS$ to a $\vP$.
  \end{itemize}

\begin{center}
$
  \begin{array}{lcl}
    \textcolor{blue}{\ve \in \vE} & \textcolor{blue}{=} & \textcolor{blue}{\vS \cup \vP}\\
    \textcolor{brown}{\vs \in \Seqp} &\textcolor{brown}{~\Coloneqq~}& \textcolor{brown}{\vf \quad|\quad \vs_1~\bullet~\vs_2 \quad|\quad \shylo{\vF}~\vs_1~\vs_2} \\

    \textcolor{red}{\vp \in \Parp}  &\textcolor{red}{~\Coloneqq~}&  \textcolor{red}{\pseq{\vT}~\vs~|~\pipe{\vp_1}{\vp_2}~|~\dc{\vn,\vT,\vF}{\vs_1~\vs_2}~|~\farm~\vn~\vp~|~\fb{\vp}}
\end{array}
$
\end{center}
\end{frame}

%THE TYPE SYSTEM
\begin{frame}{The Type System: an Example Rule}
  \resizebox{.8\textwidth}{!}{%
    \begin{minipage}{\textwidth}
\begin{center}
\[
\begin{array}{c}
%%  \inference{}{\tyd\sid~:~\vA\xmapsto{\ID}\vA}\hspace{\columnsep}
%%  \inference{}{\tyd\shin{\vF}~:~\vF(\mu\vF)\xmapsto{\IN}\mu\vF}\hspace{\columnsep}
%%  \inference{}{\tyd\shout{\vF}~:~\mu\vF\xmapsto{\OUT}\vF(\mu\vF)}\\\\%\hspace{\columnsep}
%
%  \inference{\rho(\vf)=\vA\to\vB}{\tyd\vf~:~\vA\xmapsto{\AF}\vB}\hspace{\columnsep}
%%  \inference{\tyd\vs~:~\vA\xmapsto{\sigma}\vB}{\tyd\vF~\vs~:~\vF~\vA~\vC\xmapsto{\vF~\sigma}\vF~\vB~\vC}\hspace{\columnsep}
%  \inference{\tyd\vs_1~:~\vB\xmapsto{\sigma_1}\vC & \tyd\vs_2~:~\vA\xmapsto{\sigma_2}\vB}
%            {\tyd\scomp{\vs_1}{\vs_2}~:~\vA\xmapsto{\COMP{\sigma_1}{~\sigma_2}}\vC}\\\\%\hspace{\columnsep}
%
%            \inference{\tyd\vs_1~:~\vF~\vB\xmapsto{\sigma_1}\vB & \tyd\vs_2~:~\vA\xmapsto{\sigma_2}\vF~\vA & \vG~=~\keyword{base}~\vF}
%            {\tyd\shylo{\vF}~\vs_1~\vs_2~:~\vA\xmapsto{\HYLO{\vG}~\sigma_1~\sigma_2}\vB}
%%\hspace{\columnsep}
%%  \inference{\tyd\vs~:~\vA\xmapsto{\sigma_1}\vB & \sigma_1\equiv\sigma_2}
%%            {\tyd\vs~:~\vA\xmapsto{\sigma_2}\vB}
%\\\\
%  \inference{\tyd~{\vs}~:~\vA\xmapsto{\sigma}\vB & \vF~=~\keyword{base}~\vT}
%          {\tyd~\pseq{\vT}{\vs}~:~\vT~\vA\xmapsto{\FUNC{\vF}~\sigma}\vT~\vB}\hspace{\columnsep}
%  \inference{\vn~:~\mathbb{N} & \tyd\vp~:~\vT~\vA\xmapsto{\sigma}\vT~\vB}{\tyd\farm~\vn~\vp~:~\vT~\vA\xmapsto{\FARM{\vn}~\sigma}\vT~\vB}
%          \hspace{.5\columnsep}\\\\
%  \inference{\tyd\vp_1~:~\vT~\vA\xmapsto{\sigma_1}\vT~\vB & \tyd\vp_2~:~\vT~\vB\xmapsto{\sigma_2}\vT~\vC}
%          {\tyd\vp_1\parallel\vp_2~:~\vT~\vA\xmapsto{\sigma_1~\parallel~\sigma_2}\vT~\vC}\hspace{.5\columnsep}
%  \inference{\tyd\vp~:~\vT~\vA\xmapsto{\sigma}\vT~(\vA+\vB)}{\tyd\fb~\vp~:~\vT~\vA\xmapsto{\FB~\sigma}\vT~\vB}\\\\ %\hspace{.5\columnsep}
  \inference{\tyd\vs_1~:~\vF~\vB\xmapsto{\textcolor{red}{\sigma_1}}\vB &
  \tyd\vs_2~:~\vA\xmapsto{\textcolor{red}{\sigma_2}}\vF~\vA & \vG_1~=~\keyword{base}~\vT &
                    \vG_2~=~\keyword{base}~\vF}
        {\tyd\dc{\vn,\vT,\vF}~\vs_1~\vs_2~:~\vT~\vA\xmapsto{\textcolor{red}{\DC{\vn,\vG_1\vG_2}~\sigma_1~\sigma_2}}\vT~\vB}
\end{array}
\]
\end{center}
\end{minipage} }
\vspace{.5cm}
\begin{itemize}
  \item The typing rules \emph{lift} the structure, \textcolor{red}{$\sigma$}, of a program to the type level.
  \item The \emph{structure} can be thought as the AST, \emph{pruned}, of any functionality: we just keep the structure.
  \item We need to describe a \emph{convertibility} relation for different structures.
\end{itemize}
\end{frame}

%CONVERTIBILITY
\begin{frame}{Convertibility}
  \begin{itemize}
      \item We need an extra rule to allow rewritings to be done.
\[
\begin{array}{c}
  \inference{\tyd\vs~:~\vA\xmapsto{\sigma_1}\vB & \textcolor{red}{\sigma_1\equiv\sigma_2} }
            {\tyd\vs~:~\vA\xmapsto{\sigma_2}\vB}\hspace{.5\columnsep}
\end{array}
\] \\

\[
\begin{array}{c c c}
  \inference{\sigma_1\textcolor{blue}{\equiv_s}\sigma_2}{\sigma_1\equiv\sigma_2} &
  \inference{\sigma_1\textcolor{brown}{\equiv_p}\sigma_2}{\sigma_1\equiv\sigma_2} & \inference{}{\FUNC{\vF}~\sigma\equiv\MAP{\vF}~\sigma} \\
\end{array}
\]
  \item The equivalence $\equiv\in\Sigma\times\Sigma$  is described in two levels, one in \textcolor{blue}{$\vS$} and other in \textcolor{brown}{$\vP$}, plus a rule that relates both.
  \end{itemize}
\end{frame}

\begin{frame}{Convertibility}
  \begin{itemize}

  \item The equivalence $\equiv$ can be easily lifted to $\mathcal{E}\in\vE\times\vE$.
  \item That correspondence between $\equiv$ and $\mathcal{E}$ is what allows to rewrite a well-typed program.
  \item We need a confluent term rewriting system to build a decision procedure for $\equiv$.
  \item $\equiv$ is derived from well-known laws of hylomorphisms, and the denotational semantics of
    parallel processes. E.g.\
    \vspace{.5cm}

\begin{center}
\begin{small}
$\begin{array}{l l}
  \FUNC{\vT}~\sigma_1\parallel\FUNC{\vT}~\sigma_2 &\equiv_p\FUNC{\vT}(\COMP{\sigma_2}{\sigma_1})\\
%  \DC{\vn,\vT,\vF}~\sigma_1~\sigma_2 &\equiv_p\FUNC{\vT}(\HYLO{\vF}~\sigma_1~\sigma_2) \\
%  \FARM{\vn}~\sigma&\equiv_p~\sigma\\
%  \FB(\FUNC{\vT}~\sigma)&\equiv_p\FUNC{\vT}(\HYLO{(+\vB)}~\AF~\sigma)\\ \\ % \hspace{.
  \\ \HYLO{\vF}~\sigma_1~\sigma_2&\equiv_s\HYLO{\vF}~\sigma_1~\OUT\bullet\HYLO{\vF}~\IN~\sigma_2 \\
\end{array}$
\end{small}
\end{center}

  \end{itemize}
\end{frame}

\begin{frame}{Soundness and (Partial) Completeness.}
$
\forall\ve\in\vE,\sigma
\in\Sigma,
\ \ \tyd\ve:\vA\xmapsto{\sigma}\vB\implies\tyd\ve:\vA\xmapsto{}\vB
$.

\begin{theorem}{Soundness of Conversion.}
$
\forall\vs\in\vS,\sigma
\in\Sigma,
\ \ \tyd\vs:\vA\xmapsto{\sigma}\vB
\ \Rightarrow\  \exists\ve\in\vE_{\vs} ~\text{s.t.}\,\tyd\ve:\vA\overset{\sigma}{\rightarrowtail}\vB
$
\end{theorem}

\begin{theorem}{Partial Completeness of Conversion.}
$
\forall\vs\in\vS, \ve\in\vE, \sigma
\in\Sigma,
\ \ (\tyd\ve:\vA\overset{\sigma}{\rightarrowtail}\vB ~\wedge~ \ve~\mathcal{E}~\vs)
\ \Rightarrow\ \ \tyd\vs:\vA\xmapsto{\sigma}\vB
$
\end{theorem}
\end{frame}

%EXAMPLES
\begin{frame}{Example: Quicksort Revisited}
$
\begin{array}{l l}
  \sigma &=~\DC{\vn,\vL,\vF}\,\AF\,\AF \\ \\
  \keyword{qsorts}&~:~\List(\List~\vA)\xmapsto{\sigma}\List(\List~\vA) \\
  \keyword{qsorts}&~=~\smap{\List}~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div})
\end{array}
$

\vspace{.5cm}
\begin{itemize}
\item The type system has to decide whether the following equivalence holds:

  \vspace{.3cm}
$
\MAP{\vL}(\HYLO{\vF}\,\AF\,\AF)\equiv\DC{\vn,\vL,\vF}\,\AF\,\AF%
$
  \vspace{.3cm}

\item
This can be trivially done by applying the following rewriting step:
  \vspace{.3cm}

$
\DC{\vn,\vL,\vF}~\AF~\AF
\rightsquigarrow~\MAP{\vL}~(\HYLO{\vF}~\AF~\AF)
$
\end{itemize}

\end{frame}

\begin{frame}{Example: Quicksort Revisited}
\begin{itemize}
\item The convertibility proof can be lifted from $\Sigma$ to $\vE$, and it provides
  the necessary rewriting steps to convert the original structured expression to the
  desired parallel form:

  \vspace{.5cm}
$
\smap{\List}~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div})\rightsquigarrow\dc{\vn,\List,\vF}~\keyword{merge}~\keyword{div}
$
\end{itemize}

\end{frame}

\begin{frame}{Example: Quicksort Revisited}
$
\onslide<1,3->{\MAP{\vL}~(\HYLO{\vF}~\AF~\AF)}~\equiv~\onslide<1,2,4->{\PIPE{(\FARM{\vn}~\sigma_2)}{\sigma_1}}
$.

\vspace{.5cm}

\begin{minipage}{\textwidth}
$
\begin{array}{l}
  \onslide<2>{  \PIPE{(\FARM~\vn~\sigma_2)}{\sigma_1} } \\
   \onslide<2>{  \rightsquigarrow~\PIPE{\sigma_2}{\sigma_1}} \\
 \onslide<2>{  \rightsquigarrow~\FUNC{\vf}~(\COMP{\sigma_1'}{\sigma_2'})
            \quad \sigma_1~\sim~\FUNC{\vf}~\sigma_1'~%
          \wedge~\sigma_2~\sim~\FUNC{\vf}~\sigma_2'} \\
 \onslide<2>{ \rightsquigarrow~\MAP{\vf}~(\COMP{\sigma_1'}{\sigma_2'})
            \quad  \sigma_1~\sim~\FUNC{\vf}~\sigma_1'~%
          \wedge~\sigma_2~\sim~\FUNC{\vf}~\sigma_2' }\\
\onslide<2,4>{ \rightsquigarrow~\MAP{\vf}~\sigma_1'\bullet\MAP{\vf}~\sigma_2'
            \quad  \sigma_1~\sim~\FUNC{\vf}~\sigma_1'~%
          \wedge~\sigma_2~\sim~\FUNC{\vf}~\sigma_2'}
\end{array}
$
\end{minipage}

\vspace{.5cm}

\begin{minipage}{\textwidth}
$
\begin{array}{l}
  \onslide<3>{  \MAP{\vL}~(\HYLO{\vF}~\AF~\AF) } \\
 \onslide<3,4>{ \rightsquigarrow~\MAP{\vL}~(\CATA{\vF}~\AF)~\bullet~\MAP{\vL}~(\ANA{\vF}~\AF)} \\
\end{array}
$
\end{minipage}

\vspace{.5cm}

\onslide<4->{
\begin{minipage}{\textwidth}
$\begin{array}{l}
  \sigma_1~\sim~\FUNC{\vf}~\sigma_1'~\wedge~\sigma_2~\sim~\FUNC{\vf}~\sigma_2'\wedge \\ ~\sigma_1'\sim\CATA{\vF}~\AF~\wedge~\sigma_2'\sim\ANA{\vF}~\AF
\end{array}$
\end{minipage}
}

\vspace{.5cm}

\onslide<4->{
\begin{minipage}{\textwidth}
$\smap{\List}(\shylo{\vF\,\vA}\keyword{merge}~\keyword{div})\rightsquigarrow^{\text{*}}$

$\quad\farm~\vn~(\pseq{\List}~(\sana{\vF\,\vA}~\keyword{div}))\parallel\pseq{\List}(\scata{\vF\,\vA}\keyword{merge})$.
\end{minipage}
}

\end{frame}

%CONCLUSIONS
\begin{frame}{Conclusions}
  \begin{itemize}
    \item We have defined a type system that captures the \emph{structure} of a program.
    \item The type system can use this structure to calculate how to introduce parallelism to a program in a sound way.
    \item Using the properties of hylomorphisms proved useful for both:
      \begin{enumerate}
          \item Describing the structure of a program.
          \item Rewriting the structure to introduce parallelism.
      \end{enumerate}
    \item By decoupling the structure from the functionality, we can define a number of analysis on the structure alone(e.g.\ cost models), and use them in our type system.
  \end{itemize}
\end{frame}

%FUTURE WORK
\begin{frame}{Ongoing and Future Work}
  \begin{itemize}
    \item Define an operational semantics for our parallel structures, and derive \emph{cost models} from this semantics.
    \item Implement a prototype DSL \emph{\`a la Feldspar}: generate C code bypassing the host language RTS.
    \item Describe more complex rewritings in $\vS$, to expose more parallelisation alternatives.
    \item Formalisation in Idris.
    \item Allow a simpler (not pointfree style) syntax for structured expressions.
  \end{itemize}
\end{frame}

\end{document}
