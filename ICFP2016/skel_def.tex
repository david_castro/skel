\section{Type Preliminaries}
\label{skel_def}

%We begin by defining the types and domains that we use in our semantics.
Our denotational semantics is phrased in a standard categorical
language~\cite{gibbons:calculating}, with a category representing a
\emph{model of computation}, the objects of that category representing
\emph{types}, the morphisms representing \emph{programs}, and
\emph{endofunctors} from the category to itself
representing type
constructors. In line with common practice~\cite{Stoybook}, we will work
in the category of \emph{pointed complete partial orders}
(\emph{CPO}). Our type language is entirely standard:
\[
\begin{array}{l}
%  \vA,\vB \in \texttt{Type}  ~~\Coloneqq~~  \tau~|~ 1 ~|~\vA + \vB~|~\vA\times\vB~|~\vA \to \vB~|~\vF~\vA \\
%  \vF,\vG, \vH \in \texttt{Type}\rightarrow\texttt{Type}  ~~\Coloneqq~~ \listTy~|~\bagTy~|~\treeTy^\vk \\
  \vA,\vB~\Coloneqq~\tau~|~1~|~\vA+\vB~|~\vA\times\vB~|~\vA \to \vB~|~\vF~\vA~|~\mu\vF
%  \vF,\vG, \vH  ~~\Coloneqq~~ \listTy~|~\bagTy~|~\treeTy^\vk \\
\end{array}
\]
%% \khcomment{Added domains.  There is no quantification here.}
%% \dccomment{I'm not sure if we shoud add the domains. To avoid writing
%%   $\sem{\vA}$, we can abuse syntax and use $\vA$ also for
%%   $\vA\in\emph{CPO}$. And the (endo-)functors can be defined in the usual
%%   category-theory way, so they can be thought as arrows (arrows that take
%% objects in cpo to objects in cpo, i.e. types) with some extra properties (F id$_A$ = id$_FA$,
%% and if f : A -> B, F f : F A -> F B such that F (f . g) = F f . F g). In all
%% the references they just give the syntax, state that they are functors in the
%% category of CPOs and provide a suitable reference. I think we should do the
%% same.}

%% Redundant, I believe.  KH
% We can use pcpos to model the $\listTy$ and
% $\treeTy$\footnote{We are deliverately ignoring the $\bagTy$ type, since for our
%  purposes, we can use the $\listTy$ datatype with the
% \emph{equality modulo permutations} relation.} datatypes in a standard way.
% \khcomment{This paragraph is orphaned.  Please find it a good home.}

\noindent
We assume no knowledge about the definition of an \emph{atomic type} ($\tau$),
but require it to have an interpretation as an object in the category
\emph{CPO}, $\sem{\tau}\in\emph{CPO}$. For the other types, we assume a standard
domain-theoretic semantics, where types are interpreted as pointed \emph{CPO}s.
The type $1$ is interpreted as the unit of \emph{CPO}; the type $\vA+\vB$ is
interpreted as the separated sum of $\sem{A}$ and $\sem{B}$; the type
$\vA\times\vB$ is interpreted as the Cartesian product of $\sem{A}$ and
$\sem{B}$; the type $\vA\to\vB$ is interpreted as the set of continuous
functions from $\sem{\vA}$ to $\sem{\vB}$; the type $\vF~\vA$ is interpreted as
the corresponding endofunctor applied to $\sem{\vA}$; and the type $\mu\vF$ is
interpreted as the fixpoint of functor $\sem{\vF}$.
%We use
%$\vA^\vk$ for the product type $\vA\times\cdots\times\vA$ of
%$\vk$ elements, and parentheses for values of these $k$-tuples,
%$(\vx_1,\ldots,\vx_\vk)$. Since there is no ambiguity, we reuse the syntax


\subsection{Functors}

A \emph{functor} is a structure-preserving mapping between categories.
We only need \emph{endofunctors} on $\mathit{CPO}$, $\vF:\mathit{CPO}\to\mathit{CPO}$, to represent type constructors, $\vF:\keyword{Type}\to\keyword{Type}$.
\emph{Bifunctors} are functors that are generalised to multiple
arguments, and we use them to define polymorphic data types.
%
The functors in our language are either standard polynomial functors with constant types, the left
section of a bifunctor, or a polymorphic type defined as the fixpoint of some
bifunctor. Bifunctors are defined using products and sums alone.
% We will
% define a functor $\vF$
Functors are defined using a \emph{pointed} notation, with the obvious
semantic interpretation. If $\vA$ and $\vB$ are type variables, and $\vT$ is a
type, we accept the following definitions:
\[
  \begin{array}{l r}
    \vG~\vA~\vB~=~\vT & \text{(bifunctor defined using sums and products)} \\
    \vF~\vA~=~\vT & \text{(functor defined using sums and products)} \\
    \vF~\vA~=~\vG~\vT~\vA & \text{($\vG$ is a bifunctor)} \\
    \vF~\vA~=~\mu(\vG~\vA) & \text{($\vG$ is a bifunctor)}
  \end{array}
\]

\begin{example}[Lists]
  Given the bifunctor
  \[
%$
    \vL~\vA~\vB=1+\vA\times\vB,
%$
  \]
  the polymorphic $\List$ data type is defined by the fixpoint of $\vL~\vA$:
% of sectioning $\vL$ with the type parameter as one argument:
  \[
%$
    \List~\vA=\mu(\vL~\vA).
%$
  \]
 As we will discuss in
  Section~\ref{sec:skel_def},  it is well known that given a base bifunctor $\vG~\vA~\vB$, the data type
  $\vF~\vA=\mu(\vG~\vA)$ is also a functor.
  The two list constructors are defined as expected:
  \[
  \begin{array}{l}
    \begin{array}{lcl}
      \keyword{nil} & : & \List~\vA \\
      \keyword{nil} & = & \hin{\vL\,\vA}~(\inj_1~()) \\
    \end{array} \\\\
    \begin{array}{lcl}
      \keyword{cons} & : & \vA\to\List~\vA\to\List~\vA \\
      \keyword{cons}~\vx~\vl & = & \hin{\vL\,\vA}~(\inj_2~(\vx,\vl)) \\
    \end{array}
  \end{array}
  \]
  \noindent
  We define the usual notation for lists
  \[
    [\vx_1,\vx_2,\ldots,\vx_n]~=~\keyword{cons}~\vx_1~(\keyword{cons}~\vx_2~(\keyword{cons}~\ldots\\~(\keyword{cons}~\vx_n~\keyword{nil}))).
  \]
\end{example}

\begin{example}[Trees]
  The polymorphic binary tree type can be defined in an analogous way:
  \[
  \begin{array}{rcl}
%$
    \vT~\vA~\vB & = & 1+\vA\times\vB\times\vB \mbox{ where } \\

    %\noindent

    \Tree~\vA & = & \mu(\vT~\vA).

  \end{array}
  \]
The two tree constructors are defined below:
\[
  \begin{array}{lcll}
    \keyword{empty} &~:~& \Tree~\vA \\
    \keyword{empty} & = & \hin{\vT\,\vA}~(\inj_1~()) \\\\
    \keyword{node} &~:~& \Tree~\vA\to\vA\to\Tree~\vA\to\Tree~\vA \\
    \keyword{node}~\vt_1~\vx~\vt_2~&=& \hin{\vT\,\vA}~(\inj_2~(\vx,\vt_1,\vt_2))
  \end{array}
\]
\end{example}

\noindent
Finally, we use a syntactic notation $\keyword{base}~\vF$ in our typing rules, $\keyword{base}~\vF$ is either the bifunctor $\vG$
that is used in the definition of $\vF$, or else $\vF$ itself.
\[
    \keyword{base}~\vF~=~\begin{cases}
      \vG, &\text{if $\vF~\vA~=~\vG~\vC~\vA$ or $\vF~\vA~=~\mu(\vG~\vA)$} \\
      \vF, &\text{otherwise.}
    \end{cases}
\]
%\noindent
%Intuitively, if
%$\vF$ is described in terms of $\vG$, $\keyword{base}~\vF~=~\vG$.
%%The definition of $\keyword{base}$ only makes sense at the syntactic level.
%% Confusing and not clear how to fix.  KH
% since different definitions of the same functor may, at the semantic level, have different
% $\keyword{base}$ depending on how it was defined.
%%This is not an issue, since
%%we will use $\keyword{base}$ in Section~\ref{sec:skel_func} precisely to obtain information
%%about how a functor is defined. %, in order to simplify some of our definitions.

\begin{figure}[t!]
\[
  \begin{array}{l}
%    (\underline{\cdot})~:~\vA\to(\vB\to\vA) \\
%    \underline{\vx}~=~\lambda\vy.\vx \\ \\
%
    (\cdot\circ\cdot)~:~(\vB\to\vC)\to(\vA\to\vB)\to\vA\to\vC \\
    \vf\circ\vg~=~\lambda\vx.\vf(\vg~\vx) \\ \\

    (\cdot~\triangledown~\cdot)~:~(\vA\to\vC)\to(\vB\to\vC)\to(\vA+\vB)\to\vC \\
    (\vf\triangledown\vg)~=~\lambda\vx.\mathit{case}~\vx~(\lambda\vy.\vf~\vy)~(\lambda\vy.\vg~\vy) \\\\

    (\cdot+\cdot)~:~(\vA\to\vC)\to(\vB\to\vD)\to(\vA+\vB)\to(\vC+\vD) \\
    (\vf+\vg)~=~(\inj_1\circ\vf)~\triangledown~(\inj_2\circ\vg) \\ \\

    (\cdot\vartriangle\cdot)~:~(\vA\to\vB)\to(\vA\to\vC)\to\vA\to(\vB\times\vC) \\
    (\vf\vartriangle\vg)~=~\lambda\vx.(\vf~\vx,\vg~\vx)\\\\

    (\cdot\times\cdot)~:~(\vA\to\vC)\to(\vB\to\vD)\to(\vA\times\vB)\to(\vC\times\vD) \\
    (\vf\times\vg)~=~(\vf\circ\pi_1)~\vartriangle~(\vg\circ\pi_2) \\ \\

%    (\cdot~?)~:~(\vA\to\mathit{Bool})\to\vA\to(\vA+\vA) \\
%    \vp~?~\vx~=~\inj_1~\vx\text{, if $\vp~\vx$} \\
%    \phantom{\vp~?~\vx~}=~\inj_2~\vx\text{, otherwise} % \\ \\
%
%    case = ...
%
%%     \mathit{pred}~=~ ...
%% \\\\
%%     (\vf \uplus \vg) ~=~ ...
  \end{array}
\]
\caption{Primitive Combinators.}
\label{fig:auxdefns}
\end{figure}

\subsection{Primitive Combinators}

Our semantics is defined in terms of the primitive combinators
% on the types defined above, as shown in Figure~\ref{fig:auxdefns}.
in Figure~\ref{fig:auxdefns}. For simplicity, we use a flattened form of sum and
product types, e.g.\ we write $\vA_1+\vA_2+\cdots+\vA_n$ rather than
$\vA_1+(\vA_2 + (\ldots + \vA_n))$. Instead of the usual
$\mathit{inl}/\mathit{inr}$ and $\pi_1/\pi_2$, we
use
 % a primitive type $\mathit{Bool}$ of booleans, plus
% a type of discriminated unions $A + B$, with
the $\mathit{inj}_i, 0 < i \le n$, introduction forms
for sum types, $A_1 + A_2 +\cdots+A_n$, and the projection eliminators $\pi_i, 0 < i \le n$ for product
types, $A_1\times A_2\times\cdots\times A_n$.
%
The operator $\circ$ denotes function composition, $\triangledown$ is the usual coproduct morphism (\emph{join}),
$+$ denotes the map on coproducts, $\vartriangle$ and
$\times$ are the respective morphisms and maps on products.
%and $\vp?$ is defined in a pointwise form, to introduce \emph{guards} on a
%predicate $\vp~:~\vA\to\mathit{Bool}$, where $\mathit{Bool}=1+1$.
Finally, for
a recursive type defined as the fixpoint of a functor, $\mu\vF$, the usual $\hin{\vF}:\vF\mu\vF\to\mu\vF$ and
$\hout{\vF}:\mu\vF\to\vF\mu\vF$ capture the isomorphism between $\vF\mu\vF$ and $\mu\vF$.


\endinput
\subsection{Global Type Environments.}
% \noindent
Finally, we will assume a global environment, $\rho$, of function
definitions,
% typechecking and semantic purposes,
where $\hat{\rho}=\sem{\rho}$:
%\[
%  \begin{array}{rcl}
$
    \rho =  \lbrace\vf~:~\vA\to\vB, \vg~:~\vC\to\vD, \ldots\rbrace;\quad
    \hat{\rho} = \lbrace\sem{\vf}~\in~\sem{\vA\to\vB}, \sem{\vg}~\in~\sem{\vC\to\vD}, \ldots\rbrace
$
%  \end{array}
%\]
%
%\noindent
We will pun $\vf$, $\vg$ for $\sem{\vf}$, $\sem{\vg}$; and
$\vA$, $\vB$ for $\sem{\vA}$, $\sem{\vB}$ if there is no ambiguity.


