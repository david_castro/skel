
\subsection{Operational Semantics}

\dccomment{IMPORTANT REMINDER! Define properly the "left/right views" $\rhd, \lhd$ on
"sequences" that capture both queues and stacks, before introducing the
semantics. ANOTHER IMPORTANT REMINDER! Define the "shared state = indexed set
of queues", and define precisely that queues require a unique identifier. And
move the $\parallel^{\cup}$ definition to a proper place.}

In this section, we describe a simple operational semantics that serves
both as an example semantics and as a starting point towards a more
efficient implementation. The key idea is that the operational semantics
for structured parallel programs should provide both: a) a description of
how to execute our parallel processes using some input; and b) a
mechanism for reasoning about the cost of traces of our parallel
processes. We will explain in Section~\ref{sec:skel_cost} how to abstract
cost information from the traces into \emph{sound} cost models that can
be integrated into a type system. We describe the operational semantics
of structured parallel programs in terms of a small-step trace-semantics
on a language for executing concurrent programs, $\QProg$, with
thread-safe queue operations.  \dccomment{we can say now that this
  language is actually a monad, but the obvious question will come ``why
  didn't you do it''? With a no-answer: I just realised that it was
  better that way ...}
\khcomment{Is QProg the language?}
$\QProg$ programs will operate on \emph{queues}, values of type $\QSpec$
that are defined to be either primitive elements $\vQ_i$, sequences
of queues, or discrimated unions.
\[
\begin{array}{lcl}
  \vq\in\QSpec & \Coloneqq & \vQ_i ~|~ \langle\vq,\vq,\ldots,\vq\rangle ~|~ \vq + \vq \\
\end{array}
\]

\paragraph{Queueing Statements and expressions.}
The syntax of queueing statements, $\QProg$ and queueing expressions, $\QExpr$,
is shown below:
\[
\begin{array}{lcl}
  \ve\in\QExpr & \Coloneqq & \GetE(\vi) ~|~ \PutE(\vi) ~|~ \Eval(\seqp) ~|~ \Pack(\vk) ~|~ \Unpack(\vk)
               ~|~ \ve;\ve ~|~ \ve\triangledown\ve \\
  \vc\in\QProg & \Coloneqq & \Loop(\ve)  ~|~ \vc\parallel\vc \\
\end{array}
\]
\TODO{Queue variables $\vQ$ and $\keyword{newQ}$ operation.}

\noindent
A \emph{queuing statement} is either a repeating \emph{worker} process,
constructed from a queuing expression, $\Loop(\ve)$, or the parallel composition of two queuing statements, $\vc\parallel\vc$.
\emph{Queuing expressions} ($\QExpr$) describe a simple state machine on queues\footnote{It is easy to see that this forms a monad, although we have not yet formalised
our definitions in this way.}.
%The expression
$\GetE(\vi)$ dequeues an element from queue $\vi$ and places it at the top
of the local stack\khcomment{Stacks need to be defined}. $\PutE(\vi)$ enqueues the element at the top
of the local stack into the queue $\vi$. $\Eval(\vs)$ applies structured
expression $\vs$ to the element on the top of the local stack.
$\Pack(\vk)$ and
$\Unpack(\vk)$ respectively allow the creation and inspection of $\vk$-tuples.
Finally,
$\ve;\ve$ sequences two queuing expressions\khcomment{Odd - more of a statement thing!},
and $\vc_1\triangledown\vc_2$ introduces a choice between two queuing expressions.
%A $\QProg$ expression is
%n an expression, a sequence of
%expressions, a choice of two expressions $\vc_1\triangledown\vc_2$, a \emph{worker} that will be embedded in a statement, or the parallel composition of two statements.
\dccomment{I feel bad for this: this language should be a monad and small-step
  semantics for the monad instead of describing this using stacks in a very
  boring way that only add noise! I'll deal with that in the future work:
  should be easy to do. That way I would be describing not only the operational
semantics, but also providing a way to run this in Idris (and maybe other
dependently typed languages -- except Coq).}
\khcomment{Added a footnote above.}

%The function $\pmap{\vp}$ turns $\vp$ into an equivalent ``streaming'' parallel
%computation. Needed to convert the workers of a divide and conquer into
%equivalent computations on sequences.



\begin{figure*}[t]
\[
\begin{array}{l c l}
  % Get into stack
  \langle\State[\vi\mapsto\vx\lhd\vQ],\Stack,\GetE(\vi)\rangle
  & \xrightarrow{} & \langle\vQ_i=\vxs\uplus\State,\vx\lhd\Stack\rangle \\
  % Put into queue
  \langle\State[\vi\mapsto\vQ],\vx\lhd\Stack,\PutE(\vi)\rangle
  & \xrightarrow{} & \langle\State[\vi\mapsto\vQ\rhd\vx],\Stack\rangle \\ \\
  % Pack first k elements into a tuple
  \langle\State,\vx_1\lhd\ldots\lhd\vx_k\lhd\Stack,\Pack(\vk);\vc\rangle
    & \xrightarrow{} & \langle\State,\langle\vx_1,\ldots,\vx_k\rangle\lhd\Stack\rangle \\
  % Unpack a k-tuple into the stack
  \langle\State,\langle\vx_1,\ldots,\vx_k\rangle\lhd\Stack,\Unpack(\vk)\rangle
  & \xrightarrow{} & \langle\State,\vx_1\lhd\ldots\lhd\vx_k\lhd\Stack\rangle \\ \\
  % Eval sequential computation
  \langle\State, \vx\lhd\Stack, \Eval(\vs)\rangle
  & \xrightarrow{} & \langle\State,\ssem{\vs}(\vx)\lhd\Stack\rangle \\ \\
  % Choice with inl
  \langle\State,(\inl~\vx)\lhd\Stack,\ve_1 \triangledown \ve_2\rangle
  & \xrightarrow{} & \langle\State,\vx\lhd\Stack,\ve_1\rangle \\
  % Choice with inr
  \langle\State,(\inr~\vx)\lhd\Stack,\ve_1 \triangledown \ve_2\rangle
  & \xrightarrow{} & \langle\State,\vx\lhd\Stack,\ve_2\rangle \\
  % Loop
  \langle\State,\Stack,\Loop(\ve)\rangle
  & \xrightarrow{} & \langle\State,\Stack,\ve;\Loop(\ve)\rangle \\
  % Sequence
  \multicolumn{3}{c}{
  \inference*{\langle\State,\Stack,\ve_1\rangle
                 \xrightarrow{\alpha^\lambda}\langle\State',\Stack',\ve_1'\rangle
                                          |  \langle\State',\Stack'\rangle}
             {\langle\State,\vxs,\ve_1;\ve_2\rangle
                 \xrightarrow{\alpha^\lambda}\langle\State',\Stack',\ve_1';\ve_2\rangle
             |  \langle\State',\Stack',\ve_2\rangle}} \\
\end{array}
\]

\[
\begin{array}{l r}
  \inference*{\langle\State, \Stack_1, \vc_1\rangle
                  \xrightarrow{\alpha^\lambda} \langle\State', \Stack_1', \vc_1'\rangle}
            {\langle\State, \Stack_1|\Stack_2, \vc_1\parallel\vc_2\rangle
            \xrightarrow{\alpha^{\vl.\lambda}} \langle\State',\Stack_1'|\Stack_2, \vc_1'\parallel\vc_2\rangle} &
  \inference*{\langle\State, \Stack_2, \vc_2\rangle\xrightarrow{\alpha^{\lambda}}\langle\State', \Stack_2', \vc_2'\rangle}
            {\langle\State, \Stack_1|\Stack_2, \vc_1\parallel\vc_2\rangle
            \xrightarrow{\alpha^{\vr.\lambda}} \langle\State',\Stack_1|\Stack_2',\vc_1\parallel\vc_2'\rangle}
            \\
\end{array}
\]
\caption{Operational semantics}
\label{fig:opsem}
\end{figure*}

%\[
%  \inference{\vf(\vx)=\vy}{\vf(\vx)\Downarrow\vy} \and
%  \inference{\vs_2(\vx)\Downarrow\vy & \vs_1(\vy)\Downarrow\vz}{(\scomp{\vs_1}{\vs_2})(\vx)\Downarrow\vz} \and
%  \inference{\vs(\vx_1)\Downarrow\vy_1 & \ldots & \vs(\vx_n)\Downarrow\vy_n}{(\smap{\vs})(\lbrace\vx_1,\ldots,\vx_n\rbrace)\Downarrow(\lbrace\vy_1,\ldots,\vx_n\rbrace)} \and
%  \inference{\vs(\\vx_1)\Downarrow\vy_1 & \ldots & \vs(\vx_n)\Downarrow\vy_n}{(\smap{\vs})(\lbrace\vx_1,\ldots,\vx_n\rbrace)\Downarrow(\lbrace\vy_1,\ldots,\vx_n\rbrace)} \and
%  \inference{\vs(\langle\vx_{\vn-\vk},\ldots,\vx_{\vn}\rangle)\Downarrow\vy \and  (\sfold{\vk}{\vs})([\vx_1,\ldots,\vx_{\vn-\vk-1},\vy])\Downarrow\vz \and \vn>\vk}{(\sfold{\vk}{\vs})([\vx_1,\ldots\vx_{\vn-\vk-1},\vx_{\vn-\vk}, \ldots, \vx_{\vn}])\Downarrow\vz} \and
%  \inference{\vs(\langle\vx_{1},\ldots,\vx_{\vn}\rangle\doubleplus\keyword{rep}(\vk-\vn,\ve))\Downarrow\vy \and \vn\leq\vk}{(\sfold{\vk}{\vs})([\vx_1,\ldots, \vx_{\vn}])\Downarrow\vy} \and
%  \inference{\vs(\vx)\Downarrow\langle\vx_1,\ldots,\vx_{\vk}\rangle \and
%  (\sunfold{(\vn-1)}{\vk~\vs})(\vx_1)\Downarrow\vys_1 \ldots
%(\sunfold{(\vn-1)}{\vk~\vs})(\vx_{\vk})\Downarrow\vys_{\vk} \and
%\vn>0}{(\sunfold{\vn}{\vk~\vs})(\vx)\Downarrow\vys_1\doubleplus\ldots\doubleplus\vys_{\vk}}
%\and
%  \inference{\vn=0}{(\sunfold{\vn}{\vk~\vs})(\vx)\Downarrow[\vx]} \and
%  \inference{\vs(\vx)\Downarrow\inr~\vy}{(\iter{\vs})(\vx)\Downarrow\vy} \and
%  \inference{\vs(\vx)\Downarrow\inl~\vx' \and (\iter{\vs})(\vx')\Downarrow\vy}{(\iter{\vs})(\vx)\Downarrow\vy} \and
%\]

%\dccomment{Maybe not necessary. Could use the denotational semantics, it is almost equivalent. Worth discussing.}

\paragraph{Traces.}
The traces described by the operational semantics are described in a standard
way as sequences of program \emph{states} interleaved with the \emph{actions}
that modify the state. By actions, we mean the expressions that cause a change
in the state, $\QExpr$, annotated with a worker identifier, $\omega\in\Omega$.
\[
  \begin{array}{l}
    \omega\in\Omega~~\Coloneqq~~\epsilon~~|~~\mathtt{l}\omega ~~|~~ \mathtt{r}\omega
  \end{array}
\]

\noindent
A path $\omega$ identifies the position of the worker in the $\QProg$ AST, and
is built as a sequence of $\mathtt{l}$ and $\mathtt{r}$ primitives,
that correspond to the
left or right branches in a parallel composition. The set of actions is defined
as $\mathcal{A}=\QExpr\times\Omega$. An action $\alpha=\ve^\omega$ denotes the
evaluation of $\ve\in\QExpr$ by a worker $\omega\in\Omega$. Program states are divided
into three parts, $\mathtt{St}=\mathcal{Q}\times\mathcal{S}\times\QProg$:

\begin{enumerate}
  \item \emph{Global state}, which consists of a mapping from queue identifiers
    to queues. We will use $\mathcal{Q}$ for the global state, and
    $\mathcal{Q}[\vi\mapsto\vQ]$ to access the sequence of elements $\vQ$ in
    queue $\vi$.
  \item \emph{Local state}, $\mathcal{S}$, which is used to store the intermediate results
    of local computations, operating as a stack. We write $\mathcal{S}_1 |
    \mathcal{S}_2$ for the disjoint union of the local states of the workers in
    a parallel composition $\vc_1\parallel\vc_2$.
  \item \emph{Current statement}. The queueing statement that is currently being evaluated.
\end{enumerate}

\noindent
A program trace is, then, a sequence of states and actions of the shape:
\[
  \keyword{st}_1~\alpha_1~\keyword{st}_2~\alpha_2~\ldots~\alpha_n~\keyword{st}_n
\]

\paragraph{Operational semantics.}
The operational semantics that describes these
traces makes two basic assumptions:
\begin{enumerate}
  \item Sequential processes do not interfere with each other.
  \item Queue operations are thread-safe, and do not deadlock.
%  \item \dccomment{What?}
\end{enumerate}

\noindent
We define our operational semantics (Figure~\ref{fig:opsem} as a small-step semantics, combined
with a big-step semantics for the base computations, where each
small-step operation is a rewriting on the program state. 
% Such rewriting
% Due to the unrestricted non-determinism
\khcomment{Unfinished.}

\begin{theorem}[Soundness]
Let $\vQ_i$ and $\vQ_j$ be two arbitrary queues of tasks and $\vp\in\Parp$ a
structured, possibly parallel, program such that
$\langle\State,\Stack,\vc\rangle=\TransC{\vp}(\vQ_i,[])$.  For all traces
such that $\langle\State,\Stack,\vc\rangle\xrightarrow{\text{*}}\langle\State',\Stack',\vc'\rangle$,
$\keyword{elems}(\vQ_j)\approx_{\mathsf{perm}}\psem{\vp}(\keyword{elems}(\vQ_i))$.
\end{theorem}

\begin{nproof}[Proof Sketch]
\end{nproof}

\paragraph{Translation of $\Parp$ to $\QProg$.}

\noindent
% As shown in Figure~\ref{fig:transl},
Structured parallel processes in $\Parp$ can easily be translated into
queuing statements in $\QProg$.
The main idea is to translate each process into the
parallel composition of a corresponding set of workers, each of which repeatedly
% running in a loop
applies operations for dequeuing the next input, evaluating the
associated computation, and then enqueuing the output to the corresponding queue.
For conciseness, we omit the definition here.
\endinput
The queues must then be connected in the appropriate way.
A parallel pipeline process
is translated to the parallel composition of two queueing statements,
linked via the same intermediate queue.
% , where the output queue of the first one
% is the input queue of the second one.
A task farm is translated into $\vn$
structures, all of which share the same input and output queues.
A feedback process is
translated into a statement that decides whether to write the output back to the
input queue, or to the output queue, based on the output value\khcomment{Is this done?}. A divide-and-conquer process is translated into
the parallel composition of \emph{split} workers, \emph{map} workers,
and \emph{merge} workers. The \emph{split} workers apply a divide function and
place the output either in the input queue of the corresponding split workers, or into the input queue of the
\emph{map} workers when the last level of the tree is reached. The
\emph{map} workers apply a function
to the previously divided input, and enqueue the result
as an input for the \emph{merge}  workers.
These then repeat the process in a symmetric way.
% Figure~\ref{fig:transl} shows the translation scheme used.
\TODO{Labels and mappings to cores. For now, assuming a 1-to-1 mapping.}
