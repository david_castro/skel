\section{Timing Properties of Parallel Programs}
\label{sec:skel_cost}

\dccomment{Help! Typically, in a sized type system, all types carry size
annotations, in our system we separate size specifications from types, and
provide (and calculate) size functions that work with size specifications. I
give here a brief description of the definitions. I need to correct them and
double-check that it makes sense.}

Unlike most earlier treatments, the inclusion of the feedback construct
means that our parallel programs can include recursive behaviour, whose
execution time depends on the number of recursive calls needed to produce
an output, and which even may be data-dependent.  Moreover, the execution time
may be infinite in situations where the recursive function diverges.
This creates significant challenges for providing an accurate and usable
cost model for execution time.

\emph{Sized types} are a well-known approach that enables termination
(productivity) checking of recursive (co-recursive) definitions\TODO{Cite
  Lars Pareto}. Loidl and Hammond\TODO{cite a sized time system for a
  parallel functional language} describe a variant \emph{sized time}
system that can additionally \emph{infer} size and execution time
information for functional expressions.  They then use that information
to predict performance for different implementations of a functional program,
where specific sub-expressions are executed in parallel. %\dccomment{Kevin, please check that I didn't misinterpret your work in this sentence...}.
We follow a similar approach in this
paper, but use a different formulation of sized types.
Our approach will allow us to produce
size and execution time information for our structured parallel programs,
that will also provide a way to ensure termination.



\subsection{Size Functions and Size Specifications}

Rather than using a traditional formulation of sized types embedded into a
specific type system, in this paper we define them as an \emph{abstract
interpretation} that we will later embed into the type system, using dependent
types. The main reason for this alternative formulation is to decouple size and
time information from the type structure, which will become crucial to
parameterise the whole type system with the (possibly parallel) structure.
Using size and time abstract interpretations in the types is what will enable
to reason about alternative structures for the same program (denotationally) at
the type level.

\paragraph{Size Functions.}
Very roughly, in a standard formulation of sized types, the type of a function can be
represented using annotated types: % annotated with sizes:
\[
  \vf~:~\vA^{\var{z}_1}\to\vB^{\var{z}_2}
\]
\noindent
where $\var{z}_1$ and $\var{z}_2$ are size parameters.
In our approach, we will
instead require that every type has a \emph{measurement operator}
$|\cdot|~:~$ $\vA\to\mathbb{N}$, and define a function
\noindent
$$
\begin{array}{l}
\forall\vf~:~\vA\to\vB, \\
  \quad \exists\var{size}_f~:~\mathbb{N}\to\mathbb{N}, \\ % ~\text{such that} \\
  \qquad
  \mathit{s.t.}~\forall\vx~:~\vA,
  |\vx|~\leq~\vn~\Rightarrow~|\vf~\vx|~\leq~\var{size}_f |\vx|~\leq~\vm
\end{array}
$$

\noindent
The function $\var{size}_f$ is an abstract interpretation of $\vf$ on sizes,
which we call the \emph{size function} for $\vf$, and will generally write
$|\vf|$ instead.

\paragraph{Size Functor.}

A sized-type system typically allows types to have nested sizes. For example,
the type $\listTy^\vn~\vA$ specifies lists which have a maximum of $\vn$
elements. However, $\vA$ may itself be a sized type. Rather than just using
$\mathbb{N}$ for sizes, as might be expected, we therefore need to specify our
size functions using \emph{size specifications}. These size specifications are
types that abstract the necessary size information, i.e.\ for each type, $\vA$,
there is another type, the size specification of $\vA$, or $|\vA|$, whose
values represent sizes of elements of type $\vA$. For each function, $\vf$, we
require an associated size abstract interpretation $|\vf|$.

$$
\begin{array}{lcl}
  \vf & : & \vA\to\vB \\
  |\vf| & : & |\vA|\to|\vB| \\
\end{array}
$$

\noindent

We will not assume, except where needed, any detail about the definition of
size specifications. We will, however, require it to be a functor with some
extra properties. The reason for this is that we can build size abstract
interpretations for our language constructs derived from the properties of a
functor.

$$
\begin{array}{lcl}
  |\Idf_{\vA}| & = & \Idf_{|\vA|} \\
  |\vf\circ\vg| & = & |\vf|\circ|\vg| \\
\end{array}
$$

\noindent
We need to require some extra properties to the size functor in order to
\begin{enumerate}
    \item provide a way to derive a size abstract interpretation for hylomorphisms, and
    \item ensure that this functor is a correct size abstraction.
\end{enumerate}

\noindent
One possible solution for (1) is to require that all functors distribute
with respect to the size functor, and size functor to be distributive with
respect to all functors. That is, for each functor $\vF$, we require
$$
\begin{array}{lcl}
  \keyword{din}  & : & |\vF~\vA|\to\vF~|\vA| \\
\end{array}
$$

\noindent
and its inverse,
$$
\begin{array}{lcl}
  \keyword{dout} & : & \vF~|\vA|\to|\vF~\vA|. \\
\end{array}
$$

\noindent
It is important to note that $\vF~|\vA|$ and $|\vF~\vA|$ are not necessarily
isomorphic, since $|\vF~\vA|$ may contain less information than $\vF~|\vA|$.
For example, $\listTy~|\vA|$ is a list of sizes of elements of type $\vA$, but
$|\listTy~\vA|$ may be a natural number, $\mathbb{N}$, representing the length
of the list, or a tuple $\mathbb{N}\times|\vA|$ that represents the length of
the list and the maximum size of the elements in the list. This implies that
$$
\begin{array}{lcl}
  \keyword{dout}\circ\keyword{din} & = & \Idf\text{,} \\
\end{array}
$$
but that
$$
\begin{array}{lcl}
  \keyword{din}\circ\keyword{dout} & \not= & \Idf\text{.} \\
\end{array}
$$

\noindent
We also need to require that for all functor $\vF$ and type $\vA$, its size
specification $|\vF~\vA|$ can also be described as a functor,
$\vG~|\vA| = |\vF~\vA|$. In order to make $\vG$ a functor, both $\keyword{dout}$ and
$\keyword{din}$ must be natural transformations. Given a
$|\vf|~:~|\vA|\to|\vB|$,

$$
\begin{array}{lclr}
  \keyword{dout}\circ\vF~|\vf| & = & \vG~|\vf|\circ\keyword{dout}, & \text{and} \\
  \vF~|\vf|\circ\keyword{din} & = & \keyword{din}\circ\vG~|\vf|. & \\
\end{array}
$$

\noindent
Using these properties, we can define $\vG~|\vf|$ as
$$
\begin{array}{lclr}
  \vG~|\vf| & = & \keyword{dout}\circ\vF~|\vf|\circ\keyword{din}.
\end{array}
$$

\noindent
It is easy to prove that $\vG~|\vf|$ is indeed a functor. First, we see that
$\vG$ preserves the identity.

\[
\begin{array}{ll}
  & \vG~\Idf_{|\vA|} \\
  = & \keyword{dout}\circ\vF~\Idf_{|\vA|}\circ\keyword{din} \\
  = & \keyword{dout}\circ\Idf_{\vF~|\vA|}\circ\keyword{din} \\
  = & \keyword{dout}\circ\keyword{din} \\
  = & \Idf_{|\vF~\vA|} \\
\end{array}
\]

\noindent
And lastly, we see that $\vG$ also preserves function composition.

\[
\begin{array}{ll}
  & \vG~(\vf\circ\vg) \\
  = & \keyword{dout}\circ\vF~(\vf\circ\vg)\circ\keyword{din} \\
  = & \keyword{dout}\circ\vF~\vf\circ\vF~\vg\circ\keyword{din} \\
  = & \vG~\vf\circ\keyword{dout}\circ\keyword{din}\circ\vG~\vg \\
  = & \vG~\vf\circ\Idf\circ\vG~\vg \\
  = & \vG~\vf\circ\vG~\vg \\
\end{array}
\]


\noindent
The size function for the hylomorphisms can, then, be derived in a systematic
way from the properties of the size functor:

\[
\begin{array}{ll}
  \multicolumn{2}{c}{|\hylo{\vF}~\vf~\vg|} \\
    = & \lbrace\text{ def.\ of hylo }\rbrace \\
  \multicolumn{2}{c}{|\vf\circ\vF~(\hylo{\vF}~\vf~\vg)\circ\vg|} \\
    = & \lbrace\text{ functor preserves comp.\ of morphisms }\rbrace \\
  \multicolumn{2}{c}{|\vf|\circ|\vF~(\hylo{\vF}~\vf~\vg)|\circ|\vg|} \\
    = & \lbrace\text{ distribute in and out the size functor }\rbrace \\
  \multicolumn{2}{c}{|\vf|\circ\vG~|\hylo{\vF}~\vf~\vg|\circ|\vg|} \\
    = & \lbrace\text{ def.\ of hylo }\rbrace \\
  \multicolumn{2}{c}{\hylo{\vG}~|\vf|~|\vg|} \\
\end{array}
\]

\noindent
In general, and for efficiency reasons, we allow any functor isomorphic to
$\vG$. That way, we can use an alternative definition that does not involve the
natural transformations $\keyword{din}$ and $\keyword{dout}$, since that
involves performing the same recursive call tree as the original hylomorphism,
as described by $\vF$. The problem can be observed by expanding the definition of $\hylo{\vG}$:

\[
\begin{array}{ll}
  \multicolumn{2}{c}{\hylo{\vG}~|\vf|~|\vg|} \\
    = & \lbrace\text{ def.\ of hylo }\rbrace \\
  \multicolumn{2}{c}{|\vf|\circ\vG~(\hylo{\vG}~|\vf|~|\vg|)\circ|\vg|} \\
    = & \lbrace\text{ def.\ of $\vG$ }\rbrace \\
  \multicolumn{2}{c}{|\vf|\circ\keyword{dout}\circ\vF~(\hylo{\vG}~|\vf|~|\vg|)\circ\keyword{din}\circ|\vg|} \\
    = & \lbrace\text{ def.\ of hylo }\rbrace \\
  \multicolumn{2}{c}{\hylo{\vF}~(|\vf|\circ\keyword{dout})~(\keyword{din}\circ|\vg|)}. \\
\end{array}
\]

\noindent
Abusing the notation, we will write $|\vF|$ for such functor that is isomorphic
to $\vG$, and require two natural transformations such that

\[
  \begin{array}{lcl}
    \eta_{\text{in}} & : & |\vF~\vA|\to|\vF|~|\vA| \\
    \eta_{\text{out}} & : & |\vF|~|\vA|\to|\vF~\vA|.
  \end{array}
\]

\noindent
The hylomorphism $\hylo{|\vF|}$ characterized by $|\vF|$ does not necessarily
have the same recursive call tree as $\hylo{\vF}$, but enables an equivalent,
but more efficient size abstract interpretation.

\begin{example}
  Given the types
  \[
    \begin{array}{lcl}
      \vA & & \\
      \listTy=\mu\vL& \text{, where } & \vL~=~1\hat{+}\Const{\vA}\hat{\times}\keyword{Id}\text{, and} \\
      \treeTy=\mu\vT & \text{, where } & \vT~=~1\hat{+}\Const{\vA}\hat{\times}\keyword{Id}\hat{\times}\keyword{Id}\text{; and}
    \end{array}
  \]
  \noindent
  given two functions $\keyword{div}~:~\listTy\to\vT~\listTy$ and
  $\keyword{merge}~:~\vT~\listTy\to\listTy$, we can define the hylomorphism
  $\keyword{qsort}~=~\hylo{\vT}~\keyword{merge}~\keyword{div}$.

  Using $\keyword{din}$ and $\keyword{dout}$, we can automatically derive its
  size abstract interpretation. First, we define
  $\vG~|\vf|=~\keyword{dout}\circ\vT~|\vf|\circ\keyword{din}$, and now, the
  size abstract interpretation is just
  $|\keyword{qsort}|~=~\hylo{\vG}~|\keyword{merge}|~|\keyword{div}|$. However,
  observe from the shape of $\vG$ that a binary tree still describes the
  recursive call tree, therefore having the same complexity, although arguably
  less cost. I.e.\ this hylomorphism has the same recursive structure, but
  computed using sizes instead of values.

  A more efficient, but equivalent definition would use a list as a base
  functor. Given a function $\keyword{avg}~:~\vT~|\listTy|\to\vL~|\listTy|$
  that averages the sizes of the two lists inside $\vT~|\listTy|$, and a
  function $\keyword{dup}~:~\vL~|\listTy|\to\vT~|\listTy|$ that duplicates the
  size of the list in the input, we can define the following natural
  transformations:
  \[
    \begin{array}{lcl}
      |\vT| & = & \vL \\

      \eta_{\text{in}} & : & |\vT~\listTy|\to|\vT|~|\listTy| \\
      \eta_{\text{in}} & = & (\Idf+\keyword{avg})\circ\keyword{din} \\ \\

      \eta_{\text{out}} & : & |\vT|~|\listTy|\to|\vT~\listTy| \\
      \eta_{\text{out}} & = & \keyword{dout}\circ(\Idf+\keyword{dup}) \\
    \end{array}
  \]
  \noindent
  It is important to note that by averaging the sizes, we may lose some
  information. However, since $|\vT~\listTy|$ is not necessarily isomorphic
  with $\vT~|\listTy|$, we can define it so that it is safe to do so. Now, the
  corresponding size hylomorphism is:
  \[
    \hylo{|\vT|}~(|\keyword{merge}|\circ\eta_{\text{out}})~(\eta_{\text{in}}\circ|\keyword{div}|),
  \]
  \noindent
  which has only one recursive call, instead of the original two recursive
  calls. For simplicity, we ignored the size of the elements inside the list.
  However it can be easily extended to deal with polymorphism, and the sizes of
  the elements inside the list.
\end{example}

\noindent
For simplicity, we will assume that for each functor, $\vF$, we have another
functor $|\vF|$ such that $|\vF~\vA|~=~|\vF|~|\vA|$. That way, the natural
transformations $\eta_{\text{out}}=\eta_{\text{in}}=\Idf$. That allows us one
further simplification:
\[
  \begin{array}{cl}
      & |\hylo{\vF}~\vf~\vg| \\
    = & |\vf|\circ|\vF~\hylo{\vF}~\vf~\vg|\circ|\vg| \\
    = & |\vf|\circ\eta_{\text{out}}\circ|\vF|~|\hylo{\vF}~\vf~\vg|\circ\eta_{\text{in}}\circ|\vg| \\
    = & |\vf|\circ\Idf\circ|\vF|~|\hylo{\vF}~\vf~\vg|\circ\Idf\circ|\vg| \\
    = & |\vf|\circ|\vF|~|\hylo{\vF}~\vf~\vg|\circ|\vg| \\
    = & \hylo{|\vF|}~|\vf|~|\vg| \\
  \end{array}
\]

\noindent
It is worth noting from the previous example that the more efficient size
abstract interpretation is actually less accurate, while the less efficient one
can be defined in a way that provides an accurate size of the output (in this
case, the same as the size of the input). However, both abstract
interpretations provide a way to estimate an upper bound of the number of
recursive calls needed, and an upper bound in the output size. In order to
prove that the abstract interpretations indeed provide an upper bound, we need
to specify relations between size specifications, or
\emph{size relations}.

\paragraph{Size Relations.}
Summarising, for introducing size abstract interpretations we require the following extra parameters.

\begin{enumerate}
\item For each type $\vA$, we require a type that represents a size
  specification for values of this type, $|\vA|$;
\item we require $|\cdot|$ to be a functor, so for each function,
  $\vf~:~\vA\to\vB$ we require a size function $|\vf|~:~|\vA|\to|\vB|$ so that
  $|\vf\circ\vg|~=~|\vf|\circ|\vg|$; and,
\item for each functor, $\vF$, we require another functor $|\vF|$ such that
  $|\vF~\vA|~=~|\vF|~|\vA|$, which, as we have proven, implies that we need to
  have a natural transformation from $\vF$ to $|\vF|$ and its inverse.
\end{enumerate}

\noindent
But in order to turn these parameters into a useful size abstraction, we also need
to define a way to relate different sizes. Recall the property that we stated at
the beginning of this section. Let $\vA$ and $\vB$ be two types such that $|\vA|=|\vB|=\mathbb{N}$,
then, since $|\vf|~:~\mathbb{N}\to\mathbb{N}$:
$$
\begin{array}{l}
\forall\vf~:~\vA\to\vB, \\
  \qquad
  \mathit{s.t.}~\forall\vx~:~\vA,
  |\vx|~\leq~\vn~\Rightarrow~|\vf~\vx|~\leq~|\vf|~|\vx|~\leq~\vm
\end{array}
$$

\noindent
First, note that we can simplify this by removing the upper and lower bounds
$\vn$ and $\vm$, since all we are interested in is that the size function
determines the upper bound on the size of the output. This property now becomes:

$$
\begin{array}{l}
\forall\vf~:~\vA\to\vB, \\
  \qquad
  \mathit{s.t.}~\forall\vx~:~\vA, |\vf~\vx|~\leq~|\vf|~|\vx|
\end{array}
$$

\noindent
In order to lift this property to arbitrary size specifications, since
$|\vf|~:~|\vA|\to|\vB|$, we need to change the
$\leq\in\mathbb{N}\times\mathbb{N}$ relation into some relation
$\vR\in|\vB|\times|\vB|$ such that:

$$
\begin{array}{l}
\forall\vf~:~\vA\to\vB, \\
  \qquad
  \mathit{s.t.}~\forall\vx~:~\vA, |\vf~\vx|~\vR~|\vf|~|\vx|
\end{array}
$$

\noindent
The relation $\vR$ cannot be any arbitrary relation. Instead, we require the
irreflexive variant of $\vR$, $\vR'$, to be well-founded. In order to
differentiate the reflexive and irreflexive variants, we will use $\vR_{\leq}$
for the reflexive, and $\vR_{<}$ for the irreflexive. The reason for this
notation is that, ideally, we want to provide a way to lift the $\leq$ and $<$
relations on natural numbers to more complex size specifications built on top
of naturals.

Lifting relations to more complex types can be done in a systematic way. We
show now how this is done for a relation $\vr\in\lbrace\leq, <\rbrace$ relation. Whenever we have a natural
number as a size, we do not need to do anything.

\[
  \inference{\vn_1~\vr~\vn_2 & \vn_1, \vn_2\in\mathbb{N}}{\vn_1~\vR_{\vr}~\vn_2}
\]

\noindent
For disjoint sums, the only possibility is to lift an underlying relation on
the left or right types. It is also possible to lift two relations to both
types, but that would require specifying how to relate (if needed) values of
the left and right type. This latter case is not considered here, since it is
not required.

\[
  \begin{array}{cc}
%    \multicolumn{2}{c}{\forall\var{z}_1,\var{z}_2\in\keyword{SizeSpec}} \\ \\
    \inference*{\var{z}_1~\vR_{\vr}~\var{z}_2}{\inl~\var{z}_1~\vR^{\inl}_{\vr}~\inl~\var{z}_2} &
      \inference*{}{\inl~\var{z}_1~\vR^{\inl}_{\vr}~\inr~\var{z}_2} \\\\
    \inference*{\var{z}_1~\vR_{\vr}~\var{z}_2}{\inr~\var{z}_1~\vR^{\inr}_{\vB}~\inr~\var{z}_2} &
      \inference*{}{\inr~\var{z}_1~\vR^{\inr}_{\vr}~\inl~\var{z}_2} \\
  \end{array}
\]

\noindent
% We define $\vR^{\inr}_{\vB}$ in a symmetric way.
For product types, we define two lifting relations, corresponding to the two
projections:
\[
 \begin{array}{cc}
   \inference{\var{z}_{11}~\vR_{\vr}~\var{z}_{21}}{\langle\var{z}_{11},\var{z}_{12}\rangle~\vR^{\pi_1}_{\vr}~\langle\var{z}_{21},\var{z}_{22}\rangle} &
   \inference{\var{z}_{12}~\vR_{\vr}~\var{z}_{22}}{\langle\var{z}_{11},\var{z}_{12}\rangle~\vR^{\pi_2}_{\vr}~\langle\var{z}_{21},\var{z}_{22}\rangle} \\
 \end{array}
\]
\noindent
Again, there are different alternatives to lift two relations to a tuple
considering both components, but we do not require that case in this paper.

Finally, we show how to lift a relation $\vR_{\vr}\in|\vA|\times|\vA|$ to a
functor applied to $|\vA|$, $\vF$. In order to do this, and since this functor
is a polynomial functor with a known shape, we can lift the relation to all occurrences of
values of type $|\vA|$ in $\vF~|\vA|$.

\[
  \begin{array}{l}
    \forall~\vf_1,\vf_2~:~\vF~|\vA|.~(\forall~\vz_1,\vz_2~:~|\vA|, \\
    \qquad\vz_1\in\vf_1\wedge\vz_2\in\vf_2\Rightarrow\vz_1\vR_{\vr}\vz_2)\Rightarrow\vf_1\vR^{\vF}_{\vr}\vf_2
  \end{array}
\]

\noindent
\paragraph{Deriving Functor Size Specifications.} It is sometimes useful to
provide a relation between the sizes of the structures defined by a polynomial
functor. In order to do so, we first need to know what is this size. Observe
that, although we allow any arbitrary definition of a size specification for a
functor, we can also automatically derive one that takes into account only the
size of the structure. We first define $|\vF~\vA|=\mathbb{N}\times|\vA|$, and
derive a function $|\cdot|~:~\vF~\vA\to\mathbb{N}\times|\vA|$. Informally, if
$\vx~:~\vF~\vA$, $|\vx|=(\vn,\vz)$ means that there are $\vn$ elements of type
$\vA$, of maximum $\vz~:~|\vA|$ size. In order to define the maximum function,
we need a decidable well-founded relation on $|\vA|$. Calculating the number of
elements of type $\vA$ in $\vF$ is defined in two steps. First, all occurrences
of values of type $\vA$ in the structure are replaced by $1$ by applying
$\vF~(\Const{1})~:~\vF~\vA\to\vF~\mathbb{N}$. Then, we apply a transformation
to the structure of $\vF$ that adds all natural numbers in the positions
defined by $\vF$, or returns zero if $\vA$ does not appear in $\vF~\vA$.
\[
  \begin{array}{lcl}
    \keyword{sum} & : & \vF~\mathbb{N}\to\mathbb{N} \\

    \keyword{count} & : & \vF~\vA~\to\mathbb{N} \\
    \keyword{count} & = & \keyword{sum}\circ\vF~\Const{1} \\
  \end{array}
\]

The only case that seems slightly complicated appears in the treatment of the
type $\mu\vF$ type. However, we can treat this case in a similar way. Since there are no elements to count, we start by showing how to automatically calculate the depth of this structure. The
count function is defined recursively as follows:
\[
  \begin{array}{lcl}
    \keyword{count} & : & \mu\vF\to\mathbb{N} \\
    \keyword{count} & = & ((+1)\circ\max)\circ\vF~\keyword{count}\circ\hout{\vF}
  \end{array}
\]
\noindent
This definition is actually a catamorphism, so we could simply write,
$\hylo{\vF}~((+1)\circ\max)~\hout{\vF}$. For functors of a polymorphic type,
$\mu(\vF~\vA)$, we simply need to replace the $((+1)\circ\max)$ function for a
$\keyword{sum2}~:~\vF~\mathbb{N}~\mathbb{N}\to\mathbb{N}$ function that sums
all natural numbers that appear in the structure, by replacing every occurrence
of a type that is not a parameter of the bifunctor by zero, and replacing
product types by natural number addition. Then, the count function becomes
straightforward:
\[
  \begin{array}{lcl}
    \keyword{count} & : & \mu(\vF~\vA)\to\mathbb{N} \\
    \keyword{count} & = & \hylo{\vF~\vA}~(\keyword{sum2}\circ(\vF~\Const{1}~\Idf))~\hout{\vF~\vA}
  \end{array}
\]

\noindent
We believe that this can be further automated. However, since this is not the
scope of this paper, we will focus on showing how can we apply the technique
that we have just described in order to ensure termination and calculate costs.

\paragraph{Extended typing rules.}
Using these definitions, we can ensure termination by placing some restrictions
on the typing rules. First, we see that the only possibility for non-termination
is in the coinductive part of the hylomorphism. Using the size abstract
interpretations, we can restrict the anamorphism by requiring the following
property in the type of the hylomorphisms.

\[
  \inference{\vs_1~:~\vF~\vB\to\vB & \vs_2~:~\vA\to\vF~\vA
            \\ \forall\vx~:~\vA,\forall\vy~:~\vA, \vy\in(\vs_2~\vx)\Rightarrow\vy~\vR_{<}~\vx}
            {\hylo{\vF}~\vs_1~\vs_2~:~\vA\to\vB}
\]

\noindent
This restriction suffices for all recursive structures in the language that we
are defining that have an anamorphism part that is not $\hout{\vF}$: tail
recursion, feedback and divide and conquer.

% alternatively this, not sure ...
%  \forall\vx~:~\vA, \exists\var{z}_1,\var{z}_2~:~\keyword{SizeSpec}~\text{, s.t. }~
%|\vx|~\vR_{\leq}~\var{z_1}\Rightarrow |\vf~\vx|~\vR_{\leq}~|\vf|~|\vx|~\vR_{\leq}~\var{z}_2}

\begin{figure*}
\[
  \begin{array}{c c c c c}
%    \multicolumn{5}{c}{
    \inference{\rho(\vf)=\vA\to\vB \\
  \forall\vx:\vA,~|\vf~\vx|~\vR_{\leq}~|\vf|~|\vx|}
              {\tyd~\vf~:~\vA\to\vB} & \quad &

    \inference{\tyd~\vs_2~:~\vA\to\vB \\ \tyd~\vs_1~:~\vB\to\vC }
    {\tyd~\scomp{\vs_1}{\vs_2}~:~\vA\to\vC} \\\\

    \inference{\tyd~\vs~:~\vA\to\vB}
              {\tyd~\smap{\vs}~:~\vF~\vA\to\vF~\vB} & \quad &
    \inference{\tyd~\vs~:~\vA^\vk\to\vA}
              {\tyd~\sfold{\vk~\vn}{\vs}~:~\treeTy_{\vk,\vn}~\vA~\to\vA} \\\\ % & \quad &

    \inference{\tyd~\vs~:~\vA\to\vA^\vk}
              {\tyd~\sunfold{\vk~\vn}{\vs}~:~\vA\to~\treeTy_{\vk,\vn}~\vA}  & \quad &
    \multicolumn{3}{c}{
    \inference{\tyd~\vs~:~\vA\to(\vA+\vB) \\
    \forall\var{z}:\keyword{SizeSpec},~(\inl~\var{z})~\vR^\inl_{<}~(|\vs|~\var{z})}
      {\tyd~\iter{\vs}~:~\vA\to\vB}} \\\\
  \end{array}
\]
\caption{TODO:Outdated, Sized Typing rules for Structured Expressions, $\Seqp$.}
\label{fig:seq_types_size}
\end{figure*}
\begin{figure}
\[
%  \begin{array}{c c c c c}
  \begin{array}{c}
    \inference{\tyd\vs~:~\vA\to\vB}{\tyd\pseq{\vs}~:~\vF~\vA\to\vF~\vB} \\\\ % & \quad &
    \inference{\Gamma~\vdash~\vp_1~:~\vF~\vA\to\vF~\vB \\ \Gamma~\vdash~\vp_2~:~\vF~\vB\to\vF~\vC}{\Gamma~\vdash~\pipe{\vp_1}{\vp_2}~:~\vF~\vA\to\vF~\vC}  \\\\
    \inference{\Gamma~\vdash~\vp~:~\vF~\vA\to\vF~\vB}{\Gamma~\vdash~\farm{\vF}{\vn}{\vp}~:~\vF~\vA\to\vF~\vB}
\\\\
    %% \inference{\Gamma~\vdash~\vp_1~:~\listTy~\vA\to\listTy~\vB}{\Gamma~\vdash~\ofarm{\vn}{\vp_1}~:~\listTy~\vA\to\listTy~\vB} & \quad &
    %% \inference{\Gamma~\vdash~\vp_1~:~\bagTy~\vA\to\bagTy~\vB}{\Gamma~\vdash~\ufarm{\vn}{\vp_1}~:~\bagTy~\vA\to\bagTy~\vB} \\\\

    \inference{\Gamma~\vdash~\vs_1~:~\vA\to\vA^\vk
               \\ \Gamma~\vdash~\vs_2~:~\vA\to\vB
               \\ \Gamma~\vdash~\vs_3~:~\vB^\vk\to\vB}
              {\Gamma~\vdash~\dc{\vk,\vn}{\vs_1~\vs_2~\vs_3}~:~\vF~\vA\to\vF~\vB} \\\\
% & \quad &
    \inference{\Gamma~\vdash~\vp~:~\vF~\vA\to\vF~(\vA+\vB) \\
    \forall\var{z}:\keyword{SizeSpec},~\vn:\mathbb{N},~(\mathcal{F}^{\vn}~(\inl~\var{z}))~\vR^{\inl^\mathcal{F}}_{<}~(|\vp|~(\mathcal{F}^\vn~\var{z})) }
              {\Gamma~\vdash~\fb{\vp}~:~\bagTy~\vA\to\bagTy~\vB} \\\\
  \end{array}
\]
\caption{TODO Outdated, Sized Typing Rules for Structured Parallel Processes, $\Parp$.}
\label{fig:par_types_size}
\end{figure}


TODO\@: the following property must be proven for this type system, intuitively it holds, by induction on the structure of the typing rules for sequential expressions:

\[
  \forall (\vs~:~\vA\to\vB) (\vx~:~\vA) |\ssem{\vs}~\vx|~\vR_{\leq}~|\vs|~|\vx|
\]

\noindent
and the same for the parallel programs
\[
  \begin{array}{c l}
    \multicolumn{2}{l}{
    \forall \vp:\vH\vA\to\vH~\vB, \vx:\vH~\vA.} \\
    \qquad & \phantom{\wedge~} |\psem{\vp}~\vx|~\vR^\mathcal{\vF}_{\leq}~|\vp|~|\vx| \\
     & \wedge~|\psem{\vp}~\vx|~\vR^\uparrow_{=}~|\vp|~|\vx|
  \end{array}
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END REWRITING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Cost Models}

\dccomment{We have two costs, the amortised costs for streaming processes, costs of expressions.}

TODO\@: Change the letter for the cost models (T doesn't seem right), simplify
them (the architecture can be a global parameter that we don't need to pass
around), and relate with the operational semantics.

We define a function $\cost{\cdot}$ that takes a structured expression or
parallel process, and returns a \emph{cost model}. These cost models provide an
estimate of the run-time of a structured expression or parallel process on a
finite input of known size. TODO\@: amortised cost for streaming operations would simplify
extending this to infinite input streams.

\dccomment{The cost models are defined independently for each language
construct. That allows us to determine the minimum number of parameters needed
in each cost model (some of them require size functions, some others don't,
some of them require the number of workers as a parameter, some other don't).
An important TODO is to provide a core-instantiation, that must be equivalent
to the run-time worker-to-core mapping. If that is done, maximum or + and
division would be applied only when the mapping assigned the workers to
different cores.}

TODO\@: at the beginning of the section, include that we write $\vx^\vk$
instead of $\langle\vx,\ldots,\vx\rangle$ $\vk$ times, and that we write:

$$\langle\vx_1,\ldots,\vx_n\rangle\doubleplus\langle\vy_1,\ldots,\vy_m\rangle =\langle\vx_1,\ldots,\vx_n, \vy_1,\ldots,\vy_m\rangle$$

\noindent We assume that we know the cost models of the atomic functions used
to build structured expressions and parallel programs. Note that, although for
the size functions we need the sizes to determine an upper bound, in the case
of the cost models it is not needed, therefore providing us with more
flexibility: using average sizes, or specifying a \emph{range}.\TODO{is it
worth changing the description of the sizes to include both cases?} The cost of
a sequential composition of structured expressions is the addition of the cost
of the two sub-expressions, the second one receiving a parameter of size
determined by the size function of the first applied to the input. The cost of
a map is $\vn$ times the cost of the embedded structured expression on inputs
of the size described by the nested size specification. The cost of a fold is
the cost of applying $\vn/\vk$ $\vk$-ary operations on $\vk$ tuples of the
inner size, plus the cost of folding $\vn/\vk$ elements of the result size,
until only one element is left. The cost of an unfold is the symmetric, i.e.\
the cost of unfolding once plus the cost of $\vk$ unfolds. The cost of
$\keyword{iter}$ is the cost of sub-expression on the successive output sizes,
until a result is reached.


The cost of the structured parallel programs is defined in similar terms, but
for a program running in a \emph{steady state}. We define a \emph{steady state}
a state with its shared queues containing enough elements so that there are
enough tasks for all workers in the parallel program. The cost of a parallel
program on an input of size $\mathcal{F}^\vn~\vz$ is, then, the time needed to
produce $\vn$ outputs from a \emph{steady state}. Note that, in order to
produce more accurate estimates, we would have to split the cost into the cost
of \emph{initialising} the queues, the cost of computing $\vn$ tasks in steady
state, and the cost of \emph{flushing} the queues. Since for a sufficiently
large number of tasks the cost that will dominate will be the cost in steady
state, we can safely ignore these initial and final costs.

We can prove that,

\[
  \begin{array}{l}
  \forall\vn~:~\mathbb{N}, \vz~:~\keyword{SizeSpec}, \vp~:~\vH~\vA\to\vH~\vB, \\
  \qquad\exists\mathit{cost}~:~\keyword{SizeSpec}\to\keyword{Timing}, \\
  \qquad\qquad\text{s.t.}~\cost{\vp}(\va,\mathcal{F}^\vn~\vz)~=~\vn\times\mathit{cost}(\vz)
  \end{array}
\]

\noindent
TODO amortised cost, and show that it is exactly this $\mathit{cost}$.

\TODO{rethinking the cost models to allow an easier connection with the
operational semantics.}

TODO\@: Cost div and merge in a DC, explain,

\[
  \mathcal{T}_{\keyword{Tree}^\vk}~\vc~\var{zs}~=~\begin{cases}
    0 &\text{, if $\var{zs}=[]$,} \\
    \vc~\vz~+~\vk\times\mathcal{T}_{\keyword{Tree}^\vk}~\vc~\var{zs}' &\text{, if $\var{zs}=\vz\lhd\var{zs}'$,} \\
  \end{cases}
\]

\[
  \mathcal{A}_{\keyword{Tree}^\vk}~\vc~\var{zs}~=~\begin{cases}
    0 &\text{, if $\var{zs}=[]$,} \\
    \max\lbrace\vc~\vz,~\mathcal{T}_{\keyword{Tree}^\vk}~\vc~\var{zs}'\rbrace &\text{, if $\var{zs}=\vz\lhd\var{zs}'$,} \\
  \end{cases}
\]

\[
  \mathcal{T}_{\keyword{fold}~\vk}~\vs~(\mathcal{F}^\vn~\vz)~=~\begin{cases}
    [] &\text{, if $\vn=1$,} \\
    \mathcal{T}_{\keyword{fold}~\vk}~\vs~(\mathcal{F}^{\vn/\vk}~(\vs~\vz^\vk))\rhd\vz^\vk&\text{, otherwise.} \\
  \end{cases}
\]

\[
  \mathcal{T}_{\keyword{unfold}~\vk~\vn}~\vs~\vz~=~\begin{cases}
    [] &\text{, if $\vn=0$,} \\
    \vz\lhd\mathcal{T}_{\keyword{unfold}~\vk~(\vn-1)}~\vs~(\max~(\vs~\vz))&\text{, otherwise.} \\
  \end{cases}
\]

\[
  \mathcal{T}_{\keyword{unfold}}~\vn~\vk~\vs~\vc~\vz~=~\begin{cases}
    0 &\hspace{-3cm}\text{, if $\vn=0$,} \\
    \vc~\vz~+~\vk\times\mathcal{T}_{\keyword{unfold}}~(\vn-1)~\vk~\vs~\vc~(\max~(\vs~\vz)), \\ &\hspace{-3cm}\text{otherwise.} \\
  \end{cases}
\]

\[
  \mathcal{C}_{\keyword{div}}(\vk,\vn,\vc)(\va,\vz)~=~\begin{cases}
    0 &\text{, if $\vn=0$,} \\
    \max\lbrace\vc(\va,\vz), \mathcal{C}_{\keyword{div}}(\vk,\vn-1,\vc)(\va,\vz)\rbrace \\
  \end{cases} \\
\]


\[
  \mathcal{C}_{\keyword{merge}}(\vk,\var{size},\vc)(\va,\mathcal{F}^\vm~\vz)~=~\begin{cases}
    0 &\text{, if $\vm=1$,} \\
    \max\lbrace\vc(\va,\vz^\vk), \mathcal{C}_{\keyword{merge}}(\vk,\vn,\vc)(\va,\mathcal{F}^{\frac{\vm}{\vk}}~(\var{size}~\vz^\vk))\rbrace \\
  \end{cases} \\
\]

\begin{figure*}
\[
\begin{array}{lcl}
  \multicolumn{3}{l}{\cost{\vf}(\vz)~\text{assumed}} \\% & = & \(\va,\vz) \\

  \cost{\scomp{\vs_1}{\vs_2}}(\vz) & = & \cost{\vs_1}(|\vs_2|~\vz) + \cost{\vs_2}(\vz) \\

  \cost{\smap{\vs_1}}(\mathcal{F}^\vn~\vz) & = & \vn\times\cost{\vs_1}(\vz) \\

  \cost{\sfold{\vk~\vs_1}}(\mathcal{F}^\vn~\vz) & = & \begin{cases}
    0 &\text{, if $\vn=1$,} \\
    \cost{\vs_1}(\vz^\vn\doubleplus\ve^{\vk-\vn}) &\text{, if $\vn<\vk$,} \\
    \vn/\vk\times\cost{\vs_1}(\vz^\vk) + \cost{\sfold{\vk~\vs_1}}(\mathcal{F}^{\vn/\vk}~(|\vs_1|~\vz^\vk)) &\text{, otherwise} \\
  \end{cases} \\

  \cost{\sunfold{\vk~\vn~\vs_1}}(\vz) & = & \begin{cases}
    0 &\text{, if $\vn=0$,} \\
    \cost{\vs_1}(\vz) + \vk\times\cost{\sunfold{\vk~(\vn-1)~\vs_1}}(\max(|\vs_1|~\vz')) &\text{, otherwise} \\
  \end{cases} \\

  \cost{\iter{\vs_1}}(\vz) & = & \begin{cases}
    \cost{\vs_1}(\vz) &\text{, if $|\vs_1|~\vz=\inr~\vz'$} \\
    \cost{\vs_1}(\vz) + \cost{\iter{\vs_1}}(\vz') &\text{, if $|\vs_1|~\vz=\inl~\vz'$} \\ \\ \\
  \end{cases} \\

  \cost{\pseq{\vs_1}}(\mathcal{F}^\vn~\vz) & = & \vn\times\cost{\vs_1}(\vz) \\
  \cost{\pipe{\vp_2}{\vp_1}}(\vz) & = & \max\lbrace\cost{\vp_1}(|\vp_2|~\vz),~\cost{\vp_2}(\vz)\rbrace \\
  \cost{\farm{}{\vn}{\vp_1}}(\mathcal{F}^\vm~\vz) & = & \cost{\vp_1}(\mathcal{F}^{\frac{\vm}{\vn}}~\vz) \\

  \cost{\dc{\vk,\vn}{\vs_1~\vs_2~\vs_3}}(\mathcal{F}^\vm~\vz) & = &
    \vm\times\max\lbrace\mathcal{C}_{\keyword{div}}(\vk,\vn,\cost{\vs_1})(\vz),~\cost{\vs_2}(\vz'),~\mathcal{C}_{\keyword{merge}}(\vk,|\vs_3|,\cost{\vs_3})(\vz'')\rbrace \\
    & \multicolumn{2}{l}{\text{where}~\mathcal{F}^\vm~\vz' = |\sunfold{\vk~\vn~\vs_1}|~\vz}  \\
    & \multicolumn{2}{l}{\phantom{\text{where}}~\vz'' = \mathcal{F}^\vm~(|\vs_2|~\vz)}  \\ \\

    \cost{\fb{\vp_1}}(\mathcal{F}^\vn~\vz) & = & \begin{cases}
    \cost{\vp_1}(\mathcal{F}^\vn~\vz) &\text{, if $|\vp_1|~(\mathcal{F}^\vn~\vz)~=~\mathcal{F}^\vn~(\inr~\vz')$} \\
    \cost{\vp_1}(\mathcal{F}^\vn~\vz) + \cost{\fb{\vp_1}}(\mathcal{F}^\vn~\vz') &\text{, if $|\vp_1|~(\mathcal{F}^\vn~\vz)~=~\mathcal{F}^\vn~(\inl~\vz')$} \\
    \end{cases}
\end{array}
\]
\caption{TODO  Outdated Cost models.}
\end{figure*}

%    \cost{\Nfb}(\vsf,\vc)(\va,[\vz]^\vn) & = &
%    \begin{cases}
%       \keyword{tRet}(\va) &, \text{if $\tlsize{\vz}=0$,} \\
%      \vc(\va,[\vz]^\vn)+\cost{\Nloop}(\vsf,\vc)(\va,\vsf([\vz]^\vn)) &, \text{otherwise}.
%    \end{cases} \\
