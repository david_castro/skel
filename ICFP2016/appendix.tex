\appendix

\section{Matrix Multiplication}

% The size specification for a matrix is a tuple that contains the dimensions,
% $|\Matrix|~=~\mathbb{N}\times\mathbb{N}$.
Matrix multiplication is known to have several parallelisations.
We represent the functional behaviour of matrix multiplication using an
%structured expression that is based on a
algorithm %for % multiplying matrices
that uses block matrix decomposition. We
assume that we have a type $\Matrix$ that represents matrices, and two
functions $\keyword{dH}, \keyword{dV}~:~\Matrix\to\Matrix\times\Matrix$ that  respectively decompose a
matrix horizontally or vertically. The resulting tuple contains
the sub-matrices to be multiplied. Using these functions, it is easy to
implement a function that splits a pair of matrices into a collection of pairs
of matrices to multiply, until their size is smaller than some constant threshold, $\vz$:

$
\begin{array}{lcl}
\vF~\vA~\vB &~=~& \vA~+~(\vB^2~+~\vB^2~+~\vB^2) \\
\vG~\vA &~=~& \vF~\Matrix\times\Matrix~\vA \\

\keyword{distl} &~:~& (\vA\times\vB)\times\vC\to(\vA\times\vC)\times(\vB\times\vC) \\
\keyword{distr} &~:~& \vA\times(\vB\times\vC)\to(\vA\times\vB)\times(\vA\times\vC) \\
\keyword{dist} &~:~& (\vA\times\vB)\times(\vC\times\vD)\to(\vA\times\vC)\times(\vB\times\vD) \\\\

\keyword{div} &~:~& \Matrix\times\Matrix\to\vG~(\Matrix\times\Matrix) \\
\keyword{div}~(\vm_1,\vm_2) &~=~& \inl~(\vm_1,\vm_2),~\text{if $\max(\va,\vb,\vc)\leq\vz$}  \\
        &~=~& \inr~(\inj_1~(\keyword{distl}~(\keyword{dH}~\vm_1,\vm_2))),~\text{if $\max(\va,\vb,\vc)=\va$}  \\
        &~=~& \inr~(\inj_2~(\keyword{distr}~(\vm_1,\keyword{dV}~\vm_2))),~\text{if $\max(\va,\vb,\vc)=\vc$}  \\
        &~=~& \inr~(\inj_3~(\keyword{dist}~(\keyword{dV}~\vm_1,\keyword{dH}~\vm_2))),~\text{if $\max(\va,\vb,\vc)=\vb$}  \\
                              &\multicolumn{2}{l}{\text{where $|\vm_1|~=~(\va,\vb)$ and $|\vm_2|~=~(\vb,\vc)$}} \\
\end{array}
$

\noindent
We assume the functions $\keyword{cH},
\keyword{cV}~:~\Matrix\times\Matrix\to\Matrix$,
that invert $\keyword{dH}$ and $\keyword{dV}$,
and
$\keyword{sum}~:~\Matrix\times\Matrix\to\Matrix$.
%  that sums the two
% input matrices.

$
\begin{array}{lcl}
\keyword{merge} &~:~& \vG~\Matrix~\to~\Matrix \\
\keyword{merge}~(\inl~\vm) &=& \vm \\
\keyword{merge}~(\inr~(\inj_1~(\vm_1,\vm_2))) &=& \keyword{cH}~(\vm_1,\vm_2) \\
\keyword{merge}~(\inr~(\inj_2~(\vm_1,\vm_2))) &=& \keyword{cV}~(\vm_1,\vm_2) \\
\keyword{merge}~(\inr~(\inj_3~(\vm_1,\vm_2))) &=& \keyword{sum}~(\vm_1,\vm_2) \\
\end{array}
$

\noindent
In order to complete the algorithm, we need a function
$\keyword{imult}~:~\Matrix^2\to\Matrix$ for iterative matrix
multiplication. We ignore the parallel structure $\sigma$ for now:
\[
\begin{array}{lcl}
  \keyword{matmul} &~:~& \List~(\Matrix\times\Matrix)\xmapsto{\sigma}\List~(\Matrix) \\
  \keyword{matmul}~&~=~&~\smap{\List}~(\shylo{\vG}~(\keyword{merge}\bullet\vF~\keyword{imult})~\keyword{div}) \\
\end{array}
\]

\endinput
\noindent
The structure of the expression suggests two easy parallelisations.
First, observe that a ``reforestation'' normalisation procedure produces the
following structure:
%
$
\sigma=\MAP{\vL}(\CATA{\vF}\,\AF)\bullet\MAP{\vL}(\MAP{\vF}\,\AF)\bullet\MAP{\vL}(\ANA{\vF}\,\AF)
$.
%
\noindent
Apart from the obviously parallelisable $\MAP{\vL}$ structure
(as defined in Section~\ref{skel_def}, bifunctor $\vL$ is the base functor for lists),
$\MAP{\vF}$ can be parallelised
under a $\PEVAL$ structure. First, since all our parallel structures are
streaming, we can rewrite the structure, applying map fusion on all the $\MAP{\vL}$,
$
\sigma=\MAP{\vL}(\CATA{\vF}\,\AF\bullet\MAP{\vF}\,\AF\bullet\ANA{\vF}\,\AF)
$.
%
\noindent
Depending on the threshold, $\vz$, it may be reasonable to use a task farm for the
generated sub-matrices.
%
$
\sigma=\MAP{\vL}(\CATA{\vF}\,\AF\bullet\PEVAL(\FARM\,\vn\,(\FUNC{\vF}\,\AF))\bullet\ANA{\vF}\,\AF)
$.
%
\noindent
Alternatively, if there are enough inputs, it may be worth choosing a different parallel
version. Assuming that the cost of dividing the input matrices is much lower than the cost of
multiplying the sub-matrices or the cost of the catamorphism part, we can use a pipeline with
a task farm in the second stage,
$
\sigma=\FUNC{\vL}(\ANA{\vF}\,\AF)\parallel\FARM\,\vn\,(\FUNC{\vL}(\CATA{\vF}(\AF\bullet\vF\,\AF)))
$,
which can be simplified to
\noindent
% Note that by the structure of the expression, we could simplify it to:
%
$
\sigma~=~\FUNC{\vL}~(\ANA{\vF}~\AF)~\parallel~\FARM~\vn~\ANY
$.
%
\noindent
The reason why we cannot write $\ANY\parallel\FARM\,\vn\,\ANY$ is because there
are two possibilities for instantiating this structure. The ambiguity comes
from the \textsc{Hylo-Shift} law, since $\vF~\AF$ can happen both in the
catamorphism and anamorphism sides.
% That means that it is equivalent to
% multiply the matrices as you reach the base case in the divide part, or delay
% this multiplication until the ``merge'' part.
We need to use the procedure $\min$ to disambiguate
such situations. For example, assuming a cost function $\keyword{cost}~:~\Sigma\to\keyword{Cost}$,
%
$
\sigma=\min\,\keyword{cost}\,(\ANY\parallel\FARM\,\vn\,\ANY)
$.
%
\noindent
Finally, observe that all these structures are equivalent to divide-and-conquer,
$\sigma~=~\DC{\vn,\vL,\vF}~(\AF~\bullet~\vF~\AF)~\AF$
or
$\sigma~=~\DC{\vn,\vL,\vF}~\AF~(\vF~\AF~\bullet~\AF)$.
\noindent
Again, there are two possible instantiations
that can be disambiguated using some cost model,
$
%\begin{array}{l}
\sigma~=~\min~\keyword{cost}~(\DC{\vn,\vL,\vF}~\ANY~\ANY)
%\end{array}
$.
%
\noindent
By using a type system that abstracts over the structure, we
provide a way to change the parallel structure
that gives strong static guarantees
that the functional behaviour is not changed. Moreover, we provide a way to
reason about the different parallel structures. Since
our type system parameterises the cost model, an implementation can use user-defined
cost models that take into account different parameters of the target
architecture, for example.
%% Move to the conclusion if important
% Note also that one important limitation of our approach is the
% dependence on the particular structured expression that describes the
% functional behaviour of the parallel program. This limitation can be tackled by
% using more complex, and general decision procedures for the semantic
% equivalence of structured expressions.

\section{Proofs of Confluence}

This appendix shows the core part of the proof that the rewriting systems
$\prw$ and $\srw$ are confluent. We name the rewriting rules using the name of
the equivalence rules from which they are derived. For example,
\tsc{farm-equiv} is used for the rewriting
$\FARM{\vn}~\sigma\rightsquigarrow\sigma$.

\begin{lemma}
Both sides of all critical pairs of $\prw$ can reduce to the same form.
\end{lemma}

\renewcommand{\proofname}{Proof}

  \begin{proof} We show for each pair of rules all possible structures that yield a critical pair, and give a justification when there is none. \\
  \noindent
  \emph{Case \tsc{farm-equiv}, \tsc{pipe-equiv}. $\sigma=\FARM{\vn}~(\FUNC~\sigma_1\parallel\FUNC~\sigma_2)$}.
      \begin{itemize}
        \item[] \emph{Case 1}. By \tsc{pipe-equiv}, $\sigma\rightsquigarrow\FARM{\vn}~(\FUNC~(\sigma_2\bullet\sigma_1))$.
          By applying \tsc{farm-equiv}, $\sigma$ reduces to $\FUNC~(\sigma_2\bullet\sigma_2)$ \\

        \item[] \emph{Case 2}. By \tsc{farm-equiv}, $\sigma\rightsquigarrow(\FUNC~\sigma_1\parallel\FUNC~\sigma_2)$.
          By \tsc{pipe-equiv}, $\sigma$ reduces to $\FUNC~(\sigma_2\bullet\sigma_2)$.

      \end{itemize}

  \noindent
  \emph{Case \tsc{farm-equiv}, \tsc{dc-equiv}}. $\sigma=\FARM{\vn}~(\DC{\vm,\vF}~\sigma_1~\sigma_2)$.
  \begin{itemize}
    \item[] \emph{Case 1}. By applying first \tsc{farm-equiv} and then \tsc{dc-equiv}, $\FARM{\vn}~(\DC{\vm,\vF}~\sigma_1~\sigma_2)\rightsquigarrow\DC{\vm,\vF}~\sigma_1~\sigma_2\rightsquigarrow\FUNC~(\HYLO{\vF}~\sigma_1~\sigma_2)$. \\

    \item[] \emph{Case 2}.By applying first \tsc{dc-equiv} and then \tsc{farm-equiv}, $\FARM{\vn}~(\DC{\vm,\vF}~\sigma_1~\sigma_2)\rightsquigarrow\FARM{\vn}~(\FUNC~(\HYLO{\vF}~\sigma_1~\sigma_2))\rightsquigarrow\FUNC~(\HYLO{\vF}~\sigma_1~\sigma_2)$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{farm-equiv}, \tsc{fb-equiv}}. $\sigma=\FARM{\vn}~(\FB~(\FUNC~\sigma))$
  \begin{itemize}
    \item[] \emph{Case 1}. \tsc{farm-equiv}, then \tsc{fb-equiv}. $\FARM{\vn}~(\FB~(\FUNC~\sigma))\rightsquigarrow\FB~(\FUNC~\sigma)\rightsquigarrow\FUNC~(\HYLO{(+B)}~(\ID\triangledown\ID)~\sigma)$ \\
    \item[] \emph{Case 2}. \tsc{fb-equiv}, then \tsc{farm-equiv}. $\FARM{\vn}~(\FB~(\FUNC~\sigma))\rightsquigarrow\FARM{\vn}~(\FUNC~(\HYLO{(+B)}~(\ID\triangledown\ID)~\sigma))\rightsquigarrow\FUNC~(\HYLO{(+B)}~(\ID\triangledown\ID)~\sigma)$
  \end{itemize}

  \noindent
  \emph{Case \tsc{farm-equiv}, with a $\PEVAL_\vT$ structure}. There are no critical pairs, since a $\FARM{\vn}$ is in $\PSigma$, and $\PEVAL_\vT$ in $\Sigma$. If $\sigma=\PEVAL_\vT~(\FARM{\vn}~(\FUNC~\sigma))$, then the only possible order is \tsc{farm-equiv}, and then the \tsc{par-map} rule. \\

  \noindent
  \emph{Case \tsc{pipe-equiv}, and \tsc{dc-equiv}}. There are no
  critical pairs, since \tsc{pipe-equiv} requires both sides to be a $\FUNC$,
  which implies that \tsc{dc-equiv} must always be applied first. \\

  \noindent
  \emph{Case \tsc{pipe-equiv}, and \tsc{fb-equiv}}. There are no
  critical pairs, since \tsc{pipe-equiv} requires both sides to be a $\FUNC$,
  which implies that \tsc{fb-equiv} must always be applied first. \\

  \noindent
  \emph{Case \tsc{pipe-equiv}, with a $\PEVAL_\vT$ structure}. The
  rule for $\PEVAL{\vT}$ requires a $\FUNC$, so there are no critical pairs. \\

  \noindent
  \emph{Case \tsc{dc-equiv} and \tsc{fb-equiv}}. There are no
  critical pairs, since \tsc{fb-equiv} requires its structure to be a $\FUNC$,
  which implies that \tsc{dc-equiv} must always be applied first. \\

  \noindent
  \emph{Case \tsc{dc-equiv} and $\PEVAL_\vT$}. There are no critical pairs,
  since the rule for $\PEVAL_\vT$  requires a $\FUNC$, so \tsc{dc-equiv} must
  be applied first.

  \noindent
  \emph{Case \tsc{fb-equiv} and $\PEVAL_\vT$}. There are no critical pairs,
  since the rule for $\PEVAL_\vT$  requires a $\FUNC$, so \tsc{fb-equiv} must
  be applied first.

\end{proof}

\begin{lemma}
All critical pairs that arise from the rules $\srw$ can reduce to the same form.
\end{lemma}

\begin{proof} The proof follows the same structure as the one for $\prw$. \\
\begin{itemize}
   \item[] \emph{Case 1}.
\end{itemize}

  \noindent
  \emph{Case \tsc{id-cancel-l} and \tsc{id-cancel-r}}. $\sigma_1=\ID\bullet\ID$ and $\sigma_2=\ID\bullet\sigma\bullet\ID$.
  Trivial. $\sigma_1\rightsquigarrow^\text{*}\ID$ and $\sigma_2\rightsquigarrow^\text{*}\sigma$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{out-in-cancel}}. $\sigma_1=\ID\bullet\OUT\bullet\IN$ and  $\sigma_2=\OUT\bullet\IN\bullet\ID$. Trivial.  $\sigma_1\rightsquigarrow^\text{*}\ID$ and $\sigma_2\rightsquigarrow^\text{*}\ID$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{in-out-cancel}}. $\sigma_1=\ID\bullet\IN\bullet\OUT$ and  $\sigma_2=\IN\bullet\OUT\bullet\ID$. Trivial.  $\sigma_1\rightsquigarrow^\text{*}\ID$ and $\sigma_2\rightsquigarrow^\text{*}\ID$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{hylo-cancel}}. $\sigma_1=\ID\bullet\HYLO{\vF}~\IN~\OUT$ and  $\sigma_2=\HYLO{\vF}~\IN~\OUT\bullet\ID$. Trivial.  $\sigma_1\rightsquigarrow^\text{*}\ID$ and $\sigma_2\rightsquigarrow^\text{*}\ID$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{f-cancel}}. We do not show the symmetric case. Trivial. $\sigma_1=\ID\bullet\vF~\ID$ reduces to $\ID$.

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{f-split}}.
  \begin{itemize}
    \item[] \emph{Case 1.} $\sigma_1=\vF~(\ID\bullet\sigma)$ and  $\sigma_2=\vF~(\sigma\bullet\ID)$. We solve $\sigma_1$, the other case is symmetric. By applying \tsc{f-split}, then \tsc{f-id-cancel}, then \tsc{id-cancel-l}, we get $\vF~(\ID\bullet\sigma)\rightsquigarrow\vF~\ID\bullet\vF~\sigma\rightsquigarrow\ID\bullet\vF~\sigma\rightsquigarrow\vF~\sigma$. By applying \tsc{id-cancel-l}, we get $\vF~(\ID\bullet\sigma)\rightsquigarrow\vF~\sigma.$ \\
    \item[] \emph{Case 2.} $\sigma_1=\ID\bullet\vF~(\sigma\bullet\sigma')$ and  $\sigma_2=\vF~(\sigma\bullet\sigma')\bullet\ID$. Trivial. $\sigma_1\rightsquigarrow\vF~\sigma\bullet\vF~\sigma'$ and $\sigma_2\rightsquigarrow\vF~\sigma\bullet\vF~\sigma'$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{ana-map}}. Trivial. $\sigma_1=\ANA{\vF}~(\vF~(\ID\bullet\sigma)\circ\OUT)$, $\sigma_2=\ANA{\vF}~(\vF~(\sigma\bullet\vF)\bullet\OUT)$, $\sigma_3=\ID\bullet\ANA{\vF}~(\vF~\sigma\bullet\OUT)$, $\sigma_4=\ANA{\vF}~(\vF~\sigma\bullet\OUT)\bullet\ID$. All of these trivially reduce to $\MAP{\vF}~\sigma$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{hylo-split}}. Trivial. For simplicity, we don not show the symmetric cases. $\sigma_1=\HYLO{\vF}~(\ID\bullet\sigma)~\sigma'$, $\sigma_2=\HYLO{\vF}~\sigma~(\ID\bullet\sigma')$ and $\sigma_3=\ID\bullet\HYLO{\vF}~\sigma~\sigma'$. All of these reduce to $\HYLO{\vF}~\sigma~\sigma'$.\\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{cata-split}}. Trivial. Same as \tsc{hylo-split}. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{ana-split}}. Trivial. Same as \tsc{hylo-split}. \\

  \noindent
  \emph{Cases arising from \tsc{out-in-cancel} and \tsc{in-out-split}}.
  Trivial, similar to the \tsc{id-cancel-l/r} cases. The only exception is
  \tsc{out-in-cancel} and \tsc{f-split}, $\sigma=\vF~(\OUT\bullet\IN)$. By
  \tsc{out-in-cancel}, this reduces to $\ID$. By \tsc{f-split}, we have to use
  the rule that $\vF~\vf\bullet\vF~\vf^{-1}\rightsquigarrow\ID$. Since
  $\vF~\IN$ and $\vF~\OUT$ are inverses, this reduces to $\ID$. \\

  \noindent
  \emph{Cases arising from \tsc{hylo-cancel}, trivial due to the preconditions of the rules}. \\




\end{proof}
