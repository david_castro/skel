Module Auto.

  Require Import Utf8.
  Require Import List.

(***********************************************************************)
(**                            TACTICS                                **)
(***********************************************************************)
  (* Copied from chlipala *)
  Ltac notHyp P :=
  match goal with
    | [ _ : P |- _ ] => fail 1
    | _ =>
      match P with
        | ?P1 /\  ?P2 => first [ notHyp P1 | notHyp P2 | fail 2 ]
        | ?P1 <-> ?P2 => first [ notHyp (P1 -> P2) | notHyp (P2 -> P1) | fail 2 ]
        | _ => idtac
      end
  end.

  Ltac extend pf :=
  let t := type of pf in
    notHyp t; generalize pf; intro.
  (* Copied from chlipala *)

  Ltac prop :=
    simpl in *;
    repeat
      match goal with
             | [ H : ?x |- ?x ] => exact H
             | [ H : False |- _ ] => contradiction
             | [ H : ?x ≠ ?x |- _ ] => exfalso; apply H; reflexivity


             | [    |- ?R ?x ?x ] => reflexivity
             | [ H0: ?R ?x ?y, H1 : ?R ?y ?z |- ?R ?x ?z ] => transitivity y
             | [ H: ?R ?y ?x |- ?R ?x ?y ] => symmetry

             | [ H : prod ?A ?B|- _ ]     => destruct H
             | [ H : ?A  ∧  ?B |- _ ]     => destruct H
             | [ H : ?A <-> ?B |- _ ]     => destruct H
             | [ H : (_,_) = _ |- _ ] => injection H; clear H; intros
             | [ H : _ = (_,_) |- _ ] => injection H; clear H; intros

             | [ H : ?A ∨ ?B |- _ ]       => notHyp A; notHyp B; destruct H
             | [ H : sum ?A ?B |- _ ]     => notHyp A; notHyp B; destruct H
             | [ H : sumbool ?A ?B |- _ ] => notHyp A; notHyp B; destruct H
             | [ |- _ = _ ] => congruence
             | [ |- _ ∧ _ ] => constructor
             | [ |- _ <-> _ ] => constructor

             | [ |- ?A ∨ ?B ]       => first [ left | right ]
             | [ |- sum ?A ?B ]     => first [ left | right ]
             | [ |- sumbool ?A ?B ] => first [ left | right ]

             | [ F : forall x, ?P x -> _ , H : ?P ?X |- _ ] => extend (F X H)
             | [ F : forall (x : ?A), _ , H : ?A |- _ ] => extend (F H)
             | [ F : ?x -> _, X : ?x |- _ ] => extend (F X)

           end.

  Ltac discrNeq :=
    match goal with
      | [ F : ?A ≠ ?B, X : ?A = ?B |- False ] => exact (F X)
    end.
  
  Ltac discRec F A B :=
        let H0 := fresh in
        let H1 := fresh in
        assert (sumbool (A = B) (A ≠ B))
          as H0
            by (try (apply (F A B)); eauto with *; try (decEq F A B));
          destruct H0 as [H1 | H1];
          [ subst
          | right;  let F := fresh in intro F; injection F; intros; discrNeq
          ]
  with solveEq F :=
    match goal with
      | [ |- sumbool (?A = ?A) _ ] => left; reflexivity
      | [ |- sumbool (?T ?A _ _ _ = ?T ?B _ _ _)  _ ] => discRec F A B; solveEq F
      | [ |- sumbool (?T ?A _ _ = ?T ?B _ _)  _ ] => discRec F A B; solveEq F
      | [ |- sumbool (?T ?A _ = ?T ?B _)  _ ] => discRec F A B; solveEq F
      | [ |- sumbool (?T ?A = ?T ?B) _ ] => discRec F A B; solveEq F
      | [ |- sumbool _ (?P ≠ ?Q) ] => right; let F := fresh in intro F; discriminate F
    end
  with decEq F a b :=
    destruct a; destruct b; solveEq F.

  (*
  Ltac dec H :=
    let T := type of H in
  match goal with
    | [  |-         T ∨ ¬ T  ] => fail 1
    | [  |-         ¬ T ∨ T  ] => fail 1
    | [  |-         sum T (¬ T)  ] => fail 1
    | [  |-         sum (¬ T)  T  ] => fail 1
    | [  |-         sumbool (¬ T)  T  ] => fail 1
    | [  |-         sumbool (¬ T)  T  ] => fail 1
    | [  |-         ?A ∨ ¬ (?A)  ] => notHyp H; notHyp (¬ H); assert (H ∨ (¬ H))
    | [  |- sumbool ?A  (¬ (?A)) ] => notHyp H; notHyp (¬ H); assert (sumbool H (¬ H))
    | [  |- sum     ?A  (¬ (?A)) ] => notHyp H; notHyp (¬ H); assert (sum H (¬ H))
    | [  |- _                    ] => fail 1
  end.
*)

  Hint Extern 1 (∀ _, _ <-> _) => prop.
  Hint Extern 1 (_ <-> _)      => prop.
  Hint Extern 1 (∀ _, _ = _)   => prop.
  Hint Extern 1 (_ = _)        => prop. 
  Hint Extern 1 (sumbool _ _)  => prop. 
  Hint Extern 1 (sum _ _)      => prop. 

(***********************************************************************)
(**                          END TACTICS                              **)
(***********************************************************************)
End Auto.

Module Var.
  Require Import Utf8.

  (* Variables *)
  Parameter V : Set.
  Axiom decV : ∀ (ν₁ ν₂ : V), {ν₁ = ν₂} + {ν₁ ≠ ν₂}.


  (* External types *)
  Parameter CTy : Set.
  Axiom decCTy :  ∀ (c₁ c₂ : CTy), {c₁ = c₂} + {c₁ ≠ c₂}.

  Hint Resolve decV.
  Hint Resolve decCTy.
End Var.

(* Contexts *)
Module Ctx.
  Require Import Utf8.
  Require Import List.

  Import Auto.
  Import Var.

  Definition Ctx (A : Set) : Set := list (V * A).

  Fixpoint decCtx {A : Set} (decA : ∀ (a1 a2 : A), {a1 = a2} + {a1 ≠ a2})
           (l1 l2 : Ctx A) : {l1 = l2} + {l1 ≠ l2}.
  let F := fresh in
  generalize (decCtx A decA); intro F; decEq F l1 l2.
  Defined.
  Hint Resolve decCtx.

  Fixpoint dom {A : Set} (c : Ctx A) : list V := 
    match c with
      | nil       => nil
      | cons x xs => match x with
                       | (a,b) => a::dom xs
                     end
    end.

  Fixpoint rng {A : Set} (c : Ctx A) : list A := 
    match c with
      | nil       => nil
      | cons x xs => match x with
                       | (a,b) => b::rng xs
                     end
    end.

  Fixpoint InC {A : Set} ν α (Γ : Ctx A) : Prop :=
    match Γ with
      | nil     => False
      | ((v, a)::Γ)  => v = ν ∧ a = α ∨ (v ≠ ν ∧ InC ν α Γ)
    end.

  (* Ctx lemmas *)
  Lemma InΓUniq : ∀ {A : Set} Γ ν {a₁ a₂ : A}, InC ν a₁ Γ → InC ν a₂ Γ → a₁ = a₂.
  Proof.
    intros; induction Γ; auto.
  Qed.

  Hint Resolve InΓUniq.

  Program Fixpoint lookup {A : Set} (decA : ∀ (a₁ a₂ : A), {a₁ = a₂} + {a₁ ≠ a₂})
             v (a : A) Γ : {InC v a Γ} + {¬ (InC v a Γ)} :=
  match Γ with
    | nil            => right _
    | ((v0, a0)::Γ0) => 
        match decV v0 v with
          | left e => match decA a0 a with
                        | left  e0 => left _
                        | right  n => right _
                      end
          | right n => match lookup decA v a Γ0 with
                         | left _  => left  _
                         | right _ => right _
                       end
        end
  end.

  Hint Resolve lookup.

  Program Fixpoint find {A : Set} (decA : ∀ (a₁ a₂ : A), {a₁ = a₂} + {a₁ ≠ a₂})
             v Γ : {a : A | InC v a Γ} + {∀ a, ¬ (InC v a Γ)} :=
  match Γ with
    | nil => inright _
    | ((v0, a0)::Γ0) =>
        match decV v0 v with
          | left  _ => inleft (exist _ a0 _)
          | right _ => match find decA v Γ0 with
                          | inleft  (exist a _) => inleft  (exist _ a _)
                          | inright _           => inright _ 
                       end
        end
  end.

  Hint Resolve find.

  (*
  Fixpoint lookup {A} (decA : ∀ (a₁ a₂ : A), {a₁ = a₂} + {a₁ ≠ a₂})
             v (a : A) Γ : {InC v a Γ} + {¬ (InC v a Γ)} :=
  match Γ return ({InC v a Γ} + {¬InC v a Γ}) with
    | nil => right (λ H : InC v a nil, H)
    | ((v0, a0)::Γ0) =>
        match decV v0 v with
          | left e => match decA a0 a with
                        | left  e0 => left (or_introl (conj e e0))
                        | right  n =>
                          right (λ p, match p with
                                        | or_introl (conj a b) => n b
                                        | or_intror (conj a b) => a e
                                      end)
                      end
          | right n => match lookup decA v a Γ0 with
                         | left  i => left (or_intror (conj n i))
                         | right m => right (λ p, match p with
                                                    | or_introl (conj a b) => n a
                                                    | or_intror (conj a b) => m b
                                                  end)
                       end
        end
  end.

  Fixpoint find {A} (decA : ∀ (a₁ a₂ : A), {a₁ = a₂} + {a₁ ≠ a₂})
             v Γ : {a : A | InC v a Γ} + {∀ a, ¬ (InC v a Γ)} :=
  match Γ return ({a : A | InC v a Γ} + {∀ a, ¬ (InC v a Γ)}) with
    | nil => inright (λ a (H : InC v a nil), H)
    | ((v0, a0)::Γ0) =>
        match decV v0 v with
          | left  e  => inleft (exist _ a0 (or_introl (conj e eq_refl)))
          | right p₁ => match find decA v Γ0 with
                          | inleft (exist a p₂) =>
                              inleft _ (exist _ a (or_intror (conj p₁ p₂)))
                          | inright p₂          => 
                              inright (λ x (H : InC v x ((v0,a0)::Γ0)),
                                match H with
                                  | or_introl (conj a b) => p₁ a
                                  | or_intror (conj a b) => p₂ x b
                                end)
                        end
        end
  end.
*)
    
  Notation "Γ [ ν := τ ]" := ((ν,τ)::Γ) (at level 20, left associativity).
End Ctx.

Module Ty.
  Require Import Utf8. 
  Require Import List. 

  Import Auto.
  Import Var.
  Import Ctx.

  Definition dec (T : Set ) : Set := sumor T (T -> False).

(* Type kinds *)
  Inductive Kind : Set :=
  | Kstruct : Kind                 (* Σ *)
  | Kstar   : Kind                 (* ★ *)    
  | Karr    : Kind → Kind → Kind.  (* κ → κ *)

  Notation "'Σ"      := (Kstruct).
  Notation    "★"    := (Kstar).
  Notation "κ₁ → κ₂" := (Karr κ₁ κ₂).

  Fixpoint karr (κs : list Kind) κ : Kind :=
    match κs with
      | nil        => κ
      | cons κ₁ κs => κ₁ → (karr κs κ)
    end.

  
  Definition decKind (k₁ k₂ : Kind) : {k₁ = k₂} + {k₁ ≠ k₂}.
    decide equality.
    (* decEq decKind k₁ k₂. *)
  Defined.
  Hint Resolve decKind.

  (*
  Program Fixpoint decKind (k₁ k₂ : Kind) : {k₁ = k₂} + {k₁ ≠ k₂} :=
    match k₁, k₂ return {k₁ = k₂} + {k₁ ≠ k₂} with
      |    ★   ,    ★    => left _
      |   'Σ   ,   'Σ    => left _
      | k₁ → k₂, k₃ → k₄ =>
        match decKind k₁ k₃, decKind k₂ k₄ with
          | left  _, left  _ => left  _
          | right _,       _ => right _
          |       _, right _ => right _
        end
      |    _  ,    _     => right _
    end.

  Fixpoint decKind (k₁ k₂ : Kind) : {k₁ = k₂} + {k₁ ≠ k₂} :=
    match k₁, k₂ return ({k₁ = k₂} + {k₁ ≠ k₂}) with
      |    ★   ,    ★    => left eq_refl
      |    Σ   ,    Σ    => left eq_refl
      | k₁ → k₂, k₃ → k₄ =>
        match decKind k₁ k₃, decKind k₂ k₄ with
          | left  e₁, left  e₂ =>
            left (eqKarr e₁ e₂)
          | right e₁, _        =>
            right (λ (p : (k₁ → k₂) = (k₃ → k₄)) , e₁ (f_equal proj₁ p))
          | _       , right e₂ =>
            right  (λ (p : (k₁ → k₂) = (k₃ → k₄)) , e₂ (f_equal proj₂ p))
        end

      | ★ , Σ    => right (λ p, False_ind False
           (eq_ind ★ (λ k, match k with ★ => True | _ => False end) I Σ p))
      | ★ , (a → b) => right (λ p, False_ind False
           (eq_ind ★ (λ k, match k with ★ => True | _ => False end) I (a→b) p))
      | Σ , ★ => right (λ p, False_ind False
           (eq_ind Σ (λ k, match k with Σ => True | _ => False end) I ★ p))
      | Σ , (a → b) => right (λ p, False_ind False
           (eq_ind Σ (λ k, match k with Σ => True | _ => False end) I (a→b) p))
      | (a → b), Σ => right (λ p, False_ind False
         (eq_ind (a → b) (λ k, match k with _ → _ => True | _ => False end) I Σ p))
      | (a → b), ★    => right (λ p, False_ind False
         (eq_ind (a → b) (λ k, match k with _ → _ => True | _ => False end) I ★ p))
    end.
  *)

  
  Inductive Op : Set := Sum | Prod.

  Inductive Ty : Set :=
  (* ★ *)
  | TUnit  :                    Ty (* 1 *)
  | TVar   : V                → Ty (* ν *)
  | TMeta  : nat              → Ty (* η *)
  | TAtom  : CTy              → Ty (* τ *)
  | TArr   : Ty     → Ty → Ty → Ty (* A (σ ↦) B *)

  (* κ → κ *)
  | TAbs   : V → Kind → Ty    → Ty (* λ ν, A *)
  | TApp   : Ty     → Ty      → Ty (* F A *)
  | TFix   : Ty               → Ty (* μ F *)

  (* ∀ κ, κ *)
  | TOp    : Op     → Ty → Ty → Ty (* A + B | A × B | σ₁ ▵ σ₂ | σ₁ ▿ σ₂ *)


  (* Σ *)
  | ID     :                    Ty (* ID *)
  | IN     :                    Ty (* IN *)
  | OUT    :                    Ty (* OUT *)
  | _F     : Ty     → Ty      → Ty (* F σ *)

  | AF     :                    Ty (* AF *)
  | COMP   : Ty     → Ty      → Ty (* σ ∘ σ *)
  | HYLO   : Ty     → Ty → Ty → Ty (* HYLO F σ₁ σ₂ *)
  .
  
  Definition decNat (a b : nat) : {a = b} + {a ≠ b}.
  decide equality.
  (* decEq decNat a b. *)
  Defined.
  Hint Resolve decNat.
    
  Definition decOp (a b : Op) : {a = b} + {a ≠ b}.
    decide equality.
    (*decEq decOp a b. *)
  Defined.
  Hint Resolve decOp.
  
  Fixpoint decTy (a b : Ty) : {a = b} + {a ≠ b}.
  decide equality.
    (*decEq decTy a b.*)
  Defined.
  Hint Resolve decTy.

  Notation "σ₁ ∘ σ₂" := (COMP σ₁ σ₂) (at level 35, right associativity).
  Notation "( τ₁ ↦ τ₂ | σ )" := (TArr σ τ₁ τ₂).
  Notation "τ₁ ↦ τ₂" := (TArr AF τ₁ τ₂) (at level 90, right associativity).

  Definition CATA (F a : Ty) := HYLO F a OUT.
  Definition ANA  (F a : Ty) := HYLO F IN a.
  Definition MAP  (F a : Ty) := CATA F (IN ∘ _F F a).

  Fixpoint abs (l : Ctx Kind) (τ : Ty) :=
    match l with
      | nil       => τ
      | cons (v, k) vs => TAbs v k (abs vs τ)
    end.

  Fixpoint arr (l : list Ty) (τ : Ty) : Ty :=
    match l with
      | nil        => τ
      | cons τ₁ τs => TArr AF τ₁ (arr τs τ)
    end.

  (**** Well Kinded Relation ****)
  Inductive Wk : Ctx Kind → Ty → Kind → Prop :=
  | WkUnit  : ∀ {Γ         },                           Wk Γ  TUnit          ★
  | WkVar   : ∀ {Γ ν  κ    }, InC  ν κ Γ   →            Wk Γ (TVar  ν      ) κ
  | WkAtom  : ∀ {Γ A       },                           Wk Γ (TAtom A      ) ★
  | WkArr   : ∀ {Γ σ  τ₁ τ₂},
                Wk Γ σ 'Σ   → Wk Γ τ₁ ★ → Wk Γ τ₂ ★ →   Wk Γ (TArr  σ τ₁ τ₂) ★
  | WkAbs   : ∀ {Γ ν κ₁ κ₂ τ},
                Wk (Γ[ν:=κ₁]) τ  κ₂ →                   Wk Γ (TAbs  ν κ₁ τ ) (κ₁→κ₂)
  | WkApp   : ∀ {Γ κ₂ φ τ} κ₁,
                Wk Γ φ (κ₁→κ₂) → Wk Γ τ κ₁            → Wk Γ (TApp  φ τ    ) κ₂
  | WkFix   : ∀ {Γ τ}, Wk Γ τ (★→★)                   → Wk Γ (TFix  τ      ) ★
  | WkOp    : ∀ {Γ κ τ₁ τ₂} o, Wk Γ τ₁ κ → Wk Γ τ₂ κ  → Wk Γ (TOp   o τ₁ τ₂) κ
  | WkMeta  : ∀ {Γ η       },                           Wk Γ (TMeta η     ) 'Σ
  | WkID    : ∀ {Γ},                                    Wk Γ  ID            'Σ
  | WkIN    : ∀ {Γ},                                    Wk Γ  IN            'Σ
  | WkOUT   : ∀ {Γ},                                    Wk Γ  OUT           'Σ
  | Wk_F    : ∀ {Γ F σ}, Wk Γ F (★→★→★) → Wk Γ σ 'Σ   → Wk Γ (_F F σ)       'Σ
  | WkAF    : ∀ {Γ    },                                Wk Γ  AF            'Σ
  | WkCOMP  : ∀ {Γ σ₁ σ₂}, Wk Γ σ₁ 'Σ → Wk Γ σ₂ 'Σ    → Wk Γ (COMP σ₁ σ₂)   'Σ
  | WkHYLO  : ∀ {Γ F σ₁ σ₂}, Wk Γ F (★→★→★) →
                Wk Γ σ₁ 'Σ → Wk Γ σ₂ 'Σ               → Wk Γ (HYLO F σ₁ σ₂) 'Σ
  .

  (* "Well kinded context" *)
  Inductive WkC : Ctx Kind → Ctx Ty → Prop :=
  | WkNil    : ∀ Γ, WkC Γ nil
  | WkExtend : ∀ {Γ ρ} ν , WkC Γ ρ → WkC (Γ[ν:=★]) ρ
  | WkCons   : ∀ {Γ ρ κ τ} ν αs, Wk (αs ++ Γ) τ κ →
                                 WkC Γ ρ →
                                 WkC (Γ[ν:=karr (rng αs) κ]) (ρ[ν:=abs αs τ]).

  
  Ltac wkInv :=
    simpl in *;
    repeat
       match goal with
         (* Hint Constructors makes proof search take longer, why? *)
         | [               |- Wk _ _ _ ] => constructor
         | [  H : Wk _ _ (?k₁ → ?k₂) |- Wk _ (TApp _ _) _ ] => apply (WkApp k₁)
         | [ H0 : Wk _ ?T _ |- _       ] =>
           match T with
             | _ _ => inversion_clear H0
             | TUnit => inversion_clear H0
             | ID    => inversion_clear H0
             | IN    => inversion_clear H0
             | OUT   => inversion_clear H0
             | AF    => inversion_clear H0
           end
         | [ F : ∀ (c : Ctx Kind), _, H0 : Wk ?C _  _  |- _ ] => extend (F C)
         | [ F : ∀ (c : Kind    ), _, H0 : Wk _  _  ?C |- _ ] => extend (F C)
         | [ F : ∀ (t : Ty      ), _, H0 : Wk _  ?C _  |- _ ] => extend (F C)

         | [ F : ∀ (c : Ctx Kind), _ |- Wk ?C _  _  ] => extend (F C)
         | [ F : ∀ (c : Kind    ), _ |- Wk _  _  ?C ] => extend (F C)
         | [ F : ∀ (t : Ty      ), _ |- Wk _  ?C _  ] => extend (F C)

         | [       |- Wk _ (COMP _ _) ?k ] => notHyp (k = 'Σ); assert (k = 'Σ); subst
       end.

  Ltac wkTac := repeat (try intros; try prop; try intros; wkInv).

  Hint Extern 1 (False)        => wkTac.
  Hint Extern 1 (_ = _)        => wkTac.
  Hint Extern 1 (Wk _ _ _)     => wkTac.
  Hint Extern 1 (_ <-> _ )     => wkTac.
  Hint Extern 1 (∀ _, _ <-> _ )     => wkTac.

  Lemma WkUniq : ∀ {σ Γ κ₁}, Wk Γ σ κ₁ → ∀ {κ₂}, Wk Γ σ κ₂ → κ₁ = κ₂.
  Proof.
    intros σ Γ κ₁ H; induction H; eauto.
  Qed.

  Hint Resolve WkUniq.
  
  Notation "Γ ⊢ t .: k" :=    (Wk Γ t k)  (at level 0, no associativity). 
  Notation "Γ ⊬ t .: k" := (¬ (Wk Γ t k)) (at level 0, no associativity). 

  (*SOME TACTICS FOR DECIDING Wk*)
  Ltac expectedArg1 Q :=
    match Q with
      | TArr => constr:('Σ)
      | HYLO => constr:(★ → ★ → ★)
      | _F   => constr:(★ → ★ → ★)
      | COMP => constr:('Σ)
      | TFix => constr:(★ → ★)
    end.

  Ltac expectedArg2 Q :=
    match Q with
      | TArr => constr:(★)
      | HYLO => constr:('Σ)
      | _F   => constr:('Σ)
      | COMP => constr:('Σ)
    end.

  Ltac expectedArg3 Q :=
    match Q with
      | TArr => constr:(★)
      | HYLO => constr:('Σ)
    end.

  Ltac expectedT Q :=
    match Q with
      | TArr => constr:(★)
      | HYLO => constr:('Σ)
      | COMP => constr:('Σ)
      | _F   => constr:('Σ)
      | TFix => constr:(★)
      | TUnit    => constr:(★)
      | TAtom  _ => constr:(★)
      | TMeta  _ => constr:('Σ)
      | ID       => constr:('Σ)
      | IN       => constr:('Σ)
      | OUT      => constr:('Σ)
      | AF       => constr:('Σ)
      | _        => fail 1
    end.

  Ltac baseWk P :=
    match P with
      | { k : Kind | _ ⊢ ?t .: k} => let H := expectedT t in left; exists H; constructor
      | { k : Kind | ?Γ ⊢ TVar ?v .: k } => let VIN := fresh in let K := fresh in let R := fresh in
        destruct (find decKind v Γ) as [VIN | VIN];
          [ destruct VIN as [K R]; left; exists K; constructor; trivial
          | right; let H := fresh in intro H; destruct H as [K R]; specialize (VIN K);
                                     inversion_clear R; eauto]
    end.

  Ltac getCtx Γ T :=
    match T with
      | TAbs ?v ?k _ => constr:(Γ[v:=k])
      | _            => constr:(Γ)
    end.
  
  Ltac inst P :=
    match P with
      | { k : Kind | ?Γ ⊢ ?t .: k} =>
        let NC := getCtx Γ t in
        repeat match goal with
                 | [ H : ∀ Γ, _ |- _ ] => specialize (H NC)
               end
    end.

  Ltac wkEqualities :=
    repeat
      match goal with
        | [ H1 : ?Γ ⊢ ?t .: ?k1,
                 H2 : ?Γ ⊢ ?t .: ?k2
            |- _ ] => notHyp (k1 = k2); assert (k1 = k2) by (apply (WkUniq H1 H2))
      end.
                                              
  Ltac absurdCase :=
    match goal with
      | [ |- ?P -> False ] =>
        let H := fresh in
        let K := fresh in
        let P := fresh in
        intro H; destruct H as [K P]; inversion_clear P; wkEqualities; try congruence; eauto
    end.

  Ltac asserts :=
   match goal with
     | [ H0 : _ ⊢ ?A .: ?k1,
         H1 : _ ⊢ ?B .: ?k2,
         H2 : _ ⊢ ?C .: ?k3
         |- dec {k : Kind | _ ⊢ ?P ?A ?B ?C .: _ } ] =>
       let F := fresh in
       let K1 := expectedArg1 P in
       let K2 := expectedArg2 P in
       let K3 := expectedArg3 P in
       let R  := expectedT    P in
       assert ({k1 = K1} + {k1 ≠ K1}) as F by eauto; destruct F;
       [ subst
       | right; absurdCase ];
       assert ({k2 = K2} + {k2 ≠ K2}) as F by eauto; destruct F;
       [ subst
       | right; absurdCase ];
       assert ({k3 = K3} + {k3 ≠ K3}) as F by eauto; destruct F;
       [ subst
       | right; absurdCase ];
       left; exists R
     | [ H0 : _ ⊢ ?A .: ?k1,
         H1 : _ ⊢ ?B .: ?k2
         |- dec {k : Kind | _ ⊢ ?P ?A ?B .: _ } ] =>
       let F := fresh in
       let K1 := expectedArg1 P in
       let K2 := expectedArg2 P in
       let R  := expectedT    P in
       assert ({k1 = K1} + {k1 ≠ K1}) as F by eauto; destruct F;
       [ subst
       | right; absurdCase ];
       assert ({k2 = K2} + {k2 ≠ K2}) as F by eauto; destruct F;
       [ subst
       | right; absurdCase ];
       left; exists R
     | [ H0 : _ ⊢ ?A .: ?k1
         |- dec {k : Kind | _ ⊢ ?P ?A .: _ } ] =>
       let F := fresh in
       let K1 := expectedArg1 P in
       let R  := expectedT    P in
       assert ({k1 = K1} + {k1 ≠ K1}) as F by eauto; destruct F;
       [ subst
       | right; absurdCase ];
       left; exists R
     | [ H0 : _ ⊢ ?A .: ?k1,
         H1 : _ ⊢ ?B .: ?k2 
         |- dec {k : Kind | _ ⊢ TOp _ ?A ?B .: k } ] =>
       let F := fresh in
       let EQ := fresh in
       assert ({k1 = k2} + {k1 ≠ k2}) as F by (apply (decKind k1 k2)); destruct F as [EQ | EQ];
       [ rewrite EQ in *
       | right; absurdCase ]
       ; left; exists k2
     | [ H0 : _ ⊢ ?A .: ?k2
         |- dec {k : Kind | ?Γ ⊢ TAbs _ ?k1 ?A .: k } ] =>
       left; exists (k1 → k2)
   end; constructor; trivial.

  Ltac decideRec :=
    match goal with
      | [            |- dec ?P ] => baseWk P
      | [ H : ∀ Γ, _ |- dec ?P ] => 
        match P with
          | { k : Kind | ?Γ ⊢ ?t .: k} =>
            let NC := getCtx Γ t in specialize (H NC)
        end; decideRec

      | [ |- dec { k : Kind | _ ⊢ ?P .: _ } ] => asserts

      | [ H : {k : Kind | _ ⊢ ?A .: k} |- _  ] => 
        destruct H; decideRec

      | [ H : dec _ |- _ ] => destruct H;[ decideRec | right; absurdCase ]
      | _                  => idtac
    end.
    
  Ltac decideP X :=
    induction X; intros; decideRec.
End Ty.
  
(* Decision procedure for Γ ⊢ t .: k *)
Module KindChecking.
  Require Import Utf8.
  Import Ctx.
  Import Ty.
  Import Auto.

  Definition inferKind Γ t : dec { k : Kind | Γ ⊢ t .: k }.
    revert Γ; try decideP t.

    (* TODO: generalise tactic to handle all those cases *)
    destruct x0; [ right; absurdCase | right; absurdCase | idtac ].
    assert ({x0_1 = x} + {x0_1 ≠ x}) by (apply (decKind x0_1 x)).
    destruct H; [ subst; left; exists x0_2; apply (WkApp x); trivial | right; absurdCase ].
  Defined.

  (* Γ |- t : k *)
  Definition kc Γ t k : { Γ ⊢ t .: k } + { Γ ⊬ t .: k }.
    destruct (inferKind Γ t);
    [ destruct s; destruct (decKind k x);
      [ subst; left; trivial
      | right; intro; pose proof (WkUniq H w) as H0; apply n; trivial
      ]
    | right; intro H; apply (f (exist _ k H))
    ].
  Defined.
End KindChecking.

Recursive Extraction KindChecking.

Module Equiv.
  Require Import Utf8.

  Import Auto.
  Import Ctx.
  Import Ty.

  (* Structural type-equivalence *)
  Reserved Notation "σ₁ ≡[ Γ ] σ₂" (at level 45, right associativity).
  Inductive ΣEquiv : Ctx Kind → Ty → Ty → Prop :=
  (* Equivalence relation *)
  | Refl  : ∀ {Γ σ}       ,                           σ  ≡[Γ] σ
  | Trans : ∀ {Γ σ₁ σ₂ σ₃}, σ₁ ≡[Γ] σ₂ → σ₂ ≡[Γ] σ₃ → σ₁ ≡[Γ] σ₃
  | Sym   : ∀ {Γ σ₁ σ₂}   , σ₁ ≡[Γ] σ₂              → σ₂ ≡[Γ] σ₁

  (* Traversals *)

  (* ∀ κ, κ *)
  | EquivTArr   : ∀ {Γ σ₁ σ₁' τ₁ τ₁' τ₂ τ₂'},
                    σ₁ ≡[Γ] σ₁' → τ₁ ≡[Γ] τ₁' → τ₂ ≡[Γ] τ₂' → TArr σ₁ τ₁ τ₂ ≡[Γ] TArr σ₁' τ₁' τ₂'
  | EquivTAbs   : ∀ {Γ ν κ τ₁ τ₁'}, τ₁ ≡[Γ[ν:=κ]] τ₁' → TAbs ν κ τ₁ ≡[Γ] TAbs ν κ τ₁'
  | EquivTApp   : ∀ {Γ F F' τ τ'},
                    F ≡[Γ] F' → τ ≡[Γ] τ' → TApp F τ ≡[Γ] TApp F' τ'
  | EquivTFix   : ∀ {Γ F F'},
                    F ≡[Γ] F' → TFix F ≡[Γ] TFix F'
  | EquivTOp    : ∀ {Γ o   σ₁ σ₂ σ₁' σ₂'},
                    σ₁ ≡[Γ] σ₁' → σ₂ ≡[Γ] σ₂' → TOp o σ₁ σ₂ ≡[Γ] TOp o σ₁' σ₂'
  | Equiv_F     : ∀ {Γ   F (* F' *) σ₁ σ₁'  }, (* F ≡[Γ] F' → *)
                    σ₁ ≡[Γ] σ₁' → _F F σ₁ ≡[Γ] _F F σ₁'
  | EquivCOMP   : ∀ {Γ     σ₁ σ₂ σ₁' σ₂'}, 
                    σ₁ ≡[Γ] σ₁' → σ₂ ≡[Γ] σ₂' → σ₁ ∘ σ₂ ≡[Γ] σ₁' ∘ σ₂'
  | EquivHYLO   : ∀ {Γ  F (* F' *) σ₁ σ₂ σ₁' σ₂'}, (* F ≡[Γ] F' → *)
                    σ₁ ≡[Γ] σ₁' → σ₂ ≡[Γ] σ₂' → HYLO F σ₁ σ₂ ≡[Γ] HYLO F σ₁' σ₂'


  (* The rules *)
  | IdLCancel : ∀ {Γ σ}, Wk Γ σ 'Σ → ID ∘ σ ≡[Γ] σ
  | IdRCancel : ∀ {Γ σ}, Wk Γ σ 'Σ → σ ∘ ID ≡[Γ] σ

  | InOut     : ∀ { Γ }, IN ∘ OUT ≡[Γ] ID
  | OutIn     : ∀ { Γ }, OUT ∘ IN ≡[Γ] ID

  | FId       : ∀ {Γ F       }, Wk Γ F (★ → ★ → ★) → _F F ID ≡[Γ] ID
  | FSplit    : ∀ {Γ F σ₁ σ₂ }, _F F (σ₁ ∘ σ₂) ≡[Γ] _F F σ₁ ∘ _F F σ₂ 
  | CompAssoc : ∀ {Γ σ₁ σ₂ σ₃}, σ₁ ∘ (σ₂ ∘ σ₃) ≡[Γ] (σ₁ ∘ σ₂) ∘ σ₃

  | HyloId    : ∀ {Γ F      }, Wk Γ F (★ → ★ → ★) → HYLO F IN OUT ≡[Γ] ID
  | HyloSplit : ∀ {Γ F σ₁ σ₂}, HYLO F σ₁ σ₂ ≡[Γ] CATA F σ₁ ∘ ANA F σ₂
  | HyloShift : ∀ {Γ F σ₁ σ₂ σ₃},
                  HYLO F (σ₁ ∘ _F F σ₂) σ₃ ≡[Γ] HYLO F σ₁ (_F F σ₂ ∘ σ₃)
  where "σ₁ ≡[ Γ ] σ₂" := (ΣEquiv Γ σ₁ σ₂).

  Hint Constructors ΣEquiv.

  Lemma ΣEquivImpliesWk : ∀ Γ σ₁ σ₂, σ₁ ≡[Γ] σ₂ → ∀ k, Wk Γ σ₁ k ↔ Wk Γ σ₂ k.
  Proof.
    intros Γ₁ σ₁ σ₂ H; induction H; eauto.
  Qed.

  Hint Resolve ΣEquivImpliesWk.

  Inductive RW (R : Ty → Ty → Prop) : Ty → Ty → Prop :=
  | RwFinal : ∀ {t1 t2}, R t1 t2 → RW R t1 t2
  | RwTrans : ∀ {t1 t3} t2, R t1 t2 → RW R t2 t3 → RW R t1 t3.
  Hint Constructors RW.

  Scheme rwind := Induction for RW Sort Prop.

  Lemma RWApp : ∀ {t1 t2 t3 R}, RW R t1 t2 → RW R t2 t3 → RW R t1 t3.
    intros t1 t2 t3 R H; induction H; eauto.
  Qed.
  Hint Resolve RWApp.

  Definition normal (R : Ty → Ty → Prop) (t : Ty) := ¬ (∃ t', R t t').

  Definition semiConfluent  (R : Ty → Ty → Prop) :=
    ∀ t1 t2 t3, R t1 t2 → RW R t1 t3 → ∃ t4, RW R t2 t4 ∧ RW R t3 t4.

  Definition diamond (R : Ty → Ty → Prop) :=
    ∀ t1 t2 t3, R t1 t2 → R t1 t3 → ∃ t4, R t2 t4 ∧ R t3 t4.

  Theorem confluence : ∀ (R : Ty → Ty → Prop), semiConfluent R → diamond (RW R).
    unfold semiConfluent.
    unfold diamond.
    intros R P t1 t2 t3 RW1; revert t3; induction RW1.
    (* CASE NIL *)
    intros t3 RW.
    apply (P t1 t2 t3); eauto.
    (* INDUCTIVE STEP *)
    intros t0 RW2.
    (* CASE ANALYSIS ON RW2 *)
    pose proof (P t1 t2 t0 H RW2).
    destruct H0.
    destruct H0.
    pose proof (IHRW1 x H0).
    destruct H2.
    destruct H2.
    exists x0.
    split; eauto.
  Qed.
    
  Inductive Step (R : Ty → Ty → Prop) : Ty → Ty → Prop :=
  | SRule   : ∀ {t₁ t₂    }, R      t₁ t₂ → Step R t₁ t₂
  | SArr1   : ∀ {t₁ t₂ a b}, Step R t₁ t₂ → Step R (TArr t₁ a b) (TArr t₂ a b)
  | SArr2   : ∀ {t₁ t₂ a b}, Step R t₁ t₂ → Step R (TArr a t₁ b) (TArr a t₂ b)
  | SArr3   : ∀ {t₁ t₂ a b}, Step R t₁ t₂ → Step R (TArr a b t₁) (TArr a b t₂)
  | SAbs    : ∀ {v k t₁ t₂}, Step R t₁ t₂ → Step R (TAbs v k t₁) (TAbs v k t₂)
  | SApp1   : ∀ {t₁ t₂ a  }, Step R t₁ t₂ → Step R (TApp t₁ a) (TApp t₂ a)
  | SApp2   : ∀ {t₁ t₂ a  }, Step R t₁ t₂ → Step R (TApp a t₁) (TApp a t₂)
  | SFix    : ∀ {    t₁ t₂}, Step R t₁ t₂ → Step R (TFix   t₁) (TFix   t₂)
  | SOp1    : ∀ {t₁ t₂ a o}, Step R t₁ t₂ → Step R (TOp o t₁ a) (TOp o t₂ a)
  | SOp2    : ∀ {t₁ t₂ a o}, Step R t₁ t₂ → Step R (TOp o a t₁) (TOp o a t₂)
  | S_F1    : ∀ {t₁ t₂ a  }, Step R t₁ t₂ → Step R (_F a t₁) (_F a t₂)
                                               (*
  | S_F2    : ∀ {t₁ t₂ a  }, Step t₁ t₂ → Step (_F t₁ a) (_F t₂ a)
*)
  | SComp1  : ∀ {t₁ t₂ a  }, Step R t₁ t₂ → Step R (t₁ ∘ a) (t₂ ∘ a)
  | SComp2  : ∀ {t₁ t₂ a  }, Step R t₁ t₂ → Step R (a ∘ t₁) (a ∘ t₂)
                                                 (*
  | SComp3  : ∀ {a b c d  }, Step R (a ∘ b) c → Step R (a ∘ (b ∘ d)) (c ∘ d) 
  | SComp4  : ∀ {a b c d  }, Step R (a ∘ b) c → Step R ((d ∘ a) ∘ b) (d ∘ c) 
*)
                                               (*
  | SHylo1  : ∀ {t₁ t₂ a b}, Step t₁ t₂ → Step (HYLO t₁ a b) (HYLO t₂ a b)
*)
  | SHylo2  : ∀ {t₁ t₂ a b}, Step R t₁ t₂ → Step R (HYLO a t₁ b) (HYLO a t₂ b)
  | SHylo3  : ∀ {t₁ t₂ a b}, Step R t₁ t₂ → Step R (HYLO a b t₁) (HYLO a b t₂).
  Hint Constructors Step.
  
  Function inverse (a b : Ty) : Prop :=
    match a, b with
      | IN, OUT         => True
      | OUT, IN         => True
      | _     , _       => False
    end.

  Lemma invSym : ∀ a b, inverse a b → inverse b a.
    intros a b H; functional induction (inverse a b).
    eauto.
    eauto.
    destruct H; subst.
  Qed.
  Hint Resolve invSym.

  Inductive RId : Ty → Ty → Prop :=
  | RIdR       : ∀ σ, RId (ID ∘ σ) σ
  | RIdL       : ∀ σ, RId (σ ∘ ID) σ
  | RInOut     : RId (IN ∘ OUT) ID
  | ROutIn     : RId (OUT ∘ IN) ID
  | RFId       : ∀ F, RId (_F F ID) ID
  | RHyloId    : ∀ F, RId (HYLO F IN OUT) ID

  | RFInv      : ∀ F σ σ' , RId (_F F σ ∘ _F F σ') (_F F (σ ∘ σ'))
  | RMapInv    : ∀ F σ₁ σ₂, RId (MAP F σ₁ ∘ MAP F σ₂)
                                (ID ∘ CATA F (IN ∘ _F F σ₁ ∘ _F F σ₂)).
  Hint Constructors RId.

  Definition RWId := RW (Step RId).

  Inductive AccR (R : Ty → Ty → Prop) (x : Ty) : Prop := AccRI : (∀ y, R x y → AccR R y) → AccR R x. 
  Definition WF R := ∀ x, AccR R x.

  (* depth as termination for countRedex *)
  Fixpoint countRedex (t : Ty) : nat :=
    match t with
      | x ∘ y    => 1 + countRedex x + countRedex y
      | _F _ x   => 1 + countRedex x
      | HYLO _ x y => 1 + countRedex x + countRedex y
      | TArr a b c => countRedex a + countRedex b + countRedex c
      | TAbs v k a => countRedex a
      | TApp a b   => countRedex a + countRedex b
      | TOp o a b  => countRedex a + countRedex b
      | TFix a     => countRedex a
      | _      => 0
    end.

  Require Import Omega.
  Lemma decRId : ∀ x y, RId x y → countRedex x = S (countRedex y).
    intros x y H; inversion H; subst; simpl; omega.
  Qed.
  Hint Resolve decRId.

  Lemma decIdStep : ∀ x y, Step RId x y → countRedex x = S (countRedex y).
    induction 1; eauto; simpl; intuition.
  Qed.
  Hint Resolve decIdStep.

  Lemma decIdRW : ∀ x y, RW (Step RId) x y → countRedex y < countRedex x.
    induction 1; intuition.
    assert (countRedex t1 = S (countRedex t2)) by (apply (decIdStep _ _ H)).
    intuition.
    assert (countRedex t1 = S (countRedex t2)) by (apply (decIdStep _ _ H)).
    symmetry in H1.
    assert (countRedex t2  < countRedex t1) by intuition.
    intuition.
  Qed.

  Lemma idWF' : ∀ n, ∀ t, countRedex t < n → AccR RWId t.
    induction n; intros.
    inversion H.
    constructor.
    intros y RW.
    induction RW.
    apply IHn.
    assert (countRedex t1 = S (countRedex t2)) by (apply (decIdStep _ _ H0)). 
    rewrite H1 in H.
    intuition.
    apply IHRW.
    assert (countRedex t1 = S (countRedex t2)) by (apply (decIdStep _ _ H0)). 
    symmetry in H1.
    assert (countRedex t2 < countRedex t1) by intuition.
    intuition.
  Defined.

  Lemma idWF : WF RWId.
    unfold WF.
    intro x.
    apply (idWF' (S (countRedex x))).
    intuition.
  Defined.

  Inductive Rule : Ty → Ty → Prop :=
  | R_RId       : ∀ σ₁ σ₂,   RWId σ₁ σ₂ → Rule σ₁ σ₂
  | R_FSplit    : ∀ F σ₁ σ₂, 
                             Rule (_F F (σ₁∘σ₂)) (_F F σ₁ ∘ _F F σ₂)

  | R_HyloSplit : ∀ F σ₁ σ₂, σ₁ ≠ IN ∧ σ₂ ≠ OUT → 
                             Rule (HYLO F σ₁ σ₂) (CATA F σ₁ ∘ ANA F σ₂)
  | R_CataSplit : ∀ F σ₁ σ₂, σ₁ ≠ IN  → ¬ RWId σ₁ ID →
                              Rule (CATA F (σ₁ ∘ _F F σ₂)) (CATA F σ₁ ∘ MAP F σ₂)
  | R_AnaSplit  : ∀ F σ₁ σ₂, σ₂ ≠ OUT → ¬ RWId σ₂ ID →
                             Rule (ANA F (_F F σ₁ ∘ σ₂)) (MAP F σ₁ ∘ ANA F σ₂)
  | R_AnaMap     : ∀ F σ   , Rule (ANA F (_F F σ ∘ OUT)) (MAP F σ).
  Hint Constructors Rule.

  Ltac simplTy t :=
    match t with
      | TUnit   => idtac
      | TVar  _ => idtac
      | TMeta _ => idtac
      | TAtom _ => idtac
      | _       => fail 1
    end.
  
  Ltac absurdC :=
    cbv in *;
    intuition;
    repeat
    match goal with
      | [ H : ∃ t, _       |- _ ] => destruct H
      | [ H : RW _ ?t  _   |- _ ] => simplTy t; inversion_clear H
      | [ H : Step _ ?t _  |- _ ] => simplTy t; inversion_clear H
      | [ H : RId    ?t _  |- _ ] => simplTy t; inversion_clear H
    end.

  Definition rewritable (t : Ty) : {∃ t', RWId t t'} + {¬ ∃ t', RWId t t'}.
    induction t.
    right; intro; intros; absurdC.
    right; intro; intros; absurdC.
    right; intro; intros; absurdC.
    right; intro; intros; absurdC.
    absurdC.
  
  Fixpoint redexR (t : Ty) : nat :=
    


   (*****************************************************************************)


  
  (* Restricts associativity: we are working with a flattened version of terms *)
  Inductive Flat : Ty → Prop :=
  | RUnit : Flat TUnit
  | RVar  : ∀ v, Flat (TVar v)
  | RMeta : ∀ n, Flat (TMeta n)
  | RAtom : ∀ c, Flat (TAtom c)
  | RArr  : ∀ a b c, Flat a → Flat b → Flat c → Flat (TArr a b c)
  | RAbs  : ∀ v k a, Flat a → Flat (TAbs v k a)
  | RApp  : ∀ a b,   Flat a → Flat b → Flat (TApp a b)
  | RFix  : ∀ a  ,   Flat a →          Flat (TFix a  )
  | ROp   : ∀ o a b, Flat a → Flat b → Flat (TOp o a b)
  | RId   : Flat ID
  | RIn   : Flat IN
  | ROut  : Flat OUT
  | RAf   : Flat AF
  | R_F   : ∀ F a, Flat F → Flat a → Flat (_F F a)
  | RHylo : ∀ F a b, Flat F → FlatL a → Flat b → Flat (HYLO F a b)
  | RComp : ∀ a b , ¬ comp a → Flat a → Flat b → Flat (a ∘ b) 
  with FlatL : Ty → Prop :=
  | FL    : ∀ a   , ¬ comp a → Flat a → FlatL a
  | LComp : ∀ a b , ¬ comp b → FlatL a → Flat b → FlatL (a ∘ b) 
  .

  Ltac base :=
    let BT := match goal with
                | [ |- { r : Ty | ∀ _ _, _ ⊢ ?t .: _ → _ } ] => simplTy t
              end
    in exists BT; intros; apply Refl.
  
  Fixpoint appCompR (t1 : Ty) (t2 : Ty) :=
    match t1 with
      | (a ∘ b) => a  ∘ (appCompR b t2)
      | _       => t1 ∘ t2
    end.

  Fixpoint appCompL (t1 : Ty) (t2 : Ty) :=
    match t2 with
      | (a ∘ b) => (appCompL t1 a) ∘ b
      | _       => t1 ∘ t2
    end.
  
  Fixpoint flattenR (t : Ty) : Ty :=
    match t with
      | (a ∘ b)    => appCompR (flattenR a) (flattenR b)
      | TArr s a b => TArr (flattenR s) (flattenR a) (flattenR b)
      | TAbs v k s => TAbs v k (flattenR s)
      | TApp   a b => TApp (flattenR a) (flattenR b)
      | TFix F     => TFix (flattenR F)
      | TOp  o a b => TOp o (flattenR a) (flattenR b)
      | _F   F a   => _F    (flattenR F) (flattenR a)

      | HYLO F a b => HYLO (flattenR F) (flattenL a) (flattenR b)
      | ty         => ty
    end
  with flattenL (t : Ty) : Ty :=
    match t with
      | (a ∘ b)    => appCompL (flattenL a) (flattenL b)
      | TArr s a b => TArr (flattenR s) (flattenR a) (flattenR b)
      | TAbs v k s => TAbs v k (flattenR s)
      | TApp   a b => TApp (flattenR a) (flattenR b)
      | TFix F     => TFix (flattenR F)
      | TOp  o a b => TOp o (flattenR a) (flattenR b)
      | _F   F a   => _F    (flattenR F) (flattenR a)

      | HYLO F a b => HYLO (flattenR F) (flattenL a) (flattenR b)
      | ty         => ty
    end.

  Lemma appCompRSound : ∀ t1 t2, Flat t1 → Flat t2 → Flat (appCompR t1 t2).
    intros t1 t2 H;
    induction H;
    intros; simpl; try (constructor;
      [ simpl; intro
      | constructor
      | idtac
      ]; trivial).

    constructor;
      [ trivial | trivial | exact (IHFlat2 H2)].
  Qed.
    
  Lemma appCompLSound : ∀ t1 t2, FlatL t1 → FlatL t2 → FlatL (appCompL t1 t2).
    intros t1 t2;
    induction t2; intros; simpl;
    try (apply LComp;
         [ simpl; intro; trivial
           | trivial
           | constructor;
             repeat match goal with
                      | [ H : Flat  (_ _) |- _ ] => inversion_clear H
                      | [ H : FlatL (_ _) |- _ ] => inversion_clear H
                    end; trivial
         ]).

    inversion_clear H0;
      [ exfalso; apply H1; simpl; exact I
      | apply LComp; trivial; apply IHt2_1; trivial
      ].
  Qed.

    
  Lemma flattenSound : ∀ t, Flat (flattenR t) ∧ FlatL (flattenL t).
    intro t; induction t; repeat match goal with
                                | [ H : _ ∧ _ |- _ ] => destruct H
                                 end;
    try (split;
         [ simpl; constructor; try trivial
         | simpl; constructor;
           [ simpl; intro; trivial
           | constructor; try trivial
           ]
         ]).
    split; simpl;
    [ apply appCompRSound; try trivial
    | apply appCompLSound; try trivial
    ].
  Qed.

  Lemma compEquivR : ∀ Γ t1 t2, t1 ∘ t2 ≡[Γ] appCompR t1 t2.
    intros.
    induction t1; simpl; try (apply Refl).

    apply (Trans (Sym CompAssoc)). 
    apply EquivCOMP; try trivial.
  Qed.

  Lemma compEquivL : ∀ Γ t1 t2, t1 ∘ t2 ≡[Γ] appCompL t1 t2.
    intros.
    induction t2; simpl; try (apply Refl).

    apply (Trans CompAssoc). 
    apply EquivCOMP; try trivial.
  Qed.

  Lemma flattenCnv : ∀ t Γ, t ≡[Γ] flattenR t ∧ t ≡[Γ] flattenL t.
    let Γ := fresh in
    let t := fresh in
    let k := fresh in
    let H := fresh in
    intro t; induction t; simpl; intro Γ;
    repeat match goal with
        | [ IH : forall (C : Ctx Kind), _
          |- (?t ≡[?C] _) ∧ _ ] => let X := getCtx C t in specialize (IH X)
        | [ H0 : _ ∧ _ |- _ ] => destruct H0
    end; eauto.

    split;
      [ apply (Trans (EquivCOMP H2 H0)); apply compEquivR
      | apply (Trans (EquivCOMP H3 H1)); apply compEquivL
      ].
  Qed.

 
  

  Theorem step : ∀ t, {t' : Ty | RW t t' ∧ ¬ (∃ t'', Step t' t'')}.
  (* TODO: Define RWS
       inductive type rule + function match : Rule → Σ → ??? (sub-sigmas)
   *)

  (* TODO: Prove confluence of RWS *)
    
End Equiv.


Extraction Equiv.


Module Expr.
  Require Import Utf8.

  Import Auto.
  Import Ctx.
  Import Ty.
  Import Var.

  Inductive Expr : Set :=
  (* PRIM *)
  | ein    :                      Expr
  | eout   :                      Expr
  | eid    :                      Expr
  | eOp    : Op   → Expr → Expr → Expr
  | eF     : Ty   → Expr        → Expr
  (* /PRIM *)

  | var    : V                  → Expr
  | eapp   : Expr → Expr        → Expr
  | comp   : Expr → Expr        → Expr
  | hylo   : Ty   → Expr → Expr → Expr
  .

  Fixpoint decExpr (e1 e2 : Expr) : {e1 = e2} + {e1 ≠ e2}.
  decide equality.
  (* decEq decExpr e1 e2. *)
  Defined.
  Hint Resolve decExpr.

  Axiom WtE : ∀ {Γ} {ρ}, WkC Γ ρ → Ctx Ty → Expr → Ty → Prop.

End Expr.

Module Prog.
  Require Import Utf8.
  Require Import List.

  Import Auto.
  Import Ctx.
  Import Expr.
  Import Ty.
  Import Var.

  Inductive Def : Set :=
  | AssumeTy : V →                          Def
  | AssumeFn : V →            Ty          → Def
  | FnDef    : V → Ctx Ty   → Ty   → Expr → Def
  | TyDef    : V → Ctx Kind → Kind → Ty   → Def.

  Inductive Prog : Set :=
  | PDef  : Def → Prog
  | PDefs : Def → Prog → Prog.

  Fixpoint decDef (d1 d2 : Def) : {d1 = d2} + {d1 ≠ d2}.
  decide equality.
  Defined.
  Hint Resolve decDef.
  Fixpoint decProg (d1 d2 : Prog) : {d1 = d2} + {d1 ≠ d2}.
  decide equality.
  Defined.
  Hint Resolve decProg.

  Inductive WtDef : ∀ {Γ₁ Γ₂ ρ₁ ρ₂},
                      WkC Γ₁ ρ₁ → Ctx Ty →
                      WkC Γ₂ ρ₂ → Ctx Ty →
                      Def → Prop :=
  | WtAssumeTy : ∀ {Γ₁ ρ₁} {P : WkC Γ₁ ρ₁} {Γ₂} ν,
                   WtDef P Γ₂ (WkExtend ν P) Γ₂ (AssumeTy ν)
  | WtAssumeFn : ∀ {Γ₁ ρ₁} {P : WkC Γ₁ ρ₁} {Γ₂} ν τ,
                   Wk Γ₁ τ ★ → WtDef P Γ₂ P (Γ₂[ν:=τ])  (AssumeFn ν τ)
  | WtFnDef    : ∀ {Γ₁ ρ₁} {P : WkC Γ₁ ρ₁} {Γ₂} ν αs τ s,
                   WtE P (αs ++ Γ₂) s τ →
                   WtDef P Γ₂ P (Γ₂[ν:=arr (rng αs) τ]) (FnDef ν αs τ s)
  | WtTyDef    : ∀ {Γ₁ ρ₁} {P : WkC Γ₁ ρ₁} {Γ₂} ν αs κ τ
                   (wk : Wk (αs ++ Γ₁) τ κ),
                   WtDef P Γ₂ (WkCons ν αs wk P) Γ₂ (TyDef ν αs κ τ)
  .

  Inductive WTProg : ∀ {Γ ρ}, WkC Γ ρ → Ctx Ty → Prog → Prop :=
  | WtDefn  : ∀ {Γ₁ Γ₂ ρ₁ ρ₂} {P₁ : WkC Γ₁ ρ₁} {P₂ : WkC Γ₂ ρ₂} {Γ₃ Γ₄} d,
                WtDef P₁ Γ₃ P₂ Γ₄ d → WTProg P₁ Γ₃ (PDef d)
  | WtDefns : ∀ {Γ₁ Γ₂ ρ₁ ρ₂} {P₁ : WkC Γ₁ ρ₁} {P₂ : WkC Γ₂ ρ₂} {Γ₃ Γ₄} d p,
                WtDef P₁ Γ₃ P₂ Γ₄ d → WTProg P₂ Γ₄ p → WTProg P₁ Γ₃ (PDefs d p).
  
End Prog.

