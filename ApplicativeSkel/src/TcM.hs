{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE ViewPatterns          #-}
module TcM
  ( TcM
  , runTcM

  -- check for recursive calls
  , isRec

  -- fresh vars
  , freshTyVar
  , freshEVar
  , freshEVars
  , freshPref
  , skolem

  -- lookups
  , lookupSubst
  , lookupKSubst
  , applySubst
  , applyKSubst

  -- Source pos and contexts
  , inDef
  , inPos
  , inCtx
  , extendCtx
  , popCtx
  , lookupCtx
  , lookupVar
  , ctxSize
  , freshId
  , freshMeta
  , freshKMeta

  -- Warnings
  , warnStub

  -- Errors
  , unifyKindErr
  , nonUniformType
  , unifyError
  , unexpectedType

  -- From Env
  , Ctx      (..)
  , GammaCtx (..)
  , TcEnv.gProd
  , CtxElem
  , CtxEntry
  , TcEnv.builtInInt
  , TcEnv.builtInBool
  , TcEnv.builtInString
  ) where

import Control.Monad.State.Strict ( modify, state, get, put )
import Control.Monad.Writer ( tell )
import Control.Monad.Except ( throwError )
import Control.Monad.Extra ( eitherM, liftM2, (<=<) )
import Data.Foldable ( toList )
import Data.Sequence ( Seq )
import Text.Parsec.Pos ( SourcePos, initialPos )
import qualified Data.Map.Strict as Map
import qualified Data.Sequence as Seq
import qualified Text.PrettyPrint as Ppr

import Common
import Util
import Type

import SkelSyn.Expr -- FIXME: I really really hate this dependency here

import TcM.TcError ( TcError )
import qualified TcM.TcError as TcError
import TcM.TcWarn
import TcM.TcEnv ( TcEnv, Ctx, GammaCtx, CtxElem, CtxEntry )
import qualified TcM.TcEnv as TcEnv

-- | Logs are lists of warning messages
newtype Log = Log [TcWarn]

instance Monoid Log where
  mempty = Log mempty
  Log s1 `mappend` Log s2 = Log $ s1 `mappend` s2


instance Ppr Log where
  ppr (Log s) = Ppr.vcat $ map ppr s

instance Show Log where
  show = showPpr

type TcM = RWS TcError TcEnv Log

-- | Runs typechecker
runTcM :: State -> TcM a -> IO (a, State)
runTcM st = result . flip runRWS (TcEnv.initTcEnv st)
  where
    result (Left  e, _, _) = fail (show e)
    result (Right x, s, w) = print w *> return (x, TcEnv.gSt s)

-- | Update the current source position
inDef :: Name -> TcM a -> TcM a
inDef p m = enterDef p *> m <* popPos

-- | Update the current source position
inPos :: SourcePos -> TcM a -> TcM a
inPos p m = pushPos p *> m <* popPos

isRec :: Name -> TcM Bool
isRec n = fmap ((n ==) . TcEnv.curr) get

freshTyVar :: TcM Name
freshTyVar = fmap TcEnv.freshTyVar get

freshPref :: String -> TcM Name
freshPref s = fmap (TcEnv.freshPref s) get

freshEVar :: TcM Name
freshEVar = fmap TcEnv.freshEVar get

freshEVars :: Int -> TcM [Name]
freshEVars i = fmap (TcEnv.freshEVars i) get

-- | Local update kind environment
inCtx :: Ctx c -> Seq (CtxEntry c) -> TcM a -> TcM a
inCtx c vs m = pushCtx c vs *> m <* popCtx c (length vs)

-- | Global update kind environment
extendCtx :: Ctx c -> CtxEntry c -> TcM ()
extendCtx c = pushCtx c . Seq.singleton

-- | Lookup variable in context
lookupCtx :: Ctx c -> Name -> TcM (CtxElem c)
lookupCtx c n = get >>=
  maybe (err TcError.notInScope n) return .  TcEnv.lookupCtx c n

ctxSize :: Ctx c -> TcM Int
ctxSize c = TcEnv.ctxSize c <$> get

-- | Lookup expr variable
lookupVar :: Name -> TcM (EType, AExpr Expr)
lookupVar n = get >>= (lookupLocal  =<< lookupGlobal notinscope)
  where
    lookupGlobal k = maybe k skolM  . TcEnv.lookupCtx TcEnv.Delta n
    lookupLocal  k = maybe k return . TcEnv.lookupCtx TcEnv.Gamma n
    notinscope   = err TcError.notInScope n
    skolM (x,y) = (,y) <$> skolem x



-- | Warning for unimplemented features
warnStub :: String -> TcM ()
warnStub = warn tcStub

-- | UnifyKindError
unifyKindErr :: Kind -> Kind -> TcM a
unifyKindErr = curry $ err TcError.unifyKindErr

-- | UnifyTypeError
unifyError :: EType -> EType -> TcM a
unifyError = curry (err TcError.unifyError <=< substs)
  where
    substs (t1, t2) = liftM2 (,) (applySubst t1) (applySubst t2)

-- | UnifyTypeError
unexpectedType :: EType -> String -> TcM a
unexpectedType = curry $ err TcError.unexpectedType

-- | Non uniform type not yet supported (HO functors needed)
nonUniformType :: Name -> TcM a
nonUniformType = err TcError.nonUniformType

-- | skolemise
skolem :: ForAll -> TcM EType
skolem (ForAll vs ty)
  = (`skolTy` ty) . Map.fromList <$> mapM mkMeta (toList vs)
  where
    mkMeta v = fmap (v,) freshMeta

    skolTy env (TyVar  v)
      | v `elem` vs           = env Map.! v

    skolTy env (TySum  ts)    = TySum  $ fmap (skolTy env) ts
    skolTy env (TyProd ts)    = TyProd $ fmap (skolTy env) ts
    skolTy env (TyApp  ts)    = tyApp  $ fmap (skolTy env) ts
    skolTy env (TyArr  t1 t2) = TyArr (skolTy env t1) (skolTy env t2)
    skolTy _   t              = t

-- FIXME: loads of boilerplate!

-- | Lookup type substitution
lookupSubst :: Integer -> EType -> (EType -> TcM ()) -> TcM ()
lookupSubst i ty f = eitherM f put (TcEnv.lookupSubst i ty <$> get)

-- | Lookup kind substitution
lookupKSubst :: Integer -> Kind -> (Kind -> TcM ()) -> TcM ()
lookupKSubst i ty f = eitherM f put (TcEnv.lookupKSubst i ty <$> get)

applySubst :: EType -> TcM EType
applySubst = applySubst' []
  where
    applySubst' :: [Integer] -> EType -> TcM EType
    applySubst'  seen ty@(TyMeta t)
      | t `elem` seen = error "FIXME: throw an occurscheck"
      | otherwise     = eitherM (applySubst' (t:seen)) (const $ return ty)
                                (TcEnv.lookupSubst t ty <$> get)
    applySubst' seen (TySum  t) = substSum seen t
    applySubst' seen (TyProd t) = substProd seen t
    applySubst' seen (TyApp   t) = tyApp  <$> mapM (applySubst' seen) t
    applySubst' seen (TyArr t1 t2) = liftM2 TyArr (applySubst' seen t1)
                                                  (applySubst' seen t2)
    applySubst' _seen ty           = return ty

    substSum :: [Integer] -> OpenL EType -> TcM EType
    substSum seen (viewOL -> (xs, i))
      = mapM (applySubst' seen) xs >>= completeSum seen i

    substProd :: [Integer] -> OpenL EType -> TcM EType
    substProd seen (viewOL -> (xs, i))
      = mapM (applySubst' seen) xs >>= completeProd seen i

    -- FIXME: use views to reduce code duplication below
    completeSum :: [Integer] -> Maybe Integer -> [EType] -> TcM EType
    completeSum _seen Nothing ts  = return $ TySum $ closedL ts
    completeSum seen (Just i) ts
      | i `elem` seen = error "FIXME: thrown an occurscheck"
      | otherwise     = fixSum <$> applySubst' seen (TyMeta i)
        where
          fixSum (TySum (viewOL -> (ts', j))) = TySum $ OL   (ts ++ ts',  j)
          fixSum (TyMeta j)                   = TySum $ openL ts          j
          fixSum _ = error "Panic! Bug in unification of sums"

    completeProd :: [Integer] -> Maybe Integer -> [EType] -> TcM EType
    completeProd _seen Nothing ts  = return $ TyProd $ closedL ts
    completeProd seen (Just i) ts
      | i `elem` seen = error "FIXME: thrown an occurscheck"
      | otherwise     = fixProd <$> applySubst' seen (TyMeta i)
        where
          fixProd (TyProd (viewOL -> (ts', j))) = TyProd $ OL   (ts ++ ts',  j)
          fixProd (TyMeta j)                    = TyProd $ openL ts          j
          fixProd _ = error "Panic! Bug in unification of products"

applyKSubst :: Kind -> TcM Kind
applyKSubst = applySubst' []
  where
    applySubst' :: [Integer] -> Kind -> TcM Kind
    applySubst'  seen ty@(KMeta t)
      | t `elem` seen = error "FIXME: throw an occurscheck"
      | otherwise     = eitherM (applySubst' (t:seen)) (const $ return ty)
                                (TcEnv.lookupKSubst t ty <$> get)
    applySubst' seen (KArr k1 k2)  = liftM2 KArr (applySubst' seen k1)
                                                 (applySubst' seen k2)
    applySubst' _seen ty           = return ty

-------------------------------------------------------------------------------
-- INTERNAL DEFINITIONS
-------------------------------------------------------------------------------
warn :: (SourcePos -> String -> TcWarn) -> String -> TcM ()
warn f s =
  currentPos >>= tell . Log . (:[]) . flip f s

err :: (SourcePos -> a -> TcError) -> a -> TcM b
err f a = currentPos >>= throwError . flip f a

currentPos :: TcM SourcePos
currentPos = fmap (pop . TcEnv.srcP) get
  where pop []    = initialPos ""
        pop (x:_) = x

-- | Push source position
pushPos :: SourcePos -> TcM ()
pushPos = modify . TcEnv.pushPos

-- | Push source position
enterDef :: Name -> TcM ()
enterDef = modify . TcEnv.enterDef

-- | Out of a source position
popPos :: TcM ()
popPos = modify TcEnv.popPos

pushCtx :: Ctx a -> Seq (CtxEntry a) -> TcM ()
pushCtx c = modify . TcEnv.pushCtx c

popCtx :: Ctx a -> Int -> TcM ()
popCtx c = modify . TcEnv.popCtx c

freshId :: TcM Integer
freshId = state TcEnv.freshId

freshMeta :: TcM EType
freshMeta = state TcEnv.freshMeta

freshKMeta :: TcM Kind
freshKMeta = state TcEnv.freshKMeta
