{-# LANGUAGE FlexibleInstances #-}
module Common.Ppr
        ( Ppr(..)
        , arrow
        ) where

import qualified Text.PrettyPrint as Pretty

class Ppr a where
  ppr     :: a -> Pretty.Doc
  showPpr :: a -> String
  showPpr = Pretty.renderStyle skelStyle . ppr
    where skelStyle = Pretty.Style
                        { Pretty.mode           = Pretty.PageMode
                        , Pretty.lineLength     = 80
                        , Pretty.ribbonsPerLine = 0
                        }

instance Ppr Char where
   ppr = Pretty.char

instance Ppr Int where
   ppr = Pretty.int

instance Ppr a => Ppr [a] where
  ppr = Pretty.hsep . map ppr

instance Ppr Pretty.Doc where
  ppr = id

instance (Ppr a, Ppr b) => Ppr (a,b) where
  ppr (a, b) = Pretty.vcat [ Pretty.hcat [Pretty.text "fst = ", ppr a]
                           , Pretty.hcat [Pretty.text "snd = ", ppr b]
                           ]

instance (Ppr a, Ppr b, Ppr c) => Ppr (a,b,c) where
  ppr (a, b, c) = Pretty.vcat [ Pretty.hcat [Pretty.text "fst = ", ppr a]
                              , Pretty.hcat [Pretty.text "snd = ", ppr b]
                              , Pretty.hcat [Pretty.text "trd = ", ppr c]
                              ]

arrow :: Pretty.Doc
arrow = Pretty.text "->"
