module Common.State
  ( State

  , initial
  , fresh
  ) where

newtype State = St { unSt :: Integer }

initial :: State
initial = St 0

fresh :: (Integer -> a) -> State -> (a, State)
fresh f st@St { unSt = i } = (f i, st { unSt = i + 1})
