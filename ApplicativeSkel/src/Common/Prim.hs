module Common.Prim
  ( Prim(..)
  , pPrim
  ) where

import Text.PrettyPrint ( text, integer, (<>) )
import Text.Parsec      ( try , (<|>) )
import qualified Text.PrettyPrint as Ppr

import Common.Parser hiding ( integer )
import Common.Ppr
import Common.State

data Prim
  = Join
  | Split Int
  | Proj  [Int]
  | Inj   Int

  | Unit
  | In
  | Out
  | Id
  deriving (Eq, Ord)

instance Ppr Prim where
  ppr Unit      = Ppr.text "()"
  ppr Join      = Ppr.text "+"
  ppr (Split x) = Ppr.text "&" <> Ppr.int x
  ppr (Proj x)  = Ppr.text "proj_" <> Ppr.hcat (map Ppr.int x)
  ppr (Inj x)   = Ppr.text "inj_" <> Ppr.int x
  ppr In        = Ppr.text "in"
  ppr Out       = Ppr.text "out"
  ppr Id        = Ppr.text "id"

instance Show Prim where
  show = showPpr

pPrim :: Parser State Prim
pPrim = indent $
      try (fmap (Split . fromIntegral) prod)
  <|> try (fmap (Inj . fromIntegral) inj)
  <|> try (fmap (Proj . (:[]). fromIntegral ) proj)
  <|>     (symbol "(" *> symbol ")" *> return Unit)
