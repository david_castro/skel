{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
module Common.Name
  ( Name (..)
  , prefix
  , gName
  ) where

import Text.Parsec
  ( Stream (..)
  , SourcePos
  )
import Text.Parsec.Pos ( initialPos )

import Common.Ppr
import Text.PrettyPrint ( text )

-- | Name representation. They are strings in a source position
data Name =
  Name { nid  :: String    -- ^ Text
       , npos :: SourcePos -- ^ Source position where text happens
       }

prefix :: String -> Name -> Name
prefix p n = n { nid = p ++ nid n }

gName :: String -> Name
gName s = Name { nid = s, npos = initialPos "(generated)" }


instance Eq Name where
  n1 == n2 = nid n1 == nid n2

instance Ord Name where
  n1 `compare` n2 = nid n1 `compare` nid n2

instance (Monad st, Stream String st Char) => Stream Name st Char where
  uncons nm@Name { nid = s, npos = p }
    = fmap wrap <$> uncons s
    where wrap (t, ws) = (t, nm { nid = ws, npos = p })


instance Ppr Name where
  ppr = text . nid

instance Show Name where
  show = showPpr
