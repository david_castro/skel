module SkelSyn.Constr
  ( Constr (..)
  , pConstr
  , cprod
  ) where

import Text.Parsec ( try, (<|>) )
import Text.PrettyPrint ( (<>) )
import qualified Text.PrettyPrint as Ppr

import Common

data Constr =
    CUnit
  | CProd Integer
  | CSum  Integer
  | CName Name

instance Show Constr where
  show = showPpr

instance Ppr Constr where
  ppr  CUnit    = Ppr.text "()"
  ppr (CProd n) = Ppr.text "prod_" <> Ppr.integer n
  ppr (CSum  n) = Ppr.text "inj_"  <> Ppr.integer n
  ppr (CName c) = ppr $ nid c

pConstr :: Parser State Constr
pConstr = indent $
      try (fmap CName capitalIdent)
  <|> try (fmap CSum  inj)
  <|> try (fmap CProd prod)
  <|>     (symbol "(" *> symbol ")" *> return CUnit)

cprod :: Integral a => a -> Constr
cprod = CProd . fromIntegral
