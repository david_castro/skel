{-# LANGUAGE ViewPatterns #-}
module SkelSyn.Expr
  ( Expr (..)
  , eabs
  , eapp
  , eapps
  , isVar
  , patternToExpr
  , constrToExpr
  , uncurryExpr
  , pExpr
  , aExpr
  , pAlt
  , pprAPat
  ) where

import Control.Arrow      ( second )
import Control.Monad      ( liftM2 )
import Text.Parsec        ( try, many1, sepBy1, (<|>) )
import Text.Parsec.Indent ( withBlock, block )
import Text.PrettyPrint   ( hsep, text, (<+>), (<>)
                          , char, punctuate, vcat
                          , nest, ($$) )

import qualified Text.PrettyPrint     as Ppr

import Common

import SkelSyn.Pattern

data Expr =
    ELit  Literal
  | EPrim Prim
  | EVar  Name
  | EAbs  [Pattern] Expr
  | EApp  Expr      Expr
  | ECase Expr      [(Pattern, Expr)]
  deriving (Eq, Ord)

infixl 4 :@

eabs :: Name -> Expr -> Expr
eabs v (EAbs ps e) = EAbs (PVar v : ps) e
eabs v e           = EAbs [PVar v] e

eapp :: Expr -> Expr -> Expr
eapp (EAbs [PVar v] e) (EVar v')
  | v == v' = e -- FIXME: should I do trivial beta reductions here?
eapp e1 e2 = EApp e1 e2

eapps :: [Expr] -> Expr
eapps = foldl1 eapp

data ViewApp
  = Expr :@ [Expr]
  | NoApp

chaseApp :: Int -> Expr -> ViewApp
chaseApp 1     (EApp f                                   arg) = f :@ [arg]
chaseApp nargs (EApp (chaseApp (nargs - 1) -> f :@ args) arg) = f :@ arg:args
chaseApp nargs (EApp f                                   arg) = f :@ [arg]
chaseApp _     _                                              = NoApp

viewApp :: Int -> Expr -> ViewApp
viewApp nargs (chaseApp nargs -> f :@ revargs) = f :@ reverse revargs
viewApp _     _                                = NoApp

uncurryExpr :: Int -> Name -> Expr -> Expr
uncurryExpr nargs n (viewApp nargs -> f@(EVar m) :@ args)
  | n == m                  = EApp f $ eprod args
uncurryExpr nargs n (EApp e1 e2)  = EApp (uncurryExpr nargs n e1) $ uncurryExpr nargs n e2
uncurryExpr nargs n (EAbs x1 x2)  = EAbs x1 $ uncurryExpr nargs n x2
uncurryExpr nargs n (ECase x1 x2) = ECase (uncurryExpr nargs n x1)
                                    (map (second $ uncurryExpr nargs n) x2)
uncurryExpr nargs n e             = e

isVar :: Name -> Expr -> Bool
isVar a (EVar b) = a == b
isVar _ _        = False

eprod :: [Expr] -> Expr
eprod [e] = e
eprod es  = foldl1 EApp $ EPrim (Split $ length es) : es

instance Show Expr where
  show = showPpr

instance Ppr Expr where
  ppr (ELit l)     = ppr l
  ppr (EPrim l)    = ppr l
  ppr (EVar n)     = ppr n
  ppr (EApp e1 e2) = pprApp (chase e1 [e2])
    where
      chase (EApp el er) e3 = chase el (er:e3)
      chase el           er = el:er

      pprApp (EPrim Split{} : es)
        = Ppr.parens (hsep (punctuate Ppr.comma (map ppr es)))
      pprApp es
        = hsep (map pprPar es)

      pprAppNP (EPrim Split{} : es)
        = hsep (punctuate Ppr.comma (map ppr es))
      pprAppNP es
        = hsep (map pprPar es)

      pprPar p@(ELit _)     = ppr p
      pprPar p@(EPrim _)    = ppr p
      pprPar p@(EVar _)     = ppr p
      pprPar   (EApp a1 a2) = Ppr.parens $ pprAppNP (chase a1 [a2])
      pprPar p              = Ppr.parens $ ppr p
  ppr (EAbs ps e)  = char '\\' <> hsep (map pprAPat ps) <> Ppr.comma <+> ppr e
  ppr (ECase e ps)
    = text "case" <+> ppr e <+> text "of" $$ nest 2 (vcat $ map pprAlts ps)
    where
      pprAlts (p, ea) = ppr p <+> text "=>" <+> ppr ea

pExpr :: Parser State Expr
pExpr = indent $
      try $ fmap (foldl1 EApp) (many1 aExpr)
  <|> try (reservedOp "\\" *> liftM2 EAbs (manyTill1 aPattern comma) pExpr)
  <|>     liftM2 ECase (reserved "case" *> pExpr <* reserved "of") (block pAlt)

aExpr :: Parser State Expr
aExpr = indent $
      try (parens (eprod <$> sepBy1 pExpr comma))
  <|> try (fmap ELit pLiteral)
  <|> try (fmap EPrim pPrim)
  <|>      fmap EVar identifier

pAlt :: Parser State (Pattern, Expr)
pAlt = liftM2 (,) (pPattern <* reservedOp "=>") pExpr

-- | Converts a pattern to the equivalent expression
patternToExpr :: Pattern -> Expr
patternToExpr (PVar v) = EVar v
patternToExpr (P c []) = constrToExpr c
patternToExpr (P c ps) = foldl1 EApp $ constrToExpr c : map patternToExpr ps

constrToExpr :: Constr -> Expr
constrToExpr (CName n) = EVar n
constrToExpr CUnit     = EPrim Unit
constrToExpr (CProd i) = EPrim (Split i)
constrToExpr (CSum i)  = EPrim (Inj i)

