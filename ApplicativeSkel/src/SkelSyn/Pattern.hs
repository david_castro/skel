module SkelSyn.Pattern
  ( Constr  (..)
  , Pattern (..)
  , pPattern
  , aPattern
  , pprod

  , pprAPat
  ) where

import Control.Monad      ( liftM2 )
import Text.Parsec        ( try, many, sepBy1, (<|>), (<?>) )
import Text.PrettyPrint   ( hsep, text, (<>), (<+>), punctuate )

import qualified Text.PrettyPrint     as Ppr

import Common

data Constr
  = CName Name
  | CUnit
  | CProd Int
  | CSum  Int
  deriving (Eq, Ord)

instance Ppr Constr where
  ppr (CName n)  = ppr n
  ppr CUnit      = Ppr.text "()"
  ppr (CProd i)  = Ppr.text "&"
  ppr (CSum i  ) = Ppr.text "inj_" <> Ppr.int i

data Pattern =
    PVar      Name              -- Variable:    x
  | P Constr  [Pattern] -- Pattern Constructor
  deriving (Eq, Ord)

instance Show Pattern where
  show = showPpr

instance Ppr Pattern where
  ppr (PVar n)            = text $ nid n
  ppr (P CProd{} p)   = Ppr.parens $ hsep $ punctuate Ppr.comma $ map ppr p
  ppr (P n ps)      = ppr n <+> hsep (map pprAPat ps)

pprAPat :: Pattern -> Ppr.Doc
pprAPat p@(PVar _)          = ppr p
pprAPat p@(P _ [])    = ppr p
pprAPat p@(P CProd{} _) = ppr p
pprAPat p                   = Ppr.parens $ ppr p

pprod :: [Pattern] -> Pattern
pprod [p] = p
pprod ps  = P (CProd $ length ps) ps


pPattern :: Parser State Pattern
pPattern =
       try (inj >>= \i -> fmap (pSum i . (:[])) aPattern)
   <|> try (liftM2 (P . CName) capitalIdent $ many aPattern)
   <|>     aPattern
   <?> "pattern"
  where
    pSum = P . CSum . fromIntegral

aPattern :: Parser State Pattern
aPattern =
       try (parens pPattern)
   <|> try (fmap pProd (parens $ sepBy1 pPattern comma))
   <|> try (fmap (`patNm` []) capitalIdent)
   <|>      fmap PVar  lowerIdent
   <?> "atomic pattern"
  where
    pProd [] = P CUnit []
    pProd ps = P (CProd $ length ps) ps
    patNm = P . CName
