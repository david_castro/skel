module SkelSyn.Def
  ( Def  (..)
  , pFnDef
  , pTyDef
  , uncurryAlts


  , ConstrDef (..)
  , pConstrDef
  ) where

import Control.Arrow      ( second, (***) )
import Control.Monad      ( liftM, liftM2 )
import Control.Monad.Fix  ( mfix )
import Data.Foldable      ( toList )
import Data.Sequence      ( Seq )
import Text.Parsec        ( Stream, ParsecT, manyTill
                          , try, many, many1, sepBy1
                          , (<|>), (<?>) )
import Text.Parsec.Indent ( withBlock )
import Text.PrettyPrint   ( hsep, text, (<+>), (<>)
                          , char, int, punctuate, vcat
                          , nest, ($$), ($+$) )

import qualified Data.Sequence        as Seq
import qualified Text.Parsec.Indent   as Indent
import qualified Text.PrettyPrint     as Ppr

import Common
import Type

import SkelSyn.Constr
import SkelSyn.Expr
import SkelSyn.Pattern

type Alt = ([Pattern], Expr)

data Def =
    FnDef Name ForAll [Alt]           -- d : A -> B // d p1 ... = e1 // d p2 ... = e2 ..
  | TyDef Name (Seq Name) [ConstrDef] -- data Ty A B = C1 ... | C2 ... | ...

instance Ppr Def where
  ppr (FnDef n ty alts) = vcat (sig:ps) $+$ text ""
    where
      sig = ppr n <+> Ppr.colon <+> ppr ty
      ps  = map ((ppr n <+>) . pprAlt) alts

      pprAlt (ps, e) = hsep $ map pprAPat ps ++ [char '=', ppr e]

  ppr (TyDef  n ns cs)
    = text "data" <+> ppr n <+> hsep (map ppr (toList ns)) <+> char '=' $$
      nest 2 (punctuateIndent (char '|') $ map ppr cs) $+$ text ""

uncurryAlts :: Int -> Name -> [Alt] -> [Alt]
uncurryAlts nc n = map (uncurryPattern *** uncurryExpr nc n)
  where
    uncurryPattern [] = []
    uncurryPattern ps = [pprod ps]

punctuateIndent :: Ppr.Doc -> [Ppr.Doc] -> Ppr.Doc
punctuateIndent _ []  = Ppr.empty
punctuateIndent _ [d] = d
punctuateIndent c (d1:ds) = nest len d1 $$ aux ds
  where
    len         = length (showPpr c) + 1
    aux []      = Ppr.empty
    aux (d1:ds) = c <+> d1 $$ aux ds

instance Show Def where
  show = showPpr

pFnDef :: Parser State Def
pFnDef = pFnDefType >>= \(n, sch) -> fmap (FnDef n sch) (pBody n)

pFnDefType :: Parser State (Name, ForAll)
pFnDefType = do
  Indent.checkIndent
  f   <- lowerIdent
  reservedOp ":"
  sch <- pForAll
  return (f, sch)
  <?> "type declaration"

pBody :: Name -> Parser State [([Pattern], Expr)]
pBody n = many aux
  where aux = do Indent.checkIndent
                 symbol (nid n)
                 ps <- many aPattern
                 reservedOp "="
                 e  <- pExpr
                 return (ps, e)
                 <?> "definition body"

data ConstrDef =
    CInd EType Name [EType] -- CInd ty "n" [t1, t2, ...] ===> n : t1 -> t2 -> ... -> ty

instance Show ConstrDef where
  show = showPpr
instance Ppr ConstrDef where
  ppr (CInd _ c args) = hsep $ ppr c : map parensPpr args
    where
      parensPpr t@(TyVar  n    ) = ppr t
      parensPpr t@(TyMeta i    ) = ppr t
      parensPpr t@(TySum  ts   ) = Ppr.parens (ppr t)
      parensPpr t@(TyProd ts   ) = Ppr.parens (ppr t)
      parensPpr t@(TyApp  ts   ) = Ppr.parens (ppr t)
      parensPpr t@(TyArr  t1 t2) = Ppr.parens (ppr t)

pConstrDef :: EType -> Parser State ConstrDef
pConstrDef n = indent $ liftM2 (CInd n) capitalIdent $ many aType

-- | Parse type definition:
-- TODO: check that there are no repeated constructors
pTyDef :: Parser State Def
pTyDef = do
  Indent.checkIndent
  reservedOp "data"
  ty <- capitalIdent
  vs <- many capitalIdent <* reservedOp "="
  TyDef ty (Seq.fromList vs) <$>
      sepBy1 (pConstrDef (tyApp $ map TyVar (ty:vs)))
             (indent $ reservedOp "|")
