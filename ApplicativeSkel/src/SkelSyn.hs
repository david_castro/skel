module SkelSyn
  ( Prog (..)
  , parseF
  , pProg
  ) where

import Control.Monad      ( liftM, liftM2 )
import Text.Parsec        ( try, (<|>), runParserT, getState, eof )
import Text.Parsec.Indent ( block, runIndent     )
import Text.PrettyPrint   ( vcat )

import Common
import SkelSyn.Def

doParse :: FilePath -> Parser State a -> String -> a
doParse fp pr = either (error . show) id . runIndent fp . runParserT pr initial fp

parseF :: FilePath -> IO (Prog, State)
parseF fp = fmap  (doParse fp pr) (readFile fp)
  where pr = liftM2 (,) (pProg fp) getState

data Prog = Prog { fn    :: FilePath
                 , defns :: [Def]
                 }

instance Ppr Prog where
  ppr = vcat . map ppr . defns

instance Show Prog where
  show = showPpr

pProg :: FilePath -> Parser State Prog
pProg fp =
  whiteSpace *> fmap (Prog fp) (block (try pTyDef <|> try pFnDef)) <* eof
