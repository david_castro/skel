{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
module TcM.TcEnv
  ( TcEnv
  , gSt
  , srcP
  , curr
  , initTcEnv

  , Ctx (..)
  , GammaCtx (..)
  , gProd
  , CtxElem
  , CtxEntry

  , freshPref
  , freshId
  , freshMeta
  , freshKMeta
  , freshTyVar
  , freshEVar
  , freshEVars

  , lookupSubst
  , lookupKSubst

  , enterDef
  , pushPos
  , popPos
  , pushCtx
  , popCtx
  , lookupCtx
  , ctxSize

  -- FIXME: refactor these below, should be constructors on some literal type
  , builtInInt
  , builtInBool
  , builtInString
  ) where

import Data.Foldable ( toList, foldl' )
import Data.Map ( Map )
import Data.Sequence ( Seq, (><), ViewR(..), ViewL(..) )
import qualified Data.Map as Map
import qualified Data.Sequence as Seq
import Text.Parsec.Pos ( SourcePos, initialPos )

import Common
import Type
import SkelSyn.Expr -- FIXME: I don't like this dependency here!

-- | Typechecking environment:
data TcEnv =
  TcEnv { gSt   :: State              -- ^ Global compiler environment

        -- Environments for typechecking
        , delta :: !(Seq (Name, ForAll)) -- ^ Top-level defns
        , gamma :: !(Seq GammaCtx)       -- ^ Locally bound vars
        , kappa :: !(Seq (Name, Kind))   -- ^ Kind environment
        , funct :: !(Seq (Name, ForAll)) -- ^ Base functor defns

        -- Substitution
        , tsubs :: !(Map Integer EType)  -- ^ Substitution. Updated by 'unify'.
        , ksubs :: !(Map Integer Kind)   -- ^ Kind Substitution.
        , srcP  :: ![SourcePos]          -- ^ Stack of positions we have accessed
        , curr  :: !Name
        }

-- | Elements in a gamma context. We do not keep a flat structure, since we
-- need to know ``where'' to find a variable from a context
data GammaCtx =
    GSkip
  | GElem Name EType     -- ^ top-level var
  | GProd (Seq GammaCtx) -- ^ inside product-irrefutable pattern

gProd :: Seq GammaCtx -> Seq GammaCtx
gProd s
  | Seq.length s <  1 = Seq.singleton   GSkip
  | Seq.length s == 1 = s
  | otherwise         = Seq.singleton $ GProd s

flattenGamma :: Seq GammaCtx -> Seq (Name, EType)
flattenGamma = foldl' flattenG Seq.empty
  where
    flattenG b = (b ><) . flatten
      where
        flatten (GElem n t) = Seq.singleton (n, t)
        flatten (GProd s)   = flattenGamma s
        flatten  GSkip      = Seq.empty


-- | Initial typechecking environment
initTcEnv :: State -> TcEnv
initTcEnv st =
  TcEnv { gSt = st
        , delta = Seq.empty
        , gamma = Seq.empty
        -- TODO: builtins
        , kappa = Seq.fromList $ zip builtInTyNs $ repeat KType

        , funct = Seq.empty
        , srcP  = []
        , curr  = Name "" (initialPos "")
        , tsubs = Map.empty
        , ksubs = Map.empty
        }

intNm,boolNm,stringNm :: Name
intNm                  = Name "Int" (initialPos "")
boolNm                 = Name "Bool" (initialPos "")
stringNm               = Name "String" (initialPos "")

builtInInt,builtInBool,builtInString :: EType
builtInInt             = TyVar intNm
builtInBool            = TyVar boolNm
builtInString          = TyVar stringNm

builtInTyNs   :: [Name]
builtInTyNs   = [intNm, boolNm, stringNm]

data CtxTy = CDelta | CGamma | CKappa | CFunct

-- | Kinds of contexts
data Ctx :: CtxTy -> * where
  Delta :: Ctx 'CDelta -- ^ Global definition environment
  Gamma :: Ctx 'CGamma -- ^ Local environment
  Kappa :: Ctx 'CKappa -- ^ Kind environment (careful, both local and global)
  Funct :: Ctx 'CFunct -- ^ Base functor definitions

type family CtxEntry a where
  CtxEntry 'CDelta = (Name, ForAll)
  CtxEntry 'CGamma = GammaCtx
  CtxEntry 'CKappa = (Name, Kind)
  CtxEntry 'CFunct = (Name, ForAll)

type family CtxElem a where
  CtxElem 'CDelta = (ForAll, AExpr Expr)
  CtxElem 'CGamma = (EType,  AExpr Expr)
  CtxElem 'CKappa = Kind
  CtxElem 'CFunct = ForAll

freshId :: TcEnv -> (Integer, TcEnv)
freshId = liftState $ fresh id

-- | Creates a fresh type metavariable in a TC environment
freshMeta :: TcEnv -> (EType, TcEnv)
freshMeta = liftState $ fresh TyMeta

-- | Creates a fresh kind metavariable in a TC environment
freshKMeta :: TcEnv -> (Kind, TcEnv)
freshKMeta = liftState $ fresh KMeta

-- | Fresh variable
freshTyVar  :: TcEnv -> Name
freshTyVar = head . freshNames 1 ['A'..'Z']

freshPref :: String -> TcEnv -> Name
freshPref prefix e = gName $ head [ v | v <- varsupply, v `notElem` bound ]
  where
    bound = map nid $ map fst (toList $ delta e)
                        ++ map fst (toList $ flattenGamma $ gamma e)
                        ++ map fst (toList $ kappa e)
    varsupply =  prefix : [prefix ++ "_" ++ show i |  i <- [(1::Integer)..] ]

-- | Fresh expr variable
freshEVar  :: TcEnv -> Name
freshEVar = head . freshNames 1 ['a'..'z']

freshEVars  :: Int -> TcEnv -> [Name]
freshEVars i = freshNames i ['a'..'z']

-- | Generate a fresh name from an initial generator "abcd...z" or "ABCD...Z"
freshNames  :: Int -> String -> TcEnv -> [Name]
freshNames i gen e = map gName $ take i [ v | v <- varsupply, v `notElem` bound ]
  where
    bound = map nid $ map fst (toList $ delta e)
                        ++ map fst (toList $ flattenGamma $ gamma e)
                        ++ map fst (toList $ kappa e)
    vars = map (:[]) gen
    varsupply = vars ++
                [ v ++ show i | v <- vars, i <- [(1::Integer)..]  ]

-- TODO: remove boilerplate! (lenses start making sense?)

-- | Lookup a type substitution. If it does not exist, extend it
lookupSubst :: Integer -> EType -> TcEnv -> Either EType TcEnv
lookupSubst i ty e@TcEnv { tsubs = m }
  | Just t <- Map.lookup i m = Left t
  | otherwise                = Right e { tsubs = Map.insert i ty m }

-- | Lookup a kind substitution. If it does not exist, extend it
lookupKSubst :: Integer -> Kind -> TcEnv -> Either Kind TcEnv
lookupKSubst i ty e@TcEnv { ksubs = m }
  | Just t <- Map.lookup i m = Left t
  | otherwise                = Right e { ksubs = Map.insert i ty m }

enterDef :: Name -> TcEnv -> TcEnv
enterDef n e = e' { curr = n, tsubs = Map.empty, ksubs = Map.empty }
  where e' = pushPos (npos n) e

-- | Enter a new source position
pushPos :: SourcePos -> TcEnv -> TcEnv
pushPos p e@TcEnv{ srcP = ps } = e { srcP = p:ps }

-- | Out of a position
popPos :: TcEnv -> TcEnv
popPos e@TcEnv { srcP = []   } = e
popPos e@TcEnv { srcP = _:ps } = e { srcP = ps }

-- | Push environment
pushCtx :: Ctx a -> Seq (CtxEntry a) -> TcEnv -> TcEnv
pushCtx Kappa vs e@TcEnv { kappa = k } = e { kappa = k >< vs }
pushCtx Gamma vs e@TcEnv { gamma = k } = e { gamma = k >< vs }
pushCtx Delta vs e@TcEnv { delta = k } = e { delta = k >< vs }
pushCtx Funct vs e@TcEnv { funct = k } = e { funct = k >< vs }

-- | Pop environment
popCtx ::  Ctx a -> Int -> TcEnv -> TcEnv
popCtx Kappa n e@TcEnv { kappa = k } =
  e { kappa = Seq.take (Seq.length k - n) k }
popCtx Gamma n e@TcEnv { gamma = k } =
  e { gamma = Seq.take (Seq.length k - n) k }
popCtx Delta n e@TcEnv { delta = k } =
  e { delta = Seq.take (Seq.length k - n) k }
popCtx Funct n e@TcEnv { funct = k } =
  e { funct = Seq.take (Seq.length k - n) k }

-- | Lookup environment
lookupCtx ::  Ctx a -> Name -> TcEnv -> Maybe (CtxElem a)
lookupCtx Kappa n = lookupSeq     n . kappa
lookupCtx Gamma n = lookupGamma 0 n . gamma
lookupCtx Delta n = lookupDelta   n
lookupCtx Funct n = lookupSeq     n . funct

lookupDelta :: Name -> TcEnv -> Maybe (ForAll, AExpr Expr)
lookupDelta n s
  = fmap (, global (Seq.length (gamma s)) (EVar n)) (lookupSeq n $ delta s)


lookupGamma :: Int -> Name -> Seq GammaCtx -> Maybe (EType, AExpr Expr)
lookupGamma i n (Seq.viewr -> t :> e)
  | Just (p, ty) <- lookupGCtx n e = Just (ty, local (Seq.length t) i p)
  | otherwise                      = lookupGamma (i+1) n t
lookupGamma _ _ _                  = Nothing

lookupGCtx :: Name -> GammaCtx -> Maybe ([Int], EType)
lookupGCtx n (GElem m ty) | n == m    = Just ([], ty)
                          | otherwise = Nothing
lookupGCtx n (GProd cs)               = lookupProd 0 n cs
lookupGCtx n  GSkip                   = Nothing

lookupProd :: Int -> Name -> Seq GammaCtx -> Maybe ([Int], EType)
lookupProd i n (Seq.viewl -> e :< t)
  | Just (p, ty) <- lookupGCtx n e = Just (i:p, ty)
  | otherwise                      = lookupProd (i+1) n t
lookupProd _ _ _                   = Nothing

ctxSize ::  Ctx a -> TcEnv -> Int
ctxSize Kappa = Seq.length . kappa
ctxSize Gamma = Seq.length . gamma
ctxSize Delta = Seq.length . delta
ctxSize Funct = Seq.length . funct

lookupSeq :: Name -> Seq (Name, a) -> Maybe a
lookupSeq n (Seq.viewr -> t :> (m,x))
  | n == m    = Just x
  | otherwise = lookupSeq n t
lookupSeq _ _                         = Nothing


-------------------------------------------------------------------------------
-- INTERNAL DEFINITIONS -------------------------------------------------------
-------------------------------------------------------------------------------

-- | Lifts operations in global states to TC environments
liftState :: (State -> (a, State)) -- ^ Computation on states
          -> TcEnv                 -- ^ Input enviromnent
          -> (a, TcEnv)            -- ^ Result in modified environment
liftState f env@TcEnv { gSt = st } = (x, env{gSt=st'})
  where
    (x, st') = f st
