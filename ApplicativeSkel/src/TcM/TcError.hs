module TcM.TcError
  ( TcError
  , notInScope
  , unifyKindErr
  , nonUniformType
  , unifyError
  , unexpectedType
  ) where

import Common
import Type

import Text.PrettyPrint as Ppr
import Text.Parsec ( SourcePos )

data TcError = TcError SourcePos TcErrorMsg

instance Ppr TcError where
  ppr (TcError p m) = Ppr.text "Error in" <+>
                        Ppr.text (show p) <+> Ppr.colon <+> ppr m

data TcErrorMsg =
    NotInScope     Name
  | UnifyKindErr   Kind  Kind
  | NonUniformType Name
  | UnifyError     EType EType
  | UnexpectedType EType String

instance Ppr TcErrorMsg where
  ppr (NotInScope n)     = Ppr.text "Variable" <+> Ppr.text "'"
                        <> ppr n <> Ppr.text "'" <+> Ppr.text "not in scope."
  ppr (UnifyKindErr e i) =
    Ppr.text "Kind unification." <+> Ppr.text "Expected '" <> ppr e
                                 <> Ppr.text "', inferred '" <>
                                 ppr i <> Ppr.text "'."
  ppr (NonUniformType v) =
    Ppr.text "Not-supported feature: non-uniform types."
    <+> Ppr.text "Found non-uniform type: " <> ppr v

  ppr (UnifyError t1 t2) =
    Ppr.text "Cannot unify types. Expected: " <+> ppr t2
                    <+> Ppr.text "Actual: " <+> ppr t1

  ppr (UnexpectedType t1 msg) =
    Ppr.text "Unexpected: " <+> ppr t1 <+> Ppr.colon <+> Ppr.text msg

notInScope :: SourcePos -> Name -> TcError
notInScope p = TcError p . NotInScope

unifyKindErr :: SourcePos -> (Kind, Kind) -> TcError
unifyKindErr p = TcError p . uncurry UnifyKindErr

nonUniformType :: SourcePos -> Name -> TcError
nonUniformType p = TcError p . NonUniformType

unifyError :: SourcePos -> (EType, EType) -> TcError
unifyError p = TcError p . uncurry UnifyError

unexpectedType :: SourcePos -> (EType, String) -> TcError
unexpectedType p = TcError p . uncurry UnexpectedType

instance Show TcError where
  show = showPpr
