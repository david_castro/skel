module TcM.TcWarn
  ( TcWarn
  , tcStub
  ) where

import Text.PrettyPrint ( (<>), (<+>) ) 
import qualified Text.PrettyPrint as Ppr
import Text.Parsec ( SourcePos )

import Common

data TcWarn = TcWarn SourcePos TcWarnMsg

instance Ppr TcWarn where
  ppr (TcWarn p w) =
    Ppr.text (show p) <> Ppr.colon <+> Ppr.text "Warning:" <+> ppr w

-- | Warning messages
data TcWarnMsg =
    TcStub String -- ^ Warning for unimplemented features

-- | Warn unimplemented features
tcStub :: SourcePos -> String -> TcWarn
tcStub p = TcWarn p . TcStub

instance Ppr TcWarnMsg where
  ppr (TcStub w) = Ppr.text "Stub:" <+> Ppr.text w 
