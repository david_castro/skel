module TcSkelSyn
  ( typeCheck
  , tcProg
  ) where

import Text.Parsec.Pos
  ( initialPos )

import Common
import SkelSyn
import TcM
import Util

import TcSkelSyn.TcDef

typeCheck :: State -> Prog -> IO (Prog, State)
typeCheck st = runTcM st . tcProg

-- | Typecheck a program
tcProg :: Prog -> TcM Prog
tcProg p = do
  dfns <- inPos (initialPos $ fn p) $ mapM tcDef $ defns p
  return p { defns = concat dfns }
