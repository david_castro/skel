{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE ViewPatterns          #-}
module Util.RWS
        ( RWS
        , runRWS
        ) where

import Control.Monad.Except
import Control.Monad.State.Strict
import Control.Monad.Writer

import Util.Util

data RWS e s l a = RWS { runRWS :: s -> (Either e a, s, l) }

deriving instance Functor (RWS e s l)

instance Monoid l => Applicative (RWS e s l) where
  pure a = RWS { runRWS = \s -> (Right a, s, mempty) }
  af <*> ax = RWS { runRWS = doApp }
    where
      doApp (runRWS af -> (Left  e, s, l)) = (Left  e, s, l)
      doApp (runRWS af -> (Right f, s, l)) = mappend3 l $ runRWS (fmap f ax) s
      doApp _                              =
            error "Panic! Impossible case instance Monad RWS:<*>"

instance Monoid l => Monad (RWS e s l) where
  return  = pure
  m >>= f = RWS { runRWS = doBind }
    where
      doBind (runRWS m -> (Left  e, s, l)) = (Left e, s, l)
      doBind (runRWS m -> (Right x, s, l)) = doBind2 (f x)
        where
          doBind2 (flip runRWS s -> (Left  e, s', l'))
            = (Left  e, s', l')
          doBind2 (flip runRWS s -> (Right y, s', l'))
            = (Right y, s', l `mappend` l')
          doBind2 _
            =
            error "Panic! Impossible case instance Monad RWS: >>= (2)"

      doBind _                             =
            error "Panic! Impossible case instance Monad RWS: >>="

instance Monoid l => MonadError e (RWS e s l) where
  throwError e = RWS { runRWS = doThrowError }
    where doThrowError s = (Left e, s, mempty)

  catchError ma fma = RWS { runRWS = doCatch }
    where
      doCatch (runRWS ma -> (Left  e, s, l)) = mappend3 l $ runRWS (fma e) s
      doCatch (runRWS ma -> (Right x, s, l)) = (Right x, s, l)
      doCatch _                              =
            error "Panic! Impossible case instance MonadError RWS: catchError"

instance Monoid l => MonadWriter l (RWS e s l) where
  writer (a, l) = RWS { runRWS = doWriter }
    where doWriter s = (Right a, s, l)
  listen ma     = RWS { runRWS = doListen }
    where doListen (runRWS ma -> (Left  e, s, l)) = (Left e, s, l)
          doListen (runRWS ma -> (Right x, s, l)) = (Right (x,l), s, l)
          doListen _                                =
            error "Panic! Impossible case instance MonadWriter RWS: listen"
  pass mw       = RWS { runRWS = doPass }
    where doPass (runRWS mw -> (Left  e    , s, l)) = (Left e   , s, l)
          doPass (runRWS mw -> (Right (x,f), s, l)) = (Right x  , s, f l)
          doPass _                                  =
           error "Panic! Impossible case instance MonadWriter RWS: pass"

instance Monoid l => MonadState s (RWS e s l) where
  state f = RWS { runRWS = doState }
    where doState (f -> (x, s)) = (Right x, s, mempty)
