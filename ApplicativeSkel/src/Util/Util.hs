{-# LANGUAGE TupleSections #-}
module Util.Util
        ( mapSndM
        , mapFstM
        , mappend3
        , maybeM
        , splitL
        , mapMM
        , dup
        ) where

import Control.Monad ( join )
import qualified Data.Map.Strict  as Map

mapSndM :: Monad m => (b -> m c) -> (a, b) -> m (a, c)
mapSndM f (a, b) = fmap (a,) (f b)

mapFstM :: Monad m => (a -> m c) -> (a, b) -> m (c, b)
mapFstM f (a, b) = fmap (,b) (f a)

mappend3 :: Monoid m => m -> (a, b, m) -> (a, b, m)
mappend3 m (a, b, m') = (a, b, m `mappend` m')

maybeM :: Monad m => m b -> (a -> m b) -> m (Maybe a) -> m b
maybeM mb f = join . fmap (maybe mb f)

splitL :: [a] -> ([a], a)
splitL [] = error "Bug when calling Util.Util.splitL: empty list"
splitL [x] = ([], x)
splitL (x:xs) = (x:ys, y)
  where (ys, y) = splitL xs

dup :: a -> (a,a)
dup x = (x,x)

mapMM :: Monad m => (k -> a -> m ()) -> Map.Map k a -> m ()
mapMM f = mapM_ (uncurry f) . Map.assocs
