module Main where

import SkelSyn
import TcSkelSyn

import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    []    -> putStrLn "Usage: skelc <prog.skel>"
    (a:_) -> compile a

compile :: String -> IO ()
compile fn = do
  (s , st1) <- parseF fn
  (s', st2) <- typeCheck st1 s
  -- print s'
  return ()
