{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ViewPatterns #-}
module Type.Type
  ( EType (..)
  , Struct (..)
  , Skel (..)
  , Alg  (..)

  , (+:), (*:)
  , prodTy
  , pType
  , aType
  , uncurryTy
  , uncurryEType
  , tyApp
  , flatten

  , OpenL (..)
  , openL
  , closedL
  , olToList
  , olElems
  ) where

import Control.Monad       ( liftM2, liftM3 )
import Text.Parsec         ( try, many1, sepBy1, (<|>) )
import Text.PrettyPrint    ( hsep, text, (<+>), (<>), char, punctuate )

import qualified Text.PrettyPrint     as Ppr

import Common

newtype OpenL a = OL { viewOL :: ([a], Maybe Integer) }
  deriving (Show, Functor, Foldable)

openL :: [a] -> Integer -> OpenL a
openL xs = curry OL xs . Just

closedL :: [a] -> OpenL a
closedL xs = OL (xs, Nothing)

olElems :: OpenL EType -> [EType]
olElems = fst . viewOL

olToList :: OpenL EType -> [EType]
olToList (viewOL -> (xs, Just i)) = xs ++ [TyMeta i]
olToList ol                       = fst $ viewOL ol

data EType =
    TyVar  Name
  | TyMeta Integer

  | TyUnit
  | TySum  (OpenL EType)
  | TyProd (OpenL EType)

  | TyApp  [EType]            -- F A B
  | TyArr  EType EType        -- A -> B FIXME: consistent repr
  | TyAnn  EType (Struct Int) -- Structure annotation

infixl 6 +:
infixl 7 *:

(+:) :: EType -> EType -> EType
TySum (OL (xs, Nothing)) +: TySum (OL (ys, m))       = TySum $ OL (xs ++ ys, m)
TySum (OL (xs, Nothing)) +: t                        = TySum $ OL (xs ++ [t], Nothing)
t                        +: TySum (OL (xs, Nothing)) = TySum $ OL (t:xs, Nothing)
t1                       +: t2                       = TySum $ OL ([t1,t2], Nothing)

(*:) :: EType -> EType -> EType
TyProd (OL (xs, Nothing)) *: TyProd (OL (ys, m))       = TyProd $ OL (xs ++ ys, m)
TyProd (OL (xs, Nothing)) *: t                         = TyProd $ OL (t:xs, Nothing)
t                         *: TyProd (OL (xs, Nothing)) = TyProd $ OL (t:xs, Nothing)
t1                        *: t2                        = TyProd $ OL ([t1,t2], Nothing)

prodTy :: [EType] -> EType
prodTy []  = TyUnit
prodTy [t] = t
prodTy ts  = TyProd $ closedL $ flattenProds ts
  where
    flattenProds (TyProd (OL (xs, Nothing)) : ps) = flattenProds xs ++ flattenProds ps
    flattenProds (p : ps) = p : flattenProds ps
    flattenProds []       = []

-- A bit stupid maybe ...
instance Num EType where
  negate      = id
  (+)         = (+:)
  (*)         = (*:)
  fromInteger = TyMeta
  abs         = id
  signum      = const TyUnit

tyApp :: [EType] -> EType
tyApp []  = TyUnit
tyApp [x] = x
tyApp xs  = TyApp xs

uncurryEType :: Int -> EType -> EType
uncurryEType i t =
  case ts of
    []   -> r
    [ty] -> TyArr ty r
    _    -> TyArr (TyProd $ closedL ts) r
  where
    (ts, r) = uncurryTy i t

uncurryTy :: Int -> EType -> ([EType], EType)
uncurryTy i = chaseArr i []
  where
    chaseArr 0 acc t             = (reverse acc, t)
    chaseArr i acc (TyArr t1 t2) = chaseArr (i-1) (t1:acc) t2
    chaseArr _ acc t             = (reverse acc, t)

-- | Used to create sums, products and applications
mkTy :: ([EType] -> EType) -> [EType] -> EType
mkTy _f []  = TyUnit
mkTy _f [x] = x
mkTy  f xs  = f xs

instance Ppr EType where
  ppr (TyVar  n    ) = ppr n
  ppr (TyMeta i    ) = char '?' <> Ppr.integer i
  ppr  TyUnit        = text "()"
  ppr (TySum  ts   ) = hsep $ punctuate (text " +") $
                         map parensSum $ fst $ viewOL ts
    where
      parensSum p@(TySum _) = Ppr.parens $ ppr p
      parensSum p@ TyArr{}  = Ppr.parens $ ppr p
      parensSum p           = ppr p
  ppr (TyProd ts   ) = hsep $ punctuate (text " *") $
                         map parensProd $ fst $ viewOL ts
    where
      parensProd p@(TySum  _) = Ppr.parens $ ppr p
      parensProd p@(TyProd _) = Ppr.parens $ ppr p
      parensProd p@ TyArr{}   = Ppr.parens $ ppr p
      parensProd p            = ppr p
  ppr (TyApp  ts   ) = hsep $ map parensApp ts
    where
      parensApp p@(TySum  _) = Ppr.parens $ ppr p
      parensApp p@(TyProd _) = Ppr.parens $ ppr p
      parensApp p@ TyArr{}   = Ppr.parens $ ppr p
      parensApp p@(TyApp _)  = Ppr.parens $ ppr p
      parensApp p            = ppr p
  ppr (TyArr  t1 t2) = parensArr t1 <+> arrow <+> ppr t2
    where
      parensArr p@(TyArr _ _) = Ppr.parens $ ppr p
      parensArr p             = ppr p
  ppr (TyAnn t1 s) = ppr t1 <+> Ppr.text "~" <+> ppr s


pType :: Parser State EType
pType = indent $
       try (liftM2 TyAnn pArr (reservedOp "~" *> pStruct))
   <|> pArr

pArr :: Parser State EType
pArr = indent $ fmap (foldr1 TyArr) (sepBy1 sumType $ reservedOp "->")

sumType :: Parser State EType
sumType = indent $ fmap (mkTy $ TySum . closedL)
                        (sepBy1 prodType $ reservedOp "+")

prodType :: Parser State EType
prodType = indent $ fmap (mkTy $ TyProd . closedL)
                         (sepBy1 appType $ reservedOp "*")

appType :: Parser State EType
appType = indent $ fmap (mkTy tyApp) (many1 aType)

aType :: Parser State EType
aType = indent $
      try (parens pType)
  <|> try (symbol "1" *> return TyUnit)
  <|>      fmap TyVar capitalIdent

fvs :: EType -> [Name]
fvs (TyVar t)          = undefined
fvs (TyMeta t)         = undefined
fvs TyUnit             = undefined
fvs (TySum t)          = undefined
fvs (TyProd t)         = undefined
fvs (TyApp t)          = undefined
fvs (TyArr t1 t2)      = undefined
fvs (TyAnn t1 t2)      = undefined

-- Structures

-- TODO: Refactor with Prim
data Struct a
  = HYLO  (Struct a) (Struct a)
  | COMP  [Struct a]
  | SPLIT [Struct a]
  | JOIN  [Struct a]

  | BMAP  (Struct a) (Struct a)
  | IN
  | OUT
  | INJ Int
  | PROJ Int
  | ID

  | PAR   (Skel a)
  | ATOM a

instance Ppr a => Ppr (Struct a) where
  ppr (HYLO s1 s2)    = Ppr.hsep [Ppr.text "HYLO", pprPar s1, pprPar s2]
    where
      pprPar e@HYLO{}  = Ppr.parens $ ppr e
      pprPar e@COMP{}  = Ppr.parens $ ppr e
      pprPar e@SPLIT{} = Ppr.parens $ ppr e
      pprPar e@JOIN{}  = Ppr.parens $ ppr e
      pprPar e         = ppr e
  ppr (COMP s)         = Ppr.hsep $ Ppr.punctuate (Ppr.text " .") $ map pprPar s
    where
      pprPar e@COMP{} = Ppr.parens $ ppr e
      pprPar e@SPLIT{} = Ppr.parens $ ppr e
      pprPar e@JOIN {} = Ppr.parens $ ppr e
      pprPar e        = ppr e
  ppr (SPLIT s)       = Ppr.hsep $ Ppr.punctuate (Ppr.text " *") $ map pprPar s
    where
      pprPar e@COMP{} = Ppr.parens $ ppr e
      pprPar e@SPLIT{} = Ppr.parens $ ppr e
      pprPar e@JOIN {} = Ppr.parens $ ppr e
      pprPar e        = ppr e
  ppr (JOIN s)        = Ppr.hsep $ Ppr.punctuate (Ppr.text " +") $ map pprPar s
    where
      pprPar e@COMP{} = Ppr.parens $ ppr e
      pprPar e@SPLIT{} = Ppr.parens $ ppr e
      pprPar e@JOIN {} = Ppr.parens $ ppr e
      pprPar e        = ppr e

  ppr (BMAP  m n) = Ppr.hsep [Ppr.text "FMAP", pprPar m, pprPar n]
    where
      pprPar e@HYLO{}  = Ppr.parens $ ppr e
      pprPar e@COMP{}  = Ppr.parens $ ppr e
      pprPar e@SPLIT{} = Ppr.parens $ ppr e
      pprPar e@JOIN{}  = Ppr.parens $ ppr e
      pprPar e         = ppr e
  ppr  IN      = Ppr.text "IN"
  ppr  (INJ i) = Ppr.hcat [Ppr.text "INJ_", Ppr.int i]
  ppr  (PROJ i) = Ppr.hcat [Ppr.text "PROJ_", Ppr.int i]
  ppr  OUT     = Ppr.text "OUT"
  ppr  ID      = Ppr.text "ID"

  ppr (PAR p1)        = ppr p1
  ppr (ATOM a)        = Ppr.braces $ ppr a

pStruct :: Parser State (Struct Int)
pStruct = indent $
      reserved "DC"  *> liftM3 mkDC natural aStruct aStruct
  <|> aStruct
  where
    mkDC (fromIntegral -> i) s1 s2 = PAR $ SKCOMP [SOURCE i Nothing s1, REDUCE i Nothing s2]

aStruct :: Parser State (Struct Int)
aStruct = indent $
  reservedOp "_" *> pstate (fresh $ ATOM . fromIntegral)

data Skel a
  = SOURCE Int (Maybe Int) (Struct a)
  | REDUCE Int (Maybe Int) (Struct a)
  | STREAM (Alg a)
  | SKCOMP [Skel a]

instance Ppr a => Ppr (Skel a) where
  ppr (SOURCE i Nothing s)
    = Ppr.text "GEN<" <> Ppr.int i <> Ppr.comma <> ppr s <> Ppr.text ">"
  ppr (REDUCE i Nothing s)
    = Ppr.text "RED<" <> Ppr.int i <> Ppr.comma <> ppr s <> Ppr.text ">"
  ppr (SOURCE i (Just b) s)
    = Ppr.text "GEN<" <> Ppr.int i <> Ppr.comma <> Ppr.int b <> Ppr.comma <> ppr s <> Ppr.text ">"
  ppr (REDUCE i (Just b) s)
    = Ppr.text "RED<" <> Ppr.int i <> Ppr.comma <> Ppr.int b <> Ppr.comma <> ppr s <> Ppr.text ">"
  ppr (STREAM     p) = Ppr.text "PAR<" <+> ppr p <+> Ppr.text ">"
  ppr (SKCOMP     p) = Ppr.hsep $ Ppr.punctuate (Ppr.text "||") $ map ppr p

data Alg a
  = FARM Int (Alg a)
  | PIPE [Alg a]
  | FUN (Struct a)

instance Ppr a => Ppr (Alg a) where
  ppr (FARM n s) = Ppr.text "FARM" <+> Ppr.int n <+> pprPar s
    where
      pprPar p@(FUN HYLO{}) = Ppr.parens $ ppr p
      pprPar p@(FUN COMP{}) = Ppr.parens $ ppr p
      pprPar p@(FUN SPLIT{}) = Ppr.parens $ ppr p
      pprPar p@(FUN JOIN{}) = Ppr.parens $ ppr p
      pprPar p              = ppr p
  ppr (PIPE s)   = Ppr.hsep $ Ppr.punctuate (Ppr.text "||") $ map pprPar s
    where
      pprPar p@(FUN COMP{}) = Ppr.parens $ ppr p
      pprPar p@(FUN SPLIT{}) = Ppr.parens $ ppr p
      pprPar p@(FUN JOIN{}) = Ppr.parens $ ppr p
      pprPar p              = ppr p
  ppr (FUN s)    = ppr s

flatten :: [Struct a] -> [Struct a]
flatten (COMP xs : ys) = flatten $ xs ++ ys
flatten (x : xs)       = x : flatten xs
flatten []             = []

