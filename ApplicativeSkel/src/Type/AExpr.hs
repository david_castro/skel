{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns #-}
module Type.AExpr
  ( AExpr (..)
  , aconst
  , local
  , global
  , app
  , comp
  , aplus

  , splitfn
  ) where

import Control.Arrow ( first, second, (***) )
import Data.Foldable ( toList )
import Data.List     ( foldl', transpose, stripPrefix )
import Data.Maybe    ( mapMaybe )
import Data.Sequence ( Seq, ViewL(..), ViewR(..)
                     , (<|) , (|>), viewl, viewr)
import Text.PrettyPrint ( (<>), (<+>) )
import qualified Data.Sequence as Seq
import qualified Text.PrettyPrint as Ppr

import Common

-- depth of the context
type Depth = Int


data AExpr a =
    AAtom a               -- ^ Atomic expression (specified by the programmer)
  | AMeta Int             -- ^ Metavariable
  | AVar  Name            -- ^ Global variable
  | APrim Prim            -- ^ Products and sums

  -- Combinators
  | AP  Depth (AExpr a) [AExpr a] -- ^ (AP n f e1 ... ek) x1 .. xn = f (e1 x1 ... xn) (ek x1 ... xn)
  | AS  Depth [AExpr a]     -- ^ application @1 @2 ...
  | AK  Depth               -- ^ K^0   =
  | AB  [AExpr a]           -- ^ B combinator
  | AI                      -- ^ identity
  deriving (Eq, Ord, Functor, Foldable)

instance Ppr a => Ppr (AExpr a) where
  ppr (AAtom a)   = ppr a
  ppr (APrim p)   = ppr p
  ppr (AMeta i)   = Ppr.text "?" <> Ppr.int i
  ppr (AVar n )   = ppr n
  ppr (AP d f es) = pprPar angles f <> Ppr.char '^' <> Ppr.int d <+> Ppr.hsep (map (pprPar Ppr.parens) es)
    where
      angles dc       = Ppr.text "<" <> dc <> Ppr.text ">"
      pprPar t e@AP{} = t (ppr e)
      pprPar t e@AS{} = t (ppr e)
      pprPar t e@AB{} = t (ppr e)
      pprPar t e      = ppr e
  ppr (AS d es)
    | d > 0     = Ppr.text "@^" <> Ppr.int d <+> Ppr.hsep (map pprPar es)
    | otherwise = Ppr.hsep (map pprPar es)

    where
      pprPar e@AP{} = Ppr.parens (ppr e)
      pprPar e@AS{} = Ppr.parens (ppr e)
      pprPar e@AB{} = Ppr.parens (ppr e)
      pprPar e      = ppr e
  ppr (AK d)      = Ppr.text "K^" <> Ppr.int d
  ppr AI          = Ppr.text "I"
  ppr (AB es)     = Ppr.hsep (Ppr.text " ." `Ppr.punctuate` map ppr es)

aK :: Depth -> AExpr a
aK n
  | n <= 0    = AI
  | otherwise = AK n

infixl 4 @@
(@@) :: AExpr a -> AExpr a -> AExpr a
AI             @@ n  = n
AS 0 [AK n, e] @@ _  = aK (n-1)        @@ e
AS n e1        @@ e2 = AS (n-1) $ map (@@ e2) e1

AP 0 f e1      @@ e2 = AS 0 (f:e1)       @@ e2
AP n f e1      @@ e2 = AP (n-1) f $ map (@@ e2) e1

AB es          @@ e  = foldr (@@) e es

AS 0 xs        @@ n = AS 0 (xs ++ [n])
m              @@ n = AS 0 [m,n]

aconst :: Depth -> AExpr a -> AExpr a
aconst d e = aK d @@ e

app  :: Depth -> AExpr a -> AExpr a -> AExpr a
app 0 e1                e2                = e1 @@ e2
-- float consts up
app n (AS 0 [AK i, e1]) (AS 0 [AK j, e2]) = aconst m $ app (n-m) (aconst (i-m) e1) (aconst (j-m) e2)
  where
    m = minimum [n,i,j]
-- turn to compositions
app 1 (AS 0 [AK 1, f])  g                 = f `comp` g

-- identify identities
app 1 (AP 1 (APrim (Split n)) e1) e2 | projTo n (e1 ++ [e2]) = AI

app 1 (AB [APrim (Split 2), APrim (Proj [0])]) (APrim (Proj [1])) = AI

-- leave at least one AP (creates structure!)
app 1 (AB (e1:es1))  e2 = AP 1 e1 [compose es1, e2]

-- application
app n (AS 0 [AK m, e1]) e
  | n == m           = AP n e1 [e]
app n (AP m e1 e2)      e3
  | n == m           = AP n e1 $ e2 ++ [e3]
app n (AS m e1)         e2
  | n == m           = AS n $ e1 ++ [e2]
app n e1 e2          = AS n [e1,e2]


comp :: AExpr a -> AExpr a -> AExpr a
comp AI       e            = e
comp e        AI           = e
-- comp e        (AP 1 f [g])  = AP 1 (e `comp` f) [g]
comp (AB es1) (AB es2) = AB $ es1 ++ es2
comp (AB es1) es2      = AB $ es1 ++ [es2]
comp es1      (AB es2) = AB $ es1 :  es2
comp es1      es2      = AB [es1, es2]

global :: Depth -> a -> AExpr a
global n e = aK n @@ AAtom e

local :: Depth -> Depth -> [Int] -> AExpr a
local n m p = aK n @@ aK m `comp` aproj p

aproj :: [Int] -> AExpr a
aproj []  = AI
aproj is = APrim $ Proj is

ajoin :: [AExpr a] -> AExpr a
ajoin = AS 0 . (APrim Join :)

asplit :: AExpr a -> [AExpr a] -> AExpr a
asplit out es = AB [AS 0 (APrim Join : es), out]

aplus :: [AExpr a] -> AExpr a
aplus es = AP 0 (APrim Join) $ zipWith (comp . APrim . Inj) [1..] es

incIdx :: [Int] -> [Int]
incIdx []     = []
incIdx (i:is) = i + 1 : is

count :: (a -> Bool) -> AExpr a -> Int
count p = sum . fmap (btoint . p)
  where btoint False = 0
        btoint True  = 1

simplIds :: AExpr a -> AExpr a
simplIds (AP 1 (APrim (Split n)) es)
  | allProj es [0..(n-1)] = AI
  where
    allProj [] [] = True
    allProj (APrim (Proj [i]):xs) (j:ns)
      | i == j = allProj xs ns
    allProj _ _ = False
simplIds e = e

compose :: [AExpr a] -> AExpr a
compose = foldl' comp AI . flattenTuples . simplComp . fc

-- TESTS
-- struct1 = let s = (foldl (app 1) (aconst 1 $ APrim $ Split 2) $ [APrim $ Proj
-- [0], APrim $ Proj [1]])::AExpr Int in showPpr s



-- flatten compositions
fc :: [AExpr a] -> [AExpr a]
fc (AB e1 : e2) = fc $ e1 ++ e2
fc (e1    : e2) = e1 : fc e2
fc []           = []

unComp :: AExpr a -> [AExpr a]
unComp (AB xs) = fc xs
unComp x       = [x]

align :: [AExpr a] -> [[AExpr a]]
align []                    = []
align (   f  : (align -> [])) = [unComp f]
align ( (unComp -> r1)  : (align -> m@(r2:_)) ) = paddingR r1 : map paddingM m
  where
    n1 = length r1
    n2 = length r2
    paddingR = (replicate (n2 - n1) AI ++)
    paddingM = (replicate (n1 - n2) AI ++)

projTo :: Int -> [AExpr a] -> Bool
projTo n xs = go n $ reverse xs
  where
    go i (APrim (Proj [j]) : r)
      | i' == j    = go i' r
      | otherwise = False
      where i' = i - 1
    go 0 [] = True
    go _ _  = False

allProj :: AExpr a -> [AExpr a] -> Bool
allProj (APrim (Split _)) = const True
allProj _ = go 0
  where
    go i (APrim (Proj [j]) : r)
      | i == j    = go (i+1) r
      | otherwise = False
      where i' = i + 1
    go _ []  = True
    go _ _   = False

splitM :: [[a]] -> ([[a]],[a])
splitM [] = ([],[])
splitM m  = (init m, last m)

mapProduct :: Int -> [AExpr a] -> AExpr a
mapProduct n = mkSplit n . zipWith mapProj [0..(n-1)]
  where mapProj i f = f `comp` nproj i

splitfn :: (a -> Bool) -> AExpr a -> (AExpr a, AExpr a)
splitfn f (normaliseA -> es) = ( optComp $ combineF f es
                               , optComp $ splitF f es)

combineF :: (a -> Bool) -> [AExpr a] -> [AExpr a]
combineF f l@(e:es)
  | not (any f e) = e : combineF f es
  | any f e       = map (filterComb f) $ takeWhile (any f) l
  | otherwise     = []
combineF f []     = []

filterComb :: (a -> Bool) -> AExpr a -> AExpr a
filterComb f (AP 1 s@(APrim Split{}) fs)
  = foldl' (app 1) (aconst 1 s) $ zipWith (\i e -> filterComb f e `comp`  APrim (Proj [i])) [0..] fs
filterComb f (AB fs)
  | any (any f) fs                       = compose $ takeWhile (not . any f) fs
filterComb f e                           = AI

splitF :: (a -> Bool) -> [AExpr a] -> [AExpr a]
splitF f l@(e:es)
  | not (any f e) = splitF f es
  | otherwise     = splitF_ f (optComp $ takeWhile (any f) l) : dropWhile (any f) l
splitF f []       = []

-- return also "recursive structure"
splitF_ :: (a -> Bool) -> AExpr a -> AExpr a
splitF_ f (AP 1 s@(APrim Split{}) fs) = foldl' (app 1) (aconst 1 s) $ map (splitF_ f) fs
splitF_ f (AB fs)
  | any (any f) fs                    = compose $ dropWhile (any f) $ dropWhile (not . any f) fs
splitF_ f e                           = e

normaliseA :: AExpr a -> [AExpr a]
normaliseA = splitCombine . iter 1000 flattenAExpr -- FIXME: TOTAL HACK

iter :: Int -> (a -> a) -> a -> a
iter 0 _ x = x
iter n f x = iter (n-1) f (f x)

mkSplit :: Int -> [AExpr a] -> AExpr a
mkSplit n fs
  | projTo n fs = AI
  | otherwise   = AP 1 (APrim $ Split n) fs

nsplit :: Int -> AExpr a
nsplit = APrim . Split

nproj :: Int -> AExpr a
nproj = APrim . Proj . (:[])

project :: Int -> Int -> [AExpr a]
project i j = map nproj [i..j]

appn :: Int -> AExpr a -> AExpr a
appn n e = foldl' (app 1) (aconst 1 e) $ project 0 n

splitCombine :: AExpr a -> [AExpr a]
splitCombine (AB es) = flattenTuples $ simplComp $ fc $ map (optComp . splitCombine) es
splitCombine a
  | isCombine a || isSplit a = [a]
  | otherwise                = flattenTuples [combineTuple a, splitTuple a]

isCombine :: AExpr a -> Bool
isCombine (AP 1 _ fs)    = all isCombine fs
isCombine (AS 1   fs)    = all isCombine fs
isCombine (APrim Proj{}) = True
isCombine _              = False

isSplit :: AExpr a -> Bool
isSplit (AP 1 (APrim Split{}) fs) = all isSplit fs
isSplit (AS 1   fs)               = all isSplit fs
isSplit AP{}                      = False
isSplit AS{}                      = False
isSplit _                         = True

isPureSplit :: AExpr a -> Bool
isPureSplit (AP 1 (APrim Split{}) fs) = all isPureSplit fs
isPureSplit (AS 1 fs)                 = all isPureSplit fs
isPureSplit (APrim Proj{})            = True
isPureSplit (AB l@(_:_))              = isPureSplit $ last l
isPureSplit _                         = False

-- assumes there's just "depth" one in all expressions (i.e. flattened -- expression)
splitTuple :: AExpr a -> AExpr a
splitTuple (AP 1 f fs) = AP 1 (nsplit $ length fs) (map splitTuple fs)
-- splitTuple (AS 1   fs) = foldl1 (app 1) (map splitTuple fs)
splitTuple e           = e

combineTuple :: AExpr a -> AExpr a
combineTuple = comb []
  where
    comb idx (AP 1 f fs) = AP 1 f (zipWith comb (map (:idx) [0..]) fs)
    -- comb idx (AS 1   fs) = foldl1 (app 1) (map (comb idx) fs)
    comb idx e           = aproj $ reverse idx
-- flattenAExpr (AP 1 f fs)
--   | not (projTo n fs) && notSplit f = compose $ AP 1 f (project 0 $ n-1) : [flattenAExpr $ mkSplit n fs]
--   where
--     n           = length fs
--     notSplit (APrim Split {}) = False
--     notSplit _                = True
-- flattenAExpr (AP 1 (APrim (Split n)) fs)
--   | projTo n fs = AI

-- Flattens @^k and <f>^k where k > 1 to k' = 1
flattenAExpr :: AExpr a -> AExpr a
-- Rule 1) @1 (K^n f) A1 ... An-1 An An+1 ... Am === <f>^1 An An+1 ...
flattenAExpr (AS 1 (e@(AS 0 [AK k, m]) : xs))
  | k <= length xs = foldl' (app 1) (aconst 1 m) (drop (k-1) xs)
flattenAExpr (AP 1 (AS 0 [AK k, f]) xs)
  | k <= length xs = foldl' (app 1) (aconst 1 f) (drop k xs)
flattenAExpr (AP 1 (AK k) (x:xs))
  | k == length xs = x
-- Rule 2) b < k ===>
--        @^1 (@^k A1 ... Aa) B1 ... Bb  === @^k-b+1 (<A1>^1 proj_0 ... proj_b)
--                                                   ...
--                                                   (<Aa>^1 proj_0 ... proj_b)
--                                           . (&b+1)^1 I B1 ... Bb
flattenAExpr (AS 1 (AS k as : bs))
  | b < k && k > 0
  = compose [ foldl1 (app k') $ map (appn b) as
            , foldl' (app 1 ) (aconst 1 $ nsplit $ b+1) $ AI : bs
            ]
  where
    b       = length bs
    k'      = k - b
-- Rule 3) b >= k ===>
--        @^1 (@^k A1 ... Aa) B1 ... Bk-1 Bk ... Bb
--             ===
--        @^1 (<A1>^1 proj_0 ... proj_k-1)
--            ...
--            (<Aa>^1 proj_0 ... proj_k-1) proj_k ... proj_b
--        . (&b+1)^1 I B1 ... Bb
flattenAExpr (AS 1 (AS k as : bs))
  | b >= k && k > 0
  = compose [ foldl1 (app 1) $ map (appn (k-1)) as ++ project k b
            , foldl' (app 1) (aconst 1 $ nsplit (b+1)) $ AI : bs
            ]
  where
    b       = length bs
-- Rule 4) b < k ===>
--        @^1 (<f>^k A1 ... Aa) B1 ... Bb
--             ===
--        <f>^k-b (<A1>^1 proj_0 ... proj_b)
--                ...
--                (<Aa>^1 proj_0 ... proj_b)
--        . (&b+1)^1 I B1 ... Bb
flattenAExpr (AS 1 (AP k f as : bs))
  | b < k && k > 0
  = compose [ foldl' (app k') (aconst k' f) $ map (appn b) as
            , foldl' (app 1)  (aconst 1 $ nsplit (b+1)) $ AI : bs
            ]
  where
    b       = length bs
    k'      = k-b
-- Rule 5) b >= k ===>
--        @^1 (<f>^k A1 ... Aa) B1 ... Bk-1 Bk ... Bb
--             ===
--        <f>^1 (<A1>^1 proj_0 ... proj_k-1)
--              ...
--              (<Aa>^1 proj_0 ... proj_k-1) prok_k ... proj_b
--        . (&b+1)^1 I B1 ... Bb
flattenAExpr (AS 1 (AP k f as : bs))
  | b >= k && k > 0
  = compose [ foldl' (app 1) (aconst 1 f) $ map (appn $ k - 1) as ++ project k b
            , foldl' (app 1)  (aconst 1 $ nsplit (b+1)) $ AI : bs
            ]
  where
    b       = length bs
-- Rule 6) b < k ===>
--        <<f>^k A1 ... Aa>^1 B1 ... Bb
--             ===
--        <f>^k-b+1 (<A1>^1 proj_0 ... proj_b-1)
--                  ...
--                  (<Aa>^1 proj_0 ... proj_b-1)
--        . (&b)^1 B1 ... Bb
flattenAExpr (AP 1 (AP k f as) bs)
  | b < k && k > 0
  = compose [ foldl' (app k') (aconst k' f) $ map (appn (b-1)) as
            , foldl' (app 1)  (aconst 1 $ nsplit b) bs
            ]
  where
    b       = length bs
    k'      = k - b
-- Rule 7) b >= k ===>
--        <<f>^k A1 ... Aa>^1 B1 ... Bk Bk+1 ... Bb
--             ===
--        <f>^1 (<A1>^1 proj_0 ... proj_k-1)
--              ...
--              (<Aa>^1 proj_0 ... proj_k-1)
--              proj_k ... proj_b-1
--        . (&b)^1 B1 ... Bb
flattenAExpr (AP 1 (AP k f as) bs)
  | b >= k && k > 0
  = compose [ foldl' (app 1) (aconst 1 f) $ map (appn $ k-1) as ++ project k (b-1)
            , foldl' (app 1) (aconst 1 $ nsplit b) bs
            ]
  where
    b       = length bs
-- Rule 8) b < k ===>
--        <@^k A1 ... Aa>^1 B1 ... Bb
--             ===
flattenAExpr (AP 1 (AS k as) bs)
  | b < k && k > 0
  = compose [ foldl1 (app k') $ map (appn (b-1)) as
            , foldl' (app 1)  (aconst 1 $ nsplit b) bs
            ]
  where
    b       = length bs
    k'      = k - b
-- Rule 9) b >= k ===>
--        <@^k A1 ... Aa>^1 B1 ... Bk Bk+1 ... Bb
--             ===
flattenAExpr (AP 1 (AS k as) bs)
  | b >= k && k > 0
  = compose [ foldl1 (app 1) $ map (appn $ k-1) as ++ project k (b-1)
            , foldl' (app 1) (aconst 1 $ nsplit b) bs
            ]
  where
    b       = length bs

-- Rule 10) <K^n . f>^1 A A1 ... An === f . A
flattenAExpr (AP 1 (AB [AK n, f]) (a:as))
  | length as == n = compose [f,a]


-- Extract compositions
flattenAExpr (AP 1 (AB (f:g)) (e:es))
   = AP 1 f $ AP 1 (compose g) [e] : es
flattenAExpr (AP 1 f [g])
   = compose $ simplComp [f,g]

-- Recursive
flattenAExpr (AP 1 f as) = AP 1 (flattenAExpr f) (map flattenAExpr as)
flattenAExpr (AS 1 as) = AS 1 (map flattenAExpr as)
flattenAExpr (AB bs)
  = compose $ map flattenAExpr bs
flattenAExpr e                        = e

optComp :: [AExpr a] -> AExpr a
optComp = compose . optComp_

optComp_ :: [AExpr a] -> [AExpr a]
optComp_ (AP 1 f fs : AP 1 (APrim Split{}) gs : rest)
  | Just hs <- mergeTuples fs gs  = optComp_ $ AP 1 f hs : rest
optComp_ (x : xs) = x : optComp_ xs
optComp_ []       = []

simplComp :: [AExpr a] -> [AExpr a]
-- merge "pure" splits, i.e. "splits" that apply functions to "projections" on
-- previous tuple
simplComp (a@(AP 1 f fs) : b@(AP 1 _ gs) : rest)
  | isPureSplit a, isPureSplit b, Just hs <- mergeTuples fs gs
    = simplComp $ AP 1 f hs : rest
simplComp (h@(AP 1 (APrim (Split n)) fs):t@(AP 1 (APrim (Split m)) gs : rest))
  | n == m, Just hs <- mergeBranches 0 fs gs = simplComp $ AP 1 (APrim (Split n)) hs : rest
 --merges f^1 pi0 pi1 . &2 h i  ===> f^1 h i
simplComp (h@(AP 1 f fs):t@(AP 1 (APrim (Split m)) gs : rest))
  | not (isSplit h), Just hs <- mergeTuples fs gs = simplComp $ AP 1 f hs : rest
simplComp ( APrim (Proj (i:j)) : AP 1 (APrim (Split m)) gs : rest)
  | i < m = simplComp $ aproj j : gs !! i : rest
simplComp (APrim (Proj n) : APrim (Proj m) : rest)
  = APrim (Proj $ m ++ n) : rest
simplComp (x : xs) = x : simplComp xs
simplComp []       = []

mergeTuples :: [AExpr a] -> [AExpr a] -> Maybe [AExpr a]
mergeTuples [] _ = Just []
mergeTuples ((lastExpr -> (fs, APrim (Proj (j:js))))          : gs) hs
  | length hs > j = ((compose $ fs ++ [aproj js, hs!!j]) :) <$> mergeTuples gs hs
mergeTuples ((lastExpr -> (fs, AP 1 f@(APrim Split{}) fs1)) : gs) hs
  | Just fs2 <- mergeTuples fs1 hs = ((compose $ fs ++ [AP 1 f fs1]) :) <$> mergeTuples gs hs
mergeTuples (_:_) _ = Nothing

safeLast :: [a] -> Maybe a
safeLast [] = Nothing
safeLast l  = Just $ last l

lastExpr :: AExpr a -> ([AExpr a], AExpr a)
lastExpr (AB l@(_:_)) = (init l, last l)
lastExpr e            = ([]    , e)

flattenTuples :: [AExpr a] -> [AExpr a]
flattenTuples = reverse . doFlatten . reverse

doFlatten :: [AExpr a] -> [AExpr a]
doFlatten []  = []
doFlatten [e] = [e]
doFlatten (e1:es@(e2:t))
  | (_, []) <- flattenTuple e1 = e1 : doFlatten es
  | (e, m ) <- flattenTuple e1 = e  : doFlatten (reMap m e2 : t)

flattenTuple :: AExpr a -> (AExpr a, [([Int], Int)])
flattenTuple e@(AP 1 (APrim Split{}) _)
  | (l  , idx) <- flattenTuple_ e = (AP 1 (APrim (Split (length l))) l, zip idx [0..])
flattenTuple e                    = (e, [])

flattenTuple_ :: AExpr a -> ([AExpr a], [[Int]])
flattenTuple_ (AP 1 (APrim Split{}) fs) = (concat *** getIdx) $ unzip $ map flattenTuple_ fs
  where
    getIdx :: [[[Int]]] -> [[Int]]
    getIdx = concat . zipWith (\i l -> map (i:) l) [0..]
flattenTuple_ e                         = ([e], [[]])

reMap :: [([Int], Int)] -> AExpr a -> AExpr a
reMap m (APrim (Proj idx))
  | Just (ni,idx') <- matchIdx m idx = compose [aproj idx', aproj [ni]]
reMap m (AP x1 x2 x3)      = AP x1 (reMap m x2) $ map (reMap m) x3
reMap m (AS x1 x2)         = AS x1              $ map (reMap m) x2
reMap m (AB x)             = foldl1 comp        $ map (reMap m) x
reMap m x                  = x

matchIdx :: [([Int], Int)] -> [Int] -> Maybe (Int, [Int])
matchIdx [] _ = Nothing
matchIdx ((id1, ni):r) id2
  | Just id1' <- stripPrefix id2 id1 = Just (ni, id1')
  | otherwise                        = matchIdx r id2

mergeBranches :: Int -> [AExpr a] -> [AExpr a] -> Maybe [AExpr a]
mergeBranches i [] [] = Just []
mergeBranches i [] _  = Nothing
mergeBranches i _  [] = Nothing
mergeBranches i (APrim (Proj [j]) : l1) (c@(APrim (Proj [k])) : l2)
  | i == j && j == k = (c :) <$> mergeBranches (i+1) l1 l2
mergeBranches i (APrim (Proj [j]) : l1)
                (c@(AB (safeLast -> Just (APrim (Proj [k])))) : l2)
  | i == j && j == k = (c :) <$> mergeBranches (i+1) l1 l2
mergeBranches i (c@(AB (safeLast -> Just (APrim (Proj [k])))) : l2)
                (APrim (Proj [j]) : l1)
  | i == j && j == k = (c :) <$> mergeBranches (i+1) l1 l2
mergeBranches _ (_ : _) (_ : _) = Nothing

