module Type.Kind
  ( Kind (..)
  , pKind
  , aKind
  ) where

import Text.PrettyPrint ( (<>) )
import qualified Text.PrettyPrint as Ppr
import Text.Parsec
  ( try, many1 )

import Common


data Kind =
    KType          -- ^ Kind for types
  | KMeta Integer  -- ^ Meta-kind for kind unification
  | KArr Kind Kind -- ^ * -> *, * -> * -> ... -> *

instance Ppr Kind where
  ppr KType        = Ppr.text "*"
  ppr (KMeta i)    = Ppr.text "@" <> Ppr.integer i
  ppr (KArr k1 k2) = pprPar k1 <> arrow <> pprPar k2
    where
      pprPar k@KArr {} = Ppr.parens $ ppr k
      pprPar k         = ppr k

pKind :: Parser State Kind
pKind = indent $ try $ fmap (foldr1 KArr) (many1 aKind)

aKind :: Parser State Kind
aKind = indent $ try $ reservedOp "*" *> return KType

instance Show Kind where
  show = showPpr
