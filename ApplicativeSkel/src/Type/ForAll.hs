module Type.ForAll
  ( ForAll (..)
  , pForAll
  ) where

import Data.Sequence      ( Seq )
import Text.PrettyPrint   ( hsep, text )
import Text.Parsec        ( try, many1, (<|>) )
import Data.Foldable      ( toList )

import qualified Data.Sequence        as Seq
import qualified Text.PrettyPrint     as Ppr

import Common

import Type.Type

data ForAll =
  ForAll (Seq Name) EType

instance Ppr ForAll where
  ppr (ForAll ns e)
    | Seq.null ns = ppr e
    | otherwise
      = hsep $
          text "forall" :
            (map ppr (toList ns) ++ [Ppr.comma, ppr e])

instance Show ForAll where
  show = showPpr

pForAll :: Parser State ForAll
pForAll = indent $ do
  ns <- try (reserved "forall" *> many1 capitalIdent <* comma) <|> return []
  ty <- pType
  return (ForAll (Seq.fromList ns) ty)
