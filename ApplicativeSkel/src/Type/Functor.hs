module Type.Functor
  ( f
  ) where


import Common
import Type.Type
import Type.ForAll

fConst :: EType -> ForAll
fConst t =
