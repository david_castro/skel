{-# LANGUAGE TupleSections #-}
module TcSkelSyn.TcExpr
  ( tcExpr
  , inferExpr
  , inPattern
  , tcAlts
  , tcAlt
  ) where

import Control.Arrow ( first )
import Control.Monad ( void, liftM2, replicateM )
import Data.Sequence ( Seq, (><) )
import qualified Data.Sequence as Seq

import Common
import TcM
import Type

import SkelSyn.Expr
import SkelSyn.Pattern

import TcSkelSyn.TcLiteral
import TcSkelSyn.TcPrim
import TcSkelSyn.Unify

-- | Typecheck expression
-- warnStub "tcExpr" >> return e
tcExpr :: Expr -> EType -> TcM (AExpr Expr)
tcExpr e t = do
  (t', e') <- inferExpr e
  unify t' t
  void (applySubst t')
  return e'

-- | Infer expression type and structure
inferExpr :: Expr -> TcM (EType, AExpr Expr)
inferExpr r@(ELit e)      = liftM2 (,) (inferLit e)  (constExpr r)
inferExpr r@(EPrim e)     = liftM2 (,) (inferPrim e) (constAExpr $ APrim e)
inferExpr r@(EVar e)      = lookupVar e
inferExpr r@(EAbs e1 e2)
  = do ts <- replicateM (length e1 + 1) freshMeta
       let arr = foldr1 TyArr ts
       inPattern e1 arr $ \rty ->
        do s1 <- tcExpr e2 rty
           arr' <- applySubst arr
           return (arr', s1)
inferExpr r@(EApp e1 e2)
  = do m1@(TyArr m2 m3) <- liftM2 TyArr freshMeta freshMeta
       s1 <- tcExpr e1 m1
       s2 <- tcExpr e2 m2

       n   <- ctxSize Gamma
       m3' <- applySubst m3
       return (m3', app n s1 s2)
inferExpr r@(ECase e1 e2) -- FIXME
  = do (t1, s1)  <- inferExpr e1
       ty        <- freshMeta
       (out, cs) <- tcAlts (map (first (:[])) e2) (TyArr t1 ty)
       outS      <- constExpr out
       n         <- ctxSize Gamma
       return (ty, AP (n-1) (APrim Join) (cs ++ [app n outS s1])) -- FIXME: Join Int

constExpr :: Expr -> TcM (AExpr Expr)
constExpr e = (`global` e) <$> ctxSize Gamma

constAExpr :: AExpr Expr -> TcM (AExpr Expr)
constAExpr e = (`aconst` e) <$> ctxSize Gamma


-- | Extends local environment from a pattern. Assumes that constructors with
-- multiple arguments will end-up being equivalent to a pattern on a tuple.
inPattern :: [Pattern] -> EType -> (EType -> TcM a) -> TcM a
inPattern ps ty fn = tcPatterns ps ty >>= \(newG, rty) ->
  inCtx Gamma newG $ fn rty


-- | Create a context using the variables bound in a pattern and a type,
-- returning a context, plus the return type.
--
-- An error is thrown in the "TcM" monad if the type does not match the
-- list of patterns
tcPatterns :: [Pattern] -> EType -> TcM (Seq GammaCtx, EType)
tcPatterns []     t            = return (Seq.empty, t)
tcPatterns (p:ps) (TyArr t ts) = do
  ctx1       <- tcPattern  p  t
  (ctx2, rt) <- tcPatterns ps ts
  return (gProd ctx1 >< ctx2, rt)
tcPatterns p      t            = unexpectedType t "Expected function type"

tcPattern :: Pattern -> EType -> TcM (Seq GammaCtx)
tcPattern (PVar v)       t = return (Seq.singleton $ GElem v t)
tcPattern (P c ps) t = do
  cty       <- fst <$> inferExpr (constrToExpr c)
  (ctx,rty) <- tcPatterns ps cty
  unify rty t
  return ctx

-- TYPECHECKING ALTERNATIVES

-- | Classifies the free variables in the patterns, groups them in tuples
-- of recursive and non-recursive occurences and returns a new function from
-- [Pattern] to Expr (in terms of these tuples), plus a series of lambdas for
-- each case. E.g.
-- > zip [] l = []
-- > zip l [] = []
-- > zip (x:xs) (y:ys) = (x,y):zip xs ys
--
-- > classifyBoundVars zip ===> (curry2 out_P, [expr_1, expr_2, expr_3]
--
-- > out_P x = case x of
-- >              ([], l)          => inj_1 l
-- >              (l, [])          => inj_2 l
-- >              ((x:xs), (y:ys)) => inj_3 ((x,y),(xs,ys))
--
-- > expr_1 = \l -> []
-- > expr_2 = \l -> []
-- > expr_3 = \((x,y),(xs,ys)) -> (x,y):zip xs, ys
--
--   typechecking would continue normally on expr_1, expr_2 and expr_3
--   TODO: check overlapping, unreeachable, missing pattern matches
tcAlts :: [([Pattern], Expr)] -> EType -> TcM (Expr, [AExpr Expr])
tcAlts [] _  = error "Panic! Report bug in \
                         \TcSkelSyn.TcAlts.classifyBoundVars: empty defn."
tcAlts [([], e)] ty -- in case this is just an expression definition, just typecheck and return a normal expr
  = do v <- freshEVar
       (EAbs [PVar v] (EVar v),) . (:[]) <$> tcExpr e ty
tcAlts ps ty = do
  (outfn, es) <- mergeCases . unzip3 <$> mapM (`tcAlt` ty) ps
  v           <- freshEVar
  return (EAbs [PVar v] (ECase (EVar v) outfn), es)
  where
    mergeCases (ps1, ps2, es) = (zipWith3 mkOutFn ps1 ps2 [1..], es)
    mkOutFn p1 p2 i = (toTuple p1, patternToExpr $ P (CSum i) [p2])
    toTuple []  = P CUnit []
    toTuple [p] = p
    toTuple ps  = P (CProd $ length ps) ps

-- | Typechecking  an alternative: FIXME: document this function properly
-- > p -> e
-- Is done in two stages: variables bound by the pattern are clasified into
-- recursive and non-recursive: i.e. used in a recursive call, or not used in a
-- recursive call. Then, the context is reorganised by tupling the non-rec
-- arguments with a single recursive argument, obtained by tupling the rec
-- variables in the original argument. This is all done by 'organiseContext'.
-- Then, the expression is typechecked in this reorganised context. The reason
-- for doing this is that the order in which a variable appears in a context is
-- going to be important for future extensions on the typechecking algorithm.
--
-- FIXME: this implies two traversals, why not just one? Just typecheck in
-- the original pattern, just return substitution for reorganising context.
tcAlt :: ([Pattern], Expr) -> EType -> TcM ([Pattern], Pattern, AExpr Expr)
tcAlt (ps, e) ty =
  do (p, ty') <- productPattern ty ps argty
     e'  <- inPattern [p] (TyArr ty' retty) $ tcExpr e
     return (ps, p, e')
  where
    (argty, retty) = uncurryTy (length ps) ty

productPattern :: EType -> [Pattern] -> [EType] -> TcM (Pattern, EType)
productPattern _origt [] []
  = return (P CUnit [], TyUnit)
productPattern _origt [p] [t]
  =  toProduct p t
productPattern origt ps ty
  = do (ps', ts') <- prods ps ty

       return ( P (CProd $ length ps') ps'
              , TyProd $ closedL ts')
  where
    prods []     _       = return ([], [])
    prods ps     []      = unexpectedType origt "Expected function type"
    prods (p:ps) (t1:t2) = do
      (p' , t' ) <- toProduct p t1
      (ps', ts') <- prods ps t2
      return (p':ps', t':ts')

toProduct :: Pattern -> EType -> TcM (Pattern, EType)
toProduct (P c ps) t
  = do ty <- fst <$> inferExpr (constrToExpr c)
       let (tuplty, retty) = uncurryTy (length ps) ty
       unify retty t
       productPattern ty ps tuplty
toProduct p              t = return (p, t)

