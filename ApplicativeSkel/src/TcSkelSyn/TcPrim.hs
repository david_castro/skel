module TcSkelSyn.TcPrim
  ( tcPrim
  , inferPrim
  ) where

import Control.Monad.Extra ( liftM2, replicateM )

import Common
import TcM
import Type
import Util ( dup )

import TcSkelSyn.Unify

tcPrim :: Prim -> EType -> TcM ()
tcPrim p t = inferPrim p >>= unify t

inferPrim :: Prim -> TcM EType
inferPrim Join          = liftM2 TyArr (TySum . openL [] <$> freshId) freshMeta
inferPrim (Split i)     = arrow <$> replicateM i freshMeta
  where
    arrow vs = foldr TyArr (TyProd $ closedL vs) vs
inferPrim (Proj xs)     = arr <$> liftM2 (,) (mkTuple xs) freshMeta
  where
    mkTuple []     = error "Bug in TcSkelSyn.TcPrim.inferPrim (Proj [])"
    mkTuple [i]    = mkTP i
    mkTuple (i:is) = liftM2 (.) (mkTP i) (mkTuple is)
    mkTP i = mkOL i <$> replicateM (i+1) freshMeta
    mkOL m (TyMeta i:vs) = \t -> TyProd (openL (take m vs ++ [t] ++ drop m vs) i)
    mkOL m _  = error "Bug in TcSkelSyn.TcPrim.inferPrim (Proj [])"

    arr (fprod, t) = TyArr (fprod t) t
inferPrim (Inj p)       = mkSum <$> replicateM (p+1) freshMeta
  where
    mkSum (TyMeta x:xs) = TyArr (xs !! (p - 1)) (TySum $ openL xs x)
    mkSum _  = error "Bug in TcSkelSyn.Prim.inferPrim inj_i"
inferPrim Unit          = return TyUnit
inferPrim In            = error "TcSkelSyn.TcPrim.inferPrim In: FIXME" -- FIXME
inferPrim Out           = error "TcSkelSyn.TcPrim.inferPrim Out: FIXME" -- FIXME
inferPrim Id            = uncurry TyArr . dup <$> freshMeta
