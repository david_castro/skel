module TcSkelSyn.TcType
  ( kcEType
  , kiEType
  , generalise
  , specialise
  ) where

import Control.Arrow ( (***), second )
import Control.Monad ( liftM2, replicateM )
import qualified Data.Map.Strict as Map
import Data.Map.Strict      ( Map )
import qualified Data.Sequence as Seq
import Data.Sequence ( Seq, (><) )
import Data.Foldable ( toList )

import Common
import TcM
import Type

import TcSkelSyn.Unify

-- | Unify two kinds
unifyKind :: Kind -- ^ Expected
          -> Kind -- ^ Inferred
          -> TcM ()
unifyKind (KMeta i1)   (KMeta i2) | i1 == i2 = return ()
unifyKind KType        KType                 = return ()
unifyKind (KArr k1 k2) (KArr k3 k4)          = unifyKind k1 k3 *> unifyKind k2 k4
unifyKind (KMeta i1)   k                     = extendKSubst i1 k
unifyKind k            (KMeta i1)            = extendKSubst i1 k
unifyKind k1           k2                    = unifyKindErr k1 k2

extendKSubst :: Integer -> Kind -> TcM ()
extendKSubst i k = lookupKSubst i k $ unifyKind k

-- Kind-check a type
kcEType :: EType -> Kind -> TcM ()
kcEType ty      k = kiEType ty >>= unifyKind k

kiEType :: EType -> TcM Kind
kiEType (TyVar x)     = lookupCtx Kappa x
kiEType (TyMeta x)    = freshKMeta
kiEType TyUnit        = return KType
kiEType (TySum x)     = mapM_ (`kcEType` KType) x *> return KType
kiEType (TyProd x)    = mapM_ (`kcEType` KType) x *> return KType
kiEType (TyApp x)     =
  case x of
    []     -> error "Panic! Bug in TcSkelSyn/TcType.hs"
    (x:xs) -> do
      k    <- freshKMeta
      ks   <- mapM kiEType xs
      k1   <- kiEType x
      unifyKind k1 (foldr KArr k ks)
      applyKSubst k
kiEType (TyArr x1 x2) = kcEType x1 KType *> kcEType x2 KType *> return KType
kiEType (TyAnn t1 _)  = kcEType t1 KType *> return KType -- FIXME: check "well-formedness of struct"

specialise :: ForAll -> TcM EType
specialise (ForAll vs ty) = aux <$> replicateM (Seq.length vs) freshId
  where
    aux ids = specialise_ (Map.fromList $ zip (toList vs) ids) ty

specialise_ :: Map Name Integer -> EType -> EType
specialise_  m t@(TyVar x)
  | Just i <- Map.lookup x m        = TyMeta i
  | otherwise                        = t
specialise_  m (TySum (OL (xs, t)))  = TySum  $ OL (map (specialise_ m) xs, t)
specialise_  m (TyProd (OL (xs, t))) = TyProd $ OL (map (specialise_ m) xs, t)
specialise_  m (TyApp x)             = TyApp $ map (specialise_ m) x
specialise_  m (TyArr x1 x2)         = TyArr (specialise_ m x1) (specialise_ m x2)
specialise_  m (TyAnn x1 x2)         = TyAnn (specialise_ m x1) x2
specialise_ _m x                     = x

generalise :: EType -> TcM ForAll
generalise x = do
  (vs, t) <- generalise_ x
  popCtx Kappa $ Seq.length vs
  return $ ForAll vs t

generalise_ :: EType -> TcM (Seq Name, EType)
generalise_ (TyVar x)     = return (Seq.empty, TyVar x)
generalise_ t@(TyMeta x)  = do
  t' <- applySubst t
  case t' of
    TyMeta y -> do
      v <- freshTyVar
      extendCtx Kappa (v, KType)
      unify t' (TyVar v)
      return (Seq.singleton v, TyVar v)
    _        ->
      generalise_ t'
generalise_ TyUnit        = return (Seq.empty, TyUnit)
generalise_ (TySum x)     = aux <$> mapM generalise_ (olElems x)
  where aux = (concatS *** TySum . closedL) . unzip
generalise_ (TyProd x)    = aux <$> mapM generalise_ (olElems x)
  where aux = (concatS *** TyProd . closedL) . unzip
generalise_ (TyApp x)     = aux <$> mapM generalise_ x
  where aux = (concatS *** TyApp) . unzip
generalise_ (TyArr x1 x2) = aux <$> mapM generalise_ [x1,x2]
  where aux = (concatS *** foldr1 TyArr) . unzip
generalise_ (TyAnn x1 x2) = second (`TyAnn` x2) <$> generalise_ x1

concatS :: [Seq a] -> Seq a
concatS = foldr (><) Seq.empty
