module TcSkelSyn.TcLiteral
  ( tcLit
  , inferLit
  ) where

import Common
import Type
import TcM

import TcSkelSyn.Unify

tcLit :: Literal -> EType -> TcM ()
tcLit l t = inferLit l >>= unify t

inferLit :: Literal -> TcM EType
inferLit ULit     = return TyUnit
inferLit (ILit _) = return builtInInt
inferLit (BLit _) = return builtInBool
inferLit (SLit _) = return builtInString

