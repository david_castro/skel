{-# LANGUAGE ViewPatterns #-}
module TcSkelSyn.Unify
  ( unify
  , instantiateSkel
  ) where

import Control.Monad ( zipWithM_ )

import qualified Data.Map.Strict as Map
import Data.Map.Strict ( Map )

import Common
import TcM
import Type

data D = L | R

unify :: EType -> EType -> TcM ()
unify (TyMeta i1 )    (TyMeta i2)  | i1 == i2 = return ()
unify (TyVar  v1 )    (TyVar  v2 ) | v1 == v2 = return ()
unify  TyUnit          TyUnit                 = return ()

unify (TyMeta i1 )    t2                      = extendSubst L i1 t2
unify t1              (TyMeta i2 )            = extendSubst R i2 t1
unify o1@(TySum  ts1)  o2@(TySum  ts2)        = unifyOL (USum  o1 o2) ts1 ts2
unify o1@(TyProd ts1)  o2@(TyProd ts2)        = unifyOL (UProd o1 o2) ts1 ts2
unify (TyApp  ts1)    (TyApp  ts2)            = zipWithM_ unify ts1 ts2
unify (TyArr  t1 t3)  (TyArr  t2 t4)          = unify t1 t2 >> unify t3 t4
unify t1              t2                      = unifyError t1 t2

extendSubst :: D -> Integer -> EType -> TcM ()
-- TODO: why is this not the following?
-- extendSubst d i = lookupSubst i <$> unifyDir d
extendSubst d i ty = lookupSubst i ty $ unifyDir d ty
  where
    unifyDir L = flip unify
    unifyDir R = unify

data KL = UProd EType EType | USum EType EType

unifyOL :: KL -> OpenL EType -> OpenL EType -> TcM ()

unifyOL knd (viewOL -> (xs, Nothing)) (viewOL -> (ys, Nothing))
  | length xs /= length ys = errKnd knd
  | otherwise              = zipWithM_ unify xs ys

unifyOL knd (viewOL -> (x:xs, m1)) (viewOL -> (y:ys, m2))
  = unify x y >> unifyOL knd (OL (xs,m1)) (OL (ys,m2))
unifyOL knd (viewOL -> ([], Just i)) l2
  = lookupSubst i (mkKnd knd l2) $ \ty1 ->
      do l1 <- matchKnd knd ty1
         unifyOL knd l1 l2
unifyOL knd l1 (viewOL -> ([], Just i))
  = lookupSubst i (mkKnd knd l1) $ \ty2 ->
      do l2 <- matchKnd knd ty2
         unifyOL knd l1 l2

unifyOL knd _ _ = errKnd knd

matchKnd :: KL -> EType -> TcM (OpenL EType)
matchKnd (UProd _ _)   (TyProd l) = return l
matchKnd (USum  _ _)   (TySum  l) = return l
matchKnd knd           _          = errKnd knd

mkKnd :: KL -> OpenL EType -> EType
mkKnd (UProd _ _) = TyProd
mkKnd (USum  _ _) = TySum

errKnd :: KL -> TcM a
errKnd (UProd k1 k2) = unifyError k1 k2
errKnd (USum  k1 k2) = unifyError k1 k2


replaceAtomsWith :: Struct Int -> Map Int (Struct a) -> Struct a
replaceAtomsWith (ATOM s) m     = m Map.! s
replaceAtomsWith (HYLO s1 s2) m = HYLO (replaceAtomsWith s1 m) (replaceAtomsWith s2 m)
replaceAtomsWith (COMP s) m     = COMP $ map (`replaceAtomsWith` m) s
replaceAtomsWith (SPLIT s) m    = SPLIT $ map (`replaceAtomsWith` m) s

replaceAtomsWith (JOIN s) m     = JOIN $ map (`replaceAtomsWith` m) s
replaceAtomsWith (BMAP s1 s2) m = BMAP (replaceAtomsWith s1 m) (replaceAtomsWith s2 m)
replaceAtomsWith (PAR s) m      = PAR $ replaceSkel s m
replaceAtomsWith IN _m       = IN
replaceAtomsWith OUT _m      = OUT
replaceAtomsWith (INJ e) _m  = INJ e
replaceAtomsWith (PROJ e) _m = PROJ e
replaceAtomsWith ID _m       = ID


replaceSkel :: Skel Int -> Map Int (Struct a) -> Skel a
replaceSkel (SOURCE s1 b s2) m = SOURCE s1 b $ replaceAtomsWith s2 m
replaceSkel (REDUCE s1 b s2) m = REDUCE s1 b $ replaceAtomsWith s2 m
replaceSkel (STREAM s) m     = STREAM $ s `replaceAlg` m
replaceSkel (SKCOMP s) m     = SKCOMP $ map (`replaceSkel` m) s

replaceAlg :: Alg Int -> Map Int (Struct a) -> Alg a
replaceAlg (FARM a1 a2) m = FARM a1 (replaceAlg a2 m)
replaceAlg (PIPE a) m = PIPE $ map (`replaceAlg` m) a
replaceAlg (FUN a) m = FUN $ replaceAtomsWith a m

-- Semantic unification of structures
instantiateSkel :: Ppr a => Struct Int -> Struct a -> TcM [Struct a]
instantiateSkel a (normalise -> b) = do
  mapping <- na `unifiesWith` b
  warnStub $ showPpr na
  warnStub $ showPpr b

  return [a `replaceAtomsWith` mapping]
  where na = normalise a

-- FIXME! TODO!
unifiesWith :: Struct Int -> Struct a -> TcM (Map Int (Struct a))
unifiesWith (COMP [HYLO (ATOM i1) OUT, HYLO IN (ATOM i2)])
            (COMP [HYLO a         OUT, HYLO IN b])
            = return (Map.fromList [(i1,a),(i2,b)])
unifiesWith (ATOM i) b = return $ Map.singleton i b

normalise :: Struct a -> Struct a
normalise (HYLO s1 s2)
  | notIn s1 && notOut s2 = COMP $ flatten [ normalise $ HYLO s1 OUT
                                           , normalise $ HYLO IN s2
                                           ]
  | otherwise             = HYLO (normalise s1) (normalise s2)
  where
    notIn IN = False
    notIn _  = True
    notOut OUT = False
    notOut _   = True
normalise (COMP s)     = COMP $ map normalise $ flatten s
normalise (SPLIT s)    = SPLIT $ map normalise s
normalise (JOIN s)     = JOIN $ map normalise s
normalise (BMAP s1 s2) = BMAP (normalise s1) (normalise s2)
normalise (PAR s)      = rmPar s
normalise s            = s

rmPar :: Skel a -> Struct a
rmPar (SOURCE i _ s) = HYLO IN s
rmPar (REDUCE i _ s) = HYLO s OUT
rmPar (STREAM a)   = rmAlg a
rmPar (SKCOMP a)   = COMP $ flatten $ map rmPar $ reverse a

rmAlg :: Alg a -> Struct a
rmAlg (FARM i a) = rmAlg a
rmAlg (PIPE as)  = COMP $ flatten $ map rmAlg as
rmAlg (FUN s)    = HYLO (COMP [IN, BMAP s ID]) OUT
