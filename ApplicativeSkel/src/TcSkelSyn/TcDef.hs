{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}
module TcSkelSyn.TcDef
  ( tcDef
  ) where

import Control.Arrow ( first, second )
import Control.Monad ( liftM2, foldM, replicateM )
import Control.Monad.Extra ( whenM )
import Control.Monad.Except ( catchError )
import Data.Sequence ( Seq, (|>), (<|) )
import Text.PrettyPrint ( vcat )

import qualified Data.Sequence as Seq

import Util
import Common
import TcM
import Type hiding ( normalise )
import SkelSyn.Expr
import SkelSyn.Pattern
import SkelSyn.Constr
import SkelSyn.Def

import TcSkelSyn.TcType
import TcSkelSyn.TcExpr
import TcSkelSyn.Unify

aexprToExpr :: AExpr Expr -> TcM Expr
aexprToExpr (AAtom x)     = return x
aexprToExpr (AVar x)      = return $ EVar x
aexprToExpr (APrim x)     = return $ EPrim x
aexprToExpr (AP 0 x2 x3)  =  eapps <$> liftM2 (:) (aexprToExpr x2) (mapM aexprToExpr x3)
aexprToExpr (AP n x2 x3)  = do
  v <- freshEVar
  e <- aexprToExpr $ AP (n-1) x2 (map (flip (app 0) (AVar v)) x3)
  return (eabs v e)
aexprToExpr (AS 0 x2)     = eapps <$> mapM aexprToExpr x2
aexprToExpr (AS n x2)     =  do
  v <- freshEVar
  e <- aexprToExpr $ AS (n-1) (map (flip (app 0) (AVar v)) x2)
  return (eabs v e)
aexprToExpr (AK x)        = return $ EVar $ gName "const"
aexprToExpr (AB x)        = foldl eapp (EVar (gName "comp")) <$> mapM aexprToExpr x
aexprToExpr AI            = return $ EVar $ gName "id"

aexprToExpr (AMeta x)     = error "Bug in aexprToExpr"

aexprToStruct :: AExpr Expr -> TcM (Struct (AExpr Expr))
aexprToStruct  AI                     = return ID
aexprToStruct (APrim (Inj i))         = return $ INJ i
aexprToStruct (APrim (Proj i))        = return $ comps $ map PROJ i
  where
    comps []  = error "Bug in TcSkelSyn.TcDef.aexprToStruct"
    comps [x] = x
    comps xs  = COMP xs
aexprToStruct (AB x)                  = COMP  <$> mapM aexprToStruct x
aexprToStruct (AP 0 (APrim (Split _)) xs) = SPLIT <$> mapM aexprToStruct xs
aexprToStruct (AP 0 (APrim Join     ) xs) = JOIN <$> mapM aexprToStruct xs
aexprToStruct a      = return $ ATOM a

-- | Typecheck definition
tcDef :: Def -> TcM [Def]

tcDef d@(TyDef ty vs cs) =
  inDef ty $ do
    (n, alts) <- inCtx Kappa kappa $ do
                   rec        <- freshTyVar
                   (b, prods) <- first or . unzip
                                   <$> mapM (kcConstrDef rec vs) cs
                   if b
                      then return (Just rec, prods)
                      else return (Nothing, prods)
    extendCtx Funct (ty, ForAll (maybe vs (vs |>) n) (TySum $ closedL alts))
    extendCtx Kappa (ty, expectedKind)
    return [d]
  where
    n            = length vs
    kappa        = (ty, expectedKind) <| fmap (, KType) vs
    expectedKind = foldr1 KArr (replicate (n+1) KType)
tcDef d@(FnDef n sch@(ForAll vs ty) []) =
  extendCtx Delta (n, sch) *> return [d]
tcDef d@(FnDef n sch@(ForAll vs (TyAnn ty skel)) alts) = do
  (out, cfn, sfn) <- inCtx Delta (Seq.singleton (n, ForAll vs ucty)) $
                      inDef n $ inCtx Kappa (fmap (, KType) vs) $ do
                        kcEType ucty KType
                        -- (outFn, alts') <- second (map simpl) <$> tcAlts ualts ucty
                        (outFn, alts') <- tcAlts ualts ucty

                        warnStub $ "\n\t EXPR:\n\t\t" ++ (showPpr $ vcat $ map ppr ualts)
                        warnStub $ "\n\t ALTS \n\t\t" ++  (showPpr $ vcat $ map ppr alts')
                        -- warnStub $ "NORMALISED ALTS\n" ++  (showPpr $ vcat $ map (ppr . normaliseA) alts')

                        let (cfns, sfns) = unzip $ map (splitfn (isVar n)) alts'
                        cfn <- aexprToStruct $ AP 0 (APrim Join) cfns
                        sfn <- aexprToStruct $ aplus sfns

                        return (outFn, cfn, sfn)

  extendCtx Delta (n, ForAll vs ty)

   -- tc out function
  oty@(ForAll outVs (TyArr outTy1 outTy2)) <- inferExpr out >>= applySubst . fst >>= generalise
  (outS, outD) <-
      case outTy1 of
        TyApp (TyVar vrec : argsrec) -> do {
         ; k <- lookupCtx Funct vrec >>= specialise
         ; unify k outTy2
         ; return (OUT, [])
        }
        _ -> unifyError outTy1 outTy2
      `catchError` \_ -> do {
      ; v   <- freshPref "out"
      ; extendCtx Delta (v, oty)
      ; return (ATOM $ AVar v, [FnDef v oty [([], out)]])
    }

  let hylo  = HYLO cfn (COMP $ flatten [sfn, outS])

  (newStruct:_) <- instantiateSkel skel hylo

  vh <- freshPref $ nid n
  let hexpr = EVar $ gName $ showPpr newStruct -- FIXME
      hylodef = FnDef vh (ForAll vs ucty) [([],hexpr)]

  -- newdef
  vrs <- freshEVars npats
  let appl = case npats of
              1 -> foldl1 EApp $ map EVar (vh : vrs)
              _ -> EApp (EVar vh) (foldl EApp (EPrim (Split $ length vrs)) $ map EVar vrs)

  return $ outD ++
         [ hylodef
         , FnDef n (ForAll vs ty) [(map PVar vrs, appl)]]
  where
    ((pats,_):_) = alts
    npats = length pats
    ucty  = uncurryEType npats ty
    ualts = uncurryAlts npats n alts
tcDef d@(FnDef n sch@(ForAll vs ty) alts) = do
  inDef n $
    inCtx Delta (Seq.singleton (n, ForAll vs ty)) $
    inCtx Kappa (fmap (, KType) vs) $
      do kcEType ty KType
         _ <- tcAlts alts ty
         return ()
  extendCtx Delta (n, sch)
  return [d]


-- | Kind-checking constructor definition
kcConstrDef :: Name              -- ^ Name for base functor recursive case
            -> Seq Name          -- ^ Variable arguments
            -> ConstrDef         -- ^ Constructor definition
            -> TcM (Bool, EType) -- ^ The base type of constructor, true if rec
kcConstrDef rec vs (CInd ty c args) =
  extendCtx Delta (c, ForAll vs $ foldr1 TyArr $ args ++ [ty]) >>
  inPos (npos c) (mapM_ (`kcEType` KType) (ty:args)
                    *> (tyProd . unzip <$> filterRec args))
  where
    filterRec = foldr (\t ts -> liftM2 (:) (doCheck t) ts) $ return []
    doCheck (TyProd ts)     = tyProd . unzip <$> mapM doCheck (olToList ts)
    doCheck (TySum  ts)     = tySum  . unzip <$> mapM doCheck (olToList ts)
    doCheck (TyArr t1 t2)   = liftM2 tyArr (doCheck t1) (doCheck t2)
    doCheck (TyApp (flattenApp -> ts@(TyVar v:vs)))
      = do b <- isRec v
           if b
             then checkArgs vs >> return (True, TyVar rec)
             else tApp . unzip <$> mapM doCheck ts
    doCheck (TyApp (flattenApp -> ts)) = tApp . unzip <$> mapM doCheck ts
    doCheck t@(TyVar v)     = do b <- isRec v
                                 return (b, if b
                                              then TyVar rec
                                              else t)
    doCheck t               = return (False, t)

    flattenApp (TyApp ts1 : ts2) = flattenApp ts1 ++ ts2
    flattenApp ts                = ts

    checkArgs ts = whenM ((/= vs) <$> extractNames ts) $ nonUniformType c
    extractNames [] = return Seq.empty
    extractNames (TyVar v : vs) = (v <|) <$> extractNames vs
    extractNames _              = nonUniformType c

    tyProd (bs, ts) = (or bs, aux (TyProd . closedL) ts)
    tySum  (bs, ts) = (or bs, aux (TySum  . closedL) ts)
    tApp  (bs, ts) = (or bs, aux TyApp ts)
    tyArr (b1, t1) (b2, t2) = (b1 || b2, TyArr t1 t2)
    aux f []  = TyUnit
    aux f [x] = x
    aux f x   = f x
