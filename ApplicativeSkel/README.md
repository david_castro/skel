Essentially this project builds on several known facts.

1) Functions are instances of Applicative, using the typical S-K-I combinators,
where
```
Ix   = x
Kxy  = x
Sfgx = fx(gx)
```

in pseudo-Haskell:

```Haskell
instance Applicative (A ->) where
  pure      = K
  (f <*> g) = S f g
```

2) Function composition can be represented using Applicative:
Curry's B combinator

```
Bfgx = f(gx)
```

which has the following well-known definition using SKI combinators

```
B = S(KS)K
```

We unfold the definition `Bfg`

```
f . g
  = Bfg
  = S(KS)Kfg
  = (KSf)(Kf)g
  = S(Kf)g
```

Therefore

```Haskell
f . g = pure f <*> g
```

3) The usages of pure can be  pushed downwards:

```Haskell
pure (f <*> g) ==_ext pure (<*>) <*> pure f <*> pure g
```

4) Assume that we want to convert to pointfree some:

```Haskell
\x -> e1 e2
```

Generally, we would need to inspect what is e1 and e2, and see where x happens
in that definition. Instead, we can turn this definition into

```Haskell
(\x -> e1) <*> (\x -> e2)
```

Later, we can use the fact that `pure f <*> g = f . g` to turn it into a normal
function composition.

5) If we follow the process in 4, we will finally reach an almost pointfree
style, with lambdas of the following form:

```Haskell
\x1 ... xn y z1 ... zm -> y
```

Which is converted trivially to pointfree using K combinators:

```Haskell
const ( ... (const (const . ... . const)))
```

If we define:

```Haskell
pure 0 x = x
pure n x = pure (pure (n-1) x)
```

And:

```Haskell
chain 0 f g = g
chain n f g = f . chain (n-1) f
```

Then, the resulting expression is:

```Haskell
pure n (chain m pure id)
```

We can define the combinator:

```Haskell
lvar n m = pure n (chain m pure id)
gvar n x = pure n x
```

The previous points imply that we can describe compositionally how to convert a
program to pointfree, and attach those rules to the typing rules. This would
be equivalent to attaching the standard bracket abstraction to the typing
rules, with any necessary modification:

```
[x]x      = I
[x]y     = Ky
[x]E1 E2 = S([x]E1)([x]E2)
```

The types
would then carry an underlying computational structure, which could be specified
or constrained to be of less than some cost. We use then notation:

```
s ^ A
```

to define an element of type A with structure s. The symbol 's' essentially
represents a function from the local context to an expression of type A.

-------------------------------
------ The typing rules -------

We split the context into Delta (top-level definitions) and Gamma (locally bound
variables). We are going to treat Gamma as a stack, and explicitly write the
top of the stack in our rules. Types in Gamma do not have a structure
associated, but you can think of them as "any structure":

```
A == * ^ A
```

Typing judgements
```
Delta; Gamma |- e : s ^ A
```

* Global variables

```
       x not in Gamma    n = size (Gamma)
G-Var --------------------------------------------
       Delta, x : s^A ; Gamma |- x : gvar n x ^ A
```

* Local variables

```
       x not in Gamma_2    n = size (Gamma_1) m = size (Gamma_2)
L-Var ------------------------------------------------------------
       Delta ; Gamma_1, x : A ; Gamma_2 |- x : lvar n m ^ A
```

* Composition

```
          Delta; empty |- e1 : s1 ^ A -> B    Delta; empty |- e2 : s2 ^ A
Comp-G ----------------------------------------------------------------------
          Delta; empty |- e1 e2 : s1 s2 ^ B

          Gamma not empty    Delta; Gamma |- e1 : s1 ^ A -> B    Delta; Gamma |- e2 : s2 ^ A
Comp-L ---------------------------------------------------------------------------------------
          Delta; Gamma |- e1 e2 : s1 <*> s2 ^ B
```

* Abstraction

```
            Delta; Gamma, x : A |- e : s ^ B
Abs    ---------------------------------------------------------------------------------------
          Delta; Gamma |- \x, e : s ^ A -> B


```

* Pattern matching

```
               data T { C1 fvs p1 ; ...; Cn fvs pn } = Flatten[[p1, ..., pn]]
               |s| = Convert[[p1, ..., pn]]
               Delta; Gamma |- e0 : s0 ^ T0
               Delta; Gamma, x:prod (types (fvs p1))) |- e1[proj_i x / x_i] : s1 ^ types (fvs p1) -> A forall x_i in fvs p1
               ....
               Delta; Gamma, x:prod (types (fvs pn))) |- en[proj_j x / x_j] : sn ^ types (fvs pn) -> A forall x_j in fvs pn
Case         ------------------------------------------------------------------------------------------------------------------------
              Delta; Gamma |- case e0 of   : (pure (join_n) <*> pure s1 <*> ... <*> pure sn <*> (pure out <*> pure |s| <*> s0)) ^ A
                                 p1 -> e1
                                 ...
                                 pn -> em

```

* Global definitions. Remove and define

```
                  <kinding rules apply correctly>
 DataDefn --------------------------------------------------------------------------------
                |- data T a1 ... an = C1 T11 ... T1n T ... T | ... | Cn Tm1 ... Tmr T ... T :
                     base T = multi_functor F a1 ... an b := T11 x ... x T1n x b ... x b + ... + Tm1 x ... x Tmr x b ... x b

            \Delta, x : (A1, ..., An) |- case x of               : s ^ (A1, ..., An) -> B
                                           (p11, ..., p1n) -> e1
                                           ...
                                           (pm1, ..., pmn) -> em
            x fresh
Defn   --------------------------------------------------------------------------------------------------
          \Delta; empty |-  f : A1 -> ... -> An -> B    ;   Delta, f : pure s <*> tuple_n ^ A1 -> ... -> An -> B; empty
                            f p11 ... p1n = e1
                            ...
                            f pm1 ... pmn = em
```
