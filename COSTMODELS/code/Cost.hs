{-# LANGUAGE TupleSections #-}
module Cost where

import Debug.Trace

overheadPipe = 1.5
overheadFarm = 1.5
overheadDc   = 0.17

kpipe ns    = overheadPipe * fromInteger (sum ns) * log (fromInteger $ sum ns)
kfarm n1    = overheadFarm * fromInteger n1 * log (fromInteger n1)
kdc   n     = overheadDc   * (2 ** (fromInteger n - 1))

data S = A (Double -> Double) | Pipe [S] | Farm Integer S | DC Integer (Double -> Double) S S | FB S

-- Cost = Size -> Time
type Cost = (Double, Double) -> Double

workers :: S -> Integer
workers (Pipe ss) = sum $ map workers ss
workers (Farm n s) = n * workers s
workers (DC n _ _ _) = 2 ^ (n - 1)
workers _ = 1

cost :: S -> Cost
cost (A d) = \(n1, n2) -> if n1 < 1 then d n2 else n1 * d n2
cost (Pipe []) = const 0
cost (Pipe ss) = \size -> maximum (map (`cost` size) ss) + kpipe (map workers ss)
cost (Farm n s) = \(n1, n2) -> cost s (n1 / fromInteger n, n2) + kfarm n
cost (DC n d s1 s2) = \(n1, n2) -> maximum [maximum $ sizeDiv n1 n2, sizeHylo n1 n2 , maximum $ sizeConq n1 n2] + kdc n
  where sizeDiv  n1 n2 = map (cost s2 . (n1,)) $ genSizes n n2
        sizeConq n1 n2 = map (cost s1 . (n1,)) $ genSizes n n2
        sizeHylo n1 n2 = n1 * d (last (genSizes n n2)) -- sum (map (cost s1 . (n1,)) (genSizes' (last $ genSizes n n2))) + sum (map (cost s2 . (n1,)) (genSizes' ))
        genSizes 0 n2 = []
        genSizes n n2 | n2 <= 0 = []
                      | otherwise = n2:genSizes (n - 1) (n2 / 2)
        genSizes'  n2 | n2 <= 0 = []
                      | otherwise = n2:genSizes' (n2 / 2)
cost (FB s)                    = \(n1, n2) -> let szs = genSizes n2
                                                  ss  = map (n1,) szs
                                              in sum (map (cost s) ss)
  where genSizes n2 | n2 <= 0 = [0]
                    |otherwise = n2:genSizes (n2 - 1)


getMin :: [(a, Double)] -> (a, Double)
getMin ((a, d):xs) = go (a, d) xs
  where go (a, d) [] = (a,d)
        go t@(a, d) (t'@(b, d'):ds)
          | d <= d' = go t ds
          | otherwise = go t' ds

imageMerge1 n1 n2 n3 = cost (Pipe [Farm n1 (A (\e -> 10 * (e ** (1/10)))), Farm n2 (Farm n3 (A (\e -> 18 * (e ** (1/10)))))])
timesIM1 = getMin [((a,b,c), imageMerge1 a b c (1000, 10000 ** (1/10))) | a <- [1..23], c <- [1..(24-a)], b <- [1..((24 - a) `div` c)] ]

imageMerge2    n2 n3 = cost (Pipe [  A (\e -> 10 * (e ** (1/10)))      , Farm n2 (Farm n3 (A (\e -> 18 * (e ** (1/10)))))])
timesIM2 =getMin [((n1,n2), imageMerge2 n1 n2 (1000, 10000 ** (1/10))) | n1 <- [1..24], n2 <- [1..(24`div` n1)]]

imageMerge3    n2 n3 = cost (Pipe [Farm n2 (A (\e -> 10 * (e ** (1/10)))), Farm n3 (A (\e -> 18 * (e ** (1/10))))])
timesIM3 =getMin [((n1,n2), imageMerge3 n1 n2 (1000, 10000 ** (1/10))) | n1 <- [1..24], n2 <- [1..(24 - n1)]]

imageMerge4    n3 = cost (Pipe [A (\e -> 10 * (e ** (1/10))) , Farm n3 (A (\e -> 18 * (e ** (1/10))))])
timesIM4 =getMin [(n1, imageMerge4 n1 (1000, 10000)) | n1 <- [1..24] ]


nlists = 1000
sizel = 3000000

quicksort1 n
    | n == 0 = nlists * costHylo sizel
    | otherwise = cost (DC n costHylo (A (\e -> 0.0000032 * e)) (A (\e -> 0.0000142 * e))) (nlists, sizel)
  where costHylo d = 0.0000172 * d * log d
timesQS1 = getMin [(n, quicksort1 n) | n <- [1..24], workers (DC n undefined (A (\e -> 0.0000022 * e)) (A (\e -> 0.0000152 * e))) <= 24 ]

quicksort2 n = cost (Pipe [A (\e -> 0.0000032 * e), Farm n (A (\e -> 0.0000142 * e * log e))]) (nlists, sizel)
timesQS2 = getMin [(n, quicksort2 n) | n <- [1..24], workers (Pipe [A (\e -> 0.0000022 * e * log e), Farm n (A (\e -> 0.0000152 * e * log e))]) <= 24 ]

quicksort3 n = cost (Farm n (A costHylo)) (nlists, sizel)
  where costHylo d = 0.0000172 * d * log d
timesQS3 = getMin [(n, quicksort3 n) | n <- [1..24] ]


nb1 n1 n2 = cost (Pipe [Farm n1 (A (\e -> 39 * (e ** (1/10)))), Farm n2 ((A (\e -> 7 * (e ** (1/10)))))])
timesNB1 = getMin [((a,b), nb1 a b (200000, 10 ** (-14))) | a <- [1..23], b <- [1..(24-a)]]

nb2 n2 = cost (Pipe [  A (\e -> 39 * (e ** (1/10)))      , Farm n2 ((A (\e -> 7 * (e ** (1/10)))))])
timesNB2 =getMin [((n1), nb2 n1 (200000, 10 ** (-14))) | n1 <- [1..24]]

nb3 n2 = cost (Pipe [Farm n2 (A (\e -> 39 * (e ** (1/10)))), (A (\e -> 7 * (e ** (1/10))))])
timesNB3 =getMin [((n1), nb3 n1 (200000, 10 ** (-14))) | n1 <- [1..24]]

nb4 = cost (Pipe [A (\e -> 39 * (e ** (1/10))) , (A (\e -> 7 * (e ** (1/10))))])
timesNB4 = nb4 (200000, 10 ** (-14))

cost1 d = 3.2 * d * 1.57 ** 1.321
cost2 d = d * 1.57 ** 1.321

fb1 = cost (FB (Pipe [A cost1, A cost2]))
timeFB1 = fb1 (100, 8)

fb2 n = cost (FB (Pipe [Farm n (A cost1), A cost2]))
timeFB2 = getMin [(a, fb2 a (100, 8))| a <- [1..24]]

fb3 n = cost (FB (Pipe [A cost1, Farm n (A cost2)]))
timeFB3 = getMin [(a, fb3 a (100, 8))| a <- [1..24]]

fb4 n1 n2 = cost (FB (Pipe [Farm n1 (A cost1), Farm n2 (A cost2)]))
timeFB4 = getMin [((a,b), fb4 a b (100, 8))| a <- [1..24], b <- [1..24-a]]
