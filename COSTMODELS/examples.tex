%\pagebreak
\section{Examples}
\label{sec:examples}

\noindent
Our first two examples revisit \emph{image merge} from
Section~\ref{sec:introduction}, and \emph{quicksort} from
Section~\ref{sec:skel_def}. In both cases, we show how the type system
calculates the convertibility of the structured expression to a functionally
equivalent parallel process, and how the convertibility proof allows the
structured expression to be rewritten to the desired parallel process. The
final two examples show how to use types to parallelise different
algorithms, described as structured expressions. These examples
show how to easily introduce
parallel structure to these algorithms  using our type system.


%show farms and pipelines
\subsection{Image Merge}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (defined here due to latex problems)
\begin{figure*}[!ht]
\[
\inference{
  \inference{%
      \inference{%
        \inference{}{\keyword{replace}~:~\keyword{Img}\times\keyword{Img}\xmapsto{\AF}\keyword{Img}}
      & \inference{}{\keyword{mark}~:~\keyword{Img}\times\keyword{Img}\xmapsto{\AF}\keyword{Img}\times\keyword{Img}}
      }
      {\keyword{replace}~\bullet~\keyword{mark}~:~\keyword{Img}\times\keyword{Img}~\xmapsto{\AF~\bullet~\AF}~\keyword{Img}\times\keyword{Img}}
    }
    {\smap{\List}~(\keyword{replace}~\bullet~\keyword{mark})~:~\List(\keyword{Img}\times\keyword{Img})~\xmapsto{\MAP{\vL}~(\AF~\bullet~\AF)}~\List(\keyword{Img}\times\keyword{Img})}
 & \MAP{\vL}~(\AF~\bullet~\AF)~\cong~\ssc{im}_1~(\vn,\vm)
 }
 {\smap{\List}~(\keyword{replace}~\bullet~\keyword{mark})~:~\List(\keyword{Img}\times\keyword{Img})~\xmapsto{\ssc{im}_1~(\vn,\vm)}~\List(\keyword{Img}\times\keyword{Img})}
\]
\caption{Typing Derivation Tree for Image Merge}
\label{fig:example_derivation}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Recall that \emph{image merge} basically
composes two functions: $\keyword{mark}$ and $\keyword{merge}$. It can
be directly parallelised  using different combinations of farms
and pipelines.
\[
\begin{array}{l}
  %\ssc{im}~:~\mathbb{N}\times\mathbb{N}\to\Sigma \\
  \ssc{im}_1~(\vn,\vm)~=~\PEVAL_\vL~(\FARM~\vn~(\FUNC~\AF)\parallel~\FARM~\vm~(\FUNC~\AF)) \vspace{2mm}\\

  \keyword{imageMerge}~:~\List(\keyword{Img}\times\keyword{Img})~\xmapsto{\ssc{im}_1~(\vn,\vm)}~\List(\keyword{Img}\times\keyword{Img}) \\
  \keyword{imageMerge}~=~\smap{\List}~(\keyword{merge}~\bullet~\keyword{mark})
\end{array}
\]
First, we use our annotated typing rules to
produce a derivation tree with the structure
of the expression (Fig.~\ref{fig:example_derivation}).
The key part is the convertibility proof, that
$\MAP{\vL}~(\AF~\bullet~\AF)\,\cong\,\ssc{im}_1~(\vn,\vm)$.
We use the decision procedure defined in Section~\ref{sec:skel_func} to
decide the equivalence of both structures.
The $\erase$ step is applied as follows:
\[
  \ssc{im}_1~(\vn,\vm)~\rightsquigarrow^\text{*}~\MAP{\vL}~(\AF~\bullet~\AF)
\]


\pagebreak
\noindent
The final
step involves applying the decision procedure for equality of $\overline{\vS}$.
Since the expressions are identical, this is a trivial step.
We can now apply this equivalence to the original expression:
\[
  \begin{array}{l}
\smap{\List}~(\keyword{merge}~\bullet~\keyword{mark}) \rightsquigarrow^\text{*}
\\\qquad\peval_{\List}~(\farm~\vn~(\pseq~\keyword{mark}) \parallel
\farm~\vm~(\pseq~\keyword{merge}))
\end{array}
\]

\noindent
We now show an example of unification. We define $\ssc{im}_2~\vn=\PEVAL_\vL~(\ANY\parallel\FARM\,\vn\,\ANY)$. First, we instantiate the structure with fresh
metavariables $\vm_1$ and $\vm_2$. Then, we normalise the structure. We start by
applying the $\erase$ rewriting system:
\[
\MAP{\vL}~(\vm_2'\bullet\vm_1') \qquad\qquad\delta=\lbrace\vm_1\sim\FUNC~\vm_1',~\vm_2\sim\FUNC~\vm_2'\rbrace
\]
We then apply the normalisation in $\overline{\SSigma}$, and the unification rules:
\[
\MAP{\vL}~\AF\bullet\MAP{\vL}~\AF\sim\MAP{\vL}~\vm_2'\bullet\MAP{\vL}~\vm_1'\Rightarrow\lbrace\lbrace\vm_1'\sim\AF,\vm_2'\sim\AF\rbrace\rbrace
\]
The final step is to calculate the extension of the environment $\delta$,
and the set of environments that are obtained from the unification:
\[
  \begin{array}{l}
\Delta=\lbrace\delta\rbrace\otimes\lbrace\lbrace\vm_1'\sim\AF,\vm_2'\sim\AF\rbrace\rbrace=\\
\qquad\lbrace\lbrace\vm_1\sim\FUNC~\vm_1',~\vm_2\sim\FUNC~\vm_2',\vm_1'\sim\AF,\vm_2'\sim\AF\rbrace\rbrace
  \end{array}
\]
\noindent
Applying the substitution environment in $\Delta$ to the $\ssc{im}_2(\vn)$, we obtain the structure $\PEVAL_\vL~(\FUNC~\AF\parallel\FARM\vn~(\FUNC~\AF))$.

Finally, we briefly discuss how to extend the environment further using a procedure $\min~\keyword{cost}$. First,
$\min$ attempts further rewritings to $\vm_1$ and $\vm_2$. To ensure
termination, the process stops whenever the only option is to introduce a
task farm to an existing task farm structure.
\[
\begin{array}{l l}
\delta_1=\lbrace\vm_1~\sim~\FARM~\vn_1~(\FUNC~\AF),
  &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~\AF)\rbrace  \\

\delta_2=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~\AF)\rbrace  \\

\delta_3=\lbrace\vm_1~\sim~\FARM~\vn_1~(\FUNC~\AF),
  &
  \vm_2~\sim~\FUNC~\AF\rbrace  \\

\delta_4=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FUNC~\AF\rbrace  \\
\end{array}
\]
We will show how to select one of those structures using a simple example cost
model. In future work, we will consider how to extend and formalise this cost model.
A cost model provides size functions $|\sigma|$ over structures, similar
to the idea of \emph{sized types}~\cite{sizedrp96}. We assume that all atomic
functions are annotated with their cost models, $\AF_{\vc}$. The cost of a structure
is a function that receives a size, $\var{sz}$, and returns an estimation of
its run-time in milliseconds.

In our example, we assume that $\var{sz}={[d]}^{1000}$.  This represents the
size of 1000 pairs of images of $d$ dimensions. Arithmetic operations on
$\var{sz}$ are applied to the superscript. The size function of the first stage
$|\AF_{\vc_1}|$ is the identity, since we are not modifying the images.  The
parameters for the number of farm workers are fixed to be those with the least cost,
given some maximum number of available cores. In this example, we assume that a
maximum of 24 cores are available.  For $\delta_1$, we determine that $\vn_1=9$, $\vn=3$ and
$\vn_2=5$.  The values of the costs on those sizes, and the overheads of farms
and pipelines ($\kappa_1$ and $\kappa_2$) are given below. In the following
examples, we omit these numbers and just provide the estimation for a 24-core
architecture with similar overheads for farms, pipelines, divide-and-conquer
and feedback.
\[
    \begin{array}{l}
    \begin{array}{l r}
        \multicolumn{2}{c}{\vc_1~{[(2048\times2048, 2048\times2048)]}^\vn~=~\vn\times25.11ms} \\
        \multicolumn{2}{c}{\vc_2~{[(2048\times2048, 2048\times2048)]}^\vn~=~\vn\times45.21ms} \\
        \kappa_1~(9)~=~29.66ms & \kappa_1~(3\times5)~=~60.93ms \\ \multicolumn{2}{c}{\kappa_2~(9,3\times5)~=~114.4ms}  \\
    \end{array} \\ \\
  \begin{array}{l}
    \keyword{cost}~(\delta_1\ssc{im}_2(\vn))~\var{sz}\\
    \ =~\max~\lbrace\vc_1~(\cfrac{\var{sz}}{\vn_1})+\kappa_1(\vn_1),~\vc_2~(\cfrac{|\AF_{\vc_1}|(\var{sz})}{\vn\times\vn_2})+\kappa_1(\vn\times\vn_2)\rbrace
    \\\hspace{0.8cm}~+~\kappa_2(\vn_1,~\vn\times\vn_2)~=~3145.69ms \\
    \keyword{cost}~(\delta_2\ssc{im}_2(\vn))~\var{sz}\hspace{1.19cm}=~25123.81ms\\
    \keyword{cost}~(\delta_3\ssc{im}_2(\vn))~\var{sz}\hspace{1.19cm}=~3189.60ms \\
    \keyword{cost}~(\delta_4\ssc{im}_2(\vn))~\var{sz}\hspace{1.19cm}=~25123.81ms\\
  \end{array}
  \end{array}
\]
The structure that results from applying $\delta_1$ is the least cost one,
$\delta_1(\ssc{im}_2(3))$, with $\vn_1=9$ and $\vn_2=5$.

% divide and conquer
\subsection{Quicksort}

\noindent
We will now revisit \emph{quicksort}
and show how it can exploit a divide-and-conquer parallel structure.
%We start with the definition of \emph{quicksort}.
\[
\begin{array}{l}
  \keyword{qsorts}~:~\List(\List~\vA)\to\List(\List~\vA) \\
  \keyword{qsorts}~=~\smap{\List}~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div})
\end{array}
\]
%
%\noindent
%Even without altering % the definitions of
%$\keyword{merge}$ or $\keyword{div}$,
%this has many possible parallelisations. % implementations.
%\[
%\begin{array}{ll}
%  \keyword{qsorts} &:~\List(\List~\vA)\to\List(\List~\vA) \\
%  \keyword{qsorts} %   &~=~& \pseq~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div}) \\
%                   &=~\peval_\List~(\dc{\vn,\List,\vF}~\keyword{merge}~\keyword{div}) \\
%                   &=~\peval_\List~(\farm~\vn~(\pseq~(\shylo{\vF\,\vA}~\keyword{merge}~\keyword{div}))) \\
%                   &=~\peval_\List~(\pseq~(\sana{\vF\,\vA}~\keyword{div})\\ & \qquad\qquad\parallel\farm~\vn~(\pseq~(\scata{\vF\,\vA}~\keyword{merge}))) \\
%                   &=~\ldots
%\end{array}
%\]
%\noindent
In order to introduce a divide-and-conquer parallel structure, the type system needs to decide:
\[
  \MAP{\vL}(\HYLO{\vF}\,\AF\,\AF)\cong\PEVAL_\vL~(\DC{\vn,\vF}\,\AF\,\AF)%
\]
This can be achieved using a simple parallelism erasure.
Consider now a slightly more complex structure:
\[
  \MAP{\vL}~(\HYLO{\vF}~\AF~\AF)~\cong~\PEVAL_\vL~(\FARM{\vn}~\ANY\parallel\ANY)
\]
Let $\vm_1$, $\vm_2$ be two fresh metavariables. The parallelism erasure of the right hand side returns the following structure and substitution:
\[
\begin{array}{l}
  \PEVAL_\vL~(\FARM~\vn~\vm_2\parallel\vm_1)~\rightsquigarrow^{\text{*}}~\MAP{\vL}~(\COMP{\vm_1'}{\vm_2'}) \\
  \multicolumn{1}{r}{
  \quad \delta=\lbrace\vm_1~\sim~\FUNC~\vm_1',\vm_2\sim\FUNC~\vm_2'\rbrace}
\end{array}
\]
The normalisation procedure continues by normalising the left and right hand
sides of the equivalence following a parallelism erasure. The left hand side is
normalised by applying \tsc{hylo-split}, \tsc{f-split} and \tsc{cata-split}:
\[
\MAP{\vL}~(\HYLO{\vF}~\AF~\AF)\rightsquigarrow^{\text{*}}\MAP{\vL}~(\CATA{\vF}~\AF)~\bullet~\MAP{\vL}~(\ANA{\vF}~\AF)
\]
%
\noindent
The right hand side of the equivalence is normalised by applying \tsc{f-split} and \tsc{cata-split}:
\[
\begin{array}{l}
  \MAP{\vL}~(\COMP{\vm_1'}{\vm_2'})\rightsquigarrow^{\text{*}}\MAP{\vL}~\vm_1'\bullet\MAP{\vL}~\vm_2'
\end{array}
\]


\noindent
The decision procedure finishes by unifying both structures, and extending the
substitution $\delta$ with all possible unifications.
\[
  \begin{array}{l}
    \MAP{\vL}~(\CATA{\vF}~\AF)~\bullet~\MAP{\vL}~(\ANA{\vF}~\AF)\sim\MAP{\vL}~\vm_1'\bullet\MAP{\vL}~\vm_2'\\
    \qquad\qquad\Rightarrow\Delta_1=\lbrace\vm_1'\sim\CATA{\vF}~\AF,\vm_2'\sim\ANA{\vF}~\AF\rbrace \\\\
    \Delta=\lbrace\delta\rbrace\otimes\Delta_1
  \end{array}
\]
\noindent
Again, by applying the only substitution in $\delta'\in\Delta$, we select the final structure:
\[
  \PEVAL_\vL~(\FARM{\vn}~(\FUNC~(\ANA{\vF}~\AF))\parallel\FUNC~(\CATA{\vF}~\AF))
\]
%
\noindent
The full proof of equivalence ($\cong$) allows us to rewrite quicksort to our
desired parallel structure:
\[
  \begin{array}{l}
\smap{\List}(\shylo{\vF\,\vA}\keyword{merge}~\keyword{div})\rightsquigarrow^{\text{*}}\\
\qquad\peval_\List~(\pipe{\farm~\vn~(\pseq~(\sana{\vF\,\vA}~\keyword{div}))}{\newline\pseq~(\scata{\vF\,\vA}\keyword{merge})})
\end{array}
\]
We can use our cost model again, where $\kappa_3$ is the overhead of a divide-and-conquer structure. In this example, we set the size parameter of our cost
model to 1000 lists of 3,000,000 elements, and use the following structure:
\[
  \begin{array}{l}
    \keyword{qsorts}~:~\List(\List~\vA)\xmapsto{\min~\keyword{cost}~\ANY}\List(\List~\vA) \\\\
    \keyword{cost}~(\PEVAL_\vL~(\DC{\vn,\vF}~\AF_{\vc_1}~\AF_{\vc_2}))~\var{sz}
    \\\ =~\max\lbrace\max\limits_{1\leq i \le n}\lbrace\vc_2~(|\AF_{\vc_2}|^i\var{sz})
    \\\hspace{.6cm},\keyword{cost}~(\HYLO{\vF}~\AF_{\vc_1}~\AF_{\vc_2})~(|\AF_{\vc_2}|^\vn\var{sz})
    \\\hspace{.6cm},\max\limits_{1\leq i \le
    n}\lbrace\vc_1~(|\AF_{\vc_1}|^\vi|\AF_{\vc_2}|^\vn\var{sz})\rbrace\rbrace~+~\kappa_3(\vn)~=~42602.72ms\\
    \keyword{cost}~(\PEVAL_\vL~(\FARM{\vn}~(\FUNC~(\ANA{\vL}~\AF_{\vc_2}))\parallel(\FUNC~(\CATA{\vL}~\AF_{\vc_1}))))~\var{sz}
    \\\ =~27846.13ms \\
    \keyword{cost}~(\PEVAL_\vL~(\FARM{\vn}~(\FUNC~(\HYLO{\vF}~\AF_{\vc_1}~\AF_{\vc_2}))))~\var{sz}
    \\\ =~32179.77ms \\
    \ldots
  \end{array}
\]
Since the most expensive part of the quicksort is the divide, and
flattening a tree is linear, the cost of adding a farm to the divide part is
less than using a divide-and-conquer skeleton for this example.

%iteration + peval
\subsection{N-Body Simulation}
\label{loop}

N-Body simulations are widely used in astrophysics.
They comprise a
simulation of a dynamic system of particles, usually under the influence of
physical forces.
% In this example, we show a high-level example of N-Body
% simulation based on the Barnes-Hut simulation.
The Barnes-Hut simulation recursively divides the $n$ bodies storing them in a $\keyword{Octree}$, or a
$8$-ary tree. Each node in the tree represents a region of the space, where the
topmost node represents the whole space and the eight children the eight
octants of the space. The leaves of the tree contain the bodies. Then, the
cumulative mass and center of mass of each region of the space are calculated.
Finally, the algorithm calculates the net force on each particular body by
traversing the tree, and updates its velocity and position. This process is
repeated for a number of iterations.
We will here abstract most of the concrete, well known details of the algorithm,
and present its high-level structure, using the following types
and functions:
\[
\begin{array}{lcl}
  \vC              &~=~& \mathbb{Q}\times\mathbb{Q} \\
  \vF~\vA~\vB      &~=~& \vA~+~\vC\times\vB^8 \\
  \vG~\vA          &~=~& \vF~\keyword{Body} \\
  \keyword{Octree} &~=~& \mu\vG \\
  \keyword{insert} &~:~& \keyword{Body}\times\keyword{Octree}\to\keyword{Octree} \\
\end{array}
\]

\noindent
Since this algorithm also involves iterating for a fixed number of steps, we define
iteration as a hylomorphism. We assume that the combinator $+$
(Fig.~\ref{fig:auxdefns}) is also defined in $\SSigma$. Additionally, we assume
a primitive combinator, that tests a
predicate on a value, $(\cdot~?)~:~(\vA\to\keyword{Bool})\to\vA\to\vA+\vA$.
\[
\begin{array}{l}
\ssc{loop}~:~\Sigma\to\Sigma \\
\ssc{loop}~\sigma~=~\HYLO{(+)}~(\ID\triangledown\sigma)~((\AF+(\AF\vartriangle(\AF\bullet\AF)))\bullet(\AF\bullet\AF?)) \\\\

\keyword{loop}_\vA~:~(\vA\xmapsto{\vm}\vA)\to\vA\times\mathbb{N}\xmapsto{\ssc{loop}~\vm}\vA \\
\keyword{loop}_\vA~\vs~=\\\quad\shylo{(\vA+)}~(\sid~\triangledown~\vs)\\\hspace{1.57cm}((\pi_1+(\pi_1\vartriangle((-1)\bullet\pi_2)))\bullet((==0)\bullet\pi_2)?) \\
\end{array}
\]

\noindent
This example uses some additional functions: $\keyword{calcMass}$ annotates
each node with the total mass and centre of mass; $\keyword{dist}$ distributes
the octree to all the bodies, to allow independent calculations,
% so that each can independently calculate its
%forces and update the velocity and position;
 $\keyword{calcForce}$ calculates
the force of one body; and $\keyword{move}$ updates the velocity and position
of the body.

\vspace{-12pt}
\[
  \begin{array}{l}
  \keyword{calcMass}~:~\vG~\keyword{Octree}\to\vG~\keyword{Octree} \\
  \keyword{dist}~:~\keyword{Octree}\times\List~\keyword{Body}\\\phantom{\keyword{dist}~:~}
  \to\vL~(\keyword{Octree}\times\keyword{Body})~(\keyword{Octree}\times\List~\keyword{Body})
\end{array}
\]
\vspace{-12pt}

\noindent
The algorithm is:

\vspace{-12pt}
\[
\begin{array}{l}
  \keyword{nbody}~:~\List~\keyword{Body}\times\mathbb{N}\xmapsto{\ssc{loop}~\sigma}\List~\keyword{Body} \\
  \keyword{nbody}~=~\keyword{loop}~(\sana{\vL}~(\vL~(\keyword{move}\bullet\keyword{calcForce})\bullet\keyword{dist}) \\
  \hspace{1.7cm}\bullet((\scata{\vG}~(\shin{\vG}\bullet\keyword{calcMass})\bullet\scata{\vL}~\keyword{insert})\vartriangle\sid)) \\
\end{array}
\]
\vspace{-12pt}


\noindent
Since the $\ssc{loop}$ defines a fixed structure, we do not allow any rewriting
that changes this structure. However, note that our type system still enables some
interesting rewritings. In particular, the structure of the loop body is:

\vspace{-12pt}
\[
\sigma=\ANA{\vL}(\vL\,(\AF\bullet\AF)\bullet\AF)\bullet(\CATA{\vG}(\IN\bullet\AF)\bullet\CATA{\vL}\,\AF)\vartriangle\ID%
\]
\vspace{-12pt}

\noindent
The normalised structure reveals more opportunities for parallelism introduction:
\[
\sigma=\MAP{\vL}\AF\bullet\MAP{\vL}\,\AF\bullet\ANA{\vL}\,\AF\bullet(\CATA{\vG}(\IN\bullet\AF)\bullet\CATA{\vL}\,\AF)\vartriangle\ID%
\]

\noindent
\vspace{-6pt}
After normalisation, this structure is equivalent to:
\[
\sigma=\PEVAL_\vL~(\FUNC~(\AF\bullet\AF))\bullet\ANY
\]

\noindent
%\vspace{-6pt}
The structure makes it clear that there are many possibilities for
parallelism using farms and pipelines.
%An extension to our work to consider a
%\emph{reduce} skeleton allows the parallelisation of both the octree generation
%and the calculation of the centre of mass and cumulative mass.
As before,
parallelism can be introduced semi-automatically using a cost model. For
example, setting the input size to 20,000 bodies:
\[
  \begin{array}{l}
  \sigma\phantom{'}=\PEVAL_\vL~(\FARM\,\vn\,\ANY\parallel\ANY)\bullet\ANY  \\
  \sigma'=\PEVAL_\vL~(\min\,\keyword{cost}\,(\ANY\parallel\ANY))\bullet\ANY \\\\
  \keyword{cost}~(\FUNC~\AF_{\vc_1}\parallel\FUNC~\AF_{\vc_2})~\var{sz}~=~310525.67ms \\
  \keyword{cost}~(\FARM{6}~(\FUNC~\AF_{\vc_1})\parallel(\FUNC~\AF_{\vc_2}))~\var{sz}~=~55755.43ms \\
  \keyword{cost}~(\FUNC~\AF_{\vc_1}\parallel\FARM{1}~(\FUNC~\AF_{\vc_2}))~\var{sz}~=~310525.67ms \\
  \keyword{cost}~(\FARM{20}(\FUNC~\AF_{\vc_1})\parallel\FARM{4}(\FUNC~\AF_{\vc_2}))~\var{sz}~=~15730.46ms \\

\end{array}
\]

%feedback + dc
\vspace{-12pt}
\subsection{Iterative Convolution}

Image convolution is also widely used in image processing applications. We
assume the type $\keyword{Img}$ of images, the type $\keyword{Kern}$ of
kernels, the functor $\vF~\vA~\vB\,=\,\vA+\vB\times\vB\times\vB\times\vB$, and the following functions.
%
%defined below.% and the functions defined below.
%\[
%\begin{array}{l}
%%  %\keyword{Img} \\
%  \vF~\vA~\vB~=~\vA~+~\vB\times\vB\times\vB\times\vB \\
%%  \keyword{split}~:~\keyword{Kern}\to\keyword{Img}\to\vF~\keyword{Img}~\keyword{Img} \\
%%  \keyword{combine}~:~\vF~\keyword{Img}~\keyword{Img}\to\keyword{Img} \\
%%  \keyword{kern}~:~\keyword{Kern}\to\keyword{Img}\to\keyword{Img} \\
%%  \keyword{finished}~:~\keyword{Img}\to\keyword{Bool} \\
%\end{array}
%\]
%
%\noindent
The $\keyword{split}$  function splits an image into 4 sub-images
with overlapping borders, as required for the
kernel. The $\keyword{combine}$  function concatenates the sub-images in the
corresponding positions. The $\keyword{kern}$  function applies a kernel to an
image. Finally, the $\keyword{finished}$
function tests whether an image has the desired properties, in which case the computation terminates.
We can represent image convolution on a list of input images as follows:
\[
\begin{array}{l}
  \keyword{conv}~:~\keyword{Kern}\to(\List~\keyword{Img}\xmapsto{\sigma}\List~\keyword{Img}) \\
  \keyword{conv}~k~=\\~\smap{\List}~(\siter{\keyword{Img}}~(\keyword{finished}?\bullet\shylo{\vF}~(\keyword{combine}\bullet\vF~(\keyword{kern}~k))
  \\\hspace{4.6cm}(\keyword{split}~k)))
\end{array}
\]

%\pagebreak
\noindent
The structure of $\keyword{conv}$ is equivalent to a feedback loop, which
exposes many opportunities for parallelism. Again, we assume a suitable cost model.
Our estimates are given for 1000 images, of size 2048$\times$2048.

\[
\begin{array}{l}
  \sigma~=~\PEVAL_\vL~(\FB~(\DC{\vn,\vL,\vF}~(\AF\bullet\vF~\AF)~\AF\parallel\ANY)) \\
  \phantom{\sigma}~=~\PEVAL_\vL~(\FB~(~\FARM~\vn~\ANY~\parallel~\ANY~\parallel\ANY))  \\
  \phantom{\sigma}~=~\min~\keyword{cost}~(\PEVAL_\vL~(\FB~(\ANY~\parallel~\ANY)))  \\
  \phantom{\sigma}~=~\ldots \\\\
  \keyword{cost}~(\PEVAL_\vL~(\FB~(\AF_{\vc_1}\parallel\AF_{\vc_2})))~\var{sz}~=
  \\~\sum\limits_{1\le\vi, |\AF_{\vc_1}\parallel\AF_{\vc_2}|^i\var{sz}>0}~\keyword{cost}~(\AF_{\vc_1}\parallel\AF_{\vc_2})~(|\AF_{\vc_1}\parallel\AF_{\vc_2}|^i\var{sz})\\
  \quad =~20923.02ms \\
  \keyword{cost}~(\PEVAL_\vL~(\FB~(\FARM{4}~(\FUNC~\AF_{\vc_1})\parallel(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad =~6649.55ms\\
  \keyword{cost}~(\PEVAL_\vL~(\FB~(\FUNC~\AF_{\vc_1}\parallel\FARM{1}~(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad =~20923.02ms\\
  \keyword{cost}~(\PEVAL_\vL~(\FB~(\FARM{14}~(\FUNC~\AF_{\vc_1})\parallel\FARM{4}~(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad=~~2694.30ms\\
  \ldots
\end{array}
\]
\noindent
Collectively, our examples have demonstrated the use of our techniques for all
the  parallel structures we have considered, showing that we can easily and
automatically introduce parallelism according to some required structure, while
maintaining functional equivalence with the original form.

