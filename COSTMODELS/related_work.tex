\section{Related Work}
\label{sec:related_work}

There have been some previous treatments of parallelism using types, but
these deal only with sizes and productivity.  One line
of work is using \emph{sized types}~\cite{sizedrp96} to internalise a notion of
sizes of streaming data into a type system. This has been extended to a small
number of skeletons in the Eden language~\cite{pena2002sized}.
While types were useful to prove the termination and productivity of Eden
skeletons, our own work focuses on the different, but important, properties of semantic
equivalence and cost.
%
The expressive power of hylomorphisms for parallel programming was first
explored by Fischer and Gorlatch~\cite{fischer2002turing},
who showed that a programming language based on catamorphisms
and anamorphisms is \emph{Turing-universal}.
The idea of using hylomorphisms for parallel programming also appears in
Morihata's work~\cite{morihata2013short}. Morihata explores a theory for
developing parallelisation theorems based on the \emph{third homomorphism
theorem} and \emph{shortcut fusion}, and generalises it to hylomorphisms. In
contrast, our work directly exploits the properties of hylomorphisms, in
order to choose a suitable parallel skeleton implementation for
hylomorphisms.
Both lines of work are
therefore orthogonal, and we can potentially benefit from
Morihata's results.

Deriving parallel implementations from small, simple specifications has
been widely studied. The third homomorphism theorem, list homomorphisms,
and the Bird-Meertens Formalism are amongst the many techniques that have
been explored~%
\cite{gorlatch1995parallelization,hu1997formal,hu1998parallelization,keller1998flattening,matsuzaki2006towards,misra1994powerlist,morihata2010automatic,reif1993synthesis,skillicorn1993bird,skillicorn1991models}.
The third homomorphism theorem states that if a function can be written
both as a left fold and a right fold, then it can also be evaluated in a
divide-and-conquer manner~\cite{gibbons1996third}. This theorem has been
widely used for parallelism~%
\cite{chi2011constructing,geser1999parallelizing,gibbons1996computing,gorlatch1999extracting,liu2011towards,morihata2013short,morita2007automatic}.
%Although we do not currently exploit the third homomorphism theorem, our
%work is related, since we use algebraic properties of recursion patterns
%to introduce parallelism. It differs in that we are more
%interested in providing a \emph{framework} for choosing amongst
%alternative parallelisations, with strong static guarantees, based on
%well-known properties of programs expressed using hylomorphisms. A
%possible extension of our work is to enable some automatic
%transformations using the third homomorphism theorem.
The majority of this work enables suitable automation and
derivation of efficient parallel implementations. Our work differs in that we
allow part of the parallel structure to be chosen in a semi-automated way. This
adds flexibility, enabling a parallel implementation to be changed quickly and
easily by changing only a single type annotation. One possible extension of our
work is to include some automatic transformations derived from the third
homomorphism theorem.
%
By parameterising our type system over some cost function on parallel
structures, we smoothly integrate the introduction of parallelism with the ability to
reason about the run-time behaviour of the parallel program. 
Skillicorn and Cai~\cite{skillicorn1995cost} have previously shown the utility of such an
integration of a cost calculus with derivational software development,
illustrating the approach for the Bird-Meertens theory of
lists. We take this approach one step further by using a more general equational
theory based on hylomorphisms. Moreover, our type-based approach introduces new
benefits, by providing a mechanism for specifying new parallel
structures whose denotational semantics can be described as a composition of
hylomorphisms.

Finally, in a practical setting, Steuwer et al~\cite{steuwer2015generating} generate
high-performance OpenCL code from a high-level specification by applying a
simple set of rewrite rules, and using Monte-Carlo search to traverse the corresponding
search space to find an implementation.
Our semi-automated approach provides a way to narrow down this search space,
while using cost models to automate the rest.  Our approach is in a sense more
general, since we allow our parallel structures to be easily extended. However, we
could benefit from exploiting their work in GPU-specific rewriting rules and
skeletons.

%Various synchronous data flow languages have been devised that
%operate over streams,  e.g.  Lucid~\cite{Lucid77cacm},
%LUSTRE~\cite{LUSTRE87}, Ptolemy~\cite{ptolemy87}.  These languages have a natural notion of
%clocks~\cite{lucy:icfp96,Cohen:kahn2006}. Our work is closely related to
%the typed elaboration of this idea, \emph{clock
%  types}~\cite{lucy:islip95}. Most recently, two different teams have
%used a specialisation of clock types to infer rates in data flow
%programs: Rate Types~\cite{Bartenstein:2014} to infer maximum throughput;
%and Data Flow Fusion~\cite{Lippmeier:2013}, infer costs to guide a stream
%fusion procedure.  In comparison, our work is much more narrowly
%applicable (to algorithmic skeletons), but is also in a sense much more
%general. For instance, we can usefully parameterise our model over our
%cost models, which lets us embed e.g. a generic form of rate types or rate
%type variables into our work.
%
% Our notion of timing information for types
% is determined in the same way as sized types for compound types.
%
%Within the general algorithmic skeletons community, it is folklore that
%skeletons enable predictable cost models. This has been convincingly
%demonstrated using both static analysis~\cite{hayashi2002static} and
%dynamic profiling~\cite{lobachev2010estimating}. We go further in
%incorporating static analysis into the very type structure of the
%language itself. Moreover, we allow the cost models used in this static
%analysis to be easily changed. This potentially lets us automate the
%choice of the ``best'' parallel structure, an idea that is also explored
%less formally in \emph{autotuning}
%parallel programs~\cite{masif2013,partrans2013}.O
