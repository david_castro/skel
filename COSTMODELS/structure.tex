
\section{Structured Parallel Programs}
\label{sec:skel_def}


Structured parallel approaches, such as \emph{algorithmic skeletons}~\cite{cole1989algorithmic},
offer many advantages in terms of built-in safety and
parallelism-by-construction. %~\cite{TODO}.
For example, they can eliminate \emph{by design} common but hard-to-debug problems including \emph{deadlock} and \emph{race conditions}.
Such problems are prevalent in typical low-level \emph{concurrency based} designs for
parallel systems, e.g. \emph{pthreads}, OpenMP, etc.
Algorithmic skeletons also provide good structural cost models~\cite{hayashi2002static,lobachev2010estimating}.
In this paper, we will use four basic parallel skeletons, each of which operates
over a stream of input values, producing a stream of results: task farms,
pipelines, feedbacks, and divide-and-conquer.

\paragraph{\textbf{Task farms}.} The task farm skeleton, $\Nfarm$,
applies the same operation to each element of an input stream of tasks,
using a fixed number of parallel workers.
The input tasks must be independent,
and the outputs can be produced in an arbitrary order\footnote{Although this
does complicate the semantics, it improves parallelism.}. For example, a task
farm could be used to apply some filter to an input stream
of images, in order to parallelise the filter operation.

\tikzstyle{every node}=[align=center]
\tikzset{>=latex}

\begin{center}
  \resizebox{\columnwidth}{!}{%
    \begin{tikzpicture}
      \draw (0,1.25) [thick] rectangle (3,1.75) node[midway]{$\ldots,\vx_{12}, \vx_{11},\vx_{10}$};
      \draw (5,0)    [thick] circle [radius=0.5] node{$\vf$};
      \draw (5,1.5)  [thick] circle [radius=0.5] node{$\vf$};
      \draw (5,3)    [thick] circle [radius=0.5] node{$\vf$};
      \draw (7,1.25) [thick] rectangle (10,1.75) node[midway]{$\vf~\vx_2,\vf~\vx_1,\vf~\vx_3,\ldots$};

      \path [->] (3,1.5) edge node [above=3.5pt, near start] {$\vx_7$} (4.5,3);
      \path [->] (3,1.5) edge node [above, midway] {$\vx_9$} (4.5,1.5);
      \path [->] (3,1.5) edge node [below=3pt, near start] {$\vx_8$} (4.5,0);

      \path [->] (5.5,3)   edge node [above=3.5pt, near end] {$\vf~\vx_5$} (7,1.5);
      \path [->] (5.5,1.5) edge node [above, midway] {$\vf~\vx_6$} (7,1.5);
      \path [->] (5.5,0)   edge node [below=3pt, near end] {$\vf~\vx_4$} (7,1.5);

    \end{tikzpicture}
  }
\end{center}

\paragraph{\textbf{Pipeline}.} The pipeline skeleton, $\Npipe$,  composes
two streaming computations, in parallel.
% While the first stage is computing the next input task, the second stage can compute, in parallel, on the previous
% output task of the first stage.
It can be used to parallelise
two or more stages of a computation, e.g.\
filtering and edge detection.

\begin{center}
  \resizebox{.75\columnwidth}{!}{%
    \begin{tikzpicture}
      \draw (0,2.25) [thick] rectangle (3.5,2.75) node[midway]{$\ldots, \vx_{12}, \vx_{11}, \vx_{10}$};

      \path [->] (3.5  ,2.5) edge node [above] {$\vx_9$} (5,2.5);

      \draw (5.5,2.5)  [thick] circle [radius=0.5] node{$\vf$};

      \draw [->] (6  ,2.5)  to
                 (6.5, 2.5) to [out=0, in=0]
                 (6.5, 1.5) to node [above] {$\vf~\vx_8$}
                 (0.5, 1.5) to [out=180, in=180]
                 (0.5, 0.5) to (1, 0.5);

      \draw (1   ,0.25) [thick] rectangle (4.5,0.75) node[midway]{$\vf~\vx_7,\vf~\vx_6, \vf~\vx_5$};


      \path [->] (4.5,0.5) edge node [above] {$\vf~\vx_4$} (6,0.5);

      \draw (6.5 ,0.5)  [thick] circle [radius=0.5] node{$\vg$};

      \draw [->] (7, 0.5)  to
                 (7, 0.5) to [out=0, in=0]
                 (7,-0.5) to node [above] {$\vg~(\vf~\vx_3)$}
                 (1.5,-0.5) to [out=180, in=180]
                 (1.5,-1.5) to (2, -1.5);

      \draw (2 ,-1.75) [thick] rectangle (5.5,-1.25) node[midway]{$\vg~(\vf~\vx_2), \vg~(\vf~\vx_1), \ldots$};

    \end{tikzpicture}
  }
\end{center}

\paragraph{\textbf{Feedback}.} The feedback skeleton, $\Nfb$, captures recursion in a
streaming computation.
A feedback could be used, for example, to repeatedly transform an image until
some dynamic condition was met.

\begin{center}
  \resizebox{\columnwidth}{!}{%
    \begin{tikzpicture}
      \draw (0,0.25) [thick] rectangle (3.5,0.75) node[midway]{$\ldots,~\vx_9,~\vx_7,~\vf~\vx_{2}$};
      \draw (5.5,0.5)  [thick] circle [radius=0.5] node{$\vf$};
      \draw (7.5,0.25) [thick] rectangle (11,0.75) node[midway]{$\vf~(\vf~\vx_1),~\vf~\vx_3,~\ldots$};

      \draw [->] (3.5,0.5) to node[above]{$\vx_6$} (5,0.5);
      \draw [->] (6,0.5) to node[above]{$\vf~\vx_5$} (7.5,0.5);
      \draw [->] (6.75, 0.5) to (6.75, -0.5) to node[below]{$\vf~\vx_4$} (1.75, -0.5) to (1.75,0.25);

    \end{tikzpicture}
}
\end{center}

\paragraph{\textbf{Parallel Divide and Conquer}.} A divide-and-conquer
skeleton, $\Ndc$, is a parallel implementation of a classical divide-and-conquer algorithm.
% over some associative computation, $\mathit{conq}$.
% This does not
% necessarily yield a streaming computation:
The parallelism arises from
performing each of the recursive calls in
parallel.
% An important requirement of the divide and conquer is that the
%conquer, $\mathit{conq}$, computation must be associative.

\begin{center}
  \resizebox{\columnwidth}{!}{%
\begin{tikzpicture}
  %DIV
  \draw (0,9.5) [thick] node[draw,circle,minimum size=1cm] (d)  {$\mathit{div}$};

  \draw[->] (-1,10.5) to [out=0, in=90] node [above] {$\vx$} (d) ;

  %DIV
  \draw (-3,8.5) [thick] node[draw,circle,minimum size=1cm] (d1) {$\mathit{div}$};
  %\draw (0 ,8.9)   [thick] node[            minimum size=1cm] (d2) {$\cdots$};
  \draw (3 ,8.5) [thick] node[draw,circle,minimum size=1cm] (d3) {$\mathit{div}$};

  %arrows
  \draw[->] (d) -- node[left] {$\vx_1$} (d1);
  %\draw (d)  node[left] {$\cdots$} (d2);
  \draw[->] (d) -- node[right] {$\vx_n$} (d3);

  %DIV
  \draw (-5,7) [thick] node[draw,circle,minimum size=1cm] (d11) {$\mathit{div}$};
  \draw (-3,7) [thick] node[            minimum size=1cm] (d12) {$\cdots$};
  \draw (-1,7) [thick] node[draw,circle,minimum size=1cm] (d13) {$\mathit{div}$};
  %\draw (-2,6) [thick] node[draw,circle,minimum size=1cm] (d21) {$\mathit{div}$};
  \draw (0 ,7) [thick] node[            minimum size=1cm] (d22) {$\cdots$};
  %\draw ( 2,6) [thick] node[draw,circle,minimum size=1cm] (d23) {$\mathit{div}$};
  \draw ( 1,7) [thick] node[draw,circle,minimum size=1cm] (d31) {$\mathit{div}$};
  \draw ( 3,7) [thick] node[            minimum size=1cm] (d32) {$\cdots$};
  \draw ( 5,7) [thick] node[draw,circle,minimum size=1cm] (d33) {$\mathit{div}$};

  %arrows
  \draw[->] (d1) -- node[left] {$\vx_{11}$} (d11);
  %\draw[->] (d1) -- node[left] {$\vx_{12}$} (d12);
  \draw[->] (d1) -- node[right] {$\vx_{1n}$} (d13);
  %\draw[->] (d2) -- node[left] {$\vx_{21}$} (d21);
  %\draw[->] (d2) -- node[left] {$\vx_{22}$} (d22);
  %\draw[->] (d2) -- node[right] {$\vx_{23}$} (d23);
  \draw[->] (d3) -- node[left] {$\vx_{n1}$} (d31);
  %\draw[->] (d3) -- node[left] {$\vx_{32}$} (d32);
  \draw[->] (d3) -- node[right] {$\vx_{nn}$} (d33);

  %CONQ
  \draw (-5,5) [thick] node[draw,circle,minimum size=1cm] (c11) {$\mathit{conq}$};
  \draw (-3,5) [thick] node[            minimum size=1cm] (d12) {$\cdots$};
  \draw (-1,5) [thick] node[draw,circle,minimum size=1cm] (c13) {$\mathit{conq}$};
  \draw (0 ,5) [thick] node[            minimum size=1cm] (c22) {$\cdots$};
  \draw ( 1,5) [thick] node[draw,circle,minimum size=1cm] (c31) {$\mathit{conq}$};
  \draw ( 3,5) [thick] node[            minimum size=1cm] (d12) {$\cdots$};
  \draw ( 5,5) [thick] node[draw,circle,minimum size=1cm] (c33) {$\mathit{conq}$};

  %arrows and dots
  \draw (-5,6) [thick] node[            minimum size=1cm] (m11) {$\cdots$};
  \draw (-1,6) [thick] node[            minimum size=1cm] (m13) {$\cdots$};
  \draw ( 1,6) [thick] node[            minimum size=1cm] (m31) {$\cdots$};
  \draw ( 5,6) [thick] node[            minimum size=1cm] (m33) {$\cdots$};
  \draw[] (d11) -- node[left]  {}  (-5,6.1);
  \draw[] (d13) -- node[right] {}  (-1,6.1);
  \draw[] (d31) -- node[left]  {}  (1,6.1);
  \draw[] (d33) -- node[right] {}  (5,6.1);
  \draw[->] (-5,5.9) -- node[left]  {}(c11);
  \draw[->] (-1,5.9) -- node[right] {}(c13);
  \draw[->] ( 1,5.9) -- node[left]  {}(c31);
  \draw[->] ( 5,5.9) -- node[right] {}(c33);

  %CONQ
  \draw (-3,3.5) [thick] node[draw,circle,minimum size=1cm] (c1) {$\mathit{conq}$};
  \draw (3 ,3.5) [thick] node[draw,circle,minimum size=1cm] (c3) {$\mathit{conq}$};

  %arrows
  \draw[->] (c11) -- node[left] {$\vy_{11}$}(c1);
  %\draw[->] (c12) -- node[left] {$\vy_{12}$}(c1);
  \draw[->] (c13) -- node[right] {$\vy_{1n}$}(c1);
  %\draw[->] (c21) -- node[left] {$\vy_{21}$}(c2);
  %\draw[->] (c22) -- node[left] {$\vy_{22}$}(c2);
  %\draw[->] (c23) -- node[right] {$\vy_{23}$}(c2);
  \draw[->] (c31) -- node[left] {$\vy_{n1}$}(c3);
  %\draw[->] (c32) -- node[left] {$\vy_{32}$}(c3);
  \draw[->] (c33) -- node[right] {$\vy_{nn}$}(c3);

  %CONQ
  \draw (0 ,2.5) [thick] node[draw,circle,minimum size=1cm] (c) {$\mathit{conq}$};

  %arrows
  \draw[->] (c1) -- node[left] {$\vy_{1}$}(c);
  %\draw[->] (c2) -- node[left] {$\vy_{2}$}(c);
  \draw[->] (c3) -- node[right] {$\vy_{n}$}(c);

  \draw[->] (c) to [out=270, in= 180] node[below] {$\vy$} (1,1.5);

\end{tikzpicture}
}
\end{center}

%% \noindent
%% Note that this is just a high-level overview of these skeletons, and that
%% they may admit many alternative implementations with characteristics, such as
%% load balancing, more suitable for certain specific contexts. We provide a
%% basic, but more precise formal definition of the operational semantics of these
%% skeletons later, by the end of this section.


%\vspace{-12pt}
\subsection{Structured Parallel Processes}

%We can now formalise our approach.
We define a language $\vP$ of structured parallel processes,
built by composing skeletons
over atomic operations.
\[
  \vp \in \Parp~\Coloneqq~\pseq_\vT~\vf~|~\vp_1\parallel\vp_2~|~\dc{\vn,\vT,\vF}{\vf~\vg}~|~\farm~\vn~\vp~|~\fb{\vp}
\]
\noindent
%% KH: The description of DC was *very* badly worded.  I'm still not sure it's right.
%% The term levels is just introduced without discussion and it's not clear what the
%% call tree is.
The $\pseq_\vT~\vf$ construct \emph{lifts} an atomic function to a streaming
operation on a collection $\vT$. The arguments of the $\keyword{dc}$ skeleton
are: the number of \emph{levels} of the divide-and-conquer, $n$;
the collection $\vT$ on which the $\keyword{dc}$ skeleton
works; and the functor $\vF$ that describes the divide-and-conquer call tree.

\paragraph{Denotational Semantics.}
The denotational semantics is split into two parts:
$\ssem{\cdot}$ describes the base semantics,
and $\psem{\cdot}$ lifts this to a \emph{streaming} form.
We use a global environment
for atomic function types, $\rho$, and the corresponding global environment of functions, $\hat{\rho}$:
\[
  \begin{array}{lcl}
    \rho~=~\lbrace\vf:\vA\to\vB,~\ldots\rbrace & \quad &
    \hat{\rho}~=~\lbrace\sem{\vf}\in\sem{\vA\to\vB},~\ldots\rbrace
  \end{array}
\]
\[
    \begin{array}{lcl}
    \ssem{\vp~:~\vT~\vA\to\vT~\vB} &~:~& \sem{\vA\to\vB} \\
      \ssem{\pseq~\vf} & = & \hat{\rho}(\vf) \\

      \ssem{\pipe{\vp_1}{\vp_2}} & = &
%      \multicolumn{3}{r}{
        \ssem{\vp_2}\circ\ssem{\vp_1} \\

      \ssem{\farm~\vn~\vp}
        & = & \ssem{\vp} \\

      \ssem{\fb{\vp}}
        & = & \iter~\ssem{\vp} \\

      \ssem{\dc{\vn,\vT,\vF}{\vf~\vg}}
      & = & \cata{\vF}~(\hat{\rho}(\vf)) \circ \ana{\vF}~(\hat{\rho}(\vg))\\\\

      \psem{\vp~:~\vT~\vA\to\vT~\vB} &~:~& \sem{\vT~\vA\to\vT~\vB} \\
      \psem{\vp} & = & \map{\vT}~\ssem{\vp}

    \end{array}
\]


\begin{figure*}[!ht]
  \resizebox{\columnwidth}{!}{%
\begin{tikzpicture}
  %% \path(-3,  0) -- node[above,midway] {$\begin{array}{lcl}
  %%     \vF~\vA~\vB &~=~& 1~+~\vA\times\vB\times\vB \\
  %%     \vf &~:~& \vB\to\vF~\vA~\vB %\text{, where} \\
  %%     %\quad\vf~\vx_i &~=~& \inj_2(\vy_i,~\vx_{2\times i},~\vx_{2\times i+1})\text{, or} \\
  %%     %          &~=~& \inj_1()
  %%     \\ \\
  %%     \multicolumn{3}{c}{\ana{\vF\,\vA}~\vf}
  %%    \end{array}$} (8.75, 0);

     \path(-3.25,-1.5) -- node[above] {$\vA$} (-2.75, -1.5);
     \draw (-3, -3) [thick] node[draw, circle, minimum size=0.5cm] (1a) {$\vx_1$};

     \path (-2.5,-3.2) -- node[above] {\large $\xRightarrow{\vf~\vx_1}$} (-1,-3.2);

     \path(-1.25,-1.5) -- node[above] {$\vF~\vC~\vA$} ( 0.25, -1.5);
     \draw (-0.5, -2.5) [thick] node[draw, rectangle, minimum size=0.5cm] (2a) {$\vy_1$};
     \draw (-1  , -3.5) [thick] node[draw, circle, minimum size=0.5cm] (2b) {$\vx_2$};
     \draw ( 0  , -3.5) [thick] node[draw, circle, minimum size=0.5cm] (2c) {$\vx_3$};
     \draw[->] (2a) to (2b);
     \draw[->] (2a) to (2c);

     \path (0,-3.2) -- node[above] {\large $\xRightarrow{\substack{\vf~\vx_2\\\vf~\vx_3}}$} (1.5,-3.2);

     \path( 1.25,-1.5) -- node[above] {$\vF~\vC~(\vF~\vC~\vA)$} ( 4.75, -1.5);
     \draw ( 3  , -2) [thick] node[draw, rectangle, minimum size=0.5cm] (3a) {$\vy_1$};
     \draw ( 2  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (3b) {$\vy_2$};
     \draw ( 4  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (3c) {$\vy_3$};
     \draw ( 1.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3d) {$\vx_4$};
     \draw ( 2.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3e) {$\vx_5$};
     \draw ( 3.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3f) {$\vx_6$};
     \draw ( 4.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3g) {$\vx_7$};

     \draw[->] (3a) to (3b);
     \draw[->] (3a) to (3c);
     \draw[->] (3b) to (3d);
     \draw[->] (3b) to (3e);
     \draw[->] (3c) to (3f);
     \draw[->] (3c) to (3g);

     \path (4.5,-3.2) -- node[above] {\large $\xRightarrow{~\ldots~}$} (5.5,-3.2);

     \path( 5   ,-1.5) -- node[above] {$\mu(\vF~\vC)$} ( 9   , -1.5);
     \draw ( 7  , -2) [thick] node[draw, rectangle, minimum size=0.5cm] (4a) {$\vy_1$};
     \draw ( 6  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (4b) {$\vy_2$};
     \draw ( 8  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (4c) {$\vy_3$};
     \draw ( 5.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4d) {$\vy_4$};
     \draw ( 6.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4e) {$\vy_5$};
     \draw ( 7.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4f) {$\vy_6$};
     \draw ( 8.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4g) {$\vy_7$};

     \draw ( 5.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4h) {$\ldots$};
     \draw ( 5.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4i) {$\ldots$};
     \draw ( 6.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4j) {$\ldots$};
     \draw ( 6.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4k) {$\ldots$};
     \draw ( 7.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4l) {$\ldots$};
     \draw ( 7.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4m) {$\ldots$};
     \draw ( 8.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4n) {$\ldots$};
     \draw ( 8.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4o) {$\ldots$};

     \draw[->] (4a) to (4b);
     \draw[->] (4a) to (4c);
     \draw[->] (4b) to (4d);
     \draw[->] (4b) to (4e);
     \draw[->] (4c) to (4f);
     \draw[->] (4c) to (4g);

     \draw[->] (4d) to (4h);
     \draw[->] (4d) to (4i);
     \draw[->] (4e) to (4j);
     \draw[->] (4e) to (4k);
     \draw[->] (4f) to (4l);
     \draw[->] (4f) to (4m);
     \draw[->] (4g) to (4n);
     \draw[->] (4g) to (4o);

\end{tikzpicture}
\hspace{1cm}
%}
%\resizebox{\columnwidth}{!}{%
\begin{tikzpicture}

     \path ( 2.75,  1) -- node[above] {$\vB$} ( 3.25, 1);
     \draw ( 3,  3) [thick] node[draw, circle, minimum size=0.5cm] (1a) {$\vx_1$};

     \path ( 1, 2.8) -- node[above] {\large $\xRightarrow{\vf~\vt_1}$} ( 2.5, 2.8) ;

%     \path ( -2, 2) -- node[above] {\scriptsize, where $\vt_i~=~\inj_2~(\vy_i,\vx_{i\times2},\vx_{i\times2+1})$} ( 2.5, 1) ;

     \path (-0.25,  1) -- node[above] {$\vF~\vC~\vB$} ( 1.25, 1);
     \draw ( 0.5,  2.5) [thick] node[draw, rectangle, minimum size=0.5cm] (2a) {$\vy_1$};
     \draw ( 1  ,  3.5) [thick] node[draw, circle, minimum size=0.5cm] (2b) {$\vx_2$};
     \draw ( 0  ,  3.5) [thick] node[draw, circle, minimum size=0.5cm] (2c) {$\vx_3$};
     \draw[->] (2a) to (2b);
     \draw[->] (2a) to (2c);

     \path (-1.5,2.8) -- node[above] {\large $\xRightarrow{\substack{\vf~\vt_2\\\vf~\vt_3}}$} (0,2.8) ;

     \path ( -4.75, 1) -- node[above] {$\vF~\vC~(\vF~\vC~\vB)$} ( -1.25, 1);
     \draw (-3   ,  2) [thick] node[draw, rectangle, minimum size=0.5cm] (3a) {$\vy_1$};
     \draw (-2   ,  3) [thick] node[draw, rectangle, minimum size=0.5cm] (3b) {$\vy_2$};
     \draw (-4   ,  3) [thick] node[draw, rectangle, minimum size=0.5cm] (3c) {$\vy_3$};
     \draw (-1.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3d) {$\vx_4$};
     \draw (-2.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3e) {$\vx_5$};
     \draw (-3.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3f) {$\vx_6$};
     \draw (-4.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3g) {$\vx_7$};

     \draw[->] (3a) to (3b);
     \draw[->] (3a) to (3c);
     \draw[->] (3b) to (3d);
     \draw[->] (3b) to (3e);
     \draw[->] (3c) to (3f);
     \draw[->] (3c) to (3g);

     \path (-4.5,2.8) -- node[above] {\large $\xRightarrow{~\ldots~}$} (-5.5,2.8);

     \path ( -9   ,  1) -- node[above] {$\mu(\vF~\vC)$} (-5   ,  1);
     \draw (-7  ,   2) [thick] node[draw, rectangle, minimum size=0.5cm] (4a) {$\vy_1$};
     \draw (-6  ,   3) [thick] node[draw, rectangle, minimum size=0.5cm] (4b) {$\vy_2$};
     \draw (-8  ,   3) [thick] node[draw, rectangle, minimum size=0.5cm] (4c) {$\vy_3$};
     \draw (-5.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4d) {$\vy_4$};
     \draw (-6.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4e) {$\vy_5$};
     \draw (-7.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4f) {$\vy_6$};
     \draw (-8.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4g) {$\vy_7$};

     \draw (-5.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4h) {$\ldots$};
     \draw (-5.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4i) {$\ldots$};
     \draw (-6.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4j) {$\ldots$};
     \draw (-6.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4k) {$\ldots$};
     \draw (-7.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4l) {$\ldots$};
     \draw (-7.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4m) {$\ldots$};
     \draw (-8.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4n) {$\ldots$};
     \draw (-8.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4o) {$\ldots$};

     \draw[->] (4a) to (4b);
     \draw[->] (4a) to (4c);
     \draw[->] (4b) to (4d);
     \draw[->] (4b) to (4e);
     \draw[->] (4c) to (4f);
     \draw[->] (4c) to (4g);
     \draw[->] (4d) to (4h);
     \draw[->] (4d) to (4i);
     \draw[->] (4e) to (4j);
     \draw[->] (4e) to (4k);
     \draw[->] (4f) to (4l);
     \draw[->] (4f) to (4m);
     \draw[->] (4g) to (4n);
     \draw[->] (4g) to (4o);

\end{tikzpicture}
}
\caption{Binary Tree Anamorphism (left) and Catamorphism (right).}
\label{fig:cata}
\end{figure*}

\noindent
% The semantics of these constructs is straightforward.
An atomic function, $\vf$, is
applied to all the elements of a collection of data. %sequentially.
A parallel
pipeline, $\vp_1 \parallel \vp_2$, is the composition of two parallel processes, $\vp_1$ and $\vp_2$. A task
farm, $\farm~\vn~\vp$, replicates a parallel process, $\vp$, so has the same denotational semantics as $\vp$.
% to be the same as the one of its worker, although it will have a different
% operational semantics.
%
A feedback skeleton, $\fb{\vp}$, applies the computation $\vp$ iteratively, i.e.\ \emph{trampolined}, to the elements in the
input collection. Its semantics is given in terms of the function $\iter$.
\[
  \begin{array}{l}
    \iter\phantom{~\vf~}:~(\vA\to\vA+\vB)\to\vA\to\vB \\
    \iter~\vf~=~\keyword{Y}~(\lambda~\vg.(\vg\triangledown\Idf)\circ\vf) \\
  \end{array}
\]


\pagebreak
\noindent
Finally, a $\keyword{dc}$ is equivalent to \emph{folding}, using $\vf$, the tree-like structure that results from \emph{unfolding} the input using $\vg$.
% A $\keyword{dc}$ is equivalent to \emph{folding} using $\vf$ the tree-like structure that results of \emph{unfolding} the input using $\vg$.
%  that contains all the sub-tasks that result from dividing the input.
%This tree-like structure is
% the fixpoint of the functor
% $\vF$.
It is defined to be the
composition of a \emph{catamorphism} with an \emph{anamorphism}.
\[
  \begin{array}{lcl}
    \cata{\vF}     &:& (\vF~\vA\to\vA)\to\mu\vF\to\vA \\
    \cata{\vF}~\vf &=& \vf\circ\vF~(\cata{\vF}~\vf)\circ\hout{\vF} \\
    \\
    \ana{\vF}      &:& (\vA\to\vF~\vA)\to\vA\to\mu\vF \\
    \ana{\vF}~\vg  &=& \hin{\vF}\circ\vF~(\ana{\vF}~\vg)\circ\vg
  \end{array}
\]

\begin{example}[List catamorphism]
  Let $\vL$ be the base bifunctor of a polymorphic list.
%  $\vL~\vA~\vB~=~1+\vA\times\vB$.
  We define the function $\vf$ to be:
  %a value of type $\vL~\mathbb{N}~\mathbb{N}$, and returns a natural number:
  \[
    \begin{array}{lcl}
      \vf & : & \vL~\mathbb{N}~\mathbb{N}\to\mathbb{N} \\
      \vf~(\inj_1~())        & = & 0 \\
      \vf~(\inj_2~(\vx,\vn)) & = &  \keyword{add}~\vx~\vn
    \end{array}
  \]
  \noindent
  Given an input list
  $
  [\vx_1,\vx_2,\ldots,\vx_n]%=\mathit{cons}~\vx_1~(\mathit{cons}~\vx_2~(\mathit{cons}~\ldots\\(\mathit{cons}~\vx_n~\mathit{nil})))
  $,
  the catamorphism
  $\cata{\vL\,\mathbb{N}}~\vf$ applied to this input list returns the sum
  of the $\vx_i$:

  \vspace{.1cm}
  $
  \cata{\vL\;\mathbb{N}}\,\vf\,[\vx_1,\vx_2,\ldots,\vx_n]\,=\,\keyword{add}~\vx_1~(\keyword{add}~\vx_2~(\cdots~(\keyword{add}~\vx_n~0))).
$ % \]
\end{example}

\begin{example}[List anamorphism]
%  Using the same datatype definition, w
We define a function $\vg$ that returns
  $()$ if the input $\vn$ is zero, and $(\vn,\vn-1)$ otherwise.
  \[
  \vspace{-0.1cm}
    \begin{array}{lcl}
      \vg &~:~& \mathbb{N}\to\vL~\mathbb{N}~\mathbb{N} \\
      \vg~\vn &~=~& \text{if $\vn = 0$ then $\inj_1~()$ else $\inj_2~(\vn,\vn-1)$ } \\
    \end{array}
  \]
  \noindent
  The anamorphism $\ana{\vL\,\mathbb{N}}~\vg$ applied to $\vn$
  returns a list of numbers descending from $\vn$ to 1:
%  \[
$
    \ana{\vL\,\mathbb{N}}~\vg~\vn~=~[\vn,\vn-1,\ldots,2,1].
$
%  \]
\end{example}

\begin{figure*}[!ht]
\[
  \begin{array}{c}
\inference{\rho (\vf)~=~\vA\to\vB}{\tyd\vf~:~\vA\to\vB}\hspace{\columnsep}
\inference{\tyd\ve_2~:~\vB\to\vC \\ \tyd\ve_1~:~\vA\to\vB}{\tyd\ve_2\bullet\ve_1~:~\vA\to\vC}\hspace{\columnsep}
\inference{\tyd\ve_1~:~\vF~\vB\to\vB \\ \tyd\ve_2~:~\vA\to\vF~\vA}{\tyd\shylo{\vF}~\ve_1~\ve_2~:~\vA\to\vB}\hspace{\columnsep}
\inference{\tyd\vp~:~\vT~\vA\to\vT~\vB}{\tyd\peval_\vT~\vp~:~\vT~\vA\to\vT~\vB}
\end{array}
\]
\caption{Simple types for Structured Expressions, $\vE$.}
\label{fig:simple_types}
\end{figure*}

\begin{figure*}[!ht]
\[
  \begin{array}{c}

\inference{\tyd\vs~:~\vA\to\vB}{\tyd\pseq~\vs~:~\vT~\vA\to\vT~\vB}\hspace{\columnsep}
\inference{\vn~:~\mathbb{N} &
\tyd\vp~:~\vT~\vA\to\vT~\vB}{\tyd\farm~\vn~\vp~:~\vT~\vA\to\vT~\vB}\hspace{\columnsep}
\inference{\tyd\vp~:~\vT~\vA\to\vT~(\vA+\vB)}{\tyd\fb~\vp~:~\vT~\vA\to\vT~\vB}\\\\

\inference{\tyd\vs_1~:~\vF~\vB\to\vB & \tyd\vs_2~:~\vA\to\vF~\vA}{\tyd\dc{\vn,\vF}~\vs_1~\vs_2~:~\vT~\vA\to\vT~\vB}\hspace{\columnsep}
\inference{\tyd\vp_1~:~\vT~\vA\to\vT~\vB & \tyd\vp_2~:~\vT~\vB\to\vT~\vC}
          {\tyd\vp_1\parallel\vp_2~:~\vT~\vA\to\vT~\vC}
\end{array}
\]
\caption{Simple types for Structured Parallel Processes, $\vP$.}
\label{fig:simple_types-p}
\end{figure*}

\noindent
Figure~\ref{fig:cata} shows how catamorphisms and
anamorphisms work on binary trees.
In the anamorphism, we start with an input
value, and apply the operation $\vf$ recursively until the entire data
structure is \emph{unfolded}. In the catamorphism, the operation $\vf$ is applied recursively % in a bottom-up way
until the entire structure is \emph{folded} into a single value.
%
%By defining the catamorphism and anamorphisms using bifunctors, we can define
The operation $\map{\vT}$ can be defined as a special case of a catamorphism or
anamorphism. Given a
bifunctor $\vG$, a type $\vT~\vA=\mu(\vG~\vA)$ is a polymorphic data type
that is also a functor~\cite{gibbons:calculating}. For all $\vf:\vA\to\vB$, the function $\map{\vT}~\vf$ is
the morphism $\vT~\vf$, defined as: % follows:
\[
  \begin{array}{c l}
    \map{\vT}~\vf & = \cata{\vG\,\vA}(\hin{\vG\,\vB}\circ\vG~\vf~\Idf) \\
                  & = \ana{\vG\,\vB}(\vG~\vf~\Idf\circ\hout{\vG\,\vA}) \\
  \end{array}
\]
For uniformity, we will represent all $\map{\vT}$ as catamorphisms.

\begin{example}[$\map{\List}$]
Given a function $\vf:\vA\to\vB$, $\map{\List}~\vf$ applies $\vf$ to all the
elements of the input list:
\[
\map{\List}~\vf~[\vx_1,\vx_2,\ldots,\vx_n]\\=~[\vf~\vx_1,\vf~\vx_2,\ldots,\vf~\vx_n]
\]
\end{example}

\subsection{Hylomorphisms}

\emph{Hylomorphisms} are a well known, and very general, recursion
pattern~\cite{meijer1991functional}. A \emph{hylomorphism} can be thought of as
the generalisation of a divide-and-conquer. Intuitively, $\hylo{\vF}~\vf~\vg$ is a
recursive algorithm whose recursive call tree can be represented by $\mu\vF$,
where $\vg$ describes how the algorithm divides the input problem into
sub-problems, and $\vf$ describes how the results are combined.
\[
  \begin{array}{lcl}
    \hylo{\vF} &~:~& (\vF~\vB\to\vB)\to(\vA\to\vF~\vA)\to\vA\to\vB \\
    \hylo{\vF}~\vf~\vg &~=~& \vf\circ\vF~(\hylo{\vF}~\vf~\vg)\circ\vg
  \end{array}
\]

\noindent
Since $\hout{\vF}\circ\hin{\vF}=\Idf$, we can easily show that
$\hylo{\vF}~\vf~\vg~=\\ \cata{\vF}~\vf~\circ~\ana{\vF}~\vg$.
%Note also that
Catamorphisms, anamorphisms and $\map{}$ are just special cases of hylomorphisms.
%This
%can be easily shown using the facts that $\cata{\vF}~\hin{\vF}=\Idf$ and
%$\ana{\vF}~\hout{\vF}=\Idf$. Using bifunctors again, we can conclude
%that $\map{\vT}$, $\ana{\vF}$ and $\cata{\vF}$ can all be defined in
%terms of just this single construct.  Given a
%bifunctor $\vF$,
\[
  \begin{array}{lcl}
    \vT~\vA &=& \mu(\vF~\vA) \\
    \map{\vT}~\vf &=& \hylo{\vF\,\vA}~(\hin{\vF\,\vB}\circ(\vF~\vf~\Idf))~\hout{\vF\,\vA},\\
                  & &~\text{where $\vA=\mathit{dom}(\vf)$ and $\vB=\mathit{codom}(\vf)$} \\
    \cata{\vF}~\vf & = & \hylo{\vF}~\vf~\hout{\vF} \\
    \ana{\vF}~\vf & = & \hylo{\vF}~\hin{\vF}~\vf \\
  \end{array}
\]

\begin{example}[Quicksort]
%  A typical example of hylomorphism is the quicksort algorithm.
Assuming a type $\vA$, and
  two functions, $\keyword{leq},~\keyword{gt}~:~\vA\to\List~\vA\to\List~\vA$,
  that filter the elements appropriately, we can implement
  na\"{\i}ve quicksort as:
\[
\hspace{-.1cm}
  \begin{array}{lcl}
    \keyword{qsort} & : & \List~\vA\to\List~\vA \\
    \keyword{qsort}~\keyword{nil} & = & [] \\
    \keyword{qsort}~(\keyword{cons}~\vx~\vl)
      & = & \keyword{qsort}~(\keyword{leq}~\vx~\vl)
          \doubleplus\keyword{cons}~\vx~%
%          (\keyword{qsort}~(\keyword{gt}~\vx~\vl')) & \text{, if $\vl=\vx\lhd\vl'$.} \\
          (\keyword{qsort}~(\keyword{gt}~\vx~\vl))
  \end{array}
\]

  \noindent
  %In order to see why this is a hylomorphism, we need to look at the
  %recursive structure of the algorithm.
  We make the recursive structure explicit by using a tree. The $\keyword{split}$ function unfolds the
  arguments into this tree, % for the recursive calls,
  and the $\keyword{join}$ function then
  flattens it.
\[
  \begin{array}{l}

  \begin{array}{lcl}
    \keyword{split} & : & \List~\vA\to\Tree~\vA \\
    \keyword{split}~\keyword{nil} & = & \keyword{empty} \\
    \keyword{split}~(\keyword{cons}~\vx~\vl) & = & \keyword{node}~(\keyword{split}~(\keyword{leq}~\vx~\vl))~\vx~(\keyword{split}~(\keyword{gt}~\vx~\vl))
  \end{array} \\ \\

  \begin{array}{lcl}
    \keyword{join} & :& \Tree~\vA\to\List~\vA \\
    \keyword{join}~\keyword{empty} & = & \keyword{nil} \\
    \keyword{join}~(\keyword{node}~\vl~\vx~\vr) & = & \keyword{join}~\vl\doubleplus\keyword{cons}~\vx~(\keyword{join}~\vr)
  \end{array} \\ \\

  \begin{array}{lcl}
    \keyword{qsort} & : & \List~\vA\to\List~\vA \\
    \keyword{qsort} & = & \keyword{join}\circ\keyword{split}
  \end{array}
\end{array}
\]

\noindent
We can remove the explicit recursion from these
definitions, since $\keyword{split}$ is a tree anamorphism, and
$\keyword{join}$ is a tree catamorphism.
\[
  \begin{array}{l}

  \begin{array}{lcl}
    \keyword{split} & : & \List~\vA\to\vT~\vA~(\List~\vA) \\
    \keyword{split}~\keyword{nil} & = & \inj_1~() \\
    \keyword{split}~(\keyword{cons}~\vx~\vl) & = & \inj_2~(\vx,~\keyword{leq}~\vx~\vl,~\keyword{gt}~\vx~\vl)
  \end{array} \\ \\

  \begin{array}{lcl}
    \keyword{join} & : & \vT~\vA~(\List~\vA)\to\List~\vA \\
    \keyword{join}~(\inj_1~()) & = & \keyword{nil} \\
    \keyword{join}~(\inj_2~(\vx,\vl,\vr)) & = & \vl\doubleplus\keyword{cons}~\vx~\vr
  \end{array} \\ \\

  \begin{array}{lcl}
    \keyword{qsort} & : & \List~\vA\to\List~\vA \\
    \keyword{qsort} & = & \cata{\vT\,\vA}~\keyword{join}\circ\ana{\vT\,\vA}~\keyword{split} \\
  \end{array}
\end{array}
\]

\noindent
Finally, since we have a composition of a catamorphism and an anamorphism, we
can write $\keyword{qsort}$ as the equivalent hylomorphism.
\[
    \keyword{qsort} = \hylo{\vT\,\vA}~\keyword{join}~\keyword{split}
\]

\end{example}

\noindent
% We have explained how to describe the semantics of all our parallel
%constructs using hylomorphisms, except for
The only construct that has not yet been considered is feedback.
Although the
fixpoint combinator $\keyword{Y}$ can be easily defined as a hylomorphism, we
take a different approach.
Observe that we can unfold
the definition of $\iter$ as follows:
\[
  \begin{array}{c}
    \iter~\vf~=~\keyword{Y}~(\lambda~\vg.(\vg\triangledown\Idf)\circ\vf)~=(\iter~\vf\triangledown\Idf)\circ\vf \\
  %(\iter~\vf\triangledown\Idf)\circ\vf\,=\,(\Idf\triangledown\Idf)\circ(\iter~\vf + \Idf)\circ\vf$.
  \end{array}
\]

\pagebreak
\noindent
Note that if $\vf,\vg~:~\vA+\vB\to\vC$, the function
$\vf\triangledown\vg~:~\vA+\vB\to\vC$ can be written as the composition of
$\Idf\triangledown\Idf~:~\vC+\vC\to\vC$ and $\vf~+~\vg~:~\vA+\vB\to\vC+\vC$. We
use this to rewrite $\iter$ as follows:
\[
  \begin{array}{c}
    \iter~\vf~=~(\iter~\vf\triangledown\Idf)\circ\vf~=~(\Idf\triangledown\Idf)\circ(\iter~\vf+\Idf)\circ\vf \\
  \end{array}
\]
If $\vf:\vA\to\vA+\vB$, we define the functor $(+\vB)$, with the
morphism $(+~\vB)~\vf~=~\vf+\Idf$, which trivially
preserves identities and composition. Since
$\iter~\vf = (\Idf\triangledown\Idf)\circ(+\vB)~(\iter~\vf)\circ\vf$, then:
%$\iter~\vf = \hylo{+\vB}~(\Idf\triangledown\Idf)~\vf$.%, and so we can redefine it as:
\[
 \begin{array}{l}
%\iter &~:~&(\vA\to\vA+\vB)\to\vA\to\vB \\
   \iter~\vf~=~\hylo{(+\vB)}~(\Idf\triangledown\Idf)~\vf
 \end{array}
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% STRUCTURED EXPRESSIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Structured Expressions}
\label{sec:structured-exp}

We have now seen that the denotational semantics of all our parallel constructs can
be given in terms of hylomorphisms. This semantic correspondence is not
unexpected since it has been used to describe the formal foundations of data-parallel algorithmic skeletons~\cite{rabhi2003patterns}.
We take this correspondence one step further by using hylomorphisms as a unifying structure, and by then exploiting
the reasoning power provided by the fundamental laws of hylomorphisms.
%% Move later.
% Moreover, a language based on hylomorphisms is
% known to be \emph{Turing-universal}, and has been previously exploited for
% parallel programming~\cite{fischer2002turing}.
%
%The approach we take in this paper is similar, since we describe the functional
%behaviour of our programs in a hylomorphism-based language.
%The main difference is how we treat the introduction of parallelism. In this paper, we will follow a type-based
%approach to select one of a family of functionally equivalent
%parallel programs, indexed by the hylomorphism-based equivalent.
%We define a type-based approach to
%select between alternative, semantically equivalent parallel programs.
In order to
define our type-based approach, we will first define a new language, $\vE$, that combines two
levels,
%
% define a language, $\lang$ separated into two
% levels, one sequential, $\vS$ and the other parallel, $\vP$:
%
%\begin{description}
%  \item
 \emph{Structured Expressions} ($\vS$),
    that
    enable us to describe a program as a composition of hylomorphisms;
and
%  \item
\emph{Structured Parallel Processes} ($\vP$),
    that build on $\vS$
    using nested algorithmic skeletons. A program in $\vE$ is then either a structured expression $\vs\in\vS$ or a parallel program $\peval_\vT~\vp$, where $\vp\in\vP$. %, to yield parallel processes.
%\end{description}
%
\noindent
Our revised syntax is shown below. Note that since a $\vp\in\vP$ can only
appear under a $\peval_\vT$ construct, we no longer need to annotate each
$\pseq$ and $\keyword{dc}$ with the collection $\vT$ of tasks. % on which the skeleton works.
\[
\begin{array}{l}
  \begin{array}{lcl}
    \ve \in \vE   &\Coloneqq& \vs \quad|\quad \peval_\vT~\vp \\
    \vs \in \vS   &\Coloneqq& \vf \quad|\quad \ve_1~\bullet~\ve_2 \quad|\quad \shylo{\vF}~\ve_1~\ve_2 \\
    \vp \in \Parp &\Coloneqq& \pseq~\vs~|~\vp_1\parallel\vp_2~|~\dc{\vn,\vF}{\vs_1~\vs_2}~|~\farm~\vn~\vp~|~\fb{\vp}
\end{array}
\end{array}
\]

\noindent
%% Out of place/redundant?  KH
% We can easily define $\smap{\vT}$, $\sana{\vF}$ and $\scata{\vF}$
% % on top of these definitions, with
% to have a direct denotational semantics in terms of
% hylomorphisms.
The denotational semantics of $\vP$ only changes in the
rules that mention $\ve$, and by providing a semantics for
$\peval_\vT$:
\[
\begin{array}{lcl}
  \sem{\peval_\vT~\vp} & = & \map{\vT}~\ssem{\vp} \\
  \ldots \\

  \ssem{\pseq{}~\ve} & = & \sem{\ve} \\

  \ssem{\dc{\vn,\vF}{\ve_1~\ve_2}}
      & = &
  \hylo{\vF}~\sem{\ve_2}~\sem{\ve_1} \\
    \multicolumn{3}{l}{\ldots} \\
\end{array}
\]

\noindent
The corresponding typing rules are entirely standard
(Figures~\ref{fig:simple_types}--\ref{fig:simple_types-p}).
Finally, it is convenient to define the ``parallelism erasure of $\vS$'', $\overline{\vS}$.
Intuitively, $\overline{\vS}$ contains no nested parallelism: for all
$\vs\in\vS$, $\vs\in\overline{\vS}$ if and only if $\vs$ contains no occurrences
of the $\peval_\vT$ construct. This is equivalent to defining $\vs$ in the
erasure of $\vS$, $\overline{\vS}$, if it is just a composition of atomic
functions and hylomorphisms:
\[
  \vs\in\overline{\vS}~\Coloneqq~\vf \quad|\quad \vs_1~\bullet~\vs_2
     \quad|\quad \shylo{\vF}~\vs_1~\vs_2 \\
\]

\noindent
The structure-annotated type system given in Section~\ref{sec:skel_func} below describes how
to introduce parallelism to an $\vs\in\overline{\vS}$ in a sound way.

\subsubsection{Soundness and Completeness}

It is straightforward to show that the type system from Figs.~\ref{fig:simple_types}--~\ref{fig:simple_types-p} is both sound and complete
\emph{wrt} our denotational semantics.  Our soundness property is:
$
\forall \ve\in\vE;~\vA,\vB\in\keyword{Type},~\tyd \ve : \vA\to\vB \implies (\sem{\ve}\in\sem{\vA\to\vB})
$.
%\begin{proof}
%\emph{Proof Sketch.}
The proof is by structural induction over the terms in $\vE$, using
the definitions of $\tyd \ve : \vT$ from Figs.~\ref{fig:simple_types}--~\ref{fig:simple_types-p}
and $\sem{.}$ above.
%\end{proof}
%
\noindent
The corresponding completeness property is:
$
\forall \ve\in\vE;~\vA,\vB\in\keyword{Type},~(\sem{\ve}\in\sem{\vA\to\vB}) \implies \tyd \ve : \vA\to\vB
%\forall~\ve\in~\vE, \vt\in\vT, \sem{\tyd \ve: \vt} \implies \tyd \ve : \vt \wedge \sem{\tyd \ve: \vt} \implies \tyd \ve : \vt
$.
%\begin{proof}
%\emph{Proof Sketch.}
The proof is also by structural induction over the terms in $\vE$, using
the definitions of $\tyd \ve : \vA\to\vB$ from Figs.~\ref{fig:simple_types}--~\ref{fig:simple_types-p}
and $\sem{.}$ above.
%\end{proof}=
